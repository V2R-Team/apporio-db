-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:19 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_Digitax`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Gabriel UreÃ±a', 'Chacon', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '3023184519', 'gabriel.bioscenter@gmail.com', 'Digitax Plus SAS', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'unidosenlafe', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(2, 'Gabriel', 'Chacon', 'gaf2419', '', '', 'melq2419**', '3168905328', 'unidos.enlafe@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(3, 'Guillermo', 'Rodriguez', 'dtoperativo', '', '', 'digitax', '3046827362', 'dtoperativo@digitaxplus.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'Digitax', 'uploads/logo/logo_59dc97a8331ba.png', 'gabriel.bioscenter@gmail.com', 'Gurugram, Haryana, India', 'AIzaSyAQQtD66dnP6HfDq-D_jV3Xl7NaWvhYF5c', '28.4594965', '77.0266383', 'digitexcustomer');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_allocated`
--

INSERT INTO `booking_allocated` (`booking_allocated_id`, `rental_booking_id`, `driver_id`, `status`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'El conductor se negÃ³ a venir', '', 0, 1, 1),
(3, 'El conductor esta retrasado', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 2),
(14, 'tesr', '', 0, 1, 2),
(7, 'Other ', '', 0, 1, 2),
(8, 'El pasajero no ha llegado', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 2),
(16, 'El pasajero ha durado mas de 2 minutos para  abordar', '', 0, 2, 1),
(17, 'Otro', '', 0, 2, 1),
(18, 'El pasajero nunca llego al lugar de partida', '', 0, 2, 1),
(19, 'El usuario fue grosero', '', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token_id` varchar(655) NOT NULL,
  `card_number` varchar(655) NOT NULL,
  `add_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`, `token_id`, `card_number`, `add_date`) VALUES
(1, 'Q7pJxqe8PRKdS547m', 1, 'aSsyuanDGWk5qH3wj', '6666', '2017-11-16'),
(2, 'tTZeL4C34P7iYDCed', 1, 'YXFXQxkDcsWMcdb78', '6869', '2017-11-16'),
(3, 'tTZeL4C34P7iYDCed', 1, 'WyKZ2etAEEa46WDvE', '5484', '2017-11-16');

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 2),
(2, 'Suzuki', 'uploads/car/car_2.png', 2),
(3, 'Ferrari', 'uploads/car/car_3.png', 2),
(4, 'Lamborghini', 'uploads/car/car_4.png', 2),
(5, 'Mercedes', 'uploads/car/car_5.png', 2),
(6, 'Tesla', 'uploads/car/car_6.png', 2),
(10, 'Renault', 'uploads/car/car_10.jpg', 2),
(11, 'taxi', 'uploads/car/car_11.png', 2),
(12, 'taxi', 'uploads/car/car_12.jpg', 2),
(13, 'yamaha', 'uploads/car/car_13.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(2, 'TAXI', '', 'HATCHBACK', 'uploads/car/editcar_2.png', 'webstatic/img/fleet-image/prime-play.png', 1, 1),
(3, 'TAXI PREMIUM', '', 'LUXURY', 'uploads/car/editcar_3.png', 'webstatic/img/fleet-image/lux.png', 1, 1),
(4, 'ESPECIAL', '', 'Mini', 'uploads/car/editcar_4.png', 'webstatic/img/fleet-image/mini.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#x20a8', 'AUD', '0024', 'Miles', '', '', 1),
(128, 'Soacha - Cundinamarca', '4.5827227', '-74.2117465', '', '', '', 'Km', '', '', 1),
(127, 'Cali - Valle del Cauca', '3.4516467', '-76.5319854', '', '', '', 'Miles', '', '', 2),
(126, 'Bucaramanga - Santander Department', '7.119349', '-73.1227416', '', '', '', 'Miles', '', '', 1),
(125, 'MedellÃ­n - Antioquia', '6.244203', '-75.5812119', '', '', '', 'Km', '', '', 1),
(124, 'BogotÃ¡ - Bogota', '4.7109886', '-74.072092', 'COP', 'COP', '0', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(2, 'Digitax', 'maria16s.com@gmail.com', '3023184519', 'Santa Isabel, BogotÃ¡ - Bogota, Colombia', 'COLOMBIA', 124, 'Maria Fernanda Sarmiento ', 'bios2017**', '2419', '', 1),
(4, 'bios', 'gabriel@bioscenter.com.co', '3434', 'Soacha - Cundinamarca, Colombia', 'COLOMBIA', 124, 'gaf2419', 'bios123456', '2419', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 15, 'k10', 1, 2, 1, 2, 'k10', 'Apporio Infolabs', 'k10', 'apporio@info.com2', '', 'k10', '', 'k10', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(18, 'lkdsasalsa', 'msammsa@qkjsa', 'kddepoew320-', 'saksalask'),
(17, 'qajkalk', 'SAJD@JSAG', '12344555666', 'kaal;al'),
(16, 'Yogesh', 'yogeshkumar2491@gmail.com', '', 'Testing'),
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!'),
(19, 'anurag', 'yuio@wkjlkew', '328740932', 'lfejljfsl'),
(20, 'sdllksd', 'skjs@lkdslk', 'lklkds', 'dlkslk'),
(21, 'dffhh', 'fhhjk@ghgj', '986987587', 'gkkhll'),
(22, 'asqwq', 'qaz@gmail.com', '2321235435', 'ertewtterterttrtrrttr');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD '),
(7, 58, 'COP', '0', 'COP');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 2, 'maria', 'maria', '3023184519', 'el cliente no me pagó absolutamente nada\n', 'Tuesday, Oct 10, 09:13 PM'),
(2, 2, 'Gabriel', 'Gabriel', '3023184519', 'hola', 'Thursday, Nov 2, 07:22 PM'),
(3, 2, 'hola', 'hola', '3023184519', 'holasaa', 'Monday, Nov 27, 11:40 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4123353', '77.0434555', '28.4123209', '77.043435', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:30:54 AM', '10:30:57 AM', '02:42:28 PM', '0', '0.00', '3765.00', '0.00', '0.00', '0.00', 1, '3865', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '120', '252', '3865', 0, '', 0, 1, '0000-00-00'),
(2, 3, '28.4123276', '77.0434228', '28.4123345', '77.0434443', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:03:48 AM', '11:03:56 AM', '11:11:13 AM', '0', '0.00', '90.00', '0.00', '0.00', '0.00', 1, '0', '0', '0', '100.00', '0.00', '0.00 Miles', '0.0', '7', '0.00', 1, '', 0, 1, '0000-00-00'),
(3, 4, '28.4123873', '77.0435092', '28.4123453', '77.0434363', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '11:14:12 AM', '11:14:19 AM', '11:14:37 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(4, 5, '28.4123071', '77.043443', '28.412153', '77.0433936', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:17:37 AM', '11:17:50 AM', '04:17:38 AM', '0', '0.00', '6285.00', '0.00', '0.00', '0.00', 1, '6385', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '420', '6385', 1, '', 0, 1, '0000-00-00'),
(5, 6, '28.4123432', '77.0434804', '28.4123309', '77.0435241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:20:08 AM', '06:20:15 AM', '06:20:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(6, 7, '', '', '', '', '', '', '04:24:11 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 3, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 10, 'rajni kant', 'anna@rajni.com', '0000000001', '', 'â‚¹@}^!123', '8lKWKaJtoE9XOCJk', '90', '0', '0', 'ctT6cRN5W3o:APA91bE0P37Xikk6Zs3BzxurjV4GEzHSqBd_76kUlKFT1R9N5SD-oz6CLBu1eW7vVmJNfyY0vWxvjdC_gEN3AJc72XERnPBw0VXKG854UFuC4MSB-LlEECF0ieXwvEJ8UIVoCkfraHCM', 2, '3.3', 2, 3, '0000', 56, 'Thursday, Nov 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122836', '77.0433978', '----', '05:45', 'Thursday, Nov 30, 2017', 73, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(2, 0, 4, 'diver', 'di@g.com', '9865321478', '', '123456', 'c7krBuRlJL0ekWmh', '96', '0', '', 'flybjwKS_Y4:APA91bFNTf_iVySahO3zN_EJt2kuA2BBVSApEyH4lJMcGO5cRCerhpmBUo1r2kSqmUivq13VE8e2PEfti5lmmgXfKOoDwMxHbeaodblFdAVjVZqBSZh7FFbpZ6SsGfiNogFmtaCBNT-S', 2, '2.5', 2, 3, '7788', 56, 'Friday, Nov 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4535253', '77.0530211', '----', '22:15', 'Friday, Nov 17, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(3, 0, 10, 'steven avendano', 'creativecolor0526@gmail.com', '3507990997', '', 'steven2017', 'mKwEosHhPzpMyA8F', '0', '0', '', 'cyBFprM2Nnc:APA91bHO5HfvjkzUBXJJfwVDAti0Lbyxz55e2d7sxRMxPlGx4yF_B8v6RNdo5F_RyC9dRvWVPXKiUyxgEAMxo1Q_u28FpYnUeS_VEOQcb7tXdxTJVwlA9AfL2X_Ga5UM3Zacqo5rf4SI', 2, '', 3, 31, 'USZ875', 124, 'Tuesday, Nov 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '4.6043263', '-74.1102809', '----', '16:27', 'Tuesday, Nov 28, 2017', 0, 0, 4, 1, 0, 1, 2, 0, '', 1, '', '2017-11-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '100', 9, '90', '-23235', '2017-11-16'),
(2, 2, '100', 1, '96.00', '4.00', '2017-11-17'),
(3, 1, '0.00', 1, '0.00', '0.00', '2017-11-17'),
(4, 1, '', 1, '0.00', '-6385', '2017-11-20');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 5, 1),
(2, 2, 6, 1),
(3, 3, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 124, 1, 'Thursday', '11:06 AM', '11:06 AM', '11:07 AM', '11:07 AM', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'iniciar sesión exitosamente', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Usuario inactivo', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Requerir campos faltantes', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id o contraseña incorrecta', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'Salir con éxito', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'ningún record fue encontrado', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Registrarse con éxito', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'Número de teléfono ya existe', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Ya existe el correo electrónico', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'falta copia rc', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Falta copia de licencia', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Falta copia del seguro', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Contraseña cambiada', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, 'La contraseña anterior no coincide', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, 'Código de cupón inválido', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, '\nCupón se aplica con éxito', 2),
(33, 17, 'User not exist', 1),
(34, 17, 'No existe ese usuario', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Actualizado correctamente', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'Número de teléfono ya existente', 2),
(39, 20, 'Online', 1),
(40, 20, 'En línea', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Desconectado', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, 'Otp enviado al teléfono para verificar', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Calificación exitosa', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Correo electrónico enviado con éxito', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'Reservas aceptadas', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'El conductor ha llegado', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'Paseo cancelado con éxito', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'El viaje ha terminado', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'Ride Book Successfully', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'Paseo rechazado con éxito', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'El viaje ha comenzado', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Nuevo viaje asignado', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'Paseo cancelado por el cliente', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'Reservas aceptadas', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'Reserva rechazada', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'Cancelación de reserva por conductor', 2),
(73, 40, 'Wrong Driver id', 1),
(74, 40, 'Identificación incorrecta del conductor', 2),
(75, 41, 'New Ride Allocated', 1),
(76, 41, 'Nuevo viaje asignado', 2),
(77, 42, 'Ride Cancelled by User', 1),
(78, 42, 'Paseo cancelado por el usuario', 2),
(79, 43, 'Ride Accepted by Driver', 1),
(80, 43, 'Paseo aceptado por el conductor', 2),
(81, 44, 'Ride Cancelled by driver', 1),
(82, 44, 'Paseo cancelado por el conductor', 2),
(83, 45, 'Driver Arrived on User Door', 1),
(84, 45, 'El conductor llegó a la puerta del usuario', 2),
(85, 46, 'Ride Started by Driver', 1),
(86, 46, 'Paseo iniciado por el conductor', 2),
(87, 47, 'Ride Ended By Driver', 1),
(88, 47, 'Paseo terminado por el conducto', 2),
(89, 48, 'Session Expired', 1),
(90, 48, 'Sesión expirada', 2),
(91, 49, 'Rating Successfully', 1),
(92, 49, 'calificar con éxito', 2),
(93, 50, 'ride expire', 1),
(94, 50, 'paseo expire', 2),
(95, 51, 'Document Already Upload', 1),
(96, 51, 'Documento ya subido', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 1, '', '28.41231728120792', '77.04343311488628', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '10:25:04', '10:25:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41231728120792,77.04343311488628&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 3, 1, 1),
(2, 1, '', '28.41232229435271', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '10:28:20', '10:28:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232229435271,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 3, 1, 1),
(3, 1, '', '28.41232229435271', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '10:30:17', '10:30:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232229435271,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 3, 1, 1),
(4, 3, '', '4.604340049177296', '-74.11032825708388', 'Cl. 1c #36-2 a 36-58, BogotÃ¡, Colombia', '4.7232053', '-74.0641089', 'Calle 134', '', '16:32:59', '04:32:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|4.604340049177296,-74.11032825708388&markers=color:red|label:D|4.7232053,-74.0641089&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', 'About Us and other information about company will go here.', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 2, 1, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything'),
(2, 3, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 16', '1'),
(3, 5, 2, '1', 'Cash', 'Admin', '100', 'Friday, Nov 17', '1'),
(4, 4, 1, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(54, 128, 'Km', '', 4, 10, 0, 0, 0, 0, 0, 0, '2', '70', '10', '0', '0', '0', '0'),
(53, 128, 'Km', '', 3, 10, 0, 0, 0, 0, 0, 0, '4', '80', '10', '0', '0', '0', '0'),
(52, 128, 'Km', '', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(51, 124, 'Km', 'COP', 3, 10, 0, 0, 0, 0, 0, 0, '3', '9', '10', '0', '0', '0', '0'),
(50, 124, 'Km', 'COP', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'hola maria', 'saludos maria aqui esta el fuec', 'uploads/notification/1500380956793.jpg', '', 0, 2, '2017-10-11', 2),
(2, 'Fuec Solicitado', 'Fuec Registro  X903', 'uploads/notification/1500380956793.jpg', '', 0, 2, '2017-10-11', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_booking`
--

INSERT INTO `rental_booking` (`rental_booking_id`, `user_id`, `rentcard_id`, `payment_option_id`, `coupan_code`, `car_type_id`, `booking_type`, `driver_id`, `pickup_lat`, `pickup_long`, `pickup_location`, `start_meter_reading`, `start_meter_reading_image`, `end_meter_reading`, `end_meter_reading_image`, `booking_date`, `booking_time`, `user_booking_date_time`, `last_update_time`, `booking_status`, `payment_status`, `pem_file`, `booking_admin_status`) VALUES
(1, 1, 2, 1, '', 2, 1, 0, '28.412235301512027', '77.04341668635607', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Thursday, Nov 30', '10:59 AM', 'Thursday, Nov 30, 10:59 AM', '11:00:15 AM', 15, 0, 1, 1),
(2, 1, 2, 1, '', 2, 1, 0, '28.412235301512027', '77.04341333359478', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Thursday, Nov 30', '11:02 AM', 'Thursday, Nov 30, 11:02 AM', '11:07:57 AM', 15, 0, 1, 1),
(3, 1, 2, 1, '', 2, 1, 1, '28.412291035916493', '77.04338081181048', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '0', '', '0', '', 'Thursday, Nov 30', '11:08 AM', 'Thursday, Nov 30, 11:08 AM', '11:09:20 AM', 15, 0, 1, 1),
(4, 1, 2, 1, '', 2, 1, 1, '28.412252700085872', '77.04340595752', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '1000', 'http://apporio.org.com/Digitax/digitax/uploads/1512020794312.jpg', '0', '', 'Thursday, Nov 30', '11:15 AM', 'Thursday, Nov 30, 11:15 AM', '11:16:39 AM', 13, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_distance_unit`, `rental_category_description`, `rental_category_admin_status`) VALUES
(1, 'Paquete 5 horas ', '5', '100', 'Km', 'EL usuario puede&nbsp; &nbsp;solicitar un&nbsp; servicio por&nbsp; horas&nbsp;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(1, 124, 3, 1, '60000', 10000, 1000, 1),
(2, 56, 2, 1, '1000', 10, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 1, '2017-11-16'),
(2, 2, 1, 0, '2017-11-16'),
(3, 3, 1, 1, '2017-11-16'),
(4, 4, 1, 1, '2017-11-16'),
(5, 5, 1, 1, '2017-11-16'),
(6, 6, 2, 1, '2017-11-17'),
(7, 7, 3, 1, '2017-11-28'),
(8, 8, 3, 8, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.41232229435271', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '10:30:46', '02:42:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232229435271,77.04343680292368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 0, 0, 3, 1, 1, 1, '2017-11-16'),
(2, 1, '', '28.41232229435271', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.44517246265858', '77.03340634703636', '434/16, Civil Line Rd, Civil Lines, Gurugram, Haryana 122022, India', 'Thursday, Nov 16', '11:02:35', '11:02:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232229435271,77.04343680292368&markers=color:red|label:D|28.44517246265858,77.03340634703636&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 3, 1, 1, 1, '2017-11-16'),
(3, 1, '', '28.41232229435271', '77.04343680292368', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.44517246265858', '77.03340634703636', '434/16, Civil Line Rd, Civil Lines, Gurugram, Haryana 122022, India', 'Thursday, Nov 16', '11:03:33', '11:11:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232229435271,77.04343680292368&markers=color:red|label:D|28.44517246265858,77.03340634703636&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-11-16'),
(4, 1, '', '28.412340282693915', '77.04344686120749', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '11:13:22', '11:14:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412340282693915,77.04344686120749&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-16'),
(5, 1, '', '28.412368887099028', '77.04345725476742', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 16', '11:17:09', '04:17:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412368887099028,77.04345725476742&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 3, 3, 1, 1, '2017-11-16'),
(6, 2, '', '28.412356501687455', '77.04348374158144', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Establezca su destino', 'Friday, Nov 17', '06:19:51', '06:20:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412356501687455,77.04348374158144&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(7, 3, '', '4.604340049177296', '-74.11032825708388', 'Cl. 1c #36-2 a 36-58, BogotÃ¡, Colombia', '4.7232053', '-74.0641089', 'Calle 134', 'Tuesday, Nov 28', '16:23:44', '04:25:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|4.604340049177296,-74.11032825708388&markers=color:red|label:D|4.7232053,-74.0641089&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 3, 1, 1, 9, 0, 18, 1, 0, 1, 1, '2017-11-28'),
(8, 3, '', '4.574464', '-74.1866799', 'Carrera 21 # 36 40, Soacha - Cundinamarca, Colombia', '4.740881', '-74.08380999999997', 'Suba, BogotÃ¡, Colombia', 'Wednesday, Nov 29', '15:37:11', '03:40:52 PM', '', '2017-11-30', '10 : 49', 0, 4, 2, 1, 1, 0, 0, 1, 0, 2, 1, '2017-11-29');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1),
(6, 'Breakdown', '199', 1),
(7, 'Breakdown', '199', 1),
(8, 'K10', '0800187241', 1),
(9, 'BOMBEROS', '911', 1),
(10, 'Gestion de control digitax', '3023184519', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO'),
(6, 643, 'ewqla', 'lsajlas@jdsal', 'lsalk', 'lsalk');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'Ø§Ù„Ø±Ø®ØµØ©'),
(11, 'Ø§Ù„Ø¨Ø·Ø§Ù‚Ø©'),
(12, 'ØµÙˆØ±Ø©'),
(13, 'cedula azul'),
(14, 'RevisiÃ³n de gases'),
(15, 'Cettificacionde ingresos '),
(16, 'Licencia de transito ');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(36, 56, 16, 1),
(35, 56, 15, 1),
(34, 56, 14, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(29, 124, 3, 1),
(28, 124, 2, 1),
(27, 124, 1, 1),
(30, 124, 14, 1),
(31, 128, 1, 1),
(32, 128, 2, 1),
(33, 128, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_done_rental_booking`
--

INSERT INTO `table_done_rental_booking` (`done_rental_booking_id`, `rental_booking_id`, `driver_id`, `driver_arive_time`, `begin_lat`, `begin_long`, `begin_location`, `begin_date`, `begin_time`, `end_lat`, `end_long`, `end_location`, `end_date`, `end_time`, `total_distance_travel`, `total_time_travel`, `rental_package_price`, `rental_package_hours`, `extra_hours_travel`, `extra_hours_travel_charge`, `rental_package_distance`, `extra_distance_travel`, `extra_distance_travel_charge`, `total_amount`, `coupan_price`, `final_bill_amount`, `payment_status`) VALUES
(1, 3, 1, '11:08:55 AM', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '0.00', '0.00', '0', 0),
(2, 4, 1, '11:15:30 AM', '28.4122369', '77.0434157', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '2017-11-30', '11:16:39 AM', '', '', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '0.00', '0.00', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-10-10 00.00.01 AM', '2017-10-10 02:00:22 PM', 1, '4', '0000-00-00', 0),
(2, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 3, '0', '0000-00-00', 0),
(3, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 2, '0.9', '2017-10-15', 1),
(4, '2017-10-10 02:01:22 PM', '2017-10-11 07:25:54 AM', 1, '0', '0000-00-00', 0),
(5, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 10, '0', '0000-00-00', 0),
(6, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 9, '0', '0000-00-00', 0),
(7, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 8, '0', '0000-00-00', 0),
(8, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 7, '0', '0000-00-00', 0),
(9, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 6, '0', '0000-00-00', 0),
(10, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 5, '1.8', '0000-00-00', 0),
(11, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 4, '0.9', '2017-10-27', 1),
(12, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 3, '0', '0000-00-00', 0),
(13, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 2, '0', '0000-00-00', 0),
(14, '2017-10-11 07:26:54 AM', '2017-10-16 07:48:35 AM', 1, '0', '0000-00-00', 0),
(15, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:47 AM', 11, '12', '0000-00-00', 0),
(16, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:47 AM', 10, '0', '0000-00-00', 0),
(17, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 9, '0', '0000-00-00', 0),
(18, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 8, '0', '0000-00-00', 0),
(19, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 7, '0', '0000-00-00', 0),
(20, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 6, '0', '0000-00-00', 0),
(21, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 5, '0', '0000-00-00', 0),
(22, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 4, '0.9', '2017-10-27', 1),
(23, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 3, '0', '0000-00-00', 0),
(24, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 2, '0', '0000-00-00', 0),
(25, '2017-10-16 07:49:35 AM', '2017-10-18 04:08:48 AM', 1, '0', '0000-00-00', 0),
(26, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 11, '-2114.4', '0000-00-00', 0),
(27, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 10, '0', '0000-00-00', 0),
(28, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 9, '0', '0000-00-00', 0),
(29, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 8, '0', '0000-00-00', 0),
(30, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 7, '0', '0000-00-00', 0),
(31, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 6, '2.7', '0000-00-00', 0),
(32, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 5, '0', '0000-00-00', 0),
(33, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 4, '3.6', '0000-00-00', 0),
(34, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 3, '0', '0000-00-00', 0),
(35, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 2, '0', '0000-00-00', 0),
(36, '2017-10-18 04:09:48 AM', '2017-11-02 04:01:30 PM', 1, '-328460', '0000-00-00', 0),
(37, '2017-11-02 04:02:30 PM', '2017-11-28 03:55:38 PM', 2, '4', '0000-00-00', 0),
(38, '2017-11-02 04:02:30 PM', '2017-11-28 03:55:38 PM', 1, '10', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1510827751document_image_13.jpg', '25-11-2017', 1),
(2, 1, 2, 'uploads/driver/1510827762document_image_12.jpg', '30-11-2017', 1),
(3, 1, 1, 'uploads/driver/1510827773document_image_11.jpg', '16-11-2017', 1),
(4, 1, 14, 'uploads/driver/1510827791document_image_114.jpg', '25-11-2017', 1),
(5, 2, 3, 'uploads/driver/1510898448document_image_23.jpg', '25-11-2017', 1),
(6, 2, 2, 'uploads/driver/1510898477document_image_22.jpg', '25-11-2017', 1),
(7, 2, 4, 'uploads/driver/1510898498document_image_24.jpg', '30-11-2017', 1),
(8, 3, 3, 'uploads/driver/1511886027document_image_33.jpg', '28-11-2017', 1),
(9, 3, 2, 'uploads/driver/1511886040document_image_32.jpg', '28-11-2017', 1),
(10, 3, 1, 'uploads/driver/1511886057document_image_31.jpg', '28-11-2017', 1),
(11, 3, 14, 'uploads/driver/1511886073document_image_314.jpg', '28-11-2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-10-10 11:39:24', '', '', 0, 0, '2017-10-10'),
(2, 2, '2017-10-10 20:08:04', '2017-10-11 06:01:35', '9 Hours 156 Minutes', 9, 156, '2017-10-10'),
(3, 3, '2017-10-10 15:14:08', '2017-10-10 16:03:38', '0 Hours 49 Minutes', 0, 49, '2017-10-10'),
(4, 2, '2017-10-11 06:29:19', '2017-10-11 06:06:28', '0 Hours 3 Minutes', 0, 3, '2017-10-11'),
(5, 4, '2017-10-12 16:30:21', '2017-10-18 04:46:15', '12 Hours 34 Minutes', 12, 34, '2017-10-12'),
(6, 5, '2017-10-13 01:13:22', '2017-10-13 16:54:21', '15 Hours 40 Minutes', 15, 40, '2017-10-12'),
(7, 6, '2017-10-14 17:28:30', '2017-10-14 19:19:04', '1 Hours 50 Minutes', 1, 50, '2017-10-14'),
(8, 9, '2017-10-14 19:03:50', '2017-10-27 23:06:25', '4 Hours 19 Minutes', 4, 19, '2017-10-14'),
(9, 7, '2017-10-14 18:47:24', '2017-10-31 14:28:08', '19 Hours 40 Minutes', 19, 40, '2017-10-14'),
(10, 10, '2017-10-14 19:04:23', '2017-10-14 19:17:33', '0 Hours 18 Minutes', 0, 18, '2017-10-14'),
(11, 8, '2017-10-14 19:10:14', '2017-10-14 19:12:10', '0 Hours 7 Minutes', 0, 7, '2017-10-14'),
(12, 10, '2017-10-15 14:10:29', '2017-10-15 14:11:32', '0 Hours 1 Minutes', 0, 1, '2017-10-15'),
(13, 11, '2017-10-17 08:51:01', '', '', 0, 0, '2017-10-17'),
(14, 6, '2017-10-17 19:18:42', '2017-10-28 19:15:14', '23 Hours 56 Minutes', 23, 56, '2017-10-17'),
(15, 4, '2017-10-18 04:53:56', '2017-10-28 19:20:20', '14 Hours 26 Minutes', 14, 26, '2017-10-17'),
(16, 10, '2017-10-18 20:15:21', '', '', 0, 0, '2017-10-18'),
(17, 1, '2017-10-27 12:32:59', '', '', 0, 0, '2017-10-27'),
(18, 6, '2017-10-28 19:23:14', '2017-10-28 19:36:27', '0 Hours 20 Minutes', 0, 20, '2017-10-28'),
(19, 4, '2017-10-28 19:20:23', '', '', 0, 0, '2017-10-28'),
(20, 11, '2017-10-31 09:37:46', '2017-11-14 07:07:02', '22 Hours 83 Minutes', 22, 83, '2017-10-31'),
(21, 7, '2017-10-31 14:28:12', '', '', 0, 0, '2017-10-31'),
(22, 12, '2017-11-02 18:18:14', '', '', 0, 0, '2017-11-02'),
(23, 13, '2017-11-03 21:09:30', '2017-11-03 21:13:08', '0 Hours 3 Minutes', 0, 3, '2017-11-03'),
(24, 13, '2017-11-06 15:33:34', '', '', 0, 0, '2017-11-06'),
(25, 6, '2017-11-08 15:44:38', '', '', 0, 0, '2017-11-08'),
(26, 2, '2017-11-11 05:30:18', '', '', 0, 0, '2017-11-10'),
(27, 5, '2017-11-11 05:43:31', '', '', 0, 0, '2017-11-10'),
(28, 11, '2017-11-14 07:07:08', '', '', 0, 0, '2017-11-14'),
(29, 1, '2017-11-16 10:30:28', '', '', 0, 0, '2017-11-16'),
(30, 2, '2017-11-17 06:02:07', '', '', 0, 0, '2017-11-17'),
(31, 3, '2017-11-28 16:21:27', '', '', 0, 0, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(42, 'Portuguese', 1),
(41, 'Vietnamese', 2),
(40, 'French', 2),
(39, 'Spanish', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'login Successfully'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Campos obligatorios faltantes'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver'),
(108, 1, 'es', ' INICIO DE SESIÃ“N CORRECTO'),
(109, 2, 'es', 'INACTIVO DEL USUARIO'),
(110, 3, 'es', 'REQUIERE CAMPOS MISSING'),
(111, 4, 'es', ' EMAIL-ID O CONTRASEÃ‘A INCORRECTO'),
(112, 5, 'es', 'LOGOUT EXITOSAMENTE'),
(113, 6, 'es', ' NINGÃšN RECORD FUE ENCONTRADO'),
(114, 7, 'es', 'REGISTRARSE CON Ã‰XITO'),
(115, 8, 'es', ' EL NÃšMERO DE TELÃ‰FONO YA EXISTEN'),
(116, 9, 'es', ' YA EXISTE EL CORREO ELECTRÃ“NICO'),
(117, 10, 'es', ' RC COPIA MISSING'),
(118, 11, 'es', ' LICENCIA COPIA MISSING'),
(119, 12, 'es', ' SEGURO DE COPIA MISSING'),
(120, 13, 'es', 'CONTRASEÃ‘A CAMBIADA'),
(121, 14, 'es', ' ANTIGUA CONTRASEÃ‘A NO SE JUEGA');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 3, 1, 3.5, '', 1, 4, '', '2017-10-10'),
(2, 1, 1, 4, '', 1, 0, '', '2017-10-10'),
(3, 7, 2, 0, '', 2, 1, 'mÃ¡s o menos', '2017-10-11'),
(4, 5, 2, 5, '', 2, 0, '', '2017-10-11'),
(5, 13, 2, 0, '', 4, 5, 'sÃºper', '2017-10-12'),
(6, 6, 2, 3.5, 'muy bn ', 4, 4.5, '', '2017-10-12'),
(7, 21, 2, 0, '', 5, 5, 'chichipato', '2017-10-13'),
(8, 8, 2, 5, 'sÃºper', 5, 0, '', '2017-10-13'),
(9, 11, 2, 5, 'bn', 5, 0, '', '2017-10-13'),
(10, 40, 1, 4, '', 11, 4.5, '', '2017-10-17'),
(11, 15, 1, 4.5, '', 11, 0, '', '2017-10-17'),
(12, 16, 2, 4, 'BN', 4, 0, '', '2017-10-17'),
(13, 41, 2, 4, '', 4, 4, 'bn', '2017-10-17'),
(14, 42, 1, 4.5, '', 11, 5, '', '2017-10-17'),
(15, 17, 1, 4.5, '', 11, 0, '', '2017-10-17'),
(16, 43, 1, 4.5, '', 11, 5, '', '2017-10-17'),
(17, 18, 1, 4.5, '', 11, 0, '', '2017-10-17'),
(18, 25, 2, 5, '', 4, 0, '', '2017-10-18'),
(19, 53, 2, 0, '', 4, 4, 'bn', '2017-10-18'),
(20, 52, 2, 0, '', 4, 4.5, '', '2017-10-18'),
(21, 26, 2, 5, 'k', 4, 0, '', '2017-10-18'),
(22, 54, 2, 0, '', 4, 5, '', '2017-10-18'),
(23, 27, 2, 5, '', 4, 0, '', '2017-10-18'),
(24, 57, 2, 4, '', 4, 4.5, '', '2017-10-18'),
(25, 64, 2, 0, '', 6, 5, '', '2017-10-18'),
(26, 34, 2, 5, 'muy bueno ', 6, 0, '', '2017-10-18'),
(27, 35, 2, 5, '', 6, 0, '', '2017-10-18'),
(28, 66, 2, 0, '', 6, 5, 'bien', '2017-10-18'),
(29, 36, 2, 5, '', 6, 0, '', '2017-10-18'),
(30, 69, 1, 0, '', 11, 4, '', '2017-10-25'),
(31, 38, 1, 3.5, '', 11, 0, '', '2017-10-25'),
(32, 70, 1, 0, '', 11, 4.5, '', '2017-10-25'),
(33, 39, 1, 4, '', 11, 0, '', '2017-10-25'),
(34, 71, 1, 0, '', 11, 5, '', '2017-10-25'),
(35, 79, 1, 0, '', 1, 4, '', '2017-10-27'),
(36, 44, 1, 3, '', 11, 0, '', '2017-10-31'),
(37, 45, 1, 3, '', 11, 0, '', '2017-10-31'),
(38, 94, 1, 0, '', 11, 4, '', '2017-11-06'),
(39, 49, 1, 3.5, '', 11, 0, '', '2017-11-06'),
(40, 95, 2, 0, '', 13, 3.5, '', '2017-11-06'),
(41, 50, 2, 5, 'no', 13, 0, '', '2017-11-06'),
(42, 96, 2, 0, '', 13, 5, '', '2017-11-06'),
(43, 51, 2, 4.5, 'prueba de tarjeta de crÃ©dito', 13, 0, '', '2017-11-06'),
(44, 55, 1, 4.5, '', 11, 0, '', '2017-11-14'),
(45, 56, 1, 4, '', 11, 0, '', '2017-11-15'),
(46, 113, 1, 0, '', 11, 4.5, '', '2017-11-16'),
(47, 2, 1, 4.5, '', 1, 0, '', '2017-11-16'),
(48, 4, 1, 4.5, '', 1, 3.5, '', '2017-11-16');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 0, 2),
(3, 1, 1, 1, 3),
(4, 1, 1, 1, 4),
(5, 1, 1, 1, 5),
(6, 1, 2, 2, 6),
(7, 1, 3, 3, 7),
(8, 1, 3, 0, 8),
(9, 2, 1, 0, 1),
(10, 2, 1, 0, 2),
(11, 2, 1, 1, 3),
(12, 2, 1, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'QWERTY .', 'QWERTY@gmail.com', '+919638527410', '123456', '', '', 0, '0', 'Thursday, Nov 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.08695652174', 0, '', 1, '2017-11-16', 1),
(2, 1, 'shilpa .', 'sg@g.com', '+919865321478', 'qwerty', '', '', 0, '0', 'Friday, Nov 17', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.24', 0, '', 1, '2017-11-17', 1),
(3, 1, 'Gabriel Ureña', '', '+573023184519', '', '', '', 0, '0', 'Wednesday, Nov 22', '', 0, 0, 0, 0, 0, '1744007155651711', 'unidos.enlafe@hotmail.com', 'http://graph.facebook.com/1744007155651711/picture?type=large', 'Gabriel', 'Ureña', '', '', '', '', '', '', 0, 0, '', 0, '', 2, '2017-11-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'f-n-yYBJACI:APA91bGWl9W6OIzTDDoAzHYzbcpKEsIAScDqVde5cc5atGtYukFIBOGGLFtuQtiM14KzwcAvhZk674LXtaho1DnpsVanbbZDYv7O95dZlIEI2tYjDOK6Ii6wxnCE0fU9gmxS9cpm0eQ_', 0, 'c06c40037632ed39', 1),
(2, 3, 'dAy_cP8-nIA:APA91bE7oO8XjhBveLADlZeCSnlSrhjbtY-uBBhBGCiCHrQc9Raxx9GZ9io_5wf_IcQV0nZS8cbyt1wTRv__o5s-avmMi78nWQriwqlmMc50a6efZ6vABNLmmiCb62lTKI-9P8zXmZsE', 0, '4f4107385f41fccf', 1),
(3, 2, 'fQyPjxayCtA:APA91bFd-NnjbpK62cERHFE891ic4Dv3ApKUnU702U2_e6UeC-TJqaU4EKO7tK6GB6C8aaAjHVLaQjqyrr7fw3yFSq4X-MlfUFKCUjWfc1jYlQylgbPEptWopUvcQ9dV3Bd2gaEpA1Bo', 0, '9f1891df4b8e4e77', 0);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `content`, `image`, `link`) VALUES
(1, 'Cabs for Every Pocket', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', 'webstatic/img/ola-article/why-ola-1.jpg', ''),
(2, 'Secure and Safer Rides', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'webstatic/img/ola-article/why-ola-3.jpg', ''),
(3, 'Apporio Select', 'A membership program with Ola that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', 'webstatic/img/ola-article/why-ola-2.jpg', ''),
(4, '\r\nIn Cab Entertainment', 'Play music, watch videos and a lot more with Ola Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', 'webstatic/img/ola-article/why-ola-9.jpg', ''),
(5, 'Cashless Rides', 'Now go cashless and travel easy. Simply recharge your Apporio money or add your credit/debit card to enjoy hassle free payments.', 'webstatic/img/ola-article/why-ola-5.jpg', ''),
(6, 'Share and Express', 'To travel with the lowest fares choose Apporio Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', 'webstatic/img/ola-article/why-ola-3.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
