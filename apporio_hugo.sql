-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:20 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_hugo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'Oma', 'James', 'oma_james', '', '', 'test123', '08022226365', 'jamesbron2000@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`) VALUES
(1, 'Hugotaxi', 'uploads/logo/logo_59c0c2661652b.png', 'hello@apporio.com', 'Gurugram, Haryana, India', 'AIzaSyAWuwe2iSAV3HmlRj6DPVdti6__Skne8jI', '28.4594965', '77.0266383');

-- --------------------------------------------------------

--
-- Table structure for table `booking-otp`
--

CREATE TABLE `booking-otp` (
  `otp_id` int(11) NOT NULL,
  `user_phone` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking-otp`
--

INSERT INTO `booking-otp` (`otp_id`, `user_phone`, `code`) VALUES
(1, '+2341234567890', '2017'),
(2, '++918130039030', '2017'),
(3, '+917970344870', '2017'),
(4, '+9187678687687', '2017'),
(5, '+9176576745764767', '2017'),
(6, '+9180222263655', '2017'),
(7, '+912347034498429', '2017');

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, 'cus_BVrFrvI2HL7b1U', 6);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 2),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_type_name_french` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_name_spanish` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_name_chinese` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `cartype_big_image` varchar(255) NOT NULL DEFAULT '',
  `car_longdescription` text CHARACTER SET utf8 NOT NULL,
  `car_longdescription_arabic` text CHARACTER SET utf8 NOT NULL,
  `car_longdescription_french` text CHARACTER SET utf8 NOT NULL,
  `car_longdescription_chinese` text CHARACTER SET utf8 NOT NULL,
  `car_longdescription_spanish` text CHARACTER SET utf8 NOT NULL,
  `car_description` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_description_arabic` varchar(255) CHARACTER SET utf16 NOT NULL DEFAULT '',
  `car_description_french` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_description_chinese` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_description_spanish` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_name_spanish`, `car_name_chinese`, `car_type_image`, `cartype_image_size`, `cartype_big_image`, `car_longdescription`, `car_longdescription_arabic`, `car_longdescription_french`, `car_longdescription_chinese`, `car_longdescription_spanish`, `car_description`, `car_description_arabic`, `car_description_french`, `car_description_chinese`, `car_description_spanish`, `ride_mode`, `car_admin_status`) VALUES
(2, 'HATCHBACK', 'هاتشباك', 'HATCHBACK', 'HATCHBACK', '两厢', 'uploads/car/editcar_2.png', '', 'webstatic/img/fleet-image/prime-play.png', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'ميني مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Le Mini trop familier, sans les tracas d\'attendre et de marchander pour le prix. Un moyen pratique de voyager tous les jours.', '所有太熟悉的迷你，减去等待和讨价还价的麻烦。一种方便的旅行方式。', 'El Mini demasiado familiar, sin la molestia de esperar y regatear por el precio. Una forma conveniente de viajar todos los días.', 'Get Mini at your doorstep', 'الحصول على ميني على عتبة داركم', 'Obtenez Mini à votre porte', '在您的家门口享用迷你吧', 'Obtenga Mini en su puerta', 1, 2),
(3, 'LUXURY', 'ترف', 'LUXE', 'LUJO', '豪华', 'webstatic/img/ola-fleet-svg/ola-lux-active.svg', '', 'webstatic/img/fleet-image/lux.png', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'ميني مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Le Mini trop familier, sans les tracas d\'attendre et de marchander pour le prix. Un moyen pratique de voyager tous les jours.', '所有太熟悉的迷你，减去等待和讨价还价的麻烦。一种方便的旅行方式。', 'El Mini demasiado familiar, sin la molestia de esperar y regatear por el precio. Una forma conveniente de viajar todos los días.', 'Get LUXURY at your doorstep', 'الحصول على ميني على عتبة داركم', 'Obtenez le LUXE à votre porte', '在您的家门口享受奢华', 'Obtenga LUJO en su puerta', 1, 1),
(4, 'HugoTaxi', 'هوغو سيارة أجرة', 'HugoTaxi', 'HugoTaxi', 'HugoTaxi', 'webstatic/img/ola-fleet-svg/ola-kaali-peeli-active.svg', '', 'webstatic/img/fleet-image/kaali-peeli.png', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'ميني مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', '所有太熟悉的迷你，减去等待和讨价还价的麻烦。一种方便的旅行方式。', 'El Mini demasiado familiar, sin la molestia de esperar y regatear por el precio. Una forma conveniente de viajar todos los días.', 'Get HugoTaxi at your doorstep', 'الحصول على ميني على عتبة داركم', 'Obtenez HugoTaxi à votre porte', '在你的家门口得到', 'Obtenga HugoTaxi a la vuelta de la esquina', 1, 1),
(27, 'Towing Cars', 'سيارات السحب', 'Voitures de remorquage', 'Coches de remolque', '牵引车', 'webstatic/img/ola-fleet-svg/towing_truck.png', '7077', 'webstatic/img/ola-fleet-svg/towing_truck.png', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'ميني مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', '所有太熟悉的迷你，减去等待和讨价还价的麻烦。一种方便的旅行方式。', 'El Mini demasiado familiar, sin la molestia de esperar y regatear por el precio. Una forma conveniente de viajar todos los días.', 'Get Towing Carsat your doorstep', 'الحصول على ميني على عتبة داركم', 'Obtenez des voitures de remorquage à votre porte', '在你的家门口跋涉', 'Obtenga Towing Cars en su puerta', 1, 1),
(28, 'Pickup Vans', 'شاحنات بيك آب', 'Camionnettes', 'Camionetas pickup', '皮卡车', 'webstatic/img/ola-fleet-svg/ola-shuttle-active.svg', '5014', 'webstatic/img/fleet-image/shuttle.png', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'ميني مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'The all too familiar Mini, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', '所有太熟悉的迷你，减去等待和讨价还价的麻烦。一种方便的旅行方式。', 'El Mini demasiado familiar, sin la molestia de esperar y regatear por el precio. Una forma conveniente de viajar todos los días.', 'Get Pickup Vans at your doorstep', 'الحصول على ميني على عتبة داركم', 'Obtenez des camionnettes à votre porte', '在您的门口取得皮卡车', 'Obtenga camionetas Pickup en su puerta', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#8377', 'Miles', '', '', 1),
(128, 'Port Harcourt', '4.815554', '7.0498442', 'â‚¦', 'Km', '', '', 1),
(127, 'Makurdi', '7.7321516', '8.539144', 'â‚¦', 'Km', '', '', 1),
(126, 'Benin City', '6.334986', '5.6037465', 'â‚¦', 'Km', '', '', 1),
(125, 'Ibadan', '7.3775355', '3.9470396', 'â‚¦', 'Km', '', '', 1),
(124, 'Kumasi', '6.6666004', '-1.6162709', 'â‚µ', 'Km', '', '', 1),
(123, 'Abuja', '8.8556838', '7.179026', 'â‚¦', 'Km', '', '', 1),
(122, 'Koforidua', '6.6666004', '-1.6162709', 'â‚µ', 'Km', '', '', 1),
(121, 'Accra', '5.6037168', '-0.1869644', 'â‚µ', 'Km', '', '', 1),
(120, 'Lagos', '6.5243793', '3.3792057', 'â‚¦', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(1, '', 'a@gmail.com', '', '', '', 0, '', '12345678', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-09-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-13', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'HHHHHH0', '300', 2, 1, '2017-09-21', '2017-09-22', 'Nominal', 1),
(109, 'ABCD01', '300', 2, 2, '2017-09-22', '2017-09-30', 'Nominal', 1),
(110, 'CJPETER', '20', 100, 2, '2017-10-12', '2017-10-31', 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377'),
(4, 'Naira', '&#8358;', '&#8358;'),
(5, 'Cedi', 'https://www.toptal.com/designers/htmlarrows/currency/cedi-sign/', 'https://www.toptal.com/designers/htmlarrows/currency/cedi-sign/'),
(6, 'Cedi', 'â‚µ', 'â‚µ');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM'),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM'),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM'),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM'),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM'),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM'),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM'),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM'),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM'),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM'),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM'),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM'),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM'),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM'),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM'),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM'),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM'),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM'),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM'),
(20, 1, 'James', 'jamesbron2000@gmail.com', '08022226365', 'Customer support testing ', 'Tuesday, Sep 19, 03:38 PM'),
(21, 1, 'Kenneth', 'jamesbron2000@yahoo.com', 'jamesbron2000', 'driver needed', 'Monday,Oct 23,07:47 AM'),
(22, 1, 'Testing', 'testing@gmail.com', '765465465465', 'ytfytfytfyfyfty', 'Tuesday,Oct 24,02:00 AM'),
(23, 1, 'ygcytcytcy', 'ytcytcycyt@YTcytcycyt', 'ytcytcycy', 'tycycycycy', 'Tuesday,Oct 24,02:05 AM'),
(24, 1, 'Ken Foh', 'jamesbron2000@yahoo.com', '08022226365', 'Testing send message', 'Tuesday,Oct 24,07:49 AM'),
(25, 1, 'EEEE1', 'jamesbron2000@gmail.com', '08022226365', 'testing', 'Wednesday,Nov 1,05:41 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4120985240626', '77.0432550679548', '28.4121111547839', '77.0432193764272', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:04:07 PM', '12:04:18 PM', '12:04:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(2, 4, '28.4120265166881', '77.0432344746908', '28.4120800579231', '77.0432388224425', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:21:17 PM', '12:21:31 PM', '12:21:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(3, 5, '28.4120573053174', '77.0432730384957', '28.4120895930076', '77.0432708718613', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:26:54 PM', '12:27:02 PM', '12:27:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(4, 6, '28.4120684426733', '77.0432869777508', '28.4119956155666', '77.043219703974', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:43:03 PM', '12:43:16 PM', '12:43:35 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(5, 7, '28.4120726546024', '77.0432273510756', '28.4120345441889', '77.0430906303944', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '02:14:24 PM', '02:14:32 PM', '02:14:43 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(6, 8, '28.4120760765191', '77.0431816578629', '28.4120760765191', '77.0431816578629', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:40:05 PM', '02:40:20 PM', '02:41:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(7, 9, '28.4120632522073', '77.0433289279016', '28.4120632522073', '77.0433289279016', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:09:54 AM', '11:10:01 AM', '11:10:11 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(8, 11, '28.4121812', '77.0431351', '28.4121812', '77.043135', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:15:34 AM', '08:15:52 AM', '08:15:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(9, 12, '28.4121777', '77.0431438', '28.4121777', '77.0431437', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:23:59 AM', '08:24:03 AM', '08:24:06 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(10, 13, '28.412182', '77.0431349', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:03:04 AM', '09:03:07 AM', '09:03:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(11, 14, '28.4121664', '77.0431785', '28.4121672', '77.0431761', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:06:13 AM', '09:06:42 AM', '09:06:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(12, 15, '28.4121767', '77.0431483', '28.4121768', '77.0431479', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:11:40 AM', '09:11:42 AM', '09:11:44 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(13, 16, '28.4121789', '77.043142', '28.4121789', '77.043142', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:14:01 AM', '09:14:03 AM', '09:14:05 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(14, 16, '28.4121789', '77.043142', '28.4121789', '77.043142', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:14:02 AM', '09:14:03 AM', '09:14:05 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(15, 17, '28.4121821', '77.0431325', '28.4121821', '77.0431325', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:49:31 AM', '09:49:33 AM', '09:49:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(16, 18, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:19:11 AM', '10:19:13 AM', '10:19:15 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(17, 19, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:21:23 AM', '10:21:26 AM', '10:21:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(18, 20, '28.4121856', '77.0431223', '28.4125471', '77.0436171', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:22:08 AM', '10:22:19 AM', '10:26:07 AM', '0', '0.00', '30.00', '0.00', '0.00', '0.00', 5, '130', '6.50', '123.50', '100.00', '0.00', '0.00 Miles', '0.0', '3', '130', 1, '', 1, 0, '0000-00-00'),
(19, 21, '28.4121856', '77.0431223', '28.4120547', '77.0433922', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:28:51 AM', '10:28:54 AM', '10:31:10 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 5, '115', '5.75', '109.25', '100.00', '0.00', '0.00 Miles', '0.0', '2', '115', 1, '', 1, 1, '0000-00-00'),
(20, 22, '28.4120547', '77.0433922', '28.4121856', '77.0431223', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:32:30 AM', '10:32:38 AM', '10:33:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(21, 23, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:36:09 AM', '10:36:11 AM', '10:36:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(22, 24, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:37:16 AM', '10:37:24 AM', '10:37:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(23, 25, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '10:40:35 AM', '10:41:06 AM', '10:41:48 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(24, 27, '6.4387462', '3.4352438', '6.4387014', '3.4351248', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '02:39:43 PM', '02:40:13 PM', '02:53:35 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 7, '70', '7.00', '63.00', '70.00', '0.00', '0.00 Km', '0.0', '13', '70', 1, '', 1, 1, '0000-00-00'),
(25, 28, '6.437932', '3.4350803', '6.4383771', '3.4351448', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '03:43:39 PM', '03:44:14 PM', '04:02:57 PM', '0', '0.00', '108.00', '0.00', '0.00', '0.00', 7, '424.25', '42.43', '381.82', '316.25', '0.00', '0.25 Km', '0.0', '18', '424.25', 1, '', 1, 1, '0000-00-00'),
(26, 29, '6.4378418', '3.4352228', '6.4383679', '3.4352212', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '04:30:53 PM', '04:31:23 PM', '04:36:22 PM', '0', '0.00', '24.00', '0.00', '0.00', '0.00', 7, '342.85', '34.29', '308.56', '318.85', '0.00', '0.29 Km', '100.0', '4', '342.85', 1, '', 0, 1, '0000-00-00'),
(27, 30, '6.4479414', '3.4287029', '6.4636618', '3.4207658', 'Alfred Rewane Rd, Ikoyi, Lagos, Nigeria', 'Osborne Rd, Ikoyi, Lagos, Nigeria', '08:01:41 PM', '08:01:48 PM', '08:25:12 PM', '0', '0.00', '138.00', '0.00', '0.00', '0.00', 7, '1015.2', '101.52', '913.68', '877.20', '0.00', '8.88 Km', '2880.0', '23', '1015.2', 1, '', 1, 1, '0000-00-00'),
(28, 31, '6.4622293', '3.4170148', '6.4763143', '3.3822361', 'Federal Secretariat Rd, Dolphine Estate, Lagos, Nigeria', 'Otto Police Barracks, Murtala Muhammed, Lagos Mainland, Lagos, Nigeria', '08:28:14 PM', '08:28:24 PM', '08:46:37 PM', '0', '0.00', '108.00', '0.00', '0.00', '0.00', 7, '408', '40.80', '367.20', '300.00', '0.00', '0.00 Km', '5300.0', '18', '408', 1, '', 1, 1, '0000-00-00'),
(29, 32, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '06:31:06 AM', '06:31:09 AM', '06:31:13 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(30, 33, '28.4121805', '77.0431357', '28.4121805', '77.0431357', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '06:53:07 AM', '06:53:08 AM', '06:53:13 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(31, 34, '28.4121849', '77.0431419', '28.412185', '77.043142', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:45:36 AM', '07:45:37 AM', '07:45:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 1, '0000-00-00'),
(32, 35, '28.4121868', '77.0431473', '28.4121868', '77.0431473', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:49:34 AM', '07:49:36 AM', '07:49:39 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(33, 36, '28.4121888', '77.0431529', '28.4121913', '77.0431595', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:54:20 AM', '07:54:21 AM', '07:54:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(34, 37, '6.4483731', '3.4288464', '6.4638053', '3.4221048', '8 Alfred Rewane Rd, Ikoyi, Lagos, Nigeria', 'Osborne Rd, Ikoyi, Lagos, Nigeria', '08:26:05 AM', '08:26:14 AM', '08:35:38 AM', '0', '0.00', '54.00', '0.00', '0.00', '0.00', 7, '354', '35.40', '318.60', '300.00', '0.00', '0.00 Km', '2820.0', '9', '354', 1, '', 0, 1, '0000-00-00'),
(35, 39, '28.412207', '77.0432542', '28.4122071', '77.043254', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:20:22 PM', '12:24:31 PM', '12:24:38 PM', '4', '40.00', '00.00', '0.00', '0.00', '0.00', 5, '140', '7.00', '133.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '140', 1, '', 1, 1, '0000-00-00'),
(36, 40, '6.4384771', '3.4352741', '6.4289663', '3.4341598', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '55 Adebisi Omotola Cl, Victoria Island, Lagos, Nigeria', '05:33:39 PM', '05:33:43 PM', '05:49:01 PM', '0', '0.00', '90.00', '0.00', '0.00', '0.00', 6, '572.65', '57.27', '515.38', '482.65', '0.00', '2.81 Km', '2130.0', '15', '572.65', 1, '', 1, 1, '0000-00-00'),
(37, 41, '', '', '', '', '', '', '08:18:40 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 6, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(38, 42, '', '', '', '', '', '', '08:21:19 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 6, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(39, 43, '6.4378618', '3.4711653', '6.4378618', '3.4711653', 'Bisola Durosinmi Etti Dr, Lekki Phase I, Lagos, Nigeria', 'Bisola Durosinmi Etti Dr, Lekki Phase I, Lagos, Nigeria', '02:34:48 PM', '02:35:02 PM', '02:43:35 PM', '0', '0.00', '48.00', '0.00', '0.00', '0.00', 6, '348', '34.80', '313.20', '300.00', '0.00', '0.00 Km', '0.0', '8', '348', 1, '', 1, 0, '0000-00-00'),
(40, 45, '', '', '', '', '', '', '02:08:46 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 6, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(41, 46, '6.4344371', '3.4412368', '6.4377945', '3.4310474', '46b Lasode Cres, Maroko, Lagos, Nigeria', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '05:38:47 PM', '05:39:42 PM', '06:02:17 PM', '0', '0.00', '132.00', '0.00', '0.00', '0.00', 6, '545.1', '54.51', '490.59', '413.10', '0.00', '1.74 Km', '1580.0', '22', '545.1', 1, '', 1, 1, '0000-00-00'),
(42, 47, '6.4345482', '3.4394899', '6.4377094', '3.4312482', 'FABAC CENTER, Ligali Ayorinde St, Victoria Island, Lagos, Nigeria', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '06:28:56 PM', '06:29:12 PM', '06:42:44 PM', '0', '0.00', '78.00', '0.00', '0.00', '0.00', 6, '502.15', '50.22', '451.93', '424.15', '0.00', '1.91 Km', '1640.0', '13', '502.15', 1, '', 0, 1, '0000-00-00'),
(43, 48, '', '', '', '', '', '', '01:22:55 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 6, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(44, 49, '6.4681603', '3.4469802', '6.4652082', '3.446206', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '102 Close, Banana Island, Lagos, Nigeria', '01:33:05 PM', '01:37:20 PM', '01:59:56 PM', '4', '0.00', '132.00', '0.00', '0.00', '0.00', 6, '432', '43.20', '388.80', '300.00', '0.00', '0.00 Km', '0.0', '22', '432', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(45, 50, '6.4378367', '3.431259', '6.4378367', '3.431259', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '09:41:31 PM', '09:41:38 PM', '09:41:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 0, '0000-00-00'),
(46, 51, '28.4122165', '77.0432378', '28.4122165', '77.043237', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:20:43 PM', '12:21:06 PM', '01:11:23 PM', '0', '0.00', '735.00', '0.00', '0.00', '0.00', 5, '835', '41.75', '793.25', '100.00', '0.00', '0.00 Miles', '0.0', '50', '835', 1, '', 1, 1, '0000-00-00'),
(47, 52, '6.4381147', '3.4710289', '6.4380109', '3.4711482', 'Bisola Durosinmi Etti Dr, Lekki Phase I, Lagos, Nigeria', 'Bisola Durosinmi Etti Dr, Lekki Phase I, Lagos, Nigeria', '12:55:24 PM', '12:55:44 PM', '01:05:39 PM', '0', '0.00', '54.00', '0.00', '0.00', '0.00', 6, '354', '35.40', '318.60', '300.00', '0.00', '0.00 Km', '0.0', '9', '354', 1, '', 1, 0, '0000-00-00'),
(48, 53, '', '', '', '', '', '', '01:15:56 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 5, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(49, 54, '28.4122031', '77.0432546', '28.4122115', '77.043244', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:17:46 PM', '01:24:42 PM', '01:26:00 PM', '6', '60.00', '00.00', '0.00', '0.00', '0.00', 5, '160', '8.00', '152.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '160', 1, '', 1, 1, '0000-00-00'),
(50, 55, '', '', '', '', '', '', '02:01:35 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 5, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(51, 56, '28.412181', '77.0432831', '28.412178', '77.0432868', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:24:35 PM', '12:56:45 PM', '01:12:14 PM', '27', '270.00', '210.00', '0.00', '0.00', '0.00', 5, '580', '29.00', '551.00', '100.00', '0.00', '0.00 Miles', '0.0', '15', '580', 1, '', 1, 1, '0000-00-00'),
(52, 57, '6.466953', '3.4456575', '6.466953', '3.4456575', '4th Ave, Banana Island, Lagos, Nigeria', '4th Ave, Banana Island, Lagos, Nigeria', '01:27:52 PM', '01:28:17 PM', '01:28:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 6, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 1, '0000-00-00'),
(53, 58, '28.4122143', '77.0432388', '28.4122145', '77.0432386', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:50:53 AM', '05:50:55 AM', '05:51:02 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(54, 59, '', '', '', '', '', '', '06:23:22 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 5, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(55, 60, '6.4680828', '3.4472403', '6.4652082', '3.446206', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '102 Close, Banana Island, Lagos, Nigeria', '04:20:07 PM', '04:20:22 PM', '04:31:22 PM', '0', '0.00', '66.00', '0.00', '0.00', '0.00', 9, '366', '36.60', '329.40', '300.00', '0.00', '0.00 Km', '0.0', '11', '366', 1, '', 1, 1, '0000-00-00'),
(56, 61, '6.4680002', '3.4471258', '6.4680002', '3.4471258', '401 Cl, Banana Island, Lagos, Nigeria', '401 Cl, Banana Island, Lagos, Nigeria', '04:57:58 PM', '05:02:20 PM', '05:02:54 PM', '4', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 0, 0, '0000-00-00'),
(57, 62, '6.4679228', '3.4470959', '6.4679228', '3.4470959', '401 Cl, Banana Island, Lagos, Nigeria', '401 Cl, Banana Island, Lagos, Nigeria', '05:06:29 PM', '05:07:13 PM', '05:07:48 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 1, '0000-00-00'),
(58, 63, '6.4679613', '3.4470953', '6.4680106', '3.4470893', '401 Cl, Banana Island, Lagos, Nigeria', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '05:10:01 PM', '05:10:12 PM', '05:10:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 0, '0000-00-00'),
(59, 64, '6.4393795', '3.4290334', '6.4377295', '3.4308764', 'Lekki - Epe Expy, Eti-Osa, Lagos, Nigeria', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '06:40:41 PM', '06:40:46 PM', '06:43:30 PM', '0', '0.00', '12.00', '0.00', '0.00', '0.00', 9, '312', '31.20', '280.80', '300.00', '0.00', '0.00 Km', '0.0', '2', '312', 1, '', 1, 1, '0000-00-00'),
(60, 65, '6.454944', '3.4387233', '6.4538384', '3.4336287', '18 Gerrard Road, Ikoyi, Lagos, Nigeria', '54 Glover Rd, Ikoyi, Lagos, Nigeria', '06:01:27 PM', '06:01:35 PM', '06:04:58 PM', '0', '0.00', '18.00', '0.00', '0.00', '0.00', 9, '318', '31.80', '286.20', '300.00', '0.00', '0.00 Km', '0.0', '3', '318', 1, '', 1, 0, '0000-00-00'),
(61, 66, '6.44916', '3.4289786', '6.4392463', '3.4277668', '21 Alfred Rewane Rd, Ikoyi, Lagos, Nigeria', 'Lekki - Epe Expy, Eti-Osa, Lagos, Nigeria', '06:07:39 PM', '06:08:43 PM', '06:17:17 PM', '1', '0.00', '48.00', '0.00', '0.00', '0.00', 9, '483.85', '48.39', '435.46', '435.85', '0.00', '2.09 Km', '2100.0', '8', '483.85', 1, '', 1, 1, '0000-00-00'),
(62, 67, '6.4344794', '3.4306902', '6.437665', '3.4311993', '32 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '06:20:21 PM', '06:20:27 PM', '06:24:52 PM', '0', '0.00', '24.00', '0.00', '0.00', '0.00', 9, '413.05', '41.31', '371.74', '389.05', '0.00', '1.37 Km', '1170.0', '4', '413.05', 1, '', 1, 1, '0000-00-00'),
(63, 68, '', '', '', '', '', '', '06:29:26 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 9, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(64, 77, '6.4679142', '3.4470159', '6.4679928', '3.4470173', '401 Cl, Banana Island, Lagos, Nigeria', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '02:24:14 PM', '02:25:15 PM', '02:25:41 PM', '1', '0.00', '00.00', '0.00', '0.00', '0.00', 9, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 0, '0000-00-00'),
(65, 78, '6.4680442', '3.4470705', '6.4683101', '3.4472093', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '401 Cl, Banana Island, Lagos, Nigeria', '02:28:05 PM', '02:28:46 PM', '02:31:14 PM', '0', '0.00', '12.00', '0.00', '0.00', '0.00', 9, '317.2', '31.72', '285.48', '305.20', '0.00', '0.08 Km', '110.0', '2', '317.2', 1, '', 1, 0, '0000-00-00'),
(66, 80, '28.4121063', '77.0433087', '28.4121114', '77.0433031', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:29:02 PM', '01:29:16 PM', '01:29:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(67, 81, '28.4121818', '77.0432368', '28.4121826', '77.043234', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:37:31 AM', '06:38:03 AM', '06:38:44 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(68, 82, '28.41211789359', '77.0432375076684', '28.4121660562497', '77.0432556700679', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:12:03 AM', '10:12:12 AM', '10:12:22 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(69, 83, '28.4121381330935', '77.0432243771039', '28.4121633740407', '77.0433429256799', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:03:25 PM', '01:03:43 PM', '01:04:05 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(70, 84, '28.4121179520249', '77.0432422776679', '28.4121252680237', '77.0433297282319', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:08:15 PM', '01:08:28 PM', '01:09:14 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(71, 85, '28.4120820053372', '77.0432454216627', '28.412164379869', '77.043208479953', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:10:10 PM', '01:10:35 PM', '01:10:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(72, 87, '28.4121164353829', '77.0432688296559', '28.4121164353829', '77.0432688296559', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:56:39 PM', '02:56:45 PM', '02:56:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(73, 88, '28.4123017', '77.0434396', '28.4123064', '77.0434343', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:56:32 PM', '01:56:37 PM', '01:56:42 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(74, 89, '28.4123017', '77.0434408', '28.4123029', '77.0434394', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '02:47:37 PM', '02:47:42 PM', '02:47:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(75, 92, '28.4122853', '77.0434596', '28.4123237', '77.0433922', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '05:17:27 AM', '05:17:30 AM', '05:18:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(76, 94, '28.412148454253', '77.0433286764445', '28.412148454253', '77.0433286764445', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:02:19 AM', '06:02:29 AM', '06:02:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(77, 95, '', '', '', '', '', '', '09:52:08 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 14, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(78, 98, '6.4676849', '3.4465643', '6.4677221', '3.4464323', '4th Ave, Banana Island, Lagos, Nigeria', '4th Ave, Banana Island, Lagos, Nigeria', '11:45:55 AM', '11:46:31 AM', '11:48:41 AM', '0', '0.00', '12.00', '0.00', '0.00', '0.00', 14, '312', '31.20', '280.80', '300.00', '0.00', '0.00 Km', '0.0', '2', '312', 1, '', 1, 1, '0000-00-00'),
(79, 99, '6.4685187', '3.4473429', '6.4685124', '3.4473364', '401 Cl, Banana Island, Lagos, Nigeria', '401 Cl, Banana Island, Lagos, Nigeria', '12:01:01 PM', '12:01:39 PM', '12:04:45 PM', '0', '0.00', '18.00', '0.00', '0.00', '0.00', 14, '318', '31.80', '286.20', '300.00', '0.00', '0.00 Km', '0.0', '3', '318', 1, '', 1, 1, '0000-00-00'),
(80, 112, '6.4679768', '3.4471033', '6.4685156', '3.44734', '401 Cl, Banana Island, Lagos, Nigeria', '401 Cl, Banana Island, Lagos, Nigeria', '03:21:10 PM', '03:22:03 PM', '03:27:03 PM', '0', '0.00', '30.00', '0.00', '0.00', '0.00', 14, '330', '33.00', '297.00', '300.00', '0.00', '0.00 Km', '0.0', '5', '330', 1, '', 1, 1, '0000-00-00'),
(81, 114, '28.4120581684352', '77.0432559577219', '28.4120581684352', '77.0432559577219', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:54:58 AM', '06:55:04 AM', '06:55:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(82, 115, '28.4120814271702', '77.0432917951749', '28.4120474523198', '77.0432547480585', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:52:12 AM', '07:52:22 AM', '07:52:32 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(83, 116, '28.4120635224373', '77.0432592160466', '28.4120672358348', '77.0432309275826', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:00:51 AM', '08:01:00 AM', '08:01:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(84, 118, '6.4378094', '3.431265', '6.4379698', '3.4312063', '66 Transit Village 4, Victoria Island, Lagos, Nigeria', '98 Transit Village 5, Victoria Island, Lagos, Nigeria', '10:01:33 PM', '10:02:12 PM', '10:03:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '300', '30.00', '270.00', '300.00', '0.00', '0.00 Km', '0.0', '0', '300', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `driver_category` int(255) NOT NULL DEFAULT '0',
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_model_year` varchar(255) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `account_holder_name` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `driver_category`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_model_year`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `account_holder_name`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 5, 'vikalp', 'vikalp@apporio.com', '4545454545', '', '123456', 'uA9fJ4Cfeu8Z3JOE', 0, '570', '0', '', 'FD4BAB82C587991E9DC3718F4113225A1BD3E2E9286F2229F6EC74E9FCC6EE5F', 1, '0', 3, 2, '', 'DL-10S-6788', 56, 'Thursday, Sep 14', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120918972874', '77.043212180321', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '14:41', 'Thursday, Sep 14, 2017', 6, 0, 0, 1, 0, 2, 2, 0, '2017-09-14', 1, '', '2017-09-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(2, 0, 4, 'vikalp', 'vikalp45@t.com', '3636363636', '', '123456', '2k9RTBve6HsJlq8H', 0, '96', '0', '', '879134A03966A5D0A5CE821897E44F953FC0F77AA58EE45000772A2A17307123', 1, '0', 2, 3, '', 'DL-10S-5667', 56, 'Monday, Sep 18', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121464887404', '77.0432659314419', '344, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:15', 'Friday, Sep 22, 2017', 1, 0, 1, 1, 0, 2, 2, 0, '2017-09-18', 1, '', '2017-09-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(3, 0, 4, 'Geoffrey Kwabena Oti', 'otikwabena@gmail.com', '0272633889', '', 'Siaburah1979', 'i8gUBuR0RdhPzKIJ', 0, '0', '0', '', 'B5E0E5A84D76F68521B7982A5A425FF6869EC1112FCDE80E256645DEF381F6E8', 1, '', 2, 3, '', 'GY183-13', 56, 'Monday, Sep 18', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '08:33', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-09-20', 0, '', '2017-09-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 3, 1, 3),
(4, 0, 5, 'dyhdhfhf', 'hdugufiggi34@t.com', '4343434343', '', '123456', 'XBoFFWbwXpqiBhza', 0, '0', '0', '', '', 0, '', 3, 2, '', 'DL-10S-5789', 56, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(5, 0, 5, 'Manish', 'mkg@gmail.com', '8285469066', '', '123456', 'xdCxOnLDKwxrcw2r', 0, '4617', '40', '', 'fgK49Kq4FVU:APA91bHTx5OgHX9KO9gf1jUCpPkTcZNWxR0JCRqITatkk7V75QrlFTA-YYw79p7oei-yXg-EvNskeAFQf3EMWX5_bJNxBSYKlTIIu4RjV3PCOFCTwz6s4Iaxj6kLc9LbhoPnQ-koPHnt', 2, '2.484375', 3, 31, '', 'qwert', 56, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123483', '77.0434147', '----', '10:39', 'Wednesday, Nov 1, 2017', 36, 4, 7, 2, 0, 1, 2, 0, '2017-09-19', 1, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(6, 0, 10, 'James ', 'Jamesbron2000@gmail.com', '0802222636', '', '010309', 'tdKfjhNImifMBr0u', 0, '3018.5', '0', '388.8', 'fJ1oksG4Qqw:APA91bGy8HsNdaKMWSzg847Mr0A2KP1vvU3OCNTb7ffLrVZIfhkzHbMiAOPoY9bZS41OzmL5ZfmsZE8tIaowr-skjc-Qo4z55m-M1FlU5VtzTpD41PD0fAc9TN7YYrDdjNk6wrpAeHnR', 2, '3', 3, 31, '', 'xyz1234', 120, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.4656755', '3.4474834', '', '09:05', 'Tuesday, Oct 10, 2017', 9, 1, 4, 1, 0, 1, 2, 0, '2017-10-09', 0, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 3, 1, 4),
(7, 0, 10, 'James', 'jamesbron2000@gmail.com', '08022226365', '', 'JH1415650', 'QNg7w55QGpqUyaLa', 0, '2352.86', '0', '', 'fOM2yIGyJ6E:APA91bE6WDKFEZg4rFyOC9OzmRgeIm2HPCp29sNwkPZnCgL4fdBo-CPmrb3fdnNYC1A9N1UmGobFUszytQv63qrdVPBMaDL2SSjb2A78kOnGyYtsH6vG3JInMWnPK_7Sy4vWpp-zzjTw', 2, '3.91666666667', 3, 31, '', 'xyz123', 120, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.4636386', '3.4238141', '', '08:35', 'Thursday, Sep 21, 2017', 6, 0, 0, 1, 0, 1, 2, 0, '2017-09-20', 1, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(8, 0, 12, 'Test ', 'test@demo.com', '23232434', '', '123', '', 0, '0', '0', '', '', 0, '', 2, 3, '', '2321', 56, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(10, 0, 6, 'hdhdhddh', 'yeururtytu46@y.com', '1212121212', '', '123456', 'N74W3LyKFrFGFBei', 0, '1003.92', '64.08', '', '674DD43F17B72905EEB190E8BFC94BEB5F4BFA1A2534B1414DE722CA96FC5203', 1, '0', 4, 11, '', 'DL-10S-7655', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4149754661785', '77.041796631111', '506-727, Mahashay Hansram Marg, Block S, Sispal Vihar, Sector 47 , Gurugram, Haryana 122004', '06:55', 'Wednesday, Nov 1, 2017', 6, 0, 0, 2, 0, 2, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(9, 0, 10, 'xxx', 'jamesbron2000@yahoo.com', '080222263656', '', 'TRUST123', 'ShYOZBLlR4tyu6nZ', 0, '3069.08', '183.22', '', 'f57wk_LJce8:APA91bGwBtCPLUO2YQzVUoSYG-p9Yvr3Yg9lJHQKFHAqCGqaq8OKSDsmwNCIYezjW7VA07mzeD2Sn9VD8ZKwunDHyUzhfAaUNV-dw5gxnC6zTPR4JawmUb1hf8Ufp2kA0nzxnSr8tJT7', 2, '3.42857142857', 3, 31, '', 'xxxh12344', 120, 'Wednesday, Oct 11', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.4683101', '3.4472093', '----', '05:19', 'Wednesday, Oct 18, 2017', 10, 0, 1, 1, 0, 1, 2, 0, '2017-10-11', 1, '', '2017-10-11', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 4),
(11, 0, 5, 'sdfdsfsdf sdfdsfdsfds', 'safsadsads@idsnijdsnids.com', '+91875865765675765', '', 'ygyugyuyuyu', '1wAcaXesWH', 1, '0', '0', '', '', 0, '', 3, 2, '', '231321', 56, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-25', 1, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(13, 0, 10, 'James K', 'hugotaxitech@gmail.com', '080222263658', '', 'TRUST123', 'r2FdtMchxXNhdfAo', 0, '0', '0', '', '', 0, '', 3, 31, '', 'xyz123', 120, 'Tuesday, Oct 31', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-10-31', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(12, 0, 10, 'James kwasi', 'jamesbron2000@yahoo.com', '+2348022226365', '', 'TRUST123', 'NdElZ6wPSQ', 1, '0', '0', '', '', 0, '', 3, 31, '', '0000000000000000', 120, 'Thursday, Oct 26', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-31', 1, '', '2017-10-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 4),
(14, 0, 10, 'James hugo', 'Hugotaxitest2@gmail.com', '080222263650', 'uploads/driver/1509443286driver_14.jpg', 'TRUST1', 'lV4CCb92wnzPhgQk', 0, '1134', '126', '', 'dUsESFY1Ujc:APA91bEUQbWHrBSc5mi6dLD01D2eTw2rX0S03yh0d2kR7-Z1kMbi9S9dqitmT92Vnc2Nnv_-ZVf_IxAb7BtqJvy2jbpOHRDrN7fGzo9O92hXYHIx0QppKhJFDCyOE1txrgoVHxEL_thJ', 2, '4.33333333333', 3, 31, '', 'www1234', 120, 'Tuesday, Oct 31', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.4379038', '3.4312369', '----', '22:17', 'Wednesday, Nov 1, 2017', 4, 2, 2, 1, 0, 1, 2, 0, '2017-10-31', 1, '', '2017-10-31', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 4),
(15, 0, 5, 'cjh', 'fgh@gmail.com', '8688788888855', '', '123456', 'Iia9WYLLqPAVYhOJ', 0, '0', '0', '', '', 0, '', 3, 31, '', 'gbb', 56, 'Wednesday, Nov 1', '', '', '', '', '', '', '', 'fyh', '', 'gujtuu', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(16, 0, 10, 'kofi nti2', 'hugotaxitest2@gmail.com', '+2347088804522', '', 'TRUST123', 'nWpR4EJn9A', 1, '0', '0', '', '', 0, '', 4, 11, '', '0000 0000 0000 0000', 120, 'Wednesday, Nov 1', '', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '600', 6, '570', '30', '2017-09-14'),
(2, 2, '100', 1, '96.00', '4.00', '2017-09-18'),
(3, 5, '1745', 17, '1657.75', '87.25', '2017-09-19'),
(4, 7, '2260.3', 5, '2034.26', '226.04', '2017-09-20'),
(5, 5, '740', 7, '703', '37', '2017-09-21'),
(6, 7, '354', 1, '318.60', '35.40', '2017-09-21'),
(7, 6, '572.65', 1, '515.38', '57.27', '2017-09-23'),
(8, 6, '348', 1, '313.20', '34.80', '2017-09-25'),
(9, 6, '1047.25', 2, '942.52', '104.73', '2017-09-30'),
(10, 6, '432', 1, '388.80', '-388.8', '2017-10-03'),
(11, 6, '300', 1, '270.00', '30.00', '2017-10-04'),
(12, 6, '354', 1, '318.60', '35.40', '2017-10-05'),
(13, 6, '354', 1, '318.60', '35.40', '2017-10-05'),
(14, 5, '995', 2, '945.25', '49.75', '2017-10-05'),
(15, 5, '580', 1, '551.00', '29.00', '2017-10-06'),
(16, 6, '300', 1, '270.00', '30.00', '2017-10-09'),
(17, 5, '100', 1, '95.00', '5.00', '2017-10-11'),
(18, 9, '1578', 5, '1420.2', '157.8', '2017-10-11'),
(19, 9, '1214.9', 3, '1093.4', '121.5', '2017-10-12'),
(20, 9, '617.2', 2, '555.48', '61.72', '2017-10-18'),
(21, 5, '100', 1, '95.00', '5.00', '2017-10-23'),
(22, 5, '100', 1, '95.00', '5.00', '2017-10-24'),
(23, 10, '890', 5, '836.6', '53.4', '2017-10-24'),
(24, 5, '200', 2, '190', '10', '2017-10-25'),
(25, 5, '100', 1, '95.00', '5.00', '2017-10-31'),
(26, 10, '178', 1, '167.32', '10.68', '2017-10-31'),
(27, 14, '960', 3, '864', '96', '2017-10-31'),
(28, 5, '300', 3, '285', '15', '2017-11-01'),
(29, 14, '300', 1, '270.00', '30.00', '2017-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`, `ride_status`) VALUES
(1, 1, 92, 1, 0),
(2, 2, 10, 1, 1),
(3, 5, 116, 1, 1),
(4, 7, 126, 1, 1),
(5, 6, 126, 1, 1),
(6, 9, 126, 1, 1),
(7, 10, 94, 1, 1),
(8, 14, 126, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 3, 1, 'Monday', '11:35', '11:35', '12:35', '10:35', 1, '23'),
(18, 56, 1, 'Monday', '17:19', '18:20', '17:20', '18:20', 1, '55'),
(22, 56, 2, 'Night', '17:00', '23.45', '', '', 1, '99'),
(32, 112, 1, 'Sunday', '07:30', '09:30', '14:30', '18:0', 2, '1.2'),
(33, 112, 2, 'Night', '23:30', '06:30', '', '', 2, '1.5'),
(34, 3, 1, 'Wednesday', '14:28', '23:45', '14:29', '23:59', 1, '10'),
(37, 56, 1, 'Sunday', '08:30', '09:30', '10:30', '11:30', 2, '23'),
(38, 56, 1, 'Saturday', '12:31', '12:31', '12:31', '12:31', 2, '20'),
(39, 79, 1, 'Monday', '16:19', '20:19', '22:19', '01:19', 2, '10'),
(40, 56, 1, 'Thursday', '22:30', '23:45', '23:45', '23:45', 1, '100'),
(41, 56, 1, 'Tuesday', '09:13 PM', '10:14 PM', '10:14 PM', '11:14 PM', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 4, '', '6.44419087493648', '3.43552988022566', '6 Mekunwen Road\nIkoyi, Lagos', '6.43029634144599', '3.44262432307005', '9 T.Y Danjuma. Street\nMaroko, Lagos', '', '10:50:43', '10:50:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.44419087493648,3.43552988022566&markers=color:red|label:D|6.43029634144599,3.44262432307005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(2, 4, '', '6.44419087493648', '3.43552988022566', '6 Mekunwen Road\nIkoyi, Lagos', '6.43029634144599', '3.44262432307005', '9 T.Y Danjuma. Street\nMaroko, Lagos', '', '10:58:43', '10:58:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.44419087493648,3.43552988022566&markers=color:red|label:D|6.43029634144599,3.44262432307005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 4, 0, 1),
(3, 4, '', '6.43053422252171', '3.4336245059967', '16 Molade Okoya Thomas Street\nVictoria Island, Lagos', '6.43029634144599', '3.44262432307005', '9 T.Y Danjuma. Street\nMaroko, Lagos', 'Tuesday, Sep 19', '11:02:48', '11:02:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43053422252171,3.4336245059967&markers=color:red|label:D|6.43029634144599,3.44262432307005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 4, 0, 1),
(4, 4, '', '6.43053422252171', '3.4336245059967', '16 Molade Okoya Thomas Street\nVictoria Island, Lagos', '6.43029634144599', '3.44262432307005', '9 T.Y Danjuma. Street\nMaroko, Lagos', 'Tuesday, Sep 19', '11:03:13', '11:03:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43053422252171,3.4336245059967&markers=color:red|label:D|6.43029634144599,3.44262432307005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(5, 4, '', '6.4385621437839795', '3.435406498610973', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Wednesday, Sep 20', '16:29:08', '04:29:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4385621437839795,3.435406498610973&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(6, 4, '', '6.4385621437839795', '3.435406498610973', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Wednesday, Sep 20', '16:29:44', '04:29:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4385621437839795,3.435406498610973&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(7, 3, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '08:50:40', '08:50:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(8, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:23:23', '08:23:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(9, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:23:45', '08:23:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(10, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:24:03', '08:24:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(11, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:24:24', '08:24:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(12, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:24:41', '08:24:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(13, 4, '', '6.43141931509632', '3.42858874755742', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:24:48', '08:24:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43141931509632,3.42858874755742&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(14, 6, '', '6.439379054953541', '3.4307485073804855', '1 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Friday, Oct 6', '14:04:31', '02:04:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.439379054953541,3.4307485073804855&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(15, 4, '', '6.437604636930831', '3.4310686960816383', '65 Transit Village 4, Victoria Island, Lagos, Nigeria', '', '', 'Set your drop point', 'Saturday, Oct 14', '13:44:44', '01:44:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.437604636930831,3.4310686960816383&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(16, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '17:56:00', '05:56:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '17:56:00', 0, 4, 1, 1, 0, 1, 0, 1),
(17, 4, '', '6.466666999999999', '0.0000000', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '11:33:15', '11:33:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,0.0000000&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '11:33:15', 0, 3, 1, 1, 0, 1, 1, 1),
(18, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '12:23:55', '12:23:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '12:23:55', 0, 4, 1, 1, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(2, 2, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(3, 3, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(4, 4, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(5, 5, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(6, 6, 1, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(7, 7, 1, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(8, 8, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(9, 9, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(10, 10, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(11, 11, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(12, 12, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(13, 13, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(14, 13, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(15, 13, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(16, 15, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(17, 16, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(18, 17, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(19, 18, 3, '1', 'Cash', 'Admin', '130', 'Tuesday, Sep 19', '1'),
(20, 19, 3, '1', 'Cash', 'Admin', '115', 'Tuesday, Sep 19', '1'),
(21, 20, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(22, 21, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(23, 22, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(24, 23, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 19', '1'),
(25, 24, 4, '1', 'Cash', 'Admin', '70', 'Wednesday, Sep 20', '1'),
(26, 25, 4, '1', 'Cash', 'Admin', '424.25', 'Wednesday, Sep 20', '1'),
(27, 26, 4, '1', 'Cash', 'Admin', '342.85', 'Wednesday, Sep 20', '1'),
(28, 27, 4, '1', 'Cash', 'Admin', '1015.2', 'Wednesday, Sep 20', '1'),
(29, 28, 4, '1', 'Cash', 'Admin', '408', 'Wednesday, Sep 20', '1'),
(30, 29, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(31, 30, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(32, 31, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(33, 31, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(34, 32, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(35, 33, 3, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 21', '1'),
(36, 34, 4, '1', 'Cash', 'Admin', '354', 'Thursday, Sep 21', '1'),
(37, 35, 3, '1', 'Cash', 'Admin', '140', 'Thursday, Sep 21', '1'),
(38, 36, 4, '1', 'Cash', 'Admin', '572.65', 'Saturday, Sep 23', '1'),
(39, 39, 6, '1', 'Cash', 'Admin', '348', 'Monday, Sep 25', '1'),
(40, 41, 6, '1', 'Cash', 'Admin', '545.1', 'Saturday, Sep 30', '1'),
(41, 42, 6, '1', 'Cash', 'Admin', '502.15', 'Saturday, Sep 30', '1'),
(42, 44, 6, '3', 'Credit Card', 'Stripe', '432', 'Tuesday, Oct 3', 'Payment complete.'),
(43, 45, 6, '1', 'Cash', 'Admin', '300', 'Wednesday, Oct 4', '1'),
(44, 47, 6, '1', 'Cash', 'Admin', '354', 'Thursday, Oct 5', '1'),
(45, 47, 6, '1', 'Cash', 'Admin', '354', 'Thursday, Oct 5', '1'),
(46, 46, 3, '1', 'Cash', 'Admin', '835', 'Thursday, Oct 5', '1'),
(47, 49, 3, '1', 'Cash', 'Admin', '160', 'Thursday, Oct 5', '1'),
(48, 51, 3, '1', 'Cash', 'Admin', '580', 'Friday, Oct 6', '1'),
(49, 52, 6, '1', 'Cash', 'Admin', '300', 'Monday, Oct 9', '1'),
(50, 53, 3, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 11', '1'),
(51, 55, 4, '1', 'Cash', 'Admin', '366', 'Wednesday, Oct 11', '1'),
(52, 56, 4, '1', 'Cash', 'Admin', '300', 'Wednesday, Oct 11', '1'),
(53, 57, 4, '1', 'Cash', 'Admin', '300', 'Wednesday, Oct 11', '1'),
(54, 58, 4, '1', 'Cash', 'Admin', '300', 'Wednesday, Oct 11', '1'),
(55, 59, 4, '1', 'Cash', 'Admin', '312', 'Wednesday, Oct 11', '1'),
(56, 60, 4, '1', 'Cash', 'Admin', '318', 'Thursday, Oct 12', '1'),
(57, 61, 4, '1', 'Cash', 'Admin', '483.85', 'Thursday, Oct 12', '1'),
(58, 62, 4, '1', 'Cash', 'Admin', '413.05', 'Thursday, Oct 12', '1'),
(59, 64, 8, '1', 'Cash', 'Admin', '300', 'Wednesday, Oct 18', '1'),
(60, 65, 8, '1', 'Cash', 'Admin', '317.2', 'Wednesday, Oct 18', '1'),
(61, 66, 9, '1', 'Cash', 'Admin', '100', 'Monday, Oct 23', '1'),
(62, 67, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 24', '1'),
(63, 68, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 24', '1'),
(64, 69, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 24', '1'),
(65, 70, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 24', '1'),
(66, 71, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 24', '1'),
(67, 72, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 24', '1'),
(68, 73, 3, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 25', '1'),
(69, 74, 3, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 25', '1'),
(70, 75, 3, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 31', '1'),
(71, 76, 10, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 31', '1'),
(72, 78, 4, '1', 'Cash', 'Admin', '312', 'Tuesday, Oct 31', '1'),
(73, 79, 8, '1', 'Cash', 'Admin', '318', 'Tuesday, Oct 31', '1'),
(74, 80, 8, '1', 'Cash', 'Admin', '330', 'Tuesday, Oct 31', '1'),
(75, 81, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 1', '1'),
(76, 82, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 1', '1'),
(77, 83, 10, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 1', '1'),
(78, 84, 4, '1', 'Cash', 'Admin', '300', 'Wednesday, Nov 1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 2),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(48, 120, 'Km', 'â‚¦', 28, 10, 0, 0, 0, 0, 0, 0, '3', '90', '10', '0', '0', '0', '0'),
(12, 56, 'Miles', '&#8377', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '&#8377', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '&#8377', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(43, 120, 'Km', 'â‚¦', 2, 10, 0, 0, 0, 0, 0, 0, '3', '90', '10', '0', '0', '0', '0'),
(44, 120, 'Km', 'â‚¦', 3, 10, 0, 0, 0, 0, 0, 0, '0', '300', '65', '0', '0', '0', '6'),
(45, 121, 'Km', '$', 2, 10, 0, 0, 0, 0, 0, 0, '300', '65', '0', '0', '0', '0', '6'),
(46, 121, 'Km', '$', 3, 10, 0, 0, 0, 0, 0, 0, '300', '65', '0', '0', '0', '0', '6'),
(47, 120, 'Km', 'â‚¦', 27, 10, 0, 0, 0, 0, 0, 0, '5', '80', '10', '0', '0', '0', '0'),
(51, 121, 'Km', '$', 28, 10, 0, 0, 0, 0, 0, 0, '2', '70', '10', '0', '0', '0', '0'),
(50, 121, 'Km', '$', 27, 10, 0, 0, 0, 0, 0, 0, '3', '80', '10', '0', '0', '0', '0'),
(49, 120, 'Km', 'â‚¦', 4, 10, 0, 0, 0, 0, 0, 0, '4', '50', '10', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Fare Reduction ', 'Testing', 'uploads/notification/push_image_1.jpg', '', 0, 0, '2017-09-21', 1),
(2, 'James_single', 'test', 'uploads/notification/1500380956793.jpg', '', 0, 6, '2017-09-21', 2),
(3, 'James_single2', 'Testing', 'uploads/notification/1500380956793.jpg', '', 0, 6, '2017-09-21', 2),
(4, 'James_single', 'testing', 'uploads/notification/1500380956793.jpg', '', 4, 0, '2017-09-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_distance_unit`, `rental_category_description`, `rental_category_admin_status`) VALUES
(1, '247', '24', '10', 'Km', 'Rental Package testing <span style=\"font-family: Arial;\">Testing</span><br>', 1),
(2, 'Testing #1', '5', '100', 'Km', 'Car rental for everyone<br>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(1, 120, 28, 2, '400', 1000, 70, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 107, 1, 0, '2017-09-14'),
(2, 107, 1, 0, '2017-09-14'),
(3, 107, 1, 0, '2017-09-14'),
(4, 107, 1, 0, '2017-09-14'),
(5, 107, 1, 0, '2017-09-14'),
(6, 107, 1, 0, '2017-09-14'),
(7, 107, 1, 0, '2017-09-14'),
(8, 107, 1, 0, '2017-09-14'),
(9, 9, 2, 1, '2017-09-18'),
(10, 10, 2, 1, '2017-09-19'),
(11, 107, 1, 0, '2017-09-19'),
(12, 110, 5, 0, '2017-09-19'),
(13, 107, 1, 0, '2017-09-19'),
(14, 110, 5, 0, '2017-09-19'),
(15, 107, 1, 0, '2017-09-19'),
(16, 110, 5, 0, '2017-09-19'),
(17, 107, 1, 0, '2017-09-19'),
(18, 110, 5, 0, '2017-09-19'),
(19, 107, 1, 0, '2017-09-19'),
(20, 110, 5, 0, '2017-09-19'),
(21, 107, 1, 0, '2017-09-19'),
(22, 110, 5, 0, '2017-09-19'),
(23, 107, 1, 0, '2017-09-19'),
(24, 110, 5, 0, '2017-09-19'),
(25, 107, 1, 0, '2017-09-19'),
(26, 110, 5, 0, '2017-09-19'),
(27, 107, 1, 0, '2017-09-19'),
(28, 110, 5, 0, '2017-09-19'),
(29, 107, 1, 0, '2017-09-19'),
(30, 110, 5, 0, '2017-09-19'),
(31, 107, 1, 0, '2017-09-19'),
(32, 110, 5, 0, '2017-09-19'),
(33, 107, 1, 0, '2017-09-19'),
(34, 110, 5, 0, '2017-09-19'),
(35, 107, 1, 0, '2017-09-19'),
(36, 110, 5, 0, '2017-09-19'),
(37, 107, 1, 0, '2017-09-19'),
(38, 110, 5, 0, '2017-09-19'),
(39, 107, 1, 0, '2017-09-19'),
(40, 110, 5, 0, '2017-09-19'),
(41, 105, 7, 0, '2017-09-20'),
(42, 105, 7, 0, '2017-09-20'),
(43, 105, 7, 0, '2017-09-20'),
(44, 105, 7, 0, '2017-09-20'),
(45, 105, 7, 0, '2017-09-20'),
(46, 107, 1, 0, '2017-09-21'),
(47, 110, 5, 0, '2017-09-21'),
(48, 107, 1, 0, '2017-09-21'),
(49, 110, 5, 0, '2017-09-21'),
(50, 107, 1, 0, '2017-09-21'),
(51, 110, 5, 0, '2017-09-21'),
(52, 107, 1, 0, '2017-09-21'),
(53, 110, 5, 0, '2017-09-21'),
(54, 107, 1, 0, '2017-09-21'),
(55, 110, 5, 0, '2017-09-21'),
(56, 105, 7, 0, '2017-09-21'),
(57, 107, 1, 0, '2017-09-21'),
(58, 110, 5, 0, '2017-09-21'),
(59, 107, 1, 0, '2017-09-21'),
(60, 110, 5, 0, '2017-09-21'),
(61, 124, 6, 0, '2017-09-23'),
(62, 105, 7, 0, '2017-09-23'),
(63, 124, 6, 0, '2017-09-23'),
(64, 105, 7, 0, '2017-09-23'),
(65, 124, 6, 0, '2017-09-23'),
(66, 105, 7, 0, '2017-09-23'),
(67, 124, 6, 0, '2017-09-25'),
(68, 105, 7, 0, '2017-09-25'),
(69, 124, 6, 0, '2017-09-29'),
(70, 105, 7, 0, '2017-09-29'),
(71, 105, 7, 0, '0000-00-00'),
(72, 124, 6, 0, '2017-09-29'),
(73, 105, 7, 0, '2017-09-29'),
(74, 124, 6, 0, '2017-09-30'),
(75, 105, 7, 0, '2017-09-30'),
(76, 124, 6, 0, '2017-09-30'),
(77, 105, 7, 0, '2017-09-30'),
(78, 124, 6, 0, '0000-00-00'),
(79, 124, 6, 0, '2017-10-03'),
(80, 105, 7, 0, '2017-10-03'),
(81, 124, 6, 0, '2017-10-04'),
(82, 105, 7, 0, '2017-10-04'),
(83, 107, 1, 0, '2017-10-05'),
(84, 110, 5, 0, '2017-10-05'),
(85, 124, 6, 0, '2017-10-05'),
(86, 105, 7, 0, '2017-10-05'),
(87, 107, 1, 0, '2017-10-05'),
(88, 110, 5, 0, '2017-10-05'),
(89, 107, 1, 0, '2017-10-05'),
(90, 110, 5, 0, '2017-10-05'),
(91, 107, 1, 0, '2017-10-05'),
(92, 110, 5, 0, '2017-10-05'),
(93, 107, 1, 0, '2017-10-05'),
(94, 110, 5, 0, '2017-10-05'),
(95, 124, 6, 0, '2017-10-06'),
(96, 105, 7, 0, '2017-10-06'),
(97, 107, 1, 0, '2017-10-11'),
(98, 110, 5, 0, '2017-10-11'),
(99, 107, 1, 0, '2017-10-11'),
(100, 110, 5, 0, '2017-10-11'),
(101, 124, 6, 0, '2017-10-11'),
(102, 105, 7, 0, '2017-10-11'),
(103, 105, 9, 0, '2017-10-11'),
(104, 124, 6, 0, '2017-10-11'),
(105, 105, 7, 0, '2017-10-11'),
(106, 105, 9, 0, '2017-10-11'),
(107, 124, 6, 0, '2017-10-11'),
(108, 105, 7, 0, '2017-10-11'),
(109, 105, 9, 0, '2017-10-11'),
(110, 124, 6, 0, '2017-10-11'),
(111, 105, 7, 0, '2017-10-11'),
(112, 105, 9, 0, '2017-10-11'),
(113, 124, 6, 0, '2017-10-11'),
(114, 105, 7, 0, '2017-10-11'),
(115, 105, 9, 0, '2017-10-11'),
(116, 124, 6, 0, '2017-10-12'),
(117, 105, 7, 0, '2017-10-12'),
(118, 105, 9, 0, '2017-10-12'),
(119, 124, 6, 0, '2017-10-12'),
(120, 105, 7, 0, '2017-10-12'),
(121, 105, 9, 0, '2017-10-12'),
(122, 124, 6, 0, '2017-10-12'),
(123, 105, 7, 0, '2017-10-12'),
(124, 105, 9, 0, '2017-10-12'),
(125, 124, 6, 0, '2017-10-12'),
(126, 105, 7, 0, '2017-10-12'),
(127, 105, 9, 0, '2017-10-12'),
(128, 107, 1, 0, '2017-10-16'),
(129, 110, 5, 0, '2017-10-16'),
(130, 107, 1, 0, '2017-10-16'),
(131, 110, 5, 0, '2017-10-16'),
(132, 107, 1, 0, '2017-10-16'),
(133, 110, 5, 0, '2017-10-16'),
(134, 107, 1, 0, '2017-10-16'),
(135, 110, 5, 0, '2017-10-16'),
(136, 107, 1, 0, '2017-10-16'),
(137, 110, 5, 0, '2017-10-16'),
(138, 107, 1, 0, '2017-10-16'),
(139, 110, 5, 0, '2017-10-16'),
(140, 124, 6, 0, '2017-10-17'),
(141, 105, 7, 0, '2017-10-17'),
(142, 105, 9, 0, '2017-10-17'),
(143, 124, 6, 0, '2017-10-18'),
(144, 105, 7, 0, '2017-10-18'),
(145, 105, 9, 0, '2017-10-18'),
(146, 124, 6, 0, '2017-10-18'),
(147, 105, 7, 0, '2017-10-18'),
(148, 105, 9, 0, '2017-10-18'),
(149, 110, 5, 0, '0000-00-00'),
(150, 107, 1, 0, '2017-10-23'),
(151, 110, 5, 0, '2017-10-23'),
(152, 107, 1, 0, '2017-10-23'),
(153, 110, 5, 0, '2017-10-23'),
(154, 107, 1, 0, '2017-10-24'),
(155, 110, 5, 0, '2017-10-24'),
(156, 82, 10, 1, '2017-10-24'),
(157, 83, 10, 1, '2017-10-24'),
(158, 84, 10, 1, '2017-10-24'),
(159, 85, 10, 1, '2017-10-24'),
(160, 86, 10, 0, '2017-10-24'),
(161, 87, 10, 1, '2017-10-24'),
(162, 107, 1, 0, '2017-10-25'),
(163, 110, 5, 0, '2017-10-25'),
(164, 107, 1, 0, '2017-10-25'),
(165, 110, 5, 0, '2017-10-25'),
(166, 124, 6, 0, '2017-10-26'),
(167, 105, 7, 0, '2017-10-26'),
(168, 105, 9, 0, '2017-10-26'),
(169, 124, 6, 0, '2017-10-29'),
(170, 105, 7, 0, '2017-10-29'),
(171, 105, 9, 0, '2017-10-29'),
(172, 107, 1, 0, '2017-10-31'),
(173, 110, 5, 0, '2017-10-31'),
(174, 93, 10, 0, '2017-10-31'),
(175, 94, 10, 1, '2017-10-31'),
(176, 124, 6, 0, '2017-10-31'),
(177, 105, 7, 0, '2017-10-31'),
(178, 105, 9, 0, '2017-10-31'),
(179, 122, 14, 1, '2017-10-31'),
(180, 124, 6, 0, '2017-10-31'),
(181, 105, 7, 0, '2017-10-31'),
(182, 105, 9, 0, '2017-10-31'),
(183, 122, 14, 1, '2017-10-31'),
(184, 124, 6, 0, '2017-10-31'),
(185, 105, 7, 0, '2017-10-31'),
(186, 105, 9, 0, '2017-10-31'),
(187, 122, 14, 1, '2017-10-31'),
(188, 124, 6, 0, '2017-10-31'),
(189, 105, 7, 0, '2017-10-31'),
(190, 105, 9, 0, '2017-10-31'),
(191, 122, 14, 1, '2017-10-31'),
(192, 124, 6, 0, '2017-10-31'),
(193, 105, 7, 0, '2017-10-31'),
(194, 105, 9, 0, '2017-10-31'),
(195, 122, 14, 1, '2017-10-31'),
(196, 124, 6, 0, '2017-10-31'),
(197, 105, 7, 0, '2017-10-31'),
(198, 105, 9, 0, '2017-10-31'),
(199, 122, 14, 1, '2017-10-31'),
(200, 124, 6, 0, '2017-10-31'),
(201, 105, 7, 0, '2017-10-31'),
(202, 105, 9, 0, '2017-10-31'),
(203, 122, 14, 1, '2017-10-31'),
(204, 124, 6, 0, '2017-10-31'),
(205, 105, 7, 0, '2017-10-31'),
(206, 105, 9, 0, '2017-10-31'),
(207, 122, 14, 1, '2017-10-31'),
(208, 124, 6, 0, '2017-10-31'),
(209, 105, 7, 0, '2017-10-31'),
(210, 105, 9, 0, '2017-10-31'),
(211, 122, 14, 1, '2017-10-31'),
(212, 124, 6, 0, '2017-10-31'),
(213, 105, 7, 0, '2017-10-31'),
(214, 105, 9, 0, '2017-10-31'),
(215, 122, 14, 1, '2017-10-31'),
(216, 124, 6, 0, '2017-10-31'),
(217, 105, 7, 0, '2017-10-31'),
(218, 105, 9, 0, '2017-10-31'),
(219, 122, 14, 1, '2017-10-31'),
(220, 107, 1, 0, '2017-10-31'),
(221, 110, 5, 0, '2017-10-31'),
(222, 110, 5, 0, '0000-00-00'),
(223, 107, 1, 0, '2017-10-31'),
(224, 110, 5, 0, '2017-10-31'),
(225, 124, 6, 0, '2017-10-31'),
(226, 124, 6, 0, '2017-10-31'),
(227, 112, 7, 0, '2017-10-31'),
(228, 112, 9, 0, '2017-10-31'),
(229, 122, 14, 1, '2017-10-31'),
(230, 124, 6, 0, '2017-10-31'),
(231, 114, 5, 1, '2017-11-01'),
(232, 115, 5, 1, '2017-11-01'),
(233, 116, 5, 1, '2017-11-01'),
(234, 122, 14, 1, '2017-11-01'),
(235, 122, 14, 1, '2017-11-01'),
(236, 122, 14, 1, '2017-11-01'),
(237, 122, 14, 1, '0000-00-00'),
(238, 122, 14, 1, '2017-11-01'),
(239, 124, 6, 0, '2017-11-09'),
(240, 125, 6, 0, '2017-11-10'),
(241, 125, 7, 0, '2017-11-10'),
(242, 125, 9, 0, '2017-11-10'),
(243, 125, 14, 0, '2017-11-10'),
(244, 126, 6, 0, '2017-11-10'),
(245, 126, 7, 0, '2017-11-10'),
(246, 126, 9, 0, '2017-11-10'),
(247, 126, 14, 0, '2017-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 44, 6),
(2, 75, 5),
(3, 75, 5),
(4, 106, 5),
(5, 110, 5),
(6, 119, 14),
(7, 119, 14);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.4121003840383', '77.0432518143924', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4354513021912', '77.0522378012538', '997, Sector 34, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '12:03:43', '12:04:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121003840383,77.0432518143924&markers=color:red|label:D|28.4354513021912,77.0522378012538&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(2, 1, '', '28.4121132165236', '77.0432715117931', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4305844901941', '77.0549863949418', '1318, Satpaul Mittal Marg, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '12:07:23', '12:07:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121132165236,77.0432715117931&markers=color:red|label:D|28.4305844901941,77.0549863949418&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(3, 1, '', '28.4121146909808', '77.0432239025831', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4034070734675', '77.0391643792391', 'Dhani, Sector 72\nGurugram, Haryana', 'Thursday, Sep 14', '12:10:00', '12:10:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121146909808,77.0432239025831&markers=color:red|label:D|28.4034070734675,77.0391643792391&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(4, 1, '', '28.4120523955298', '77.043259091789', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4514364685565', '77.0601345598698', '475, Dalal Street, Urban Estate, Sector 40\nGurugram, Haryana 122022', 'Thursday, Sep 14', '12:21:04', '12:21:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120523955298,77.043259091789&markers=color:red|label:D|28.4514364685565,77.0601345598698&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(5, 1, '', '28.4120849158436', '77.0432480654428', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 14', '12:26:32', '12:27:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120849158436,77.0432480654428&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(6, 1, '', '28.4120592513751', '77.0432855933905', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4218067393763', '77.0480632781982', '7, Pine Drive, Malibu Town, Sector 47\nGurugram, Haryana 122018', 'Thursday, Sep 14', '12:42:46', '12:43:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120592513751,77.0432855933905&markers=color:red|label:D|28.4218067393763,77.0480632781982&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(7, 1, '', '28.4120752802383', '77.0432111621621', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4173721800043', '77.0474168658257', 'Gali Number 2, Sector 46, South City II, Sector 49\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:14:06', '02:14:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120752802383,77.0432111621621&markers=color:red|label:D|28.4173721800043,77.0474168658257&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(8, 1, '', '28.4120545331095', '77.0431836694479', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4254343945828', '77.0505074411631', 'Crest Wood, Malibu Town, Sector 47\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:39:47', '02:41:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120545331095,77.0431836694479&markers=color:red|label:D|28.4254343945828,77.0505074411631&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(9, 1, '', '28.4120857916155', '77.0432490482926', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4833420867221', '77.0584829896688', '258, Sukhrali Enclave, Sector 17\nGurugram, Haryana 122001', 'Monday, Sep 18', '11:09:34', '11:10:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120857916155,77.0432490482926&markers=color:red|label:D|28.4833420867221,77.0584829896688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(10, 1, '', '28.4121282559863', '77.0430237427354', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4450418664655', '77.0627158507705', '31, Block B, Greenwood City, Sector 45\nGurugram, Haryana 122022', 'Tuesday, Sep 19', '07:16:13', '07:16:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121282559863,77.0430237427354&markers=color:red|label:D|28.4450418664655,77.0627158507705&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-19'),
(11, 3, '', '28.412181041534517', '77.04313606023788', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '08:15:17', '08:15:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412181041534517,77.04313606023788&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(12, 3, '', '28.4121780926219', '77.04314276576042', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '08:23:46', '08:24:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121780926219,77.04314276576042&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(13, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '09:02:55', '09:03:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(14, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '09:06:02', '09:06:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(15, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '09:11:35', '09:11:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(16, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '09:13:55', '09:14:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(17, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '09:49:22', '09:49:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(18, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '10:18:59', '10:19:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(19, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '10:21:11', '10:21:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(20, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 19', '10:21:53', '10:26:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(21, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Tuesday, Sep 19', '10:28:42', '10:31:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(22, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Tuesday, Sep 19', '10:32:17', '10:33:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(23, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Tuesday, Sep 19', '10:34:52', '10:36:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(24, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Tuesday, Sep 19', '10:37:05', '10:37:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(25, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.52784164523497', '77.20470897853374', 'Press Enclave Marg, Block A, Geetanjali Enclave, Lado Sarai, New Delhi, Delhi 110017, India', 'Tuesday, Sep 19', '10:40:02', '10:41:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.52784164523497,77.20470897853374&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-19'),
(26, 5, '', '5.6374694', '-0.24125400000002628', 'Achimota Mall, Accra, Greater Accra Region, Ghana', '5.6053204', '-0.2254229000000123', 'Tesano Police Station, Accra, Greater Accra Region, Ghana', 'Wednesday, Sep 20', '10:44 AM', '10:44:32 AM', '', '2017-09-20', '11 : 38', 0, 3, 2, 1, 1, 0, 0, 1, 0, 2, 1, '2017-09-20'),
(27, 4, '', '6.438589129859109', '3.435409516096115', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Wednesday, Sep 20', '14:38:55', '02:53:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.438589129859109,3.435409516096115&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(28, 4, '', '6.438589129859109', '3.435409516096115', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Wednesday, Sep 20', '15:42:32', '04:02:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.438589129859109,3.435409516096115&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(29, 4, '', '6.4385621437839795', '3.435406498610973', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Wednesday, Sep 20', '16:30:16', '04:36:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4385621437839795,3.435406498610973&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(30, 4, '', '6.43882880908502', '3.42548151132641', 'Lekki - Epe Expressway\nEti-Osa, Lagos', '6.4565212', '3.3823672', 'Leventis BUS Park, MARINA', 'Wednesday, Sep 20', '20:01:33', '08:25:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43882880908502,3.42548151132641&markers=color:red|label:D|6.4565212,3.3823672&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(31, 4, '', '6.4634708594591', '3.42447381466946', 'Osborne Road\nIkoyi, Lagos', '6.44988317604973', '3.36734980344772', '3 Wharf Road\nApapa Quays, Lagos', 'Wednesday, Sep 20', '20:28:00', '08:46:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4634708594591,3.42447381466946&markers=color:red|label:D|6.44988317604973,3.36734980344772&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-20'),
(32, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '06:30:55', '06:31:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(33, 3, '', '28.412183105773313', '77.04312968999147', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '06:52:59', '06:53:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412183105773313,77.04312968999147&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(34, 3, '', '28.412183990447062', '77.04313773661852', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '07:45:27', '07:45:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412183990447062,77.04313773661852&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(35, 3, '', '28.412183990447062', '77.04313773661852', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '07:49:24', '07:49:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412183990447062,77.04313773661852&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(36, 3, '', '28.412183990447062', '77.04313773661852', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '07:54:13', '07:54:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412183990447062,77.04313773661852&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(37, 4, '', '6.43882695688517', '3.42546433531252', 'Lekki - Epe Expressway\nEti-Osa, Lagos', '6.462465', '3.4178612', 'Osborne Road', 'Thursday, Sep 21', '08:25:46', '08:35:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43882695688517,3.42546433531252&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(38, 3, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '08:51:01', '08:51:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-21'),
(39, 3, '', '28.412210825547344', '77.04324804246426', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Sep 21', '12:20:05', '12:24:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412210825547344,77.04324804246426&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-21'),
(40, 4, '', '6.43893994870522', '3.43554798513651', 'Lekki - Epe Expressway\nEti-Osa, Lagos', '6.4758319', '3.3819035', 'Otto Police Barracks', 'Saturday, Sep 23', '17:33:26', '05:49:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.43893994870522,3.43554798513651&markers=color:red|label:D|6.4758319,3.3819035&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-23'),
(41, 4, '', '6.4313356044683', '3.42853862101586', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:18:22', '08:20:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4313356044683,3.42853862101586&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-23'),
(42, 4, '', '6.4313356044683', '3.42853862101586', '25 Sanusi Fafunwa Street\nVictoria Island, Lagos', '6.4395233', '3.4340605', '1004 Housing Estate', 'Saturday, Sep 23', '20:20:59', '08:23:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4313356044683,3.42853862101586&markers=color:red|label:D|6.4395233,3.4340605&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-23'),
(43, 6, '', '6.437861837974798', '3.4711653739213943', 'Bisola Durosinmi Etti Dr, Lekki Phase I, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Monday, Sep 25', '14:34:25', '02:43:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.437861837974798,3.4711653739213943&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-25'),
(44, 6, '', '6.4680077615969225', '3.44706941395998', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '', '', 'Set your drop point', 'Friday, Sep 29', '14:06:01', '02:06:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4680077615969225,3.44706941395998&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-29'),
(45, 6, '', '6.4680077615969225', '3.44706941395998', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '', '', 'Set your drop point', 'Friday, Sep 29', '14:07:10', '02:09:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4680077615969225,3.44706941395998&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 9, 0, 8, 4, 0, 1, 1, '2017-09-29'),
(46, 6, '', '6.432365636543614', '3.4408721700310703', '4 Ligali Ayorinde St, Victoria Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Saturday, Sep 30', '17:38:21', '06:02:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.432365636543614,3.4408721700310703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(47, 6, '', '6.432365636543614', '3.4408721700310703', '4 Ligali Ayorinde St, Victoria Island, Lagos, Nigeria', '6.4355129999999985', '3.4428409999999996', 'Sandfill', 'Saturday, Sep 30', '18:28:45', '06:42:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.432365636543614,3.4408721700310703&markers=color:red|label:D|6.4355129999999985,3.4428409999999996&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-30'),
(48, 4, '123456', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4376466', '3.431630899999959', 'Transit Village 4, Lagos, Nigeria', 'Tuesday, Oct 3', '13:19:51', '01:26:16 PM', '', '2017-10-03', '14 : 16', 6, 3, 2, 1, 9, 0, 8, 1, 0, 2, 1, '2017-10-03'),
(49, 6, '', '6.468169335472215', '3.4471076354384422', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Tuesday, Oct 3', '13:32:41', '01:59:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.468169335472215,3.4471076354384422&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-03'),
(50, 6, '', '6.438689078273006', '3.434533439576626', '1004 Apartment Complex, Victoria Island, Lagos, Nigeria', '6.438739052472582', '3.4313077479600906', 'Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria', 'Wednesday, Oct 4', '21:41:16', '09:41:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.438689078273006,3.434533439576626&markers=color:red|label:D|6.438739052472582,3.4313077479600906&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(51, 3, '', '28.433120673499534', '77.05521438270807', '432, Sushant Lok Phase I, Sector 27, Gurugram, Haryana 122022, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 5', '12:16:43', '01:11:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.433120673499534,77.05521438270807&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(52, 6, '', '6.4364592244821885', '3.4695590659976006', '7 Hunponu-Wusu Rd, Lekki Phase I, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Thursday, Oct 5', '12:55:05', '01:05:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4364592244821885,3.4695590659976006&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(53, 3, '', '28.412216723370673', '77.04323630779982', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 5', '13:15:03', '01:16:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216723370673,77.04323630779982&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-05'),
(54, 3, '', '28.412216428479518', '77.04323630779982', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 5', '13:17:05', '01:26:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216428479518,77.04323630779982&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(55, 3, '', '28.412216428479518', '77.04323630779982', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 5', '14:00:54', '02:02:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412216428479518,77.04323630779982&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-05'),
(56, 3, '', '28.398871173170164', '76.98216475546359', 'Gurgaon - Delhi Expy, Sector 75A, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 5', '14:02:36', '01:12:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.398871173170164,76.98216475546359&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(57, 6, '', '6.439379054953541', '3.4307485073804855', '1 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Friday, Oct 6', '14:05:11', '01:28:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.439379054953541,3.4307485073804855&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(58, 3, '', '28.388921299664837', '76.97452783584595', 'Delhi - Jaipur Expy, Sector 78, Gurugram, Haryana 122004, India', '28.459767855622573', '77.07415379583836', '711, Sector 30 M Wide Main Rd, Sushant Lok Phase I, Sector 27, Gurugram, Haryana 122022, India', 'Wednesday, Oct 11', '05:50:40', '05:51:02 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.388921299664837,76.97452783584595&markers=color:red|label:D|28.459767855622573,77.07415379583836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(59, 3, '', '28.412217902935303', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 11', '06:23:09', '02:38:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412217902935303,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-11'),
(60, 4, '', '6.468143017256879', '3.446937315165996', 'Adunola House, 401 Cl, Banana Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Wednesday, Oct 11', '16:19:53', '04:31:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.468143017256879,3.446937315165996&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(61, 4, '', '6.436873406419', '3.43201095513033', '55 Transit Village 3\nVictoria Island, Lagos', '6.46567176404501', '3.44827305525541', 'Kwara Street\nBanana Island, Lagos', 'Wednesday, Oct 11', '16:57:22', '05:02:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.436873406419,3.43201095513033&markers=color:red|label:D|6.46567176404501,3.44827305525541&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(62, 4, '', '6.468000099329394', '3.4471257403492928', '401 Cl, Banana Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Wednesday, Oct 11', '17:05:46', '05:07:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.468000099329394,3.4471257403492928&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(63, 4, '', '6.467937801758616', '3.4470965713262554', '401 Cl, Banana Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Wednesday, Oct 11', '17:09:50', '05:10:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.467937801758616,3.4470965713262554&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(64, 4, '', '6.439610601722121', '3.428698629140854', 'Lekki - Epe Expy, Eti-Osa, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Wednesday, Oct 11', '18:40:20', '06:43:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.439610601722121,3.428698629140854&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(65, 4, '', '6.45810235158272', '3.445261269807816', '9b Onikoyi Ln, Ikoyi, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Thursday, Oct 12', '18:01:07', '06:04:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.45810235158272,3.445261269807816&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(66, 4, '', '6.4372181687959795', '3.4316282719373703', '49 Transit Village 3, Victoria Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Thursday, Oct 12', '18:07:21', '06:17:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4372181687959795,3.4316282719373703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(67, 4, '', '6.439351735759494', '3.4305423125624657', '1 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3', 'Thursday, Oct 12', '18:20:09', '06:24:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.439351735759494,3.4305423125624657&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(68, 4, '', '6.4376466', '3.4316309', 'Transit Village 4', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Thursday, Oct 12', '18:29:03', '06:35:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4376466,3.4316309&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-12'),
(69, 4, '', '6.437604636930831', '3.4310686960816383', '65 Transit Village 4, Victoria Island, Lagos, Nigeria', '6.4377938729447655', '3.4313161298632617', '68 Transit Village 4, Victoria Island, Lagos, Nigeria', 'Saturday, Oct 14', '13:46:34', '01:46:34 PM', '', '14/10/2017', '15:46', 0, 3, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-14'),
(70, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '18:37:56', '02:09:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-16'),
(71, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '18:48:08', '02:19:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-16'),
(72, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '18:51:37', '02:22:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-16'),
(73, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '14:45:32', '02:45:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(74, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '14:48:06', '02:48:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-16'),
(75, 3, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Monday, Oct 16', '15:07:56', '03:07:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(76, 8, '', '6.4613001', '3.4586703', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 17', '19:47:25', '07:47:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4613001,3.4586703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 3, 0, 1, 1, '2017-10-17'),
(77, 8, '', '6.4613001', '3.4586703', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Wednesday, Oct 18', '14:23:12', '02:25:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4613001,3.4586703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(78, 8, '', '6.4613001', '3.4586703', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Wednesday, Oct 18', '14:27:30', '02:31:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4613001,3.4586703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(79, 9, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Monday, Oct 23', '13:27:04', '01:27:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-23'),
(80, 9, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Monday, Oct 23', '13:28:23', '01:29:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(81, 3, '', '28.41212324283226', '77.04330906271935', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.5355161', '77.39102649999995', 'Noida, Uttar Pradesh, India', 'Tuesday, Oct 24', '06:36:49', '06:38:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41212324283226,77.04330906271935&markers=color:red|label:D|28.5355161,77.39102649999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(82, 10, '', '28.4121523384475', '77.0432502702856', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '10:11:37', '10:12:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121523384475,77.0432502702856&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(83, 10, '', '28.4120885236453', '77.0432027802589', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.5245787', '77.206615', 'Saket', 'Tuesday, Oct 24', '13:02:54', '01:04:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120885236453,77.0432027802589&markers=color:red|label:D|28.5245787,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(84, 10, '', '28.4121633740407', '77.0433429256799', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:07:29', '01:09:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121633740407,77.0433429256799&markers=color:red|label:D|28.5245787,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(85, 10, '', '28.4120973869467', '77.0432555421182', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '13:09:53', '01:10:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120973869467,77.0432555421182&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(86, 10, '', '28.4121909129752', '77.0432399117368', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '14:55:45', '02:55:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121909129752,77.0432399117368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(87, 10, '', '28.4121909129752', '77.0432399117368', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '14:56:12', '02:56:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121909129752,77.0432399117368&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(88, 3, '', '28.412297228626297', '77.04344183206558', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.524578699999996', '77.206615', 'Saket', 'Wednesday, Oct 25', '13:56:16', '01:56:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412297228626297,77.04344183206558&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(89, 3, '', '28.412297228626297', '77.04344183206558', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.524578699999996', '77.206615', 'Saket', 'Wednesday, Oct 25', '14:47:07', '02:47:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412297228626297,77.04344183206558&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-25'),
(90, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Thursday, Oct 26', '14:04:05', '02:04:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(91, 4, '', '6.438129700063456', '3.43131847679615', '98 Transit Village 5, Victoria Island, Lagos, Nigeria', '', '', 'Set your drop point', 'Sunday, Oct 29', '21:20:29', '09:20:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.438129700063456,3.43131847679615&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(92, 3, '', '28.412346180510045', '77.0433922111988', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '05:17:13', '05:18:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412346180510045,77.0433922111988&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(93, 10, '', '28.4120666236648', '77.0432607829571', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '06:01:27', '06:01:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120666236648,77.0432607829571&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(94, 10, '', '28.4120666236648', '77.0432607829571', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '06:01:49', '06:02:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120666236648,77.0432607829571&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-31');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(95, 4, '', '6.4377938729447655', '3.4313161298632617', '68 Transit Village 4, Victoria Island, Lagos, Nigeria', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Tuesday, Oct 31', '09:51:38', '11:33:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4377938729447655,3.4313161298632617&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-31'),
(96, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '11:35:08', '11:35:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(97, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '11:36:45', '11:36:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(98, 4, '', '6.4372181687959795', '3.4316282719373703', '49 Transit Village 3, Victoria Island, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '11:44:08', '11:48:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4372181687959795,3.4316282719373703&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(99, 8, '', '6.468518134981504', '3.447342999279499', '401 Cl, Banana Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '11:59:56', '12:04:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.468518134981504,3.447342999279499&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(100, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '12:24:58', '12:24:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(101, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '12:28:12', '12:28:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(102, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '13:31:10', '01:31:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(103, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '13:34:35', '01:34:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(104, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '13:38:28', '01:38:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(105, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '13:49:16', '01:49:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(106, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Tuesday, Oct 31', '14:10:55', '02:10:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(107, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Tuesday, Oct 31', '14:13:48', '02:13:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(108, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Tuesday, Oct 31', '14:14:37', '02:14:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(109, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Tuesday, Oct 31', '14:15:00', '02:15:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(110, 1, '', '28.4135', '77.04153399999996', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4592693', '77.07241920000001', 'Huda Metro Station, Delhi, India', 'Tuesday, Oct 31', '14:15:11', '02:15:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4135,77.04153399999996&markers=color:red|label:D|28.4592693,77.07241920000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(111, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '15:07:21', '03:07:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(112, 8, '', '6.4372181687959795', '3.4316282719373703', '49 Transit Village 3, Victoria Island, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '15:20:26', '03:27:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4372181687959795,3.4316282719373703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(113, 4, '', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', 'Tuesday, Oct 31', '16:38:23', '04:38:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.466666999999999,3.4500000000000455&markers=color:red|label:D|6.4372183,3.4316283000000567&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-31'),
(114, 10, '', '28.4120580718087', '77.0432443544269', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Nov 1', '06:54:31', '06:55:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120580718087,77.0432443544269&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(115, 10, '', '28.4120545331095', '77.0432540774345', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Nov 1', '07:51:53', '07:52:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120545331095,77.0432540774345&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(116, 10, '', '28.4120720360791', '77.0433249989753', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Nov 1', '08:00:28', '08:01:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120720360791,77.0433249989753&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(117, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', 'Wednesday, Nov 1', '21:57:58', '09:57:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.466666999999999,3.4500000000000455&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(118, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', 'Wednesday, Nov 1', '22:00:30', '10:03:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.466666999999999,3.4500000000000455&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(119, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.466666999999999', '3.4500000000000455', 'Banana Island, Lekki, Lagos, Nigeria', 'Wednesday, Nov 1', '22:04:28', '10:04:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.466666999999999,3.4500000000000455&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(120, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.431064000000001', '3.433946600000013', 'Ajose Adeogun Street, Lagos, Nigeria', 'Wednesday, Nov 1', '22:07:42', '10:07:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.431064000000001,3.433946600000013&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', 'Wed , 01 Nov', '11:15 pm', 0, 3, 2, 1, 1, 0, 0, 1, 0, 1, 1, '0000-00-00'),
(121, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.431064000000001', '3.433946600000013', 'Ajose Adeogun Street, Lagos, Nigeria', 'Wednesday, Nov 1', '22:15:32', '10:15:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.431064000000001,3.433946600000013&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', 'Wed , 01 Nov', '11:15 pm', 0, 3, 2, 1, 1, 0, 0, 1, 0, 1, 1, '0000-00-00'),
(122, 4, '', '6.4372183', '3.4316283000000567', 'Transit Village 3, Lagos, Nigeria', '6.431064000000001', '3.433946600000013', 'Ajose Adeogun Street, Lagos, Nigeria', 'Wednesday, Nov 1', '22:16:33', '10:17:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283000000567&markers=color:red|label:D|6.431064000000001,3.433946600000013&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-01'),
(123, 13, '', '-33.88395999999999', '151.18150000000003', 'Forest Lodge, New South Wales, Australia', '-33.8775', '151.24119999999994', 'Double Bay, New South Wales, Australia', 'Wednesday, Nov 8', '11:35 AM', '11:35:57 AM', '', '', '17 : 04', 0, 4, 1, 1, 8, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(124, 4, '', '6.4613001', '3.4586703', 'Banana Island, Lekki, Lagos, Nigeria', '6.4372183', '3.4316283', 'Transit Village 3, Lagos, Nigeria', 'Thursday, Nov 9', '12:09:01', '12:09:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|6.4613001,3.4586703&markers=color:red|label:D|6.4372183,3.4316283&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-09'),
(125, 4, '', '6.4372183', '3.4316283', 'Transit Village 3', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Friday, Nov 10', '20:21:23', '08:21:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(126, 4, '', '6.4372183', '3.4316283', 'Transit Village 3', '6.466666999999999', '3.4499999999999997', 'Banana Island', 'Friday, Nov 10', '20:24:36', '08:24:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|6.4372183,3.4316283&markers=color:red|label:D|6.466666999999999,3.4499999999999997&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '112', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'Road Worthiness');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(20, 120, 4, 1),
(19, 120, 2, 1),
(18, 120, 1, 1),
(16, 121, 4, 1),
(15, 121, 2, 1),
(14, 121, 1, 1),
(17, 121, 10, 1),
(21, 120, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-09-19 00.00.01 AM', '2017-09-20 04:53:45 PM', 7, '83.72', '0000-00-00', 0),
(2, '2017-09-19 00.00.01 AM', '2017-09-20 04:53:45 PM', 6, '0', '0000-00-00', 0),
(3, '2017-09-19 00.00.01 AM', '2017-09-20 04:53:45 PM', 5, '82.25', '0000-00-00', 0),
(4, '2017-09-19 00.00.01 AM', '2017-09-20 04:53:45 PM', 4, '0', '0000-00-00', 0),
(5, '2017-09-18 00.00.01 AM', '2017-09-20 04:53:45 PM', 3, '0', '0000-00-00', 0),
(6, '2017-09-18 00.00.01 AM', '2017-09-20 04:53:45 PM', 2, '4', '0000-00-00', 0),
(7, '2017-09-14 00.00.01 AM', '2017-09-20 04:53:45 PM', 1, '30', '0000-00-00', 0),
(8, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 9, '157.8', '0000-00-00', 0),
(9, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 8, '0', '0000-00-00', 0),
(10, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 7, '177.72', '0000-00-00', 0),
(11, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 6, '-96.6', '0000-00-00', 0),
(12, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 5, '120.75', '0000-00-00', 0),
(13, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 4, '0', '0000-00-00', 0),
(14, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 3, '0', '0000-00-00', 0),
(15, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 2, '0', '0000-00-00', 0),
(16, '2017-09-20 04:54:45 PM', '2017-10-12 12:42:33 PM', 1, '0', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1505386572document_image_13.jpg', '2017-12-19', 2),
(2, 1, 2, 'uploads/driver/1505386591document_image_12.jpg', '2017-12-27', 2),
(3, 1, 4, 'uploads/driver/1505386606document_image_14.jpg', '2018-01-25', 2),
(4, 2, 3, 'uploads/driver/1505729158document_image_23.jpg', '2017-11-29', 2),
(5, 2, 2, 'uploads/driver/1505729167document_image_22.jpg', '2017-12-27', 2),
(6, 2, 4, 'uploads/driver/1505729177document_image_24.jpg', '2018-02-28', 2),
(7, 3, 3, 'uploads/driver/1505755290document_image_33.jpg', '2017-10-21', 2),
(8, 3, 2, 'uploads/driver/1505755309document_image_32.jpg', '2017-10-21', 2),
(9, 3, 4, 'uploads/driver/1505755325document_image_34.jpg', '2017-10-21', 2),
(10, 5, 3, 'uploads/driver/1505804198document_image_53.jpg', '2-5-2019', 2),
(11, 5, 2, 'uploads/driver/1505804216document_image_52.jpg', '13-9-2018', 2),
(12, 5, 4, 'uploads/driver/1505804231document_image_54.jpg', '18-1-2018', 2),
(13, 7, 1, 'uploads/driver/1505842753document_image_71.jpg', '29-12-2017', 2),
(14, 7, 2, 'uploads/driver/1505842782document_image_72.jpg', '29-12-2017', 2),
(15, 7, 3, 'uploads/driver/1505842799document_image_73.jpg', '29-12-2017', 2),
(16, 6, 1, 'uploads/driver/1506005708document_image_61.jpg', '21-9-2017', 2),
(17, 6, 2, 'uploads/driver/1506005732document_image_62.jpg', '28-9-2017', 2),
(18, 6, 3, 'uploads/driver/1506005753document_image_63.jpg', '28-9-2017', 2),
(19, 8, 4, 'uploads/driver/150702013601501133712115.jpgdocument_image_8.jpg', '', 2),
(20, 8, 2, 'uploads/driver/150702013611500517914588.jpgdocument_image_8.jpg', '', 2),
(21, 8, 3, 'uploads/driver/150702013621500517914588.jpgdocument_image_8.jpg', '', 2),
(22, 9, 4, 'uploads/driver/1507734398document_image_94.jpg', '31-10-2017', 2),
(23, 9, 2, 'uploads/driver/1507734421document_image_92.jpg', '31-10-2017', 2),
(24, 9, 1, 'uploads/driver/1507734439document_image_91.jpg', '31-10-2017', 2),
(25, 9, 10, 'uploads/driver/1507734461document_image_910.jpg', '31-10-2017', 2),
(26, 10, 3, 'uploads/driver/1508836148document_image_103.jpg', '2017-12-20', 2),
(27, 10, 2, 'uploads/driver/1508836168document_image_102.jpg', '2017-12-27', 2),
(28, 10, 4, 'uploads/driver/1508836176document_image_104.jpg', '2017-12-27', 2),
(29, 11, 3, 'uploads/driver/1508933615document_image_110logo.png', '25/10/2017', 2),
(30, 11, 2, 'uploads/driver/1508933615document_image_111logo.png', '26/10/2017', 2),
(31, 11, 4, 'uploads/driver/1508933615document_image_112logo.png', '27/10/2017', 2),
(32, 12, 4, 'uploads/driver/1509020731document_image_120Capture_test.JPG', '31/10/2017', 2),
(33, 12, 2, 'uploads/driver/1509020731document_image_121Capture_test.JPG', '31/10/2017', 2),
(34, 12, 1, 'uploads/driver/1509020731document_image_122Capture_test.JPG', '31/10/2017', 2),
(35, 12, 10, 'uploads/driver/1509020731document_image_123Capture_test.JPG', '31/10/2017', 2),
(36, 13, 4, 'uploads/driver/1509434450document_image_134.jpg', '31-10-2017', 1),
(37, 13, 2, 'uploads/driver/1509434499document_image_132.jpg', '31-10-2017', 1),
(38, 13, 1, 'uploads/driver/1509434535document_image_131.jpg', '30-11-2017', 1),
(39, 13, 10, 'uploads/driver/1509434626document_image_1310.jpg', '30-11-2017', 1),
(40, 14, 4, 'uploads/driver/1509442353document_image_144.jpg', '30-11-2017', 2),
(41, 14, 2, 'uploads/driver/1509442378document_image_142.jpg', '30-11-2017', 2),
(42, 14, 1, 'uploads/driver/1509442404document_image_141.jpg', '30-11-2017', 2),
(43, 14, 10, 'uploads/driver/1509442440document_image_1410.jpg', '30-11-2017', 2),
(44, 16, 4, 'uploads/driver/1509575976document_image_160Capture_test.JPG', '30/11/2017', 1),
(45, 16, 2, 'uploads/driver/1509575976document_image_161Capture_test.JPG', '30/11/2017', 1),
(46, 16, 1, 'uploads/driver/1509575976document_image_162Capture_test.JPG', '30/11/2017', 1),
(47, 16, 10, 'uploads/driver/1509575976document_image_163Capture_test.JPG', '30/11/2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-14 14:13:49', '2017-09-14 14:06:39', '4 Hours 6 Minutes', 4, 6, '2017-09-14'),
(2, 2, '2017-09-18 11:08:51', '2017-09-19 06:54:59', '38 Hours 91 Minutes', 38, 91, '2017-09-18'),
(3, 2, '2017-09-19 07:19:08', '2017-09-19 07:19:10', '0 Hours 3 Minutes', 0, 3, '2017-09-19'),
(4, 5, '2017-09-19 08:13:41', '2017-09-21 07:43:13', '23 Hours 42 Minutes', 23, 42, '2017-09-19'),
(5, 7, '2017-09-20 16:29:55', '2017-09-20 16:29:51', '2 Hours 27 Minutes', 2, 27, '2017-09-20'),
(6, 5, '2017-09-21 07:43:20', '2017-10-25 14:51:13', '7 Hours 7 Minutes', 7, 7, '2017-09-21'),
(7, 6, '2017-09-23 20:24:49', '2017-09-23 20:24:45', '2 Hours 57 Minutes', 2, 57, '2017-09-23'),
(8, 9, '2017-10-11 16:16:51', '', '', 0, 0, '2017-10-11'),
(9, 10, '2017-10-24 14:55:09', '2017-10-31 05:45:18', '16 Hours 126 Minutes', 16, 126, '2017-10-24'),
(10, 5, '2017-10-25 14:51:16', '', '', 0, 0, '2017-10-25'),
(11, 10, '2017-10-31 06:06:36', '2017-10-31 13:43:52', '7 Hours 41 Minutes', 7, 41, '2017-10-31'),
(12, 14, '2017-10-31 09:46:28', '', '', 0, 0, '2017-10-31'),
(13, 5, '2017-11-01 06:54:13', '', '', 0, 0, '2017-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 1, 1, 0, '', 1, 4.5, '', '2017-09-14'),
(2, 4, 1, 0, '', 1, 4.5, '', '2017-09-14'),
(3, 5, 1, 0, '', 1, 5, '', '2017-09-14'),
(4, 6, 1, 0, '', 1, 4.5, '', '2017-09-14'),
(5, 7, 1, 0, '', 1, 4.5, '', '2017-09-14'),
(6, 8, 1, 0, '', 1, 4.5, '', '2017-09-14'),
(7, 9, 1, 0, '', 2, 5, '', '2017-09-18'),
(8, 11, 3, 4, '', 5, 4.5, '', '2017-09-19'),
(9, 12, 3, 2.5, '', 5, 4, '', '2017-09-19'),
(10, 14, 3, 4.5, '', 5, 4, '', '2017-09-19'),
(11, 15, 3, 4, 'dhch ', 5, 4, '', '2017-09-19'),
(12, 16, 3, 3.5, '', 5, 3.5, '', '2017-09-19'),
(13, 17, 3, 0, '', 5, 4.5, '', '2017-09-19'),
(14, 18, 3, 4, '', 5, 4, '', '2017-09-19'),
(15, 19, 3, 3.5, '', 5, 4.5, '', '2017-09-19'),
(16, 20, 3, 0, '', 5, 4.5, '', '2017-09-19'),
(17, 21, 3, 3.5, '', 5, 3, '', '2017-09-19'),
(18, 22, 3, 4, '', 5, 5, '', '2017-09-19'),
(19, 23, 3, 4, '', 5, 5, '', '2017-09-19'),
(20, 24, 3, 4, '', 5, 4, '', '2017-09-19'),
(21, 25, 3, 4, '', 5, 3.5, '', '2017-09-19'),
(22, 27, 4, 5, '', 7, 5, '', '2017-09-20'),
(23, 28, 4, 5, '', 7, 5, '', '2017-09-20'),
(24, 29, 4, 0, '', 7, 0, '', '2017-09-20'),
(25, 30, 4, 4, 'Test', 7, 5, '', '2017-09-20'),
(26, 31, 4, 4.5, '', 7, 4, '', '2017-09-20'),
(27, 32, 3, 3.5, '', 5, 4.5, '', '2017-09-21'),
(28, 33, 3, 0, '', 5, 4, '', '2017-09-21'),
(29, 34, 3, 4, '', 5, 0, '', '2017-09-21'),
(30, 35, 3, 4, '', 5, 4, '', '2017-09-21'),
(31, 36, 3, 0, '', 5, 4.5, '', '2017-09-21'),
(32, 37, 4, 5, '', 7, 0, '', '2017-09-21'),
(33, 39, 3, 4, '', 5, 3, '', '2017-09-21'),
(34, 40, 4, 5, '', 6, 5, '', '2017-09-23'),
(35, 43, 6, 0, '', 6, 5, '', '2017-09-25'),
(36, 46, 6, 4, '', 6, 5, '', '2017-09-30'),
(37, 47, 6, 5, '', 6, 0, '', '2017-09-30'),
(38, 49, 6, 5, '', 6, 5, '', '2017-10-03'),
(39, 50, 6, 0, '', 6, 5, '', '2017-10-04'),
(40, 52, 6, 0, '', 6, 5, '', '2017-10-05'),
(41, 51, 3, 3, '', 5, 4, '', '2017-10-05'),
(42, 54, 3, 3, '', 5, 2.5, '', '2017-10-05'),
(43, 56, 3, 3.5, '', 5, 4, '', '2017-10-06'),
(44, 57, 6, 5, '', 6, 5, '', '2017-10-09'),
(45, 58, 3, 0, '', 5, 4, '', '2017-10-11'),
(46, 60, 4, 4.5, '', 9, 5, '', '2017-10-11'),
(47, 62, 4, 5, '', 9, 5, '', '2017-10-11'),
(48, 63, 4, 0, '', 9, 4, '', '2017-10-11'),
(49, 64, 4, 5, '', 9, 5, '', '2017-10-11'),
(50, 65, 4, 0, '', 9, 5, '', '2017-10-12'),
(51, 66, 4, 5, '', 9, 5, '', '2017-10-12'),
(52, 67, 4, 4.5, '', 9, 5, '', '2017-10-12'),
(53, 77, 8, 0, '', 9, 5, '', '2017-10-18'),
(54, 78, 8, 0, '', 9, 5, '', '2017-10-18'),
(55, 80, 9, 0, '', 5, 4, '', '2017-10-23'),
(56, 81, 3, 4.5, '', 5, 4.5, '', '2017-10-24'),
(57, 82, 10, 0, '', 10, 4.5, '', '2017-10-24'),
(58, 83, 10, 0, '', 10, 4, '', '2017-10-24'),
(59, 84, 10, 0, '', 10, 4.5, '', '2017-10-24'),
(60, 85, 10, 0, '', 10, 5, '', '2017-10-24'),
(61, 87, 10, 0, '', 10, 4.5, '', '2017-10-24'),
(62, 88, 3, 0, '', 5, 4.5, '', '2017-10-25'),
(63, 89, 3, 0, '', 5, 3.5, '', '2017-10-25'),
(64, 92, 3, 4.5, '', 5, 4.5, '', '2017-10-31'),
(65, 94, 10, 0, '', 10, 4.5, '', '2017-10-31'),
(66, 98, 4, 5, '', 14, 5, '', '2017-10-31'),
(67, 99, 8, 4.5, '', 14, 5, '', '2017-10-31'),
(68, 112, 8, 3.5, 'cccv ', 14, 5, '', '2017-10-31'),
(69, 114, 10, 0, '', 5, 4, '', '2017-11-01'),
(70, 115, 10, 0, '', 5, 4.5, '', '2017-11-01'),
(71, 116, 10, 0, '', 5, 4.5, '', '2017-11-01'),
(72, 118, 4, 0, '', 14, 5, '', '2017-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 0, 2),
(3, 1, 1, 0, 3),
(4, 1, 1, 1, 4),
(5, 1, 1, 1, 5),
(6, 1, 1, 1, 6),
(7, 1, 1, 1, 7),
(8, 1, 1, 1, 8),
(9, 1, 1, 2, 9),
(10, 1, 1, 2, 10),
(11, 1, 3, 5, 11),
(12, 1, 3, 5, 12),
(13, 1, 3, 5, 13),
(14, 1, 3, 5, 14),
(15, 1, 3, 5, 15),
(16, 1, 3, 5, 16),
(17, 1, 3, 5, 17),
(18, 1, 3, 5, 18),
(19, 1, 3, 5, 19),
(20, 1, 3, 5, 20),
(21, 1, 3, 5, 21),
(22, 1, 3, 5, 22),
(23, 1, 3, 5, 23),
(24, 1, 3, 5, 24),
(25, 1, 3, 5, 25),
(26, 1, 5, 0, 26),
(27, 1, 4, 7, 27),
(28, 1, 4, 7, 28),
(29, 1, 4, 7, 29),
(30, 1, 4, 7, 30),
(31, 1, 4, 7, 31),
(32, 1, 3, 5, 32),
(33, 1, 3, 5, 33),
(34, 1, 3, 5, 34),
(35, 1, 3, 5, 35),
(36, 1, 3, 5, 36),
(37, 1, 4, 7, 37),
(38, 1, 3, 0, 38),
(39, 1, 3, 5, 39),
(40, 1, 4, 6, 40),
(41, 1, 4, 6, 41),
(42, 1, 4, 6, 42),
(43, 1, 6, 6, 43),
(44, 1, 6, 0, 44),
(45, 1, 6, 6, 45),
(46, 1, 6, 6, 46),
(47, 1, 6, 6, 47),
(48, 1, 4, 6, 48),
(49, 1, 6, 6, 49),
(50, 1, 6, 6, 50),
(51, 1, 3, 5, 51),
(52, 1, 6, 6, 52),
(53, 1, 3, 5, 53),
(54, 1, 3, 5, 54),
(55, 1, 3, 5, 55),
(56, 1, 3, 5, 56),
(57, 1, 6, 6, 57),
(58, 1, 3, 5, 58),
(59, 1, 3, 5, 59),
(60, 1, 4, 9, 60),
(61, 1, 4, 9, 61),
(62, 1, 4, 9, 62),
(63, 1, 4, 9, 63),
(64, 1, 4, 9, 64),
(65, 1, 4, 9, 65),
(66, 1, 4, 9, 66),
(67, 1, 4, 9, 67),
(68, 1, 4, 9, 68),
(69, 1, 4, 0, 69),
(70, 1, 3, 5, 70),
(71, 1, 3, 5, 71),
(72, 1, 3, 5, 72),
(73, 1, 3, 0, 73),
(74, 1, 3, 0, 74),
(75, 1, 3, 0, 75),
(76, 1, 8, 0, 76),
(77, 1, 8, 9, 77),
(78, 1, 8, 9, 78),
(79, 1, 9, 5, 79),
(80, 1, 9, 5, 80),
(81, 1, 3, 5, 81),
(82, 1, 10, 10, 82),
(83, 1, 10, 10, 83),
(84, 1, 10, 10, 84),
(85, 1, 10, 10, 85),
(86, 1, 10, 0, 86),
(87, 1, 10, 10, 87),
(88, 1, 3, 5, 88),
(89, 1, 3, 5, 89),
(90, 1, 4, 0, 90),
(91, 1, 4, 0, 91),
(92, 1, 3, 5, 92),
(93, 1, 10, 0, 93),
(94, 1, 10, 10, 94),
(95, 1, 4, 14, 95),
(96, 1, 4, 0, 96),
(97, 1, 4, 0, 97),
(98, 1, 4, 14, 98),
(99, 1, 8, 14, 99),
(100, 1, 4, 0, 100),
(101, 1, 4, 0, 101),
(102, 1, 4, 0, 102),
(103, 1, 4, 0, 103),
(104, 1, 4, 0, 104),
(105, 1, 4, 0, 105),
(106, 1, 1, 0, 106),
(107, 1, 1, 0, 107),
(108, 1, 1, 0, 110),
(109, 1, 4, 0, 111),
(110, 1, 8, 14, 112),
(111, 1, 4, 0, 113),
(112, 1, 10, 5, 114),
(113, 1, 10, 5, 115),
(114, 1, 10, 5, 116),
(115, 1, 4, 0, 117),
(116, 1, 4, 14, 118),
(117, 1, 4, 0, 119),
(118, 1, 4, 14, 122),
(119, 1, 13, 0, 123),
(120, 1, 4, 0, 124),
(121, 1, 4, 0, 125),
(122, 1, 4, 0, 126);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'vikalp .', 'vikalp@apporio.com', '+914545454545', '123456', '', '', 0, '0', 'Thursday, Sep 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.64285714286', 0, '', 1, '2017-09-14', 1),
(2, 1, 'Geoffrey Kwabena Oti ', '', '+233272633889', '', '', '', 0, '0', 'Monday, Sep 18', '', 0, 0, 0, 0, 0, '', '', '', '', '', '102939034257953997778', 'Geoffrey Kwabena Oti', 'otikwabena@gmail.com', 'https://lh3.googleusercontent.com/-GJkHO3lA2bw/AAAAAAAAAAI/AAAAAAAAACI/MhJyFaXxKRg/s400/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-09-18', 1),
(3, 1, 'vkvcv .', 'mjk@gmail.com', '+918285469069', 'qwerty', '', '', 0, '0', 'Tuesday, Sep 19', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '3.91071428571', 0, '', 1, '2017-09-19', 1),
(4, 1, 'Hugotaxi_test1 Test1 .', 'jamesbron2000@gmail.com', '+2348022226365', '', 'http://apporio.org/Hugotaxi/hugo/uploads/swift_file66.jpeg', '', 0, '0', 'Tuesday, Sep 19', '', 0, 0, 0, 0, 0, '', '', '', '', '', '115478624586303918922', 'Hugotaxi_test1 Test1', 'hugotaxitest1@gmail.com', 'null', '', '', 0, 0, '4.25', 0, '', 3, '2017-09-19', 1),
(13, 1, 'ebedhani@gmail.com', 'apporio@gmail.com', '8950200340', '', '', '', 0, '0', '', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1),
(5, 1, 'Kofi Two', 'jamesbron2000@gmail.com', '+233261114044', 'HEHEHE123', '', '', 0, '0', ' Wednesday, Sep 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1),
(6, 1, 'Freddy', 'jamesbron2000@gmail.com', '+234261114044', '010309', '', '', 0, '0', 'Monday, Sep 25', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.28571428571', 0, '', 1, '2017-09-25', 1),
(10, 1, 'vikalp .', 'vikalp@apporio.com', '+913636363636', '123456', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.44444444444', 0, '', 1, '2017-10-24', 1),
(7, 1, 'good .', 'bvc@gmail.com', '+919999999999', 'qwerty', '', '', 0, '0', 'Wednesday, Oct 11', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-11', 1),
(8, 4, 'Classpee .', 'jamesbron2000@gmail.com', '+2347088804526', '010309', '', '', 0, '0', 'Tuesday Oct 17', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '0000-00-00', 1),
(9, 4, 'vytvcytvyt', 'ytvytvyv@ftctrct.com', '+918285469066', '', '', '', 0, '0', 'Monday Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4', 0, '', 1, '0000-00-00', 1),
(11, 4, 'ytfytfytfyt', '76876gfcfcft@fcftcftc', '+9187678687687', '22wqer2r', '', '', 0, '0', 'Thursday Oct 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1),
(12, 4, 'dsgugds', 'cfcfgcgfcftc@FTCftcftct', '+9176576745764767', '12343456', '', '', 0, '0', 'Thursday Oct 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, '567C7F8E0CB52241B109A98D77C837C46685F98A15227C006BF55E63076C87B4', 1, 'C187857B-613B-4FED-83FD-28EC5F968866', 0),
(2, 1, 'E53C85F82F8E67B5BAEA31A906160451A9C9970AAFB2C80AB202C7AE94CE059C', 1, 'A95A9A3E-7E57-4D86-B98D-D93D59839AD2', 1),
(3, 1, '91587948277DEB17BAF030CECEE9EE3471CC6D1CDE62BC435515DFB1376E6CA3', 1, '6EA09ECA-7D56-4C22-91D8-55DB01A01D9E', 0),
(4, 2, '0ACDB06DF8EA8705D167426267DA251ACB19D95B1084A62D9ECE19D32527CCE5', 1, 'CEE0FE2D-C9A7-4948-AA75-DACDF34F4B1D', 1),
(5, 1, 'A5DCF1A9F0CD0F477D01C79147DBB0FA3293A0A03ABC6A17F5EEC98CC23BE833', 1, '6D24AC00-D569-45E3-B05E-20B36A1B2248', 1),
(6, 3, 'cqHLIp9tweU:APA91bFp0VVkxiqoDU_8OMX9j9UW1GF5ohC6EjWVn5PrSYjwN7CTUWk7vm7EUC0Uu0yRSmLCBalo5eTSLU3avu8rYLAH723MgUMS2vRkR41qjRdB0WuKa5n6L7BzmViCB1j4SeRziXQt', 0, '32c87e564d926edc', 1),
(7, 4, '79F5E6098CD9875F0D3FD0E3D91F3CF464F1EC92825427B518A9D3DF9D56A49C', 1, '996A9B27-C8AA-4A37-960F-DA0720EB3132', 1),
(8, 8, 'eucdhaQX6T4:APA91bGv3ysdlOq-U3p41WSLFf5qVpuq3FNjvTTEukxjT11_nBKCmyoZU01crH7kn28HZY3lXQN5_9mpk2asczmZi7Z0Oeh6qvUSmrNEx1yTibgGIWz6KfIhMbpemY-aEs3CvIf--9dD', 0, '298154928258e2ef', 1),
(9, 4, 'cCoxLibjcFs:APA91bF1tP5xPqPQ3Gbq2D-eYAK8OUd8IVgZR-m2W1yGKxuN_ehroDk3RDxf2GcFQW-mfHRZ4E2Qi44KMgq5ha3sbM5K8a9UVF4aoiNoZhqYSgWde3bBYw6aSaN5ikyhP0gBjb8_0UCt', 0, '4059b09043832eb1', 1),
(10, 10, 'D3EF39DBC9ED16655E9109CF5072D9FD3DB52B1AB9455FAF6C078E20A3FE8225', 1, '8540F390-6705-4C20-9DD7-0D4F2AC7023B', 1),
(11, 4, 'F130090A2FD0B865604436F7BDFDCF9A047DDEB773E8A7B1B2C5D0773BF2D100', 1, '0DE49E78-1F12-4C7E-81A3-5E85C56BA184', 1),
(12, 4, 'B4179F539B95699F77CFE290791A2CC11D11B1171BC6D8EC892E45A7A2501D70', 1, '01F3AA0D-F3AA-482E-BC98-61F553C3F8AF', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` longtext CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Hugotaxi is a smart mobile app that allows drivers to find you in minutes and quickly get you where you want to go. \r\nIts easy and hassle free to ride by our licensed and professional drivers and make cash or card payment without surge pricing.\r\n                                                                                                                                                                                                                                                         \r\nHugotaxi is very affordable, secured, fast services ranging from economy cars, towing car services, business cars, special travel cars, parcel delivery and truck delivery.'),
(2, 'معلومات عنا', 'أبوريو إنفولابس الجندي. المحدودة هي شهادة إسو تطبيقات الهاتف المتحرك وشبكة تطوير التطبيقات على شبكة الإنترنت في الهند. نحن نقدم نهاية إلى نهاية الحل من تصميم لتطوير البرمجيات. نحن فريق من 30+ الناس الذي يتضمن المطورين ذوي الخبرة والمصممين المبدعين. ومن المعروف أبوريو إنفولابس لتقديم برامج ذات جودة ممتازة لعملائها. وتنتشر قاعدة عملائنا في أكثر من 20 دولة بما في ذلك الهند والولايات المتحدة والمملكة المتحدة وأستراليا وإسبانيا والنرويج والسويد والإمارات العربية المتحدة وقطر وسنغافورة وماليزيا ونيجيريا وجنوب أفريقيا وإيطاليا وبرمودا وهونغ كونغ.'),
(3, 'À propos de nous', 'Apporio Infolabs Pvt. Ltd est un ISO certifié application mobile et société de développement d\'applications Web en Inde. Nous fournissons des solutions de bout en bout, de la conception au développement du logiciel. Nous sommes une équipe de plus de 30 personnes qui comprend des développeurs expérimentés et des concepteurs créatifs. Apporio Infolabs est connu pour fournir des logiciels d\'excellente qualité à ses clients. Notre clientèle s\'étend sur plus de 20 pays: Inde, États-Unis, Royaume-Uni, Australie, Espagne, Norvège, Suède, Émirats arabes unis, Arabie Saoudite, Qatar, Singapour, Malaisie, Nigéria, Afrique du Sud, Italie, Bermudes et Hong Kong.'),
(4, '关于我们', 'Apporio Infolabs Pvt。有限公司是通过ISO认证 移动应用和网络应用开发公司在印度。我们提供从设计到开发软件的端到端解决方案。我们是一个30多人的团队，包括有经验的开发人员和创意设计师。 Apporio Infolabs以其为客户提供优质的软件而闻名。我们的客户群遍布印度，美国，英国，澳大利亚，西班牙，挪威，瑞典，阿联酋，沙特阿拉伯，卡塔尔，新加坡，马来西亚，尼日利亚，南非，意大利，百慕​​大和香港等20多个国家。'),
(5, 'Sobre nosotros', 'Apporio Infolabs Pvt. Ltd. tiene certificación ISO empresa de desarrollo de aplicaciones web y aplicaciones móviles en India. Brindamos soluciones de extremo a extremo desde el diseño hasta el desarrollo del software. Somos un equipo de más de 30 personas que incluye desarrolladores experimentados y diseñadores creativos. Apporio Infolabs es conocida por entregar software de excelente calidad a sus clientes. Nuestra base de clientes se extiende a más de 20 países, incluyendo India, EE. UU., Reino Unido, Australia, España, Noruega, Suecia, Emiratos Árabes Unidos, Arabia Saudita, Qatar, Singapur, Malasia, Nigeria, Sudáfrica, Italia, Bermudas y Hong Kong.'),
(6, 'About Us', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'support@hugotaxi.com', '+234 703449 8429', 'hugotaxisupport', '4 Babatope Bejide Crescent, Lekki Phase 1, Lagos, Nigeria');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_headings`
--

CREATE TABLE `web_headings` (
  `heading_id` int(11) NOT NULL,
  `heading_1` text NOT NULL,
  `heading1` text NOT NULL,
  `heading2` text NOT NULL,
  `heading3` text NOT NULL,
  `heading4` text NOT NULL,
  `heading5` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_headings`
--

INSERT INTO `web_headings` (`heading_id`, `heading_1`, `heading1`, `heading2`, `heading3`, `heading4`, `heading5`) VALUES
(1, 'Book HugoTaxi to your destination in town', 'A cab for every occasion and pocket ', 'Meet our Awesome Fleet', 'The widest variety of cars to choose from', 'Why Ride with HugoTaxi?', 'A cab for every occasion and pocket'),
(2, ' المدينةحجز تاكسي هوغو إلى وجهتك في', 'سيارة أجرة في كل مناسبة والجيب', 'تلبية أسطولنا رهيبة', 'أوسع مجموعة من السياراتللاختيار من بينها', 'لماذا ركوب مع هوغو تاكسي؟', 'سيارة أجرة في كل مناسبة والجيب'),
(3, 'Réservez HugoTaxi à votre destination en ville', 'Un taxi pour chaque occasion et poche', 'Rencontrez notre flotte impressionnante', 'La plus grande variété de voitures à choisir', 'Pourquoi rouler avec HugoTaxi?', 'Un taxi pour chaque occasion et poche'),
(4, '将雨果塔溪预定到城里的目的地', '每一个场合和口袋里都有出租车', '认识我们的真棒舰队', '最多种类的汽车可供选择', '为什么骑雨果塔？', '每一个场合和口袋里都有出租车'),
(5, 'Reserva HugoTaxi a tu destino en la ciudad', 'Un taxi para cada ocasión y bolsillo', 'Conoce nuestra Flota Impresionante', 'La más amplia variedad de autos para elegir', '¿Por qué viajar con HugoTaxi?', 'Un taxi para cada ocasión y bolsillo');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `app_name` varchar(255) NOT NULL DEFAULT '',
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `app_name`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Hugotaxi || Website', 'HugoTaxi', '2017 HugoTaxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp ', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png'),
(2, 'هوغوتاكسي | | موقع الكتروني', 'هوغو تاكسي', '2017 هوغو تاكسي', 'uploads/website/banner_1501227855.jpg', 'موبايل أب', 'Neden Taxi Kiralama i', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp ', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'Low-cost to luxury', 'uploads/website/heading3_img1501228907.png', 'Neden Se', 'Taksi kiralama i', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', NULL, 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'Safe rides for everyone', 'uploads/website/features2_bg1501241213.png'),
(3, 'Hugotaxi || Site Internet', 'HugoTaxi', '2017 HugoTaxi', 'uploads/website/banner_1501227855.jpg', 'APPLICATION MOBILE', 'Why Choose Apporio for Taxi Hire', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Hugotaxi ||网站', 'HugoTaxi', '2017年雨果塔溪', 'uploads/website/banner_1501227855.jpg', '移动应用', 'Why Choose Apporio for Taxi Hire', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Hugotaxi || Sitio web', 'HugoTaxi', '2017 HugoTaxi', 'uploads/website/banner_1501227855.jpg', 'APLICACIÓN MOVIL', 'Why Choose Apporio for Taxi Hire', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `heading_arabic` varchar(255) NOT NULL DEFAULT '',
  `heading_french` varchar(255) NOT NULL DEFAULT '',
  `heading_chinese` varchar(255) NOT NULL DEFAULT '',
  `heading_spanish` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `long_content` text NOT NULL,
  `content_arabic` varchar(255) NOT NULL DEFAULT '',
  `long_contentarabic` text NOT NULL,
  `content_french` varchar(255) NOT NULL DEFAULT '',
  `long_contentfrench` int(11) NOT NULL,
  `content_chinese` varchar(255) NOT NULL DEFAULT '',
  `long_contentchinese` text NOT NULL,
  `content_spanish` varchar(255) NOT NULL DEFAULT '',
  `long_contentspanish` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `big_image` varchar(255) NOT NULL DEFAULT '',
  `blog_date` varchar(255) NOT NULL DEFAULT '',
  `blog_time` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `heading_arabic`, `heading_french`, `heading_chinese`, `heading_spanish`, `content`, `long_content`, `content_arabic`, `long_contentarabic`, `content_french`, `long_contentfrench`, `content_chinese`, `long_contentchinese`, `content_spanish`, `long_contentspanish`, `image`, `big_image`, `blog_date`, `blog_time`) VALUES
(1, 'Cabs for Every Pocket', 'سيارات الأجرة لكل جيب', 'Cabines pour chaque poche', '每个口袋的驾驶室', 'Cabinas para todos los bolsillos', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', '', 'من سيارات السيدان وسيارات الدفع الرباعي إلى السيارات الفاخرة للمناسبات الخاصة، لدينا سيارات الأجرة لتناسب كل جيب', '', 'Des berlines et des VUS aux voitures de luxe pour les occasions spéciales, nous avons des cabines pour toutes les bourses', 0, '从轿车和SUV到特殊场合的豪华轿车，我们有出租车适合每个口袋', '', 'Desde sedanes y SUV hasta autos de lujo para ocasiones especiales, tenemos cabinas para todos los bolsillos', '', 'webstatic/img/ola-article/why-ola-1.jpg', '', 'Wednesday, Oct 11', '18:50:20'),
(2, 'Secure and Safer Rides', 'ركوب آمنة وأكثر أمنا', 'Des promenades sécuritaires et plus sûres', '安全和安全的乘车', 'Paseos seguros y más seguros', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', '', 'السائقين التحقق، زر تنبيه في حالات الطوارئ، وتتبع ركوب الحية هي بعض من الميزات التي لدينا في مكان لضمان تجربة السفر آمنة.', '', 'Des conducteurs vérifiés, un bouton d\'alerte d\'urgence et un système de suivi en direct sont quelques-unes des caractéristiques que nous avons mises en place pour vous assurer une expérience de voyage sécuritaire.', 0, '已验证的驱动程序，紧急警报按钮和直播跟踪是我们确保您安全旅行体验的一些功能。', '', 'Los controladores verificados, un botón de alerta de emergencia y el seguimiento en vivo son algunas de las características que tenemos para garantizarle una experiencia de viaje segura.', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', ''),
(3, 'HogoTaxi Select', 'هوجوتاكسي حدد', 'HogoTaxi Sélectionnez', 'HogoTaxi选择', 'HogoTaxi Select', 'A membership program with HugoTaxi that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time  ', '', 'برنامج العضوية مع هوغوتاكسي التي تمكنك من ركوب رئيس سيدان في فارس ميني، سيارات الأجرة كتاب دون تسعير الذروة، ولها وقت الانتظار صفر', '', 'Un programme d\'adhésion avec HugoTaxi qui vous permet de monter une berline Prime à des tarifs mini, réserver des taxis sans prix de pointe et n\'a pas de temps d\'attente', 0, '与HugoTaxi的会员计划，让您乘坐迷你票价的Prime Sedan，无高峰定价的预订车厢，零等待时间', '', 'Un programa de membresía con HugoTaxi que le permite viajar en un Sedan Prime a tarifas Mini, reservar taxis sin precios pico y no tiene tiempo de espera', '', 'webstatic/img/ola-article/why-ola-2.jpg', '', 'Tuesday, Oct 24', '07:57:12'),
(4, 'In Cab Entertainment', 'في الكابيه الترفيه', 'Dans Cab Entertainment', '在Cab娱乐', 'En Cab Entertainment', 'Play music, watch videos and a lot more with Hugo Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', '', 'تشغيل الموسيقى ومشاهدة أشرطة الفيديو وأكثر من ذلك بكثير مع هوجو اللعب! أيضا البقاء على اتصال حتى لو كنت مسافرا من خلال مناطق الشبكة الفقيرة مع شركائنا في مرفق واي فاي مجانا.', '', 'Jouez de la musique, regardez des vidéos et bien plus encore avec Hugo Play! Restez également connecté même si vous voyagez à travers les zones de réseau pauvres avec notre installation de wifi gratuit.', 0, '用Hugo Play播放音乐，观看视频等等。即使您通过我们的免费无线网络设施访问不良的网络区域，您也可以保持联系。', '', '¡Reproduce música, mira videos y mucho más con Hugo Play! Manténgase conectado incluso si viaja a través de áreas de red pobres con nuestra instalación wifi gratuita.', '', 'webstatic/img/ola-article/why-ola-9.jpg', '', '', ''),
(5, 'Cashless Rides', 'ركوب غير النقدي', 'Rides sans numéraire', '无现金乘坐', 'Viajes sin efectivo', 'Now go cashless and travel easy. Simply recharge your HugoTaxi money or add your credit/debit card to enjoy hassle free payments. ', '', 'الآن الذهاب غير النقدي والسفر سهلة. ببساطة إعادة شحن المال هوغوتاكسي الخاص بك أو إضافة بطاقة الائتمان / الخصم الخاص بك للاستمتاع المدفوعات خالية من المتاعب.', '', 'Maintenant, allez sans argent et voyagez facilement. Simplement recharger votre argent HugoTaxi ou ajouter votre carte de crédit / débit pour profiter des paiements sans tracas.', 0, '现在去无现金，旅行很方便。只需充电您的HugoTaxi钱，或添加您的信用卡/借记卡，以享受免费的付款。', '', 'Ahora vete sin efectivo y viaja tranquilo. Simplemente recargue su dinero HugoTaxi o agregue su tarjeta de crédito / débito para disfrutar de pagos sin complicaciones.', '', 'webstatic/img/ola-article/why-ola-5.jpg', '', 'Tuesday, Oct 24', '07:56:49'),
(6, 'Share and Express', 'حصة والتعبير', 'Partager et Express', '分享和快递', 'Comparta y exprese', 'To travel with the lowest fares choose HugoTaxi Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away! ', '', 'للسفر مع أدنى الأسعار اختيار هوغوتاكسي شير. للحصول على تجربة سفر أسرع لدينا حصة اكسبرس على بعض الطرق الثابتة مع الانحرافات صفر. اختيار رحلتك والتكبير بعيدا!', '', 'Pour voyager avec les tarifs les plus bas choisissez HugoTaxi Share. Pour une expérience de voyage plus rapide, nous avons Share Express sur certaines routes fixes avec zéro écart. Choisissez votre trajet et effectuez un zoom arrière!', 0, '以最低票价旅游，选择雨果塔溪股份。为了更快的旅行体验，我们在一些零偏差的固定路线上拥有Share Express。选择你的骑行并放大！', '', 'Para viajar con las tarifas más bajas, elija HugoTaxi Share. Para una experiencia de viaje más rápida, tenemos Share Express en algunas rutas fijas con cero desviaciones. ¡Elija su viaje y aleje!', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', 'Tuesday, Oct 24', '07:57:31');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `booking-otp`
--
ALTER TABLE `booking-otp`
  ADD PRIMARY KEY (`otp_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_headings`
--
ALTER TABLE `web_headings`
  ADD PRIMARY KEY (`heading_id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking-otp`
--
ALTER TABLE `booking-otp`
  MODIFY `otp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_headings`
--
ALTER TABLE `web_headings`
  MODIFY `heading_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
