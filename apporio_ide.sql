-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:20 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_ide`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir1', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'rsdcdulax', 'rsdcdulax', 'rsdcdulax', '', '', 'rsdcdulax', '+56982428020', 'rsdcdulax@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(33, 'jesus ', 'pajares', 'jph', '', '', 'soloyolose', '992233551', 'jesuspajareshorna@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(34, 'alvin', 'wong', 'alvin123', '', '', '123456', '123123123123', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(35, 'alvin', 'wong', 'alvindispatcher', '', '', '123456', '1293012937', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'IDE', 'uploads/logo/logo_59ddeb14ae5cd.png', 'hello@apporio.com', 'Gurugram, Haryana, India', 'AIzaSyA-ZPu9UbUHaA38AVuJ-czedZz6Wz_kyzk', '28.4594965', '77.0266383', ' currentideapp');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refused to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 1),
(14, 'tesr', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 2),
(2, 'Suzuki', 'uploads/car/car_2.png', 2),
(3, 'Ferrari', 'uploads/car/car_3.png', 2),
(4, 'Lamborghini', 'uploads/car/car_4.png', 2),
(5, 'Mercedes', 'uploads/car/car_5.png', 2),
(6, 'Tesla', 'uploads/car/car_6.png', 2),
(10, 'Renault', 'uploads/car/car_10.jpg', 2),
(11, 'taxi', 'uploads/car/car_11.png', 2),
(12, 'taxi', 'uploads/car/car_12.jpg', 2),
(13, 'yamaha', 'uploads/car/car_13.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', 'webstatic/img/fleet-image/prime-play.png', 1, 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.png', 'webstatic/img/fleet-image/lux.png', 1, 1),
(4, 'Mini', '', 'Mini', 'uploads/car/editcar_4.png', 'webstatic/img/fleet-image/mini.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1',
  `state_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`, `state_id`) VALUES
(56, 'Gurugram', '28.4594965', '77.0266383', 'BRL', 'BRL', '0', 'Km', '', '', 1, 1),
(124, 'Buenos Aires', '-34.6036844', '-58.3815591', 'BRL', 'BRL', '0', 'Km', '', '', 1, 1),
(128, 'Gurugram', '28.4594965', '77.0266383', 'BRL', 'BRL', '0', 'Km', '', '', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `color_id` int(11) NOT NULL,
  `color_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`color_id`, `color_name`, `color_status`) VALUES
(1, 'white', 1),
(2, 'Yellow', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 15, 'k10', 1, 2, 1, 2, 'k10', 'Apporio Infolabs', 'k10', 'apporio@info.com2', '', 'k10', '', 'k10', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(18, 'lkdsasalsa', 'msammsa@qkjsa', 'kddepoew320-', 'saksalask'),
(17, 'qajkalk', 'SAJD@JSAG', '12344555666', 'kaal;al'),
(16, 'Yogesh', 'yogeshkumar2491@gmail.com', '', 'Testing'),
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!'),
(19, 'anurag', 'yuio@wkjlkew', '328740932', 'lfejljfsl'),
(20, 'sdllksd', 'skjs@lkdslk', 'lklkds', 'dlkslk'),
(21, 'dffhh', 'fhhjk@ghgj', '986987587', 'gkkhll'),
(22, 'asqwq', 'qaz@gmail.com', '2321235435', 'ertewtterterttrtrrttr');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD '),
(7, 63, 'BRL', '0', 'BRL');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 2, 'jvnjvjv\n', 'jvnjvjv\n', '1294829569', 'yffjffgh', 'Tuesday, Nov 14, 05:15 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.412219', '77.0432313', '28.4122189', '77.0432303', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:24:59 PM', '03:25:05 PM', '03:25:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(2, 3, '28.4122865', '77.043458', '28.4122865', '77.043458', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '10:23:39 AM', '10:23:43 AM', '10:23:47 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 38, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(3, 4, '-34.4601053', '-58.8687046', '-34.460183', '-58.8686782', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', '11:45:56 PM', '11:46:00 PM', '11:47:52 PM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 41, '115', '4.60', '110.40', '100.00', '0.00', '0.00 Miles', '0.0', '2', '115', 1, '', 1, 1, '0000-00-00'),
(4, 5, '-34.4602563', '-58.8686551', '-34.460236', '-58.8686496', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', '11:50:14 PM', '11:51:08 PM', '11:51:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 41, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(5, 6, '-34.4442677', '-58.8795784', '-34.4448977', '-58.8725444', 'El Callao 100-144, B1631FPB Villa Rosa, Buenos Aires, Argentina', 'Colectora Oeste Ramal Pilar, Pilar Centro, Buenos Aires, Argentina', '12:30:11 AM', '12:30:19 AM', '12:33:13 AM', '0', '0.00', '30.00', '0.00', '0.00', '0.00', 41, '130', '5.20', '124.80', '100.00', '0.00', '0.63 Miles', '0.0', '3', '130', 1, '', 1, 0, '0000-00-00'),
(6, 9, '', '', '', '', '', '', '01:21:50 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 45, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(7, 11, '28.4121057901024', '77.0432252293696', '28.4121226379912', '77.0432581008198', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:27:01 PM', '01:27:33 PM', '01:28:47 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 45, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '1', '178', 1, '', 1, 1, '0000-00-00'),
(8, 12, '28.4121961894144', '77.0432490401599', '28.4119966160771', '77.0433362201574', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:33:37 PM', '01:34:00 PM', '01:37:27 PM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 45, '193', '11.58', '181.42', '178.00', '0.00', '0.00 Miles', '0.0', '3', '193', 1, '', 1, 1, '0000-00-00'),
(9, 12, '28.4121961894144', '77.0432490401599', '28.4119966160771', '77.0433362201574', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:33:48 PM', '01:34:00 PM', '01:37:27 PM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 45, '193', '11.58', '181.42', '178.00', '0.00', '0.00 Miles', '0.0', '3', '193', 1, '', 1, 1, '0000-00-00'),
(10, 13, '28.4120445623875', '77.0432848924212', '28.4120491515456', '77.0432493810782', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:29:30 AM', '05:29:39 AM', '05:29:53 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 45, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(11, 15, '-34.4602678', '-58.8687606', '-34.4602292', '-58.8686994', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', '05:18:44 PM', '05:19:09 PM', '05:20:01 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 37, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(12, 16, '28.412116259697', '77.0432969054864', '28.4120727261305', '77.0432132595898', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:12:43 PM', '12:13:15 PM', '12:13:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 48, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(13, 18, '28.4120462698097', '77.0431856155154', '28.4120865747333', '77.0432446282421', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:21:56 PM', '01:22:03 PM', '01:22:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(14, 19, '28.4120837260692', '77.0432786985464', '28.4120893249644', '77.043299291889', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:24:28 PM', '01:25:22 PM', '01:25:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 1, '0000-00-00'),
(15, 20, '28.4120769391079', '77.0432908247156', '28.4120917557689', '77.0432719887927', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:30:04 PM', '01:30:12 PM', '01:30:17 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(16, 21, '28.4120732289292', '77.0432336503271', '28.4121097917458', '77.0432582303031', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:32:45 PM', '01:32:50 PM', '01:32:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 49, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(17, 23, '28.4121970692914', '77.0433707535984', '28.4121970692914', '77.0433707535984', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:50:28 PM', '12:50:40 PM', '12:50:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 58, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(18, 24, '28.4303919', '77.0411579', '28.4305383', '77.041249', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:43:03 PM', '05:43:23 PM', '05:43:35 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(19, 25, '28.4304981', '77.0412478', '28.4305329', '77.0412575', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:44:51 PM', '05:46:13 PM', '05:47:27 PM', '1', '10.00', '00.00', '0.00', '0.00', '0.00', 68, '110', '4.40', '105.60', '100.00', '0.00', '0.00 Miles', '0.0', '1', '110', 1, '', 1, 1, '0000-00-00'),
(20, 26, '28.4304501', '77.0412664', '28.4305252', '77.0412609', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '05:53:11 PM', '05:53:24 PM', '05:53:33 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(21, 29, '', '', '', '', '', '', '05:59:31 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 68, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(22, 31, '28.4305041', '77.0412707', '28.4304935', '77.0412652', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '06:01:44 PM', '06:01:49 PM', '06:01:59 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(23, 32, '', '', '', '', '', '', '06:03:22 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 68, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(24, 36, '28.4304565', '77.041255', '', '', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '', '06:18:23 PM', '06:18:37 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', 68, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(25, 40, '28.4123284', '77.0434335', '28.4123342', '77.0434337', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '05:25:47 AM', '05:26:04 AM', '05:26:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(26, 41, '28.4123126', '77.0434255', '28.4123132', '77.0434251', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '05:40:11 AM', '05:40:17 AM', '05:40:22 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(27, 42, '28.4123352', '77.0434489', '28.4123352', '77.0434489', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:04:06 AM', '06:04:17 AM', '06:04:20 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(28, 44, '28.412317', '77.0434261', '28.412312', '77.0434515', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:15:47 AM', '06:16:01 AM', '06:16:19 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(29, 47, '28.4123145', '77.0434264', '28.4123407', '77.0434477', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:22:53 AM', '06:23:04 AM', '06:23:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(30, 48, '28.4123601', '77.0434254', '28.4123683', '77.0434126', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:32:01 AM', '06:32:22 AM', '06:32:27 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(31, 50, '28.4122971', '77.0434346', '28.4123244', '77.0434252', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:38:19 AM', '06:39:20 AM', '06:39:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(32, 50, '28.4122971', '77.0434346', '28.4123244', '77.0434252', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:38:39 AM', '06:39:20 AM', '06:39:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(33, 50, '28.4122971', '77.0434346', '28.4123244', '77.0434252', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:39:02 AM', '06:39:20 AM', '06:39:43 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(34, 51, '28.4123487', '77.0434221', '28.4123487', '77.0434221', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:46:18 AM', '06:46:24 AM', '06:46:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 68, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(35, 52, '28.4123402', '77.0434523', '28.4123407', '77.0434524', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '11:28:48 AM', '11:28:51 AM', '11:29:00 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 69, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(36, 53, '28.4123375', '77.0434505', '28.4123399', '77.0434482', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '11:38:20 AM', '11:38:25 AM', '11:40:19 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 69, '115', '4.60', '110.40', '100.00', '0.00', '0.00 Miles', '0.0', '2', '115', 1, '', 1, 0, '0000-00-00'),
(37, 54, '28.4306699', '77.0414282', '28.4306699', '77.0414282', '692, Himalaya Rd, Medicity, Islampur Colony, Sector 38, Gurugram, Haryana 122005, India', '692, Himalaya Rd, Medicity, Islampur Colony, Sector 38, Gurugram, Haryana 122005, India', '06:45:49 PM', '06:46:02 PM', '06:46:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 80, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(38, 57, '', '', '', '', '', '', '07:48:42 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 81, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(39, 58, '28.4122131', '77.043403', '28.4122584', '77.0434094', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:11:19 AM', '06:11:24 AM', '06:11:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 85, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(40, 59, '28.4123069', '77.0433974', '28.4122855', '77.0434037', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:42:03 AM', '06:42:06 AM', '06:43:00 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 92, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(41, 60, '28.4122632', '77.0433873', '28.4122581', '77.043385', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:12:37 PM', '12:13:02 PM', '12:13:14 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 98, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(42, 61, '28.4122924', '77.0434118', '28.4122943', '77.0434154', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:27:23 PM', '12:27:38 PM', '12:28:28 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 98, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(43, 62, '28.4123005', '77.0434138', '28.4122992', '77.0434112', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:49:50 PM', '12:49:52 PM', '12:49:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 98, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(44, 63, '28.4122747', '77.0434262', '28.4122747', '77.0434262', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:53:33 PM', '12:53:42 PM', '12:53:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 98, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(45, 64, '28.4123297', '77.043408', '28.4123519', '77.0434242', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '12:57:18 PM', '12:57:26 PM', '12:57:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 98, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_lastname` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `car_color_name` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0',
  `driver_state_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_lastname`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `car_color_name`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`, `driver_state_id`) VALUES
(1, 0, 6, 'mahi', '', 'lmahi@gmail.com', '9807452580', '', '123456', 'qljTSmzURVD4Y199', '167.32', '10.68', '', 'fTiKSqgDphQ:APA91bG0kSMQX7_hL4ygG4ad432YKpNrqLTYW0x3e7c967zds_OCVO0HwP15t6D9wRxnAeDB7orAozpkaCB4GqABG5vFYDCyL3tOGTPyNHp7YUk3F2GC01NPSB7I7xvH3UgHUUuzPU-z', 2, '5', 4, 29, 'asdf', '0', 56, 'Tuesday, Oct 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121697', '77.0431898', '----', '11:01', 'Thursday, Oct 12, 2017', 1, 0, 1, 2, 0, 2, 3, 0, '', 0, '', '2017-10-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 0),
(2, 0, 12, 'demo', 'ab1', '', '1234', 'uploads/driver/1507874233driver_2.jpg', 'apporio7788', '', '0', '0', '', '', 0, '', 2, 3, '133', 'blue', 56, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-12', 1, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3, 0),
(6, 0, 0, 'demoo', '', '', '908549999', '', '123456', 'VgN7Ujf94lo1YOkZ', '0', '0', '', '', 0, '', 27, 44, 'asdf', '123456', 123, 'Thursday, Oct 12', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-10-12', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 12345),
(45, 0, 6, 'Atul', '', 'atul@apporio.com', '1234567890', 'uploads/driver/1509542398driver_45.jpg', 'qwerty', 'KkAd6itMEq4lKXcq', '516.06', '32.94', '', '550EEFD0DA32339CBC456D93CB51CB3E5F407DD59CB41D0875C02E616DD2349A', 1, '3', 4, 11, '1234', 'red', 56, 'Wednesday, Nov 1', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120538092616', '77.0432524144092', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '06:31', 'Friday, Nov 10, 2017', 3, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(44, 0, 5, 'ghjk', '', '9@9.com', '1472580369', 'uploads/driver/1509539292driver_44.jpg', 'qwerty', 'd24tlHMjYiK7b9xg', '0', '0', '', 'A58F05BD9D664064CB83ABD314C8DF672C1C5BA0BD3C424ABB36224009224D37', 1, '', 3, 20, '1234', 'red', 56, 'Wednesday, Nov 1', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120662091317', '77.0432578327323', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '01:14', 'Wednesday, Nov 1, 2017', 0, 0, 0, 1, 1, 1, 2, 0, '', 1, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(46, 0, 4, 'Tatiana', '', 'tatianaalves@flaconsultoriasrl.com.ar', '5197553631', 'uploads/driver/1509706357driver_46.jpg', 'tatiana', 'H4gVU0huWMuSlfFf', '0', '0', '', 'A1F253258FA57F1BE9AF999DCD2D65C6957B030707FE352FD777F13F43AD1E44', 1, '', 2, 3, 'obediÃªncia', 'vermelho', 56, 'Friday, Nov 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4600861706161', '-58.8687148411375', 'Calle Muzilli 1170 , Pilar Centro, Buenos Aires', '01:00', 'Monday, Nov 6, 2017', 0, 0, 0, 1, 0, 2, 2, 0, '', 1, '', '2017-11-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 2),
(43, 0, 6, 'name', '', 'y@9.com', '1234567809', 'uploads/driver/1510214265driver_43.jpg', 'qwerty', 'tLxMpHvtNxKTvOfx', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 4, 11, '1234', 'red', 56, 'Wednesday, Nov 1', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120912162609', '77.0432778315521', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:58', 'Thursday, Nov 9, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(42, 0, 5, 'you', '', 'y@9.com', '4567890123', 'uploads/driver/1509521380driver_42.jpg', 'qwerty', 'gIqhmlrc4F5kvnnm', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 3, 2, '1234', 'red', 56, 'Wednesday, Nov 1', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120699327412', '77.0432590480596', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '11:37', 'Wednesday, Nov 1, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-01', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(41, 0, 4, 'vilmar', '', 'vilmarjralves@yahoo.com', '5199984845', '', 'ide1234', 'vd5d9pppouP0xlgX', '331.2', '13.8', '', 'fPoFMv-bwD0:APA91bGxSX1dxwTWkNUGbqXQepfNA-83gpbkcHfq8DEyHhJoTR_x4CuMe2eDNXZWHbloL_0_1TpaahkR1FHp-c9eJlqK6IlwKULP60p5nyk0yQaUNK0lz77qMCWpinszYOncGiZfzwz9', 2, '3.33333333333', 2, 3, '1uee222', '', 56, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4602004', '-58.8687776', '----', '18:54', 'Sunday, Oct 29, 2017', 3, 0, 0, 1, 0, 2, 2, 0, '', 1, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(40, 0, 4, 'vilmar', '', 'vilmarjralves@yahoo.com', '519908159', '', 'Ju061187', 'ugfb77VDKALKH0JR', '0', '0', '', '', 0, '', 2, 3, 'qwe213', '', 56, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(39, 0, 0, 'vilmar', '', 'vilmarjralves@gmail.com', '5199085159', '', 'Ju061187', 'x6FONu4CBeNH6ZgU', '0', '0', '', '', 0, '', 0, 0, 'qwe2134', '', 124, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 0, 1),
(38, 0, 6, 'zzz', '', 'zzz@gmail.com', '7878787878', '', '123456', 'Afpb8gnpezJwAiqg', '167.32', '10.68', '', 'couAu1_PH64:APA91bGWZpomhHGvAlT8_Y8nuaTiIzWKigqZbkXHksHXkdlUDZuGitL--aWQ5bJGwcbzdc1V8UFpWUNEQRWpcjwP9H3lkLw58ogFgl2FvHq4J2zdQDU32CjaHlkFGqqRNTBb1Pc2s1Z4', 2, '2.5', 4, 11, 'asdf', '', 56, 'Thursday, Oct 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122864', '77.0434582', '----', '10:23', 'Thursday, Oct 26, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-10-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(37, 0, 10, 'vilmar', '', 'vilmarjralves@gmail.com', '1194949494', '', 'ide1234', 'uEyg25YH3X9PdL8K', '90', '10', '', 'dSen-Z2RAio:APA91bEbtrn3iR84gHuz-_PdrT5_z7xmH0kRucfdN3_iy7Ljc0IfLT9qW8DPrklqU4-e8m9zyiYxa1_D1r26yr4jVnrkctuVda3LACfF8cHS1VaMFhVv56nCMZOCNeFNc_gxhF2Qyct6', 2, '5', 2, 3, 'abd1234', '', 124, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4589508', '-58.8692363', '----', '02:07', 'Tuesday, Nov 14, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 0, 1),
(36, 0, 10, 'Vilmar junior', '', 'vilmarjralves@yahoo.com', '1169178496', 'uploads/driver/1508114501driver_36.jpg', 'ide1234', '4PcDxbaxxqfaz7LV', '0', '0', '', 'fwL__j5WRB4:APA91bG4ydOYVkhttRVei4Rr6o1_R1UWO5Y1T3tDDcBS5cZ2MMJ3jvbK9rKFy1meYS-BIOcqD6v9VU8CLh6iP2L8V_t7GHldAnTmekzJVB2eKwZlU_QG_B8LktS_TRdm8gVfx8eoU--A', 2, '', 2, 3, 'teste123', '', 124, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-37.1149887', '-56.8542867', '----', '02:39', 'Monday, Oct 16, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 0, 1),
(35, 0, 0, 'rohit', '', 'rbalhra999@gmail.com', 'skjdf', '', 'd', 'n1wrRriAjjXk7RVE', '0', '0', '', '', 0, '', 1, 2, '2', '', 1, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 2),
(34, 0, 0, 'demo', '', 'ddd@gmail.com', '5847896580', '', '123456', 'HK95f1X54IRolrDn', '0', '0', '', '', 0, '', 0, 0, 'asdf', '', 0, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 0),
(33, 0, 12, 'rohit', 'balhra', 'rohit@apporio.com', '13324', 'uploads/driver/1507893757driver_33.png', 'apporio7788', '', '0', '0', '', '', 0, '', 3, 20, '214325', 'blue', 124, 'Friday, Oct 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 0, 0, '', 0, '', '2017-10-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0, 1),
(47, 0, 4, 'aaa', '', 'aa@gmail.com', '8080808052', 'uploads/driver/1509773155driver_47.jpg', '123456', 'wfGQK0nr3yjPxw2v', '0', '0', '', 'eMK2wC-8WDU:APA91bFYJsMDSgFduCKk16RT5inkR6ei_Yth4SYGCw3AOBxRhzptZ9rT-ekP-4XFeRg-hSZkxx1Nnf2QbMpUt2PM43_wIrOCLwbgyf70vkvGrv5TNByeRGtFsdlkuV9HvqmFGdNd8N1-', 2, '', 2, 3, 'asdf', '', 56, 'Saturday, Nov 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123361', '77.0434371', '----', '05:26', 'Saturday, Nov 4, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(48, 0, 4, 'poiy', '', '0@9.com', '1234567098', 'uploads/driver/1510141896driver_48.jpg', 'qwerty', 'PDAFcVqPhoioLPB8', '96', '4', '', 'D98E415F086912D26C1A9960C26F64BB9026EA649DC086A1962C34453099AB60', 1, '0', 2, 3, '1234', 'Red', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120638464864', '77.0432435427532', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:13', 'Wednesday, Nov 8, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(49, 0, 4, 'jdjxn', '', '7@90.com', '1234563214', 'uploads/driver/1510146354driver_49.jpg', 'qwerty', 'pbWf3eDKisDvOWWQ', '384', '16', '', 'D98E415F086912D26C1A9960C26F64BB9026EA649DC086A1962C34453099AB60', 1, '3.16666666667', 2, 3, '1234', 'Red', 56, 'Wednesday, Nov 8', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120946673115', '77.0432264787999', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '06:52', 'Friday, Nov 10, 2017', 4, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-08', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(50, 0, 4, 'add', '', '8@0.com', '12345678121', 'uploads/driver/1510218032driver_50.jpg', 'qwerty', 'rDbrjNZH7QK3iJF4', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 2, 3, '1234', 'Red', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120836028655', '77.0432836195322', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:04', 'Thursday, Nov 9, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(51, 0, 4, 'you', '', '80@0.com', '1234561234', 'uploads/driver/1510218330driver_51.jpg', 'qwerty', 'dv9Z5bolrS5MNaPM', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 2, 3, '1234', 'Red', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120848775175', '77.0432403311851', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:05', 'Thursday, Nov 9, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(52, 0, 4, 'bans', '', '0@90.com', '12345671234', 'uploads/driver/1510218405driver_52.jpg', 'qwerty', 'i6IJtdD39FQjWIV5', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 2, 3, '1234', 'Red', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120848775175', '77.0432403311851', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:07', 'Thursday, Nov 9, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(53, 0, 4, 'bans', '', '90@90.com', '123456123456', 'uploads/driver/1510218481driver_53.jpg', 'qwerty', '5XKS9B69XmtcOZFA', '0', '0', '', 'FBB49F721601CC073EC9B670AFA23394B9F89DF1AC3659BB63E7B21B49F1ED37', 1, '', 2, 3, '1234', 'Red', 56, 'Thursday, Nov 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '09:08', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 2),
(54, 0, 5, 'dÃ©chire', '', '8@0.com', '123456712345', 'uploads/driver/1510297609driver_54.jpg', 'qwerty', 'h54L7f2zX22vAkgs', '0', '0', '', 'C35F55A1AFD43EE099E1D5BBBECED1C2D55C8D513823CC313DE9110E34F3B0C4', 1, '', 3, 31, '1234', 'red', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120479133245', '77.0432515629353', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:07', 'Friday, Nov 10, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(55, 0, 4, 'rhdgd', '', '6@9.com', '1234567123', 'uploads/driver/1510297889driver_55.jpg', 'qwerty', 'Kq1NPsBjf1sDAMgm', '0', '0', '', 'C35F55A1AFD43EE099E1D5BBBECED1C2D55C8D513823CC313DE9110E34F3B0C4', 1, '', 2, 3, '1234', 'red', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120752830278', '77.0432824075398', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:14', 'Friday, Nov 10, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(56, 0, 4, '56', '', 'rbalhra999@gmail.com', '8950200340', '', '12345', '3aDGUZZ6frchI3WW', '0', '0', '', '', 0, '', 2, 1, 'slkfjldsjflkds', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 2),
(57, 0, 4, '56', '', 'rbaelhra999@gmail.com', '89502010340', '', '12345', 'gRwtE9RxZfqjjiMU', '0', '0', '', '', 0, '', 2, 1, 'slkfjldsjflkds', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 2),
(58, 0, 6, 'xbxhdu', '', '9@0.com', '2580147369', 'uploads/driver/1510316090driver_58.jpg', 'qwerty', 'lMlDJnXk7pcdCVf5', '167.32', '10.68', '', 'C35F55A1AFD43EE099E1D5BBBECED1C2D55C8D513823CC313DE9110E34F3B0C4', 1, '0', 4, 29, '1234', 'red', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121970692914', '77.0433707535984', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:51', 'Friday, Nov 10, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(59, 0, 4, 'teet', '', 'teet@gmail.com', '25635866988', '', '123456', 'twiNAuWpuqXNtUCO', '0', '0', '', 'fV-4ey0YyGc:APA91bHkm41SXCRt7vO_sjSUTZuhbqI0S5PhezoMw77r0B0-hsWrKOS5xhDXw1HzQ-T2--3ATgCPtFbRqq_6KIiQiLVzd9XT-JAtWJD2Ay6ge0W2MnK9D1Ps0uPMsS15E8onN18wB6zv', 2, '', 2, 3, 'dyhcbj', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4306659', '77.0414139', 'Not yet fetched', '07:18', 'Friday, Nov 10, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(60, 0, 4, 'qqq', '', 'qq@gmail.com', '896586686', '', '123456', 'KzYKJqh17YiqPA9Z', '0', '0', '', 'fV-4ey0YyGc:APA91bHkm41SXCRt7vO_sjSUTZuhbqI0S5PhezoMw77r0B0-hsWrKOS5xhDXw1HzQ-T2--3ATgCPtFbRqq_6KIiQiLVzd9XT-JAtWJD2Ay6ge0W2MnK9D1Ps0uPMsS15E8onN18wB6zv', 2, '', 2, 3, 'qwettt', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4306699', '77.0414282', '----', '07:35', 'Friday, Nov 10, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(61, 0, 4, 'teet', '', 'teet@gmail.com', '256358667878', '', '123456', 'JAVhgrLMC9JPgy0O', '0', '0', '', '', 0, '', 2, 3, 'dyhcbj', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(62, 0, 4, 'qqq', '', 'er@gmail.com', '5685666258', 'uploads/driver/1510344423driver_62.jpg', '123456', '9WnKKEg7LWYb05l7', '0', '0', '', 'fV-4ey0YyGc:APA91bHkm41SXCRt7vO_sjSUTZuhbqI0S5PhezoMw77r0B0-hsWrKOS5xhDXw1HzQ-T2--3ATgCPtFbRqq_6KIiQiLVzd9XT-JAtWJD2Ay6ge0W2MnK9D1Ps0uPMsS15E8onN18wB6zv', 2, '', 2, 3, 'eryubk', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4306532', '77.0414038', '----', '08:23', 'Friday, Nov 10, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(63, 0, 4, 'qqq', '', 'er@gmail.com', '56995666258', '', '123456', '9erZGCEtQAiNkW5A', '0', '0', '', '', 0, '', 2, 3, 'eryubk', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(64, 0, 4, 'rrr', '', 'err@gmail.com', '6589656666', 'uploads/driver/1510509929driver_64.jpg', '123456', 'orKb4kZDTEUbZ1zQ', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'dyuuj', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4304211', '77.0412525', '----', '05:32', 'Monday, Nov 13, 2017', 0, 0, 0, 2, 0, 1, 3, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(65, 0, 4, 'rrr', '', 'err@gmail.com', '6589659999966', '', '123456', 'keb9gpPeTQXafcS5', '0', '0', '', '', 0, '', 2, 3, 'dyuuj', '', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(66, 0, 4, 'puja', '', 'puja@gmail.com', '98764635365', '', '123456', 'K6xeZgmK0JOBj89Y', '0', '0', '', '', 0, '', 2, 3, '152gd7wj', 'white', 56, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(67, 0, 5, 'Tatiana', '', 'Tatianamlalves@gmail.com', '51997553631', 'uploads/driver/1510585607driver_67.jpg', 'tatiana', 'f8CywTFd38pnUIi9', '0', '0', '', '0BFDA3B91DCB71267702C399FCC595B396B8171725D551DADA40178672EF9B35', 1, '', 3, 2, 'nadie', 'vermelho', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4601608576308', '-58.868833614182', ' , Pilar, Buenos Aires', '08:55', 'Wednesday, Nov 15, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(68, 0, 4, 'testing', '', 'test@gmail.com', '94553698566', 'uploads/driver/1510642231driver_68.jpg', '123456', '0yDI0YVkTlI2l5TB', '1257.6', '52.4', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '1.875', 2, 3, 'eryfghh', '', 56, 'Monday, Nov 13', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123252', '77.0434249', '----', '06:53', 'Tuesday, Nov 14, 2017', 13, 1, 5, 2, 0, 1, 3, 0, '', 0, '', '2017-11-13', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(69, 0, 4, 'anurag', '', 'upanu1012@gmail.com', '8874531856', 'uploads/driver/1510659106driver_69.jpg', 'qwerty', 'QF9XQaEvI2nxDXVN', '206.4', '8.6', '', 'fbAqWFTn8uc:APA91bFUY2jTefT-laV9e2-FHn4nghsHeYHyunrfwyGJwYW0vM8FY5QOXj64NlJPA9tZWSUm02cy8ggDvoQdt1NAvdZWMplk8_BpE-WTn13xj5rKtMWJMoz6Mbxx_zyDQmVCN09WqErK', 2, '2.5', 2, 3, 'qty', '', 56, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123373', '77.0434479', '----', '11:45', 'Tuesday, Nov 14, 2017', 2, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(70, 0, 6, 'tatiana', '', 'tatianamlalves@gmail.com', '5497553631', '', 'tatiana', 'Akg6yYOpDTuDVre3', '0', '0', '', 'dnJlZ0qkOeA:APA91bEcD_JwN7myn-OWAONG6k0G58vslgvqZ7Y-vUfa-TPpfh08ZcuaIday3z3UHJfcxmjPX4a1DBcEPeJ6Y9WAHV-9XEGl03sBsjyUKltjR25oD9hBb3mkqBXDCu8pj3BzGvzaBVQU', 2, '', 4, 29, 'f384', '', 56, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.460251', '-58.868744', '----', '10:26', 'Tuesday, Nov 14, 2017', 0, 0, 0, 2, 0, 2, 3, 0, '', 0, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(71, 0, 5, 'Tatiana', '', 'tatianamlalves@gmail.com', '55999995555', 'uploads/driver/1510779491driver_71.jpg', 'tatiana', 'wN2IkMNhyat9EjOV', '0', '0', '', '0BFDA3B91DCB71267702C399FCC595B396B8171725D551DADA40178672EF9B35', 1, '', 3, 20, 'ndle9383$,!', 'jdndi88', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4602657971424', '-58.8689567303889', ' , Pilar, Buenos Aires', '10:54', 'Saturday, Nov 25, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 3),
(72, 0, 4, 'ideApp', '', 'puja@g.com', '5286658655', '', '123456', 'adT2llzAYxzc3bvZ', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'wryhvct', '', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123506', '77.0434412', '----', '12:10', 'Saturday, Nov 18, 2017', 0, 0, 0, 2, 0, 1, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(73, 0, 4, 'hcjfhf', '', 'er@g.com', '685255685', '', '123456', 'JdAmHBJ5LwLXvyQg', '0', '0', '', '', 0, '', 2, 3, 'ycufhc', '', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(74, 0, 4, 'gchgufo', '', 'qw@g.com', '258556655445', '', '123456', '15sh6LpzEX7T3Hwz', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'jgifyf', '', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123549', '77.0434633', '----', '01:38', 'Saturday, Nov 18, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(75, 0, 5, 'lajfjsd', '', 'absfjdsf@gmail.com', '983274874', 'uploads/driver/1511010170driver_75.jpg', 'qwerty', 'Tql7ke1ZAtEFCCsw', '0', '0', '', '', 0, '', 1, 2, '123', '', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 2),
(76, 0, 4, 'pooojaaaaa', '', 'po@g.com', '256325632669', 'uploads/driver/1511013428driver_76.jpg', '123456', 'htpwIou1pNwBYEOB', '0', '0', '', '', 0, '', 2, 3, 'wetyijgj', 'white', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(77, 0, 4, 'pooojaaaaa', '', 'po@g.com', '256325632666', 'uploads/driver/1511013493driver_77.jpg', '123456', 'ddDq6KRNcmvJDaIL', '0', '0', '', '', 0, '', 2, 3, 'wetyijgj', 'white', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(78, 0, 4, 'newhsaja', '', 'home@g.com', '364646667676', 'uploads/driver/1511013873driver_78.jpg', '123456', 'Dmv7EkA21jF3Yd7M', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'gzhzhjsjsj', 'white', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123549', '77.0434346', '----', '02:05', 'Saturday, Nov 18, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(79, 0, 4, 'gshshs', '', 'rt@g.com', '6446646646', 'uploads/driver/1511025264driver_79.jpg', '123456', 'uo1QDmwaF1Awz1ju', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'gsysyhshs', 'syshsjsjsj', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4308985', '77.0413023', '----', '05:34', 'Saturday, Nov 18, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(80, 0, 4, 'I rutjfu', '', 'qwt@g.com', '2588555556', 'uploads/driver/1511030429driver_80.jpg', '123456', 'NlkcLCSfMKeYU2cb', '96', '4', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '1.75', 2, 3, 'tsuffkfdi', 'jgigfufur', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122423', '77.0434453', '----', '02:17', 'Monday, Nov 20, 2017', 1, 0, 0, 2, 0, 1, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(81, 0, 4, 'jsjsjs', '', 'dhdh@jdjsj.com', '499494949494', 'uploads/driver/1511034647driver_81.jpg', '123456', 'H6RRa2P8BI6JMwE2', '0', '0', '', 'fixJ9bralXw:APA91bG1MVZsYBdhgpcMf4GsKa-4ILU4S-pMrIPuPhnwNXAcup5jQQEoP-cDerHQEmwzIZBckiFgRAbXMq_TIrCLArD-cWj7A2GOaYFB1VvKdJZeEfZMIlIqixTau-yYLWEi2f1LSwgo', 2, '', 2, 3, '13434664', 'blue', 56, 'Saturday, Nov 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.448023', '77.0604829', '', '19:48', 'Saturday, Nov 18, 2017', 0, 0, 2, 1, 0, 1, 3, 0, '', 0, '', '2017-11-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(82, 0, 4, 'igihbiikhibih', '', 'yvugu@uvugu', '286827272727', 'uploads/driver/1511162965driver_82.jpg', '1234567890', '2dmxERw59UZ9npxb', '0', '0', '', 'eSldXYjML90:APA91bHa6GdfTg5X-IXooQyb8XapZz0M4sk2pA9cN6m2yGIHpo7_XdnVl-HC6iJkGQvS1vi1YNQMPS0Nei1QpsMWWsHAv0j8ENNlHkNzFFd_FqbrkUTHuDOE1ll7irYdpsEx0wjyHLXB', 2, '', 2, 3, 'yfugug', 'red', 56, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123034', '77.04341', '----', '07:46', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(83, 0, 4, 'uuyvvug', '', 'tctc@jbjvuv', '572727242727', 'uploads/driver/1511164138driver_83.jpg', 'qwerty', 'okQeRZvfzSEN0GdH', '0', '0', '', 'eSldXYjML90:APA91bHa6GdfTg5X-IXooQyb8XapZz0M4sk2pA9cN6m2yGIHpo7_XdnVl-HC6iJkGQvS1vi1YNQMPS0Nei1QpsMWWsHAv0j8ENNlHkNzFFd_FqbrkUTHuDOE1ll7irYdpsEx0wjyHLXB', 2, '', 2, 3, 'igibug', 'hbhvh', 56, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122797', '77.0433942', '----', '07:55', 'Monday, Nov 20, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(84, 0, 4, 'zvusjs', '', 'bH@g.com', '573787649464', 'uploads/driver/1511187558driver_84.jpg', '123456', 'XOb7q0rgoz7s2Iuq', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'gYHahaja', 'eahJJ', 56, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121871', '77.0434109', '----', '06:15', 'Tuesday, Nov 21, 2017', 0, 0, 0, 2, 0, 1, 3, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(100, 0, 10, 'Tatiana', '', 'tatianamlalves@gmail.com', '5199999999', 'uploads/driver/1511924603driver_100.jpg', 'tatiana', 'tv4ElOkvIxa2rQci', '0', '0', '', '0BFDA3B91DCB71267702C399FCC595B396B8171725D551DADA40178672EF9B35', 1, '', 3, 20, 'Jorge', 'bhhv', 124, 'Wednesday, Nov 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-34.4601924072644', '-58.8689170487396', ' , Pilar, Buenos Aires', '03:31', 'Wednesday, Nov 29, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 0, 3),
(85, 0, 4, 'hhhvhv', '', '12@g.com', '257566838588', 'uploads/driver/1511244621driver_85.jpg', '123456', 'a1uP88h0t6qHmMoi', '96', '4', '', 'dZmW4hLXAnY:APA91bHsgWOdeCTCKXTQRq15YoybDUmk_IzXkGrXbuKxxNuTbpHuWTA2zbiY-vKDHotKfOdCSZ45u7K7HQl8GRh0WxgO437IkB7lcWzuj6-kkEr-sLDVhYF8CUO00nU1VaxMSRyz-Bxt', 2, '4.5', 2, 3, 'd8fyfoguifyi', 'white', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122028', '77.0434041', '----', '06:28', 'Tuesday, Nov 21, 2017', 1, 0, 0, 1, 0, 1, 3, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(86, 0, 4, 'hfhfgu', '', 'qwrr@g.com', '653555868', 'uploads/driver/1511245027driver_86.jpg', '123456', 'satXN3pGXCTKvHLt', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'jgigg', 'ydufugig', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122547', '77.0434185', '----', '06:35', 'Tuesday, Nov 21, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(87, 0, 4, 'ugugufgjg', '', 'qwr@g.com', '2535888585', 'uploads/driver/1511246329driver_87.jpg', '123456', 'UiPGZaJLESC9UNTW', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'hcycuf', 'txug', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123062', '77.0433957', '----', '06:52', 'Tuesday, Nov 21, 2017', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(88, 0, 4, 'ydydu', '', 'qwt@g.com', '65355286563', '', '123456', 'H4fCQFSSYX5i8K3J', '0', '0', '', '', 0, '', 2, 3, 'ufuhoh', 'wfhyjki', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0, 1),
(89, 0, 4, 'ydydu', '', 'qwt@g.com', '3333333333', 'uploads/driver/1511247612driver_89.jpg', '123456', 'yxpw4pfK56Cdf9Wg', '0', '0', '', '', 0, '', 2, 3, 'ufuhoh', 'wfhyjki', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(90, 0, 4, 'fcfbth', '', 'ert@g.com', '488229262', 'uploads/driver/1511248270driver_90.jpg', '123456', 'jcZ54xqBIe7yQjE7', '0', '0', '', '', 0, '', 2, 3, 'dcrbyj', 'rvfbyb', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(91, 0, 4, 'khxgd', '', 'utsitd@g.com', '68383285823', 'uploads/driver/1511248457driver_91.jpg', '123456', 'WKv74ra3XY4rCEFQ', '0', '0', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '', 2, 3, 'jgxtux', 'iycoyf', 56, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122535', '77.0434038', '----', '11:24', 'Wednesday, Nov 22, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(92, 0, 4, 'puja', '', 'poh@g.com', '65465436935', 'uploads/driver/1511332850driver_92.jpg', '123456', 'RlZFr15DqHgdxfq9', '96', '4', '', 'eRTFN__FGCA:APA91bG-tkF_jNVpigd0PAKzVBpmPImRuAzo-fa5d1WJL4u9csxp5pU7DP_462orcd-RKetAJDyYiyoC0x5DczZSxIYTk8Nl9ec7yWUXvC7EWmx-RFd_zZOTaGPDydG1TdAFo5tNLJ6r', 2, '', 2, 3, 'uiuffykug', 'white', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122223', '77.043435', '----', '07:45', 'Wednesday, Nov 22, 2017', 1, 0, 0, 2, 0, 1, 3, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(93, 0, 4, '5d6fyfifi', '', '23@g.com', '6633838535', 'uploads/driver/1511336807driver_93.jpg', '123456', 'rIEnjaabwH7yr7Yl', '0', '0', '', '', 0, '', 2, 3, 'tufuyfyy', 'uyfugi', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(94, 0, 4, 'yfudyxyxhf', '', 'tzyd@hxufyd', '68382782', 'uploads/driver/1511337384driver_94.jpg', 'qwerty', 'A7IOHFtUP9jq35FJ', '0', '0', '', 'fxFBDbxMcjI:APA91bHqIruqPxGHdzBSibKA-YY_bLts-z8zeM5FE1yHNIp_Z0DKJ6-TBw54hKpYwwEmULI49tlSoOTbcg0JVAD1u21ssry6CVMBqg7LAVnom6Odi9N6guwurEu3nUOsowzJpEqUwoX3', 2, '', 2, 3, 'gdyd', 'red', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123127', '77.0434303', '----', '07:58', 'Wednesday, Nov 22, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(95, 0, 4, 'fugg', '', 'asd@gmsil', '8382882', 'uploads/driver/1511337595driver_95.jpg', 'qwerty', 'qtrNweydIWGsXPE9', '0', '0', '', 'fxFBDbxMcjI:APA91bHqIruqPxGHdzBSibKA-YY_bLts-z8zeM5FE1yHNIp_Z0DKJ6-TBw54hKpYwwEmULI49tlSoOTbcg0JVAD1u21ssry6CVMBqg7LAVnom6Odi9N6guwurEu3nUOsowzJpEqUwoX3', 2, '', 2, 3, 'txcyg', 'red', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122617', '77.0434389', '----', '08:17', 'Wednesday, Nov 22, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(96, 0, 4, 'yfyfhb', '', 'er@g.com', '586868256', 'uploads/driver/1511341214driver_96.jpg', '123456', 'SFjvP4Aim9pLv50o', '0', '0', '', '', 0, '', 2, 3, 'tctcgy', 'tvuhhh', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(97, 0, 4, 'hchv', '', 'gxgxycuv@g.com', '68356828', 'uploads/driver/1511350585driver_97.jpg', '123456', '1RBsVxdklJC7wKiH', '0', '0', '', '', 0, '', 2, 3, 'yfdfu', 'ucuf', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(98, 0, 4, 'ihxhoc', '', 'dwr@g.com', '32835506', 'uploads/driver/1511350744driver_98.jpg', '123456', 'YokhCwF4mKqDdlMS', '576', '24', '', 'fV-4ey0YyGc:APA91bGRbSWzz4igKn8JUeAUxovZiIS1vE2HHEy7tAkyg2QK7ILxGnNaav1KptJSsCAYQjwTzFNUbCzG4sDrwtx_MozqYTFoabvL4v8wd5Lys0Yc5mEOVkJBRwCmWgMYxEBhF6JllLI3', 2, '1.33333333333', 2, 3, 'ufffxi', 'yfuhiiycgkxgx', 56, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122683', '77.0434061', '----', '05:30', 'Friday, Nov 24, 2017', 6, 0, 0, 1, 0, 1, 3, 0, '', 0, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1),
(99, 0, 4, 'vilmar', '', 'ide@ide-c.com', '51998035159', '', 'ide1234', 'Orx9NYkbWFC8620g', '0', '0', '', 'e8Htho0dnpE:APA91bFPSBmWGwVk_hdVKP2d0pmhc97j08ZzTuoaOWxJgwgGFGmo5yrzYmaLdDwS6UKa2ZiTx-lGMOG3YguSmq7_2KYDsqycZr8EE0D0423HGvm6Iq6XufWI8gJvk75F0Gyiy1tiR3pW', 2, '', 2, 3, 'dhd1234', '', 56, 'Thursday, Nov 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-27.0986269', '-52.6227973', '----', '12:51', 'Thursday, Nov 23, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '178', 1, '167.32', '10.68', '2017-10-10'),
(2, 38, '178', 1, '167.32', '10.68', '2017-10-26'),
(3, 41, '345', 3, '331.2', '13.8', '2017-10-28'),
(4, 45, '371', 2, '348.74', '22.26', '2017-11-01'),
(5, 45, '178', 1, '167.32', '10.68', '2017-11-02'),
(6, 37, '100', 1, '90.00', '10.00', '2017-11-05'),
(7, 48, '100', 1, '96.00', '4.00', '2017-11-08'),
(8, 49, '400', 4, '384', '16', '2017-11-08'),
(9, 58, '178', 1, '167.32', '10.68', '2017-11-10'),
(10, 68, '610', 6, '585.6', '24.4', '2017-11-13'),
(11, 68, '700', 7, '672', '28', '2017-11-14'),
(12, 69, '215', 2, '206.4', '8.6', '2017-11-14'),
(13, 80, '100', 1, '96.00', '4.00', '2017-11-18'),
(14, 85, '100', 1, '96.00', '4.00', '2017-11-21'),
(15, 92, '100', 1, '96.00', '4.00', '2017-11-22'),
(16, 98, '600', 6, '576', '24', '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 2, 1),
(2, 38, 23, 1),
(3, 41, 6, 1),
(4, 44, 7, 1),
(5, 45, 13, 1),
(6, 43, 10, 1),
(7, 37, 15, 1),
(8, 48, 64, 1),
(9, 58, 23, 1),
(10, 68, 51, 1),
(11, 69, 64, 1),
(12, 80, 57, 1),
(13, 81, 64, 1),
(14, 83, 64, 1),
(15, 84, 58, 1),
(16, 85, 64, 1),
(17, 91, 59, 1),
(18, 92, 59, 1),
(19, 98, 64, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Login efetuado com sucesso', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Usuário Inativo', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Campos obrigatórios incompletos', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email ou senha incorretos', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'Logout efetuado com sucesso', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Nenhum registro econtrado', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Signup com sucesso', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'Telefone j existe', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email já existe', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Falta cópia do RG', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Falta cópia da habilitação', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Falta cópia do seguro', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Senha modificada', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, 'Senha antiga incorreta', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, 'Código de cupom inválido', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Cupom aplicado com sucesso', 2),
(33, 17, 'User not exist', 1),
(34, 17, 'Usuário não existe', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Atualizado com sucesso', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'Telefone já existe', 2),
(39, 20, 'Online', 1),
(40, 20, 'OnLine', 2),
(41, 21, 'Offline', 1),
(42, 21, 'OffLine', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, 'OTP enviado para telefone para verificação', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Avaliado com sucesso', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email enviado com sucesso', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'Agendamento aceito', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Motorista chegou', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'Viagem cancelado com sucesso', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fim de viagem', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'Viagem agendada com sucesso', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'Viagem recusada com sucesso', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'Iniciou a viagem', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Nova viagem estipulada', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'Viagem cancelada pelo cliente', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'Viagem aceita', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'Agendamento cancelado', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'Agendamento cancelado pelo motorista', 2),
(73, 40, 'Wrong Driver id', 1),
(74, 40, 'ID do driver errado', 2),
(75, 41, 'New Ride Allocated', 1),
(76, 41, 'novo passeio alocado', 2),
(77, 42, 'Ride Cancelled by User', 1),
(78, 42, ' Passeio cancelado pelo usuário', 2),
(79, 43, 'Ride Accepted by Driver', 1),
(80, 43, ' Passeio aceito pelo motorista', 2),
(81, 44, 'ride cancelled by driver', 1),
(82, 44, ' Passeio cancelado pelo motorista', 2),
(83, 45, 'Driver Arrived on User Door', 1),
(84, 45, 'O motorista chegou na porta do usuário', 2),
(85, 46, 'Ride Started by Driver', 1),
(86, 46, 'passeio iniciado pelo motorista', 2),
(87, 47, 'Ride Ended By Driver', 1),
(88, 47, 'passeio terminado pelo motorista', 2),
(89, 48, 'Session Expired', 1),
(90, 48, 'sessão expirada', 2),
(91, 49, 'Rating Successfully', 1),
(92, 49, 'Avaliação com sucesso', 2),
(93, 50, 'ride expire', 1),
(94, 50, 'expirar', 2),
(95, 51, 'Document Already Upload', 1),
(96, 51, 'Documento já carregado', 2),
(97, 52, 'document uploaded successfully', 1),
(98, 52, 'Cadastro pendente de aprovação', 2),
(99, 53, 'E-mail Not Found', 1),
(100, 53, 'E-mail não encontrado', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 6, '', '-34.460315554689', '-58.8690141588449', '\nPilar, Buenos Aires', '0.0', '0.0', 'No drop off point', 'Sunday, Nov 5', '17:17:32', '05:17:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.460315554689,-58.8690141588449&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 4, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', 'About Us and other information about company will go here.', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '178', 'Tuesday, Oct 10', '1'),
(2, 2, 1, '1', 'Cash', 'Admin', '178', 'Thursday, Oct 26', '1'),
(3, 3, 5, '1', 'Cash', 'Admin', '115', 'Saturday, Oct 28', '1'),
(4, 4, 5, '1', 'Cash', 'Admin', '100', 'Saturday, Oct 28', '1'),
(5, 5, 6, '1', 'Cash', 'Admin', '130', 'Sunday, Oct 29', '1'),
(6, 7, 8, '1', 'Cash', 'Admin', '178', 'Wednesday, Nov 1', '1'),
(7, 8, 8, '1', 'Cash', 'Admin', '193', 'Wednesday, Nov 1', '1'),
(8, 10, 8, '1', 'Cash', 'Admin', '178', 'Thursday, Nov 2', '1'),
(9, 11, 6, '1', 'Cash', 'Admin', '100', 'Sunday, Nov 5', '1'),
(10, 12, 9, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 8', '1'),
(11, 13, 9, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 8', '1'),
(12, 14, 9, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 8', '1'),
(13, 15, 9, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 8', '1'),
(14, 16, 9, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 8', '1'),
(15, 17, 17, '1', 'Cash', 'Admin', '178', 'Friday, Nov 10', '1'),
(16, 18, 18, '1', 'Cash', 'Admin', '100', 'Monday, Nov 13', '1'),
(17, 19, 18, '1', 'Cash', 'Admin', '110', 'Monday, Nov 13', '1'),
(18, 20, 18, '1', 'Cash', 'Admin', '100', 'Monday, Nov 13', '1'),
(19, 22, 18, '1', 'Cash', 'Admin', '100', 'Monday, Nov 13', '1'),
(20, 25, 19, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(21, 26, 19, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(22, 27, 19, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(23, 28, 20, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(24, 29, 20, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(25, 30, 21, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(26, 31, 21, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(27, 31, 21, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(28, 34, 21, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(29, 35, 23, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(30, 36, 23, '1', 'Cash', 'Admin', '115', 'Tuesday, Nov 14', '1'),
(31, 37, 20, '1', 'Cash', 'Admin', '100', 'Saturday, Nov 18', '1'),
(32, 39, 32, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 21', '1'),
(33, 40, 37, '1', 'Cash', 'Admin', '100', 'Wednesday, Nov 22', '1'),
(34, 41, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1'),
(35, 42, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1'),
(36, 42, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1'),
(37, 43, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1'),
(38, 44, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1'),
(39, 45, 36, '1', 'Cash', 'Admin', '100', 'Thursday, Nov 23', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_reference`
--

CREATE TABLE `payment_reference` (
  `pay_id` int(11) NOT NULL,
  `payment_ammount` varchar(655) COLLATE utf8_unicode_ci NOT NULL,
  `payment_date` varchar(655) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(50, 124, 'Km', '', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(51, 124, 'Km', 'BRL', 3, 10, 0, 0, 0, 0, 0, 0, '4', '80', '10', '0', '0', '0', '0'),
(52, 128, 'Km', '', 2, 676, 0, 0, 0, 0, 0, 0, '900', '9000', '98', '90', '80', '67', '90');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 1, '2017-10-10'),
(2, 2, 1, 1, '2017-10-10'),
(3, 3, 38, 1, '2017-10-26'),
(4, 4, 41, 1, '2017-10-28'),
(5, 5, 41, 1, '2017-10-28'),
(6, 6, 41, 1, '2017-10-29'),
(7, 7, 44, 1, '2017-11-01'),
(8, 8, 45, 0, '2017-11-01'),
(9, 8, 43, 0, '2017-11-01'),
(10, 8, 38, 0, '2017-11-01'),
(11, 9, 45, 1, '2017-11-01'),
(12, 9, 43, 0, '2017-11-01'),
(13, 9, 38, 0, '2017-11-01'),
(14, 10, 45, 0, '2017-11-01'),
(15, 10, 43, 0, '2017-11-01'),
(16, 10, 38, 0, '2017-11-01'),
(17, 11, 45, 1, '2017-11-01'),
(18, 11, 38, 0, '2017-11-01'),
(19, 12, 45, 1, '2017-11-01'),
(20, 12, 38, 0, '2017-11-01'),
(21, 13, 45, 1, '2017-11-02'),
(22, 13, 38, 0, '2017-11-02'),
(23, 15, 37, 1, '2017-11-05'),
(24, 16, 48, 1, '2017-11-08'),
(25, 17, 48, 0, '2017-11-08'),
(26, 18, 48, 0, '2017-11-08'),
(27, 19, 48, 0, '2017-11-08'),
(28, 20, 48, 0, '2017-11-08'),
(29, 21, 48, 0, '2017-11-08'),
(30, 22, 38, 0, '2017-11-10'),
(31, 22, 58, 0, '2017-11-10'),
(32, 23, 38, 0, '2017-11-10'),
(33, 23, 58, 1, '2017-11-10'),
(34, 24, 48, 0, '2017-11-13'),
(35, 24, 68, 1, '2017-11-13'),
(36, 25, 48, 0, '2017-11-13'),
(37, 25, 68, 1, '2017-11-13'),
(38, 26, 48, 0, '2017-11-13'),
(39, 26, 68, 1, '2017-11-13'),
(40, 27, 48, 0, '2017-11-13'),
(41, 27, 68, 1, '2017-11-13'),
(42, 28, 48, 0, '2017-11-13'),
(43, 28, 68, 0, '2017-11-13'),
(44, 29, 48, 0, '2017-11-13'),
(45, 29, 68, 1, '2017-11-13'),
(46, 30, 48, 0, '2017-11-13'),
(47, 30, 68, 0, '2017-11-13'),
(48, 31, 48, 0, '2017-11-13'),
(49, 31, 68, 1, '2017-11-13'),
(50, 32, 48, 0, '2017-11-13'),
(51, 32, 68, 1, '2017-11-13'),
(52, 33, 48, 0, '2017-11-13'),
(53, 33, 68, 0, '2017-11-13'),
(54, 34, 48, 0, '2017-11-13'),
(55, 34, 68, 1, '2017-11-13'),
(56, 35, 48, 0, '2017-11-13'),
(57, 35, 68, 0, '2017-11-13'),
(58, 36, 48, 0, '2017-11-13'),
(59, 36, 68, 1, '2017-11-13'),
(60, 37, 48, 0, '2017-11-14'),
(61, 37, 68, 1, '2017-11-14'),
(62, 38, 48, 0, '2017-11-14'),
(63, 38, 68, 0, '2017-11-14'),
(64, 39, 48, 0, '2017-11-14'),
(65, 39, 68, 0, '2017-11-14'),
(66, 40, 48, 0, '2017-11-14'),
(67, 40, 68, 1, '2017-11-14'),
(68, 41, 48, 0, '2017-11-14'),
(69, 41, 68, 1, '2017-11-14'),
(70, 42, 48, 0, '2017-11-14'),
(71, 42, 68, 1, '2017-11-14'),
(72, 43, 48, 0, '2017-11-14'),
(73, 44, 48, 0, '2017-11-14'),
(74, 44, 68, 1, '2017-11-14'),
(75, 45, 48, 0, '2017-11-14'),
(76, 45, 68, 0, '2017-11-14'),
(77, 45, 48, 0, '0000-00-00'),
(78, 46, 48, 0, '2017-11-14'),
(79, 46, 68, 0, '2017-11-14'),
(80, 47, 48, 0, '2017-11-14'),
(81, 47, 68, 1, '2017-11-14'),
(82, 48, 48, 0, '2017-11-14'),
(83, 48, 68, 1, '2017-11-14'),
(84, 49, 48, 0, '2017-11-14'),
(85, 49, 68, 1, '2017-11-14'),
(86, 50, 48, 0, '2017-11-14'),
(87, 50, 68, 1, '2017-11-14'),
(88, 51, 48, 0, '2017-11-14'),
(89, 51, 68, 1, '2017-11-14'),
(90, 52, 48, 0, '2017-11-14'),
(91, 52, 69, 1, '2017-11-14'),
(92, 53, 48, 0, '2017-11-14'),
(93, 53, 69, 1, '2017-11-14'),
(94, 54, 48, 0, '2017-11-18'),
(95, 54, 69, 0, '2017-11-18'),
(96, 54, 80, 1, '2017-11-18'),
(97, 55, 48, 0, '2017-11-18'),
(98, 55, 69, 0, '2017-11-18'),
(99, 55, 80, 0, '2017-11-18'),
(100, 55, 81, 0, '2017-11-18'),
(101, 56, 48, 0, '2017-11-18'),
(102, 56, 69, 0, '2017-11-18'),
(103, 56, 80, 0, '2017-11-18'),
(104, 56, 81, 0, '2017-11-18'),
(105, 57, 48, 0, '2017-11-18'),
(106, 57, 69, 0, '2017-11-18'),
(107, 57, 80, 0, '2017-11-18'),
(108, 57, 81, 1, '2017-11-18'),
(109, 58, 48, 0, '2017-11-21'),
(110, 58, 69, 0, '2017-11-21'),
(111, 58, 81, 0, '2017-11-21'),
(112, 58, 83, 0, '2017-11-21'),
(113, 58, 84, 0, '2017-11-21'),
(114, 58, 85, 1, '2017-11-21'),
(115, 59, 48, 0, '2017-11-22'),
(116, 59, 69, 0, '2017-11-22'),
(117, 59, 81, 0, '2017-11-22'),
(118, 59, 83, 0, '2017-11-22'),
(119, 59, 85, 0, '2017-11-22'),
(120, 59, 91, 0, '2017-11-22'),
(121, 59, 92, 1, '2017-11-22'),
(122, 60, 48, 0, '2017-11-23'),
(123, 60, 69, 0, '2017-11-23'),
(124, 60, 81, 0, '2017-11-23'),
(125, 60, 83, 0, '2017-11-23'),
(126, 60, 85, 0, '2017-11-23'),
(127, 60, 98, 1, '2017-11-23'),
(128, 61, 48, 0, '2017-11-23'),
(129, 61, 69, 0, '2017-11-23'),
(130, 61, 81, 0, '2017-11-23'),
(131, 61, 83, 0, '2017-11-23'),
(132, 61, 85, 0, '2017-11-23'),
(133, 61, 98, 1, '2017-11-23'),
(134, 62, 48, 0, '2017-11-23'),
(135, 62, 69, 0, '2017-11-23'),
(136, 62, 81, 0, '2017-11-23'),
(137, 62, 83, 0, '2017-11-23'),
(138, 62, 85, 0, '2017-11-23'),
(139, 62, 98, 1, '2017-11-23'),
(140, 63, 48, 0, '2017-11-23'),
(141, 63, 69, 0, '2017-11-23'),
(142, 63, 81, 0, '2017-11-23'),
(143, 63, 83, 0, '2017-11-23'),
(144, 63, 85, 0, '2017-11-23'),
(145, 63, 98, 1, '2017-11-23'),
(146, 64, 48, 0, '2017-11-23'),
(147, 64, 69, 0, '2017-11-23'),
(148, 64, 81, 0, '2017-11-23'),
(149, 64, 83, 0, '2017-11-23'),
(150, 64, 85, 0, '2017-11-23'),
(151, 64, 98, 1, '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 45, 68);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.412219082499934', '77.04323060810566', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '15:24:29', '03:25:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412219082499934,77.04323060810566&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(2, 1, '', '28.41217927218696', '77.0432373136282', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 10', '15:30:09', '03:30:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41217927218696,77.0432373136282&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-10'),
(3, 1, '', '28.4122916256984', '77.04345155507326', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '10:23:24', '10:23:47 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122916256984,77.04345155507326&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 38, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(4, 5, '', '-34.460126468624715', '-58.868758343160145', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', '-34.425087', '-58.57965850000001', 'Tigre', 'Saturday, Oct 28', '23:45:36', '11:47:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.460126468624715,-58.868758343160145&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(5, 5, '', '-34.460168764228904', '-58.868659436702735', 'Calle Muzilli 1170, Pilar Centro, Buenos Aires, Argentina', '-34.4452655', '-58.8725831', 'Hotel ibis Pilar', 'Saturday, Oct 28', '23:49:59', '11:51:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.460168764228904,-58.868659436702735&markers=color:red|label:D|-34.4452655,-58.8725831&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(6, 6, '', '-34.4438673361222', '-58.87255031615496', 'Ramal Pilar, La Lonja, Buenos Aires, Argentina', '-34.4452655', '-58.8725831', 'Hotel ibis Pilar', 'Sunday, Oct 29', '00:29:55', '12:33:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.4438673361222,-58.87255031615496&markers=color:red|label:D|-34.4452655,-58.8725831&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 41, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(7, 8, '', '28.4120598411583', '77.0432768762112', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4279671631346', '77.0709978416562', '1131, Orchid Island, Sector 51\nGurugram, Haryana 122022', 'Wednesday, Nov 1', '12:49:20', '12:49:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120598411583,77.0432768762112&markers=color:red|label:D|28.4279671631346,77.0709978416562&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 44, 3, 1, 1, 3, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(8, 8, '', '28.4123705261627', '77.0432705991323', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4231489538565', '77.0815047249198', 'B1/13, Sushant Lok III, Sector 57\nGurugram, Haryana 122413', 'Wednesday, Nov 1', '13:20:30', '01:20:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123705261627,77.0432705991323&markers=color:red|label:D|28.4231489538565,77.0815047249198&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(9, 8, '', '28.4123705261627', '77.0432705991323', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4231489538565', '77.0815047249198', 'B1/13, Sushant Lok III, Sector 57\nGurugram, Haryana 122413', 'Wednesday, Nov 1', '13:21:15', '01:21:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123705261627,77.0432705991323&markers=color:red|label:D|28.4231489538565,77.0815047249198&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(10, 8, '', '28.4123705261627', '77.0432705991323', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4231489538565', '77.0815047249198', 'B1/13, Sushant Lok III, Sector 57\nGurugram, Haryana 122413', 'Wednesday, Nov 1', '13:22:47', '01:23:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123705261627,77.0432705991323&markers=color:red|label:D|28.4231489538565,77.0815047249198&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(11, 8, '', '28.4123705261627', '77.0432705991323', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4231489538565', '77.0815047249198', 'B1/13, Sushant Lok III, Sector 57\nGurugram, Haryana 122413', 'Wednesday, Nov 1', '13:26:36', '01:28:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123705261627,77.0432705991323&markers=color:red|label:D|28.4231489538565,77.0815047249198&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(12, 8, '', '28.4120872938758', '77.0432827605374', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4377114175572', '77.0945308730006', 'Unnamed Road, Parsvnath Exotica, DLF Phase 5, Sector 53\nGurugram, Haryana 122003', 'Wednesday, Nov 1', '13:32:57', '01:37:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120872938758,77.0432827605374&markers=color:red|label:D|28.4377114175572,77.0945308730006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(13, 8, '', '28.412086086507', '77.0432852581143', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4252798907915', '77.0794655755162', '60, Rail Vihar, Sector 57\nGurugram, Haryana 122003', 'Thursday, Nov 2', '05:29:09', '05:29:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412086086507,77.0432852581143&markers=color:red|label:D|28.4252798907915,77.0794655755162&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 45, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(14, 6, '', '-34.4574112', '-58.8654589', 'Avenida Juan Domingo PerÃ³n', '-34.8150044', '-58.5348284', 'Aeroporto Internacional Ministro Pistarini', 'Sunday, Nov 5', '01:03:22', '01:03:22 AM', '', '2017-11-05', '22:03:06', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-05'),
(15, 6, '', '-34.460315554689', '-58.8690141588449', '\nPilar, Buenos Aires', '-34.3457209', '-58.7602562', 'Escobar', 'Sunday, Nov 5', '17:18:16', '05:20:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|-34.460315554689,-58.8690141588449&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 37, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(16, 9, '', '28.4120641317934', '77.0432460998581', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4256248703206', '77.0946153625846', '152, Devinder Vihar, Sector 56\nGurugram, Haryana 122011', 'Wednesday, Nov 8', '12:12:26', '12:13:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120641317934,77.0432460998581&markers=color:red|label:D|28.4256248703206,77.0946153625846&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 48, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(17, 9, '', '28.4120795988933', '77.0432728528976', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4238280159037', '77.0797596126795', 'B1/25, Sushant Lok III, Sector 57\nGurugram, Haryana 122003', 'Wednesday, Nov 8', '13:09:06', '01:09:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120795988933,77.0432728528976&markers=color:red|label:D|28.4238280159037,77.0797596126795&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(18, 9, '', '28.4120894024743', '77.0432580280592', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4540968416438', '77.1215256303549', 'Gurgaon - Faridabad Road, Block E, Aya Nagar Extension, Aya Nagar\nGurugram, Haryana 122011', 'Wednesday, Nov 8', '13:21:41', '01:22:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120894024743,77.0432580280592&markers=color:red|label:D|28.4540968416438,77.1215256303549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(19, 9, '', '28.41212846369', '77.0432802178062', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.427368031739', '77.0868309214711', '2016, Manohara Marg, Block B1, Sector 57\nGurugram, Haryana 122011', 'Wednesday, Nov 8', '13:24:12', '01:25:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41212846369,77.0432802178062&markers=color:red|label:D|28.427368031739,77.0868309214711&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(20, 9, '', '28.4121082341983', '77.0433049061504', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4174155268855', '77.0634997263551', '28, Nirvana Central Road, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Wednesday, Nov 8', '13:29:46', '01:30:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121082341983,77.0433049061504&markers=color:red|label:D|28.4174155268855,77.0634997263551&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(21, 9, '', '28.4120619053994', '77.0432121679187', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4297615885498', '77.1177919954062', 'Village\n174, Rajesh Pilot Road, Ghatta Kanarpur, Sector 55, Ghata, Haryana 122011', 'Wednesday, Nov 8', '13:32:35', '01:32:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120619053994,77.0432121679187&markers=color:red|label:D|28.4297615885498,77.1177919954062&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 49, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(22, 17, '', '28.4122025004921', '77.0432648973118', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4253306058013', '77.1151547133923', 'Village\n174, Rajesh Pilot Road, Ghatta Kanarpur, Sector 55, Ghata, Haryana 122011', 'Friday, Nov 10', '12:16:23', '12:16:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122025004921,77.0432648973118&markers=color:red|label:D|28.4253306058013,77.1151547133923&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(23, 17, '', '28.4122025004921', '77.0432648973118', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4253306058013', '77.1151547133923', 'Village\n174, Rajesh Pilot Road, Ghatta Kanarpur, Sector 55, Ghata, Haryana 122011', 'Friday, Nov 10', '12:50:03', '12:50:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122025004921,77.0432648973118&markers=color:red|label:D|28.4253306058013,77.1151547133923&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 58, 4, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(24, 18, '', '28.43065849500873', '77.04115994274616', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:42:53', '05:43:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43065849500873,77.04115994274616&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(25, 18, '', '28.43065849500873', '77.04115994274616', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:43:46', '05:47:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43065849500873,77.04115994274616&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(26, 18, '', '28.430430878441705', '77.04126052558422', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:52:27', '05:53:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430430878441705,77.04126052558422&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(27, 18, '', '28.430430878441705', '77.04126052558422', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:54:50', '05:56:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430430878441705,77.04126052558422&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(28, 18, '', '28.430430878441705', '77.04126052558422', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:55:47', '05:55:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430430878441705,77.04126052558422&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(29, 18, '', '28.43042114870706', '77.04125247895718', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '17:58:55', '05:59:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43042114870706,77.04125247895718&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(30, 18, '', '28.430462721203327', '77.04125985503197', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:00:49', '06:00:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430462721203327,77.04125985503197&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(31, 18, '', '28.430462721203327', '77.04125985503197', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:01:22', '06:01:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430462721203327,77.04125985503197&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(32, 18, '', '28.430462721203327', '77.04125985503197', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:02:50', '06:03:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430462721203327,77.04125985503197&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(33, 18, '', '28.430465079926034', '77.04126220196486', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.470741699999994', '77.0463719', 'Sector 14', 'Monday, Nov 13', '18:07:49', '06:07:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430465079926034,77.04126220196486&markers=color:red|label:D|28.470741699999994,77.0463719&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(34, 19, '', '28.43046242636298', '77.04126622527838', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:15:58', '06:16:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43046242636298,77.04126622527838&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(35, 19, '', '28.43040847056668', '77.04124107956886', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:17:41', '06:17:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43040847056668,77.04124107956886&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(36, 19, '', '28.43040847056668', '77.04124107956886', '737, Sector 38, Islampur Village, Sector 38, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 13', '18:18:15', '06:18:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.43040847056668,77.04124107956886&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(37, 19, '', '28.4123208198984', '77.04343244433403', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '05:13:19', '05:24:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123208198984,77.04343244433403&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-14'),
(38, 19, '', '28.412335859331613', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '05:23:59', '05:23:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(39, 19, '', '28.412335859331613', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '05:24:50', '05:24:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(40, 19, '', '28.412335859331613', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '05:25:29', '05:26:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(41, 19, '', '28.412335859331613', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '05:38:08', '05:40:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04343345016241&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(42, 19, '', '28.412306960026662', '77.0434257388115', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:03:57', '06:04:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412306960026662,77.0434257388115&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(43, 20, '', '28.41233733378572', '77.04344987869263', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:12:43', '06:12:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41233733378572,77.04344987869263&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(44, 20, '', '28.41216511740542', '77.04342171549797', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:15:39', '06:16:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41216511740542,77.04342171549797&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(45, 20, '', '28.41216511740542', '77.04342171549797', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:19:06', '06:19:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41216511740542,77.04342171549797&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(46, 20, '', '28.41232701260645', '77.04342976212502', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:22:22', '06:22:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232701260645,77.04342976212502&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(47, 20, '', '28.41232701260645', '77.04342976212502', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:22:25', '06:23:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232701260645,77.04342976212502&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(48, 21, '', '28.412273932239934', '77.04341232776642', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:31:30', '06:32:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273932239934,77.04341232776642&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(49, 21, '', '28.412273932239934', '77.04341232776642', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:33:14', '06:35:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273932239934,77.04341232776642&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 0, 13, 1, 0, 1, 1, '2017-11-14'),
(50, 21, '', '28.412273932239934', '77.04341232776642', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 14', '06:36:36', '06:39:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273932239934,77.04341232776642&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(51, 21, '', '28.412273932239934', '77.04341232776642', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.412345885619242', '77.04343512654305', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'Tuesday, Nov 14', '06:46:06', '06:46:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273932239934,77.04341232776642&markers=color:red|label:D|28.412345885619242,77.04343512654305&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 68, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(52, 23, '', '28.412335859331613', '77.04344652593136', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.455750218734153', '77.06511206924915', '97, Block G, South City I, Sector 41, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '11:27:47', '11:29:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04344652593136&markers=color:red|label:D|28.455750218734153,77.06511206924915&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 69, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(53, 23, '', '28.412335859331613', '77.04344652593136', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.455750218734153', '77.06511206924915', '97, Block G, South City I, Sector 41, Gurugram, Haryana 122022, India', 'Tuesday, Nov 14', '11:38:07', '11:40:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04344652593136&markers=color:red|label:D|28.455750218734153,77.06511206924915&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 69, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(54, 20, '', '28.430685620266456', '77.04142581671476', '692, Himalaya Rd, Medicity, Islampur Colony, Sector 38, Gurugram, Haryana 122005, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '18:45:39', '06:46:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.430685620266456,77.04142581671476&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 80, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(55, 26, '', '28.448023133964664', '77.06048291176558', '428, Block Q, South City I, Sector 40, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '19:47:39', '07:47:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.448023133964664,77.06048291176558&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(56, 26, '', '28.448023133964664', '77.06048291176558', '428, Block Q, South City I, Sector 40, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '19:48:05', '07:48:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.448023133964664,77.06048291176558&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(57, 26, '', '28.448023133964664', '77.06048291176558', '428, Block Q, South City I, Sector 40, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '19:48:30', '07:49:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.448023133964664,77.06048291176558&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 81, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-18'),
(58, 32, '', '28.412211120438524', '77.04341970384121', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Tuesday, Nov 21', '06:11:03', '06:11:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412211120438524,77.04341970384121&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 85, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(59, 37, '', '28.412213774459047', '77.04340796917677', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Wednesday, Nov 22', '06:41:52', '06:43:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412213774459047,77.04340796917677&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 92, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-22'),
(60, 36, '', '28.412289266570767', '77.04339187592268', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 23', '12:12:25', '12:13:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04339187592268&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 98, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23'),
(61, 36, '', '28.412289266570767', '77.04339187592268', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 23', '12:26:36', '12:28:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04339187592268&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 98, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23'),
(62, 36, '', '28.412289266570767', '77.04339187592268', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 23', '12:49:41', '12:49:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04339187592268&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 98, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23'),
(63, 36, '', '28.412289266570767', '77.04339187592268', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 23', '12:53:25', '12:53:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04339187592268&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 98, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23'),
(64, 36, '', '28.412289266570767', '77.04339187592268', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Thursday, Nov 23', '12:57:10', '12:57:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04339187592268&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 98, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1),
(6, 'Breakdown', '199', 1),
(7, 'Breakdown', '199', 1),
(8, 'K10', '0800187241', 1),
(9, 'BOMBEROS', '911', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`) VALUES
(1, 'test1'),
(2, 'test'),
(3, 'Haryana');

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO'),
(6, 643, 'ewqla', 'lsajlas@jdsal', 'lsalk', 'lsalk');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Certificado de Registro e Licenciamento de Veículo (CRLV) '),
(16, 'Certificado Nacional de Habilitação (CNH)'),
(15, 'Certificado de Registro de VeÃ­culo'),
(14, ' Carteira de motorista'),
(13, 'cedula azul'),
(17, 'Seguro do Veículo');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(28, 124, 14, 1),
(27, 124, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1507645168document_image_13.jpg', '14-10-2017', 1),
(2, 1, 2, 'uploads/driver/1507645179document_image_12.jpg', '26-1-2018', 1),
(3, 1, 4, 'uploads/driver/1507645190document_image_14.jpg', '27-4-2018', 1),
(4, 2, 4, '', '', 2),
(5, 2, 2, '', '', 2),
(6, 2, 3, '', '', 2),
(7, 3, 4, '', '', 1),
(8, 3, 2, '', '', 1),
(9, 3, 3, '', '', 1),
(10, 4, 3, '', '', 2),
(11, 4, 2, '', '', 2),
(12, 4, 1, '', '', 2),
(13, 7, 3, '', '', 1),
(14, 7, 2, '', '', 1),
(15, 7, 1, '', '', 1),
(16, 9, 4, '', '', 1),
(17, 9, 2, '', '', 1),
(18, 9, 3, '', '', 1),
(19, 28, 3, 'uploads/driver/1507887151document_image_283.jpg', '16-11-2017', 1),
(20, 28, 2, 'uploads/driver/1507887466document_image_282.jpg', '16-11-2017', 1),
(21, 28, 4, 'uploads/driver/1507887498document_image_284.jpg', '9-11-2017', 1),
(22, 33, 3, 'uploads/driver/15078937810Capture.PNGdocument_image_33.png', '2017-10-11', 1),
(23, 33, 2, 'uploads/driver/15078937811Capture.PNGdocument_image_33.png', '2017-10-12', 1),
(24, 33, 1, 'uploads/driver/15078937812Capture.PNGdocument_image_33.png', '2017-10-09', 1),
(25, 36, 1, 'uploads/driver/1507907107document_image_361.jpg', '13-10-2017', 1),
(26, 36, 2, 'uploads/driver/1507907122document_image_362.jpg', '13-10-2017', 1),
(27, 37, 1, 'uploads/driver/1508118892document_image_371.jpg', '15-10-2017', 1),
(28, 37, 2, 'uploads/driver/1508118911document_image_372.jpg', '15-10-2017', 1),
(29, 38, 2, 'uploads/driver/1509008559document_image_382.jpg', '1-9-2018', 1),
(30, 41, 2, 'uploads/driver/1509230644document_image_412.jpg', '28-10-2017', 1),
(31, 42, 2, 'uploads/driver/1509521416document_image_422.jpg', '2017-11-23', 1),
(32, 43, 2, 'uploads/driver/1509536475document_image_432.jpg', '2017-11-24', 1),
(33, 44, 2, 'uploads/driver/1509539301document_image_442.jpg', '2017-11-30', 1),
(34, 45, 2, 'uploads/driver/1509542405document_image_452.jpg', '2017-11-30', 1),
(35, 46, 2, 'uploads/driver/1509706394document_image_462.jpg', '2017-11-24', 1),
(36, 47, 2, 'uploads/driver/1509773012document_image_472.jpg', '20-7-2018', 1),
(37, 48, 2, 'uploads/driver/1510141915document_image_482.jpg', '2017-11-30', 1),
(38, 49, 2, 'uploads/driver/1510146365document_image_492.jpg', '2017-11-30', 1),
(39, 50, 2, 'uploads/driver/1510218062document_image_502.jpg', '2017-11-30', 1),
(40, 51, 2, 'uploads/driver/1510218339document_image_512.jpg', '2017-11-09', 1),
(41, 52, 2, 'uploads/driver/1510218413document_image_522.jpg', '2017-11-30', 1),
(42, 54, 2, 'uploads/driver/1510297621document_image_542.jpg', '2017-11-30', 1),
(43, 55, 2, 'uploads/driver/1510298013document_image_552.jpg', '2017-11-16', 1),
(44, 58, 2, 'uploads/driver/1510316098document_image_582.jpg', '2017-11-30', 1),
(45, 59, 2, 'uploads/driver/1510341506document_image_592.jpg', '30-11-2017', 1),
(46, 60, 2, 'uploads/driver/1510341599document_image_602.jpg', '11-11-2017', 1),
(47, 62, 2, 'uploads/driver/1510342662document_image_622.jpg', '24-11-2017', 1),
(48, 64, 2, 'uploads/driver/1510345580document_image_642.jpg', '30-11-2017', 1),
(49, 67, 2, 'uploads/driver/1510585622document_image_672.jpg', '2017-11-30', 1),
(50, 68, 2, 'uploads/driver/1510594929document_image_682.jpg', '24-11-2017', 1),
(51, 69, 2, 'uploads/driver/1510658543document_image_692.jpg', '14-11-2017', 1),
(52, 70, 2, 'uploads/driver/1510696204document_image_702.jpg', '30-11-2017', 1),
(53, 71, 2, 'uploads/driver/1510779580document_image_712.jpg', '2017-11-30', 1),
(54, 72, 2, 'uploads/driver/1510995569document_image_722.jpg', '24-11-2017', 1),
(55, 74, 2, 'uploads/driver/1511009358document_image_742.jpg', '25-11-2017', 1),
(56, 78, 2, 'uploads/driver/1511013891document_image_782.jpg', '30-11-2017', 1),
(57, 79, 2, 'uploads/driver/1511025236document_image_792.jpg', '30-11-2017', 1),
(58, 80, 2, 'uploads/driver/1511030444document_image_802.jpg', '24-11-2017', 1),
(59, 81, 2, 'uploads/driver/1511033863document_image_812.jpg', '19-11-2017', 1),
(60, 82, 2, 'uploads/driver/1511163003document_image_822.jpg', '20-11-2017', 1),
(61, 83, 2, 'uploads/driver/1511164151document_image_832.jpg', '20-11-2017', 1),
(62, 84, 2, 'uploads/driver/1511187570document_image_842.jpg', '24-11-2017', 1),
(63, 85, 2, 'uploads/driver/1511244633document_image_852.jpg', '23-11-2017', 1),
(64, 86, 2, 'uploads/driver/1511245173document_image_862.jpg', '23-11-2017', 1),
(65, 87, 2, 'uploads/driver/1511246459document_image_872.jpg', '24-11-2017', 1),
(66, 89, 2, 'uploads/driver/1511248021document_image_892.jpg', '25-11-2017', 1),
(67, 90, 2, 'uploads/driver/1511248283document_image_902.jpg', '22-11-2017', 1),
(68, 91, 2, 'uploads/driver/1511248476document_image_912.jpg', '21-11-2017', 1),
(69, 92, 2, 'uploads/driver/1511332863document_image_922.jpg', '25-11-2017', 1),
(70, 94, 2, 'uploads/driver/1511337428document_image_942.jpg', '22-11-2017', 1),
(71, 95, 2, 'uploads/driver/1511337890document_image_952.jpg', '22-11-2017', 1),
(72, 98, 2, 'uploads/driver/1511350817document_image_982.jpg', '24-11-2017', 1),
(73, 99, 2, 'uploads/driver/1511441454document_image_992.jpg', '23-11-2017', 1),
(74, 100, 14, 'uploads/driver/1511923304document_image_10014.jpg', '2017-11-30', 1),
(75, 100, 15, 'uploads/driver/1511923319document_image_10015.jpg', '2017-11-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-10-10 15:19:57', '2017-10-12 11:01:17', '19 Hours 41 Minutes', 19, 41, '2017-10-10'),
(2, 28, '2017-10-13 10:38:42', '', '', 0, 0, '2017-10-13'),
(3, 36, '2017-10-13 16:05:37', '2017-10-13 16:05:42', '0 Hours 0 Minutes', 0, 0, '2017-10-13'),
(4, 38, '2017-10-26 10:23:13', '2017-10-26 10:02:54', '0 Hours 0 Minutes', 0, 0, '2017-10-26'),
(5, 41, '2017-10-29 00:27:29', '2017-10-29 00:37:09', '0 Hours 16 Minutes', 0, 16, '2017-10-28'),
(6, 42, '2017-11-01 11:36:42', '2017-11-01 11:37:19', '0 Hours 0 Minutes', 0, 0, '2017-11-01'),
(7, 43, '2017-11-01 11:41:33', '2017-11-01 13:24:26', '1 Hours 42 Minutes', 1, 42, '2017-11-01'),
(8, 44, '2017-11-01 12:28:29', '', '', 0, 0, '2017-11-01'),
(9, 45, '2017-11-01 13:20:13', '2017-11-10 06:31:04', '17 Hours 10 Minutes', 17, 10, '2017-11-01'),
(10, 46, '2017-11-03 10:53:47', '2017-11-04 23:35:56', '12 Hours 42 Minutes', 12, 42, '2017-11-03'),
(11, 47, '2017-11-04 05:26:07', '2017-11-04 05:26:09', '0 Hours 0 Minutes', 0, 0, '2017-11-04'),
(12, 37, '2017-11-05 17:18:16', '2017-11-05 17:21:32', '0 Hours 3 Minutes', 0, 3, '2017-11-05'),
(13, 48, '2017-11-08 12:12:09', '2017-11-08 12:12:04', '0 Hours 6 Minutes', 0, 6, '2017-11-08'),
(14, 43, '2017-11-09 07:57:39', '2017-11-09 07:58:18', '0 Hours 0 Minutes', 0, 0, '2017-11-09'),
(15, 50, '2017-11-09 09:01:13', '2017-11-09 09:04:00', '0 Hours 2 Minutes', 0, 2, '2017-11-09'),
(16, 51, '2017-11-09 09:05:45', '2017-11-09 09:05:51', '0 Hours 0 Minutes', 0, 0, '2017-11-09'),
(17, 52, '2017-11-09 09:07:00', '2017-11-09 09:07:06', '0 Hours 0 Minutes', 0, 0, '2017-11-09'),
(18, 54, '2017-11-10 07:07:10', '2017-11-10 07:07:58', '0 Hours 0 Minutes', 0, 0, '2017-11-10'),
(19, 55, '2017-11-10 09:14:48', '2017-11-10 09:14:56', '0 Hours 1 Minutes', 0, 1, '2017-11-10'),
(20, 58, '2017-11-10 12:49:53', '2017-11-10 12:42:27', '0 Hours 1 Minutes', 0, 1, '2017-11-10'),
(21, 37, '2017-11-13 15:00:30', '', '', 0, 0, '2017-11-13'),
(22, 67, '2017-11-13 15:07:09', '2017-11-15 20:55:15', '10 Hours 95 Minutes', 10, 95, '2017-11-13'),
(23, 64, '2017-11-13 17:20:03', '', '', 0, 0, '2017-11-13'),
(24, 68, '2017-11-13 17:42:23', '', '', 0, 0, '2017-11-13'),
(25, 69, '2017-11-14 11:27:21', '', '', 0, 0, '2017-11-14'),
(26, 70, '2017-11-14 22:23:34', '2017-11-14 22:25:17', '0 Hours 1 Minutes', 0, 1, '2017-11-14'),
(27, 72, '2017-11-18 08:59:46', '', '', 0, 0, '2017-11-18'),
(28, 80, '2017-11-18 18:41:01', '', '', 0, 0, '2017-11-18'),
(29, 81, '2017-11-18 19:43:55', '2017-11-18 19:43:53', '0 Hours 3 Minutes', 0, 3, '2017-11-18'),
(30, 83, '2017-11-20 07:50:07', '', '', 0, 0, '2017-11-20'),
(31, 84, '2017-11-20 14:53:23', '', '', 0, 0, '2017-11-20'),
(32, 85, '2017-11-21 06:10:44', '', '', 0, 0, '2017-11-21'),
(33, 91, '2017-11-21 13:32:37', '', '', 0, 0, '2017-11-21'),
(34, 92, '2017-11-22 06:41:12', '', '', 0, 0, '2017-11-22'),
(35, 94, '2017-11-22 07:57:58', '2017-11-22 07:58:01', '0 Hours 0 Minutes', 0, 0, '2017-11-22'),
(36, 95, '2017-11-22 08:14:14', '', '', 0, 0, '2017-11-22'),
(37, 98, '2017-11-23 12:11:57', '', '', 0, 0, '2017-11-23'),
(38, 71, '2017-11-25 22:42:07', '2017-11-25 22:54:25', '0 Hours 12 Minutes', 0, 12, '2017-11-25'),
(39, 100, '2017-11-29 02:53:33', '2017-11-29 03:31:04', '0 Hours 73 Minutes', 0, 73, '2017-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(42, 'Portuguese', 1),
(41, 'Vietnamese', 2),
(40, 'French', 2),
(39, 'Spanish', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver'),
(108, 1, 'pt', 'Login efetuado com sucesso'),
(109, 2, 'pt', 'UsuÃ¡rio Inativo'),
(110, 3, 'pt', 'Campos obrigatÃ³rios incompletos'),
(111, 4, 'pt', 'Email ou senha incorretos'),
(112, 5, 'pt', 'Logout efetuado com sucesso'),
(113, 6, 'pt', 'Nenhum registro econtrado'),
(114, 7, 'pt', 'Signup com sucesso'),
(115, 8, 'pt', 'Telefone jÃ¡ existe'),
(116, 9, 'pt', 'Email jÃ¡ existe'),
(117, 10, 'pt', 'Falta cÃ³pia do RG'),
(118, 11, 'pt', 'Falta cÃ³pia da habilitaÃ§Ã£o'),
(119, 12, 'pt', 'Falta cÃ³pia do seguro'),
(120, 13, 'pt', 'Senha modificada'),
(121, 14, 'pt', 'Senha antiga incorreta'),
(122, 15, 'pt', 'CÃ³digo de cupom invÃ¡lido'),
(123, 16, 'pt', 'Cupom aplicado com sucesso'),
(124, 17, 'pt', 'UsuÃ¡rio nÃ£o existe'),
(125, 18, 'pt', 'Atualizado com sucesso'),
(126, 19, 'pt', 'Telefone jÃ¡ existe'),
(127, 20, 'pt', 'OnLine'),
(128, 21, 'pt', 'OffLine'),
(129, 22, 'pt', 'OTP enviado para telefone para verificaÃ§Ã£o'),
(130, 23, 'pt', 'Avaliado com sucesso'),
(131, 24, 'pt', 'Email enviado com sucesso'),
(132, 25, 'pt', 'Agendamento aceito'),
(133, 26, 'pt', 'Motorista chegou'),
(134, 27, 'pt', 'Viagem cancelado com sucesso'),
(135, 28, 'pt', 'Fim de viagem'),
(136, 29, 'pt', 'Viagem agendada com sucesso'),
(137, 30, 'pt', 'Viagem recusada com sucesso'),
(138, 31, 'pt', 'Iniciou a viagem'),
(139, 32, 'pt', 'Nova viagem estipulada'),
(140, 33, 'pt', 'Viagem cancelada pelo cliente'),
(141, 34, 'pt', 'Viagem aceita'),
(142, 35, 'pt', 'Agendamento cancelado'),
(143, 36, 'pt', 'Agendamento cancelado pelo motorista');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 1, 1, 5, '', 1, 5, '', '2017-10-10'),
(2, 3, 1, 0, '', 38, 5, '', '2017-10-26'),
(3, 2, 1, 5, '', 38, 0, '', '2017-10-26'),
(4, 4, 5, 5, '', 41, 4.5, '', '2017-10-28'),
(5, 5, 5, 5, 'muito bom.motorista ', 41, 5, '', '2017-10-28'),
(6, 6, 6, 0, '', 41, 5, '', '2017-10-29'),
(7, 11, 8, 0, '', 45, 5, '', '2017-11-01'),
(8, 12, 8, 4.5, '', 45, 5, '', '2017-11-01'),
(9, 13, 8, 4.5, '', 45, 5, '', '2017-11-02'),
(10, 15, 6, 5, '', 37, 5, 'vgghh', '2017-11-05'),
(11, 16, 9, 0, '', 48, 5, '', '2017-11-08'),
(12, 18, 9, 5, '', 49, 5, '', '2017-11-08'),
(13, 19, 9, 0, '', 49, 5, '', '2017-11-08'),
(14, 20, 9, 4.5, '', 49, 5, '', '2017-11-08'),
(15, 23, 17, 0, '', 58, 4.5, '', '2017-11-10'),
(16, 24, 18, 0, '', 68, 3.5, '', '2017-11-13'),
(17, 25, 18, 4, '', 68, 4.5, '', '2017-11-13'),
(18, 26, 18, 4.5, '', 68, 4, '', '2017-11-13'),
(19, 31, 18, 5, '', 68, 4, '', '2017-11-13'),
(20, 40, 19, 0, '', 68, 3.5, '', '2017-11-14'),
(21, 27, 19, 4, '', 68, 0, '', '2017-11-14'),
(22, 42, 19, 0, '', 68, 4.5, '', '2017-11-14'),
(23, 41, 19, 4, '', 68, 4, '', '2017-11-14'),
(24, 44, 20, 0, '', 68, 5, '', '2017-11-14'),
(25, 28, 20, 0, '', 68, 0, '', '2017-11-14'),
(26, 47, 20, 0, '', 68, 4.5, '', '2017-11-14'),
(27, 29, 20, 4, '', 68, 0, '', '2017-11-14'),
(28, 48, 21, 0, '', 68, 4, '', '2017-11-14'),
(29, 30, 21, 4, '', 68, 0, '', '2017-11-14'),
(30, 51, 21, 0, '', 68, 4, '', '2017-11-14'),
(31, 34, 21, 4.5, '', 68, 0, '', '2017-11-14'),
(32, 52, 23, 0, '', 69, 5, '', '2017-11-14'),
(33, 35, 23, 5, '', 69, 0, '', '2017-11-14'),
(34, 53, 23, 0, '', 69, 5, '', '2017-11-14'),
(35, 36, 23, 5, '', 69, 0, '', '2017-11-14'),
(36, 54, 20, 0, '', 80, 4.5, '', '2017-11-18'),
(37, 37, 20, 3.5, '', 80, 0, '', '2017-11-18'),
(38, 39, 32, 4.5, '', 85, 0, '', '2017-11-21'),
(39, 58, 32, 0, '', 85, 4, '', '2017-11-21'),
(40, 59, 37, 0, '', 92, 5, '', '2017-11-22'),
(41, 60, 36, 0, '', 98, 4.5, '', '2017-11-23'),
(42, 61, 36, 0, '', 98, 5, '', '2017-11-23'),
(43, 45, 36, 4, '', 98, 0, '', '2017-11-23'),
(44, 64, 36, 0, '', 98, 4, '', '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 1, 2),
(3, 1, 1, 38, 3),
(4, 1, 5, 41, 4),
(5, 1, 5, 41, 5),
(6, 1, 6, 41, 6),
(7, 1, 8, 0, 8),
(8, 1, 8, 45, 9),
(9, 1, 8, 0, 10),
(10, 1, 8, 45, 11),
(11, 1, 8, 45, 12),
(12, 1, 8, 45, 13),
(13, 1, 6, 0, 14),
(14, 1, 6, 37, 15),
(15, 1, 9, 48, 16),
(16, 1, 9, 0, 17),
(17, 1, 9, 49, 18),
(18, 1, 9, 49, 19),
(19, 1, 9, 49, 20),
(20, 1, 9, 49, 21),
(21, 1, 17, 0, 22),
(22, 1, 17, 58, 23),
(23, 1, 18, 68, 24),
(24, 1, 18, 68, 25),
(25, 1, 18, 68, 26),
(26, 1, 18, 68, 27),
(27, 1, 18, 0, 28),
(28, 1, 18, 68, 29),
(29, 1, 18, 0, 30),
(30, 1, 18, 68, 31),
(31, 1, 18, 68, 32),
(32, 1, 18, 0, 33),
(33, 1, 19, 68, 34),
(34, 1, 19, 0, 35),
(35, 1, 19, 68, 36),
(36, 1, 19, 68, 37),
(37, 1, 19, 0, 38),
(38, 1, 19, 0, 39),
(39, 1, 19, 68, 40),
(40, 1, 19, 68, 41),
(41, 1, 19, 68, 42),
(42, 1, 20, 0, 43),
(43, 1, 20, 68, 44),
(44, 1, 20, 0, 45),
(45, 1, 20, 0, 46),
(46, 1, 20, 68, 47),
(47, 1, 21, 68, 48),
(48, 1, 21, 68, 49),
(49, 1, 21, 68, 50),
(50, 1, 21, 68, 51),
(51, 1, 23, 69, 52),
(52, 1, 23, 69, 53),
(53, 1, 20, 80, 54),
(54, 1, 26, 0, 55),
(55, 1, 26, 0, 56),
(56, 1, 26, 81, 57),
(57, 1, 32, 85, 58),
(58, 1, 37, 92, 59),
(59, 1, 36, 98, 60),
(60, 1, 36, 98, 61),
(61, 1, 36, 98, 62),
(62, 1, 36, 98, 63),
(63, 1, 36, 98, 64);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'ankit .', 'ankit@apporio.com', '+919540956147', '123456', '', '', 0, '0', 'Tuesday, Oct 10', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-10', 1),
(2, 1, 'Vilmar Alves .', '', '+541169178496', '', '', '', 0, '0', 'Friday, Oct 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '113514402139939405346', 'Vilmar Alves', 'vilmarjralves@gmail.com', 'https://lh5.googleusercontent.com/-Y6_s1JNFiVg/AAAAAAAAAAI/AAAAAAAAABs/3h6NZEf3D2A/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-10-13', 2),
(3, 1, 'Vilmar Junior .', '', '+551173737373', '', '', '', 0, '0', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '108417310661260294079', 'Vilmar Junior', 'gaucho1987@gmail.com', 'https://lh4.googleusercontent.com/-fjO0TQDr3NA/AAAAAAAAAAI/AAAAAAAAFMw/0EEaj9sItnI/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-10-16', 1),
(4, 1, 'Shilpa .', 'shilpa@gmail.com', '+558130039030', 'qwerty', '', '', 0, '0', 'Saturday, Oct 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-28', 1),
(5, 1, 'vilmar .', 'vilmarjralves@gmail.com', '+555199085159', 'Ju0611', '', '', 0, '0', 'Saturday, Oct 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.75', 0, '', 1, '2017-10-28', 1),
(6, 1, 'Tatiana Alves  .', 'tatianamlalves@gmail.com', '+5551997553631', 'tatiana', '', '', 0, '0', 'Sunday, Oct 29', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-29', 1),
(7, 1, 'atul .', 'atul@apporio.com', '+911234567890', 'qwerty', '', '', 0, '0', 'Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-01', 1),
(8, 1, 'fgdghd .', '89@9.com', '+912580369147', 'qwerty', 'http://apporio.org.com/IDE/ide/uploads/swift_file65.jpeg', '', 0, '0', 'Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-11-01', 1),
(9, 1, 'hahah .', '0@0.com', '+911234567321', 'qwerty', '', '', 0, '0', 'Wednesday, Nov 8', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-11-08', 1),
(10, 1, 'tatiana mara legal alves ', '', '+9151997553631', '', '', '', 0, '0', 'Wednesday, Nov 8', '', 0, 0, 0, 0, 0, '', '', '', '', '', '109785126430722886939', 'tatiana mara legal alves', 'tatianamlalves@gmail.com', 'https://lh4.googleusercontent.com/-AeuWrVSfMkk/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf6JJj0_QxfsxtEYA_sh_b0ghCfS3Q/s400/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-11-08', 1),
(11, 1, 'Tatiana Alves .', 'tatianamlalves@gmail.com', '+915197553631', 'tatiana', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '109785126430722886939', 'tatiana mara legal alves', 'tatianamlalves@gmail.com', 'https://lh4.googleusercontent.com/-AeuWrVSfMkk/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf6JJj0_QxfsxtEYA_sh_b0ghCfS3Q/s400/photo.jpg', '', '', 0, 1, '', 0, '', 3, '2017-11-09', 1),
(12, 1, 'bshshsvs .', '0@90.com', '+9112345678901', 'wwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(13, 1, 'asssddf .', '67@90.com', '+912222222222', 'qwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(14, 1, 'qwdhx .', '4@9.com', '+91121212122', 'qwertt', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(15, 1, 'weeds .', '0@90.com', '+9111112222333', 'qwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(16, 1, 'qwerty .', '8@8.com', '+91123456789012', 'qwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(17, 1, 'gdhdy .', '90@09.com', '+911234567832', 'qwerty', 'http://apporio.org.com/IDE/ide/uploads/swift_file66.jpeg', '', 0, '0', 'Friday, Nov 10', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.5', 0, '', 1, '2017-11-10', 1),
(18, 1, 'idepoja .', 'idp@gmail.com', '+555268956658', '123456', '', '', 0, '0', 'Sunday, Nov 12', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4', 0, '', 1, '2017-11-12', 1),
(19, 1, 'deeksha .', 'deeksha@gmail.com', '+558568623556', '123456', '', '', 0, '0', 'Monday, Nov 13', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3', 0, '', 1, '2017-11-13', 1),
(20, 1, 'pujasri .', 'pujasri@g.com', '+558965535569', '123456', '', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.8', 0, '', 1, '2017-11-14', 1),
(21, 1, 'shikha .', 'shikha@g.com', '+555369874563', '123456', '', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.66666666667', 0, '', 1, '2017-11-14', 1),
(22, 1, 'Pooja Apporio .', '', '+918526548828', '', '', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '117223531907833843873', 'Pooja Apporio', 'pooja@apporio.com', 'null', '', '', 0, 0, '', 0, '', 3, '2017-11-14', 1),
(23, 1, 'Preeti Sinha', 'qwerty', '+918874531856', 'qwerty', 'http://apporio.org.com/IDE/ide/uploads/15106591194481.jpg', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '166630797221728', 'upanu1012@gmail.com', 'http://graph.facebook.com/166630797221728/picture?type=large', 'Preeti', 'Sinha', '', '', '', '', '', '', 0, 0, '3.33333333333', 0, '', 1, '2017-11-14', 1),
(24, 1, 'Vilmar Jr Alves', 'vilmarjralves@gmail.com', '+5551998035159', 'Ju061187', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '1616944248328039', 'vilmaralves1@hotmail.com', 'http://graph.facebook.com/1616944248328039/picture?type=large', 'Vilmar Jr', 'Alves', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-15', 1),
(25, 1, 'hello .', 'hellodemo55@y.com', '+914545454545', '123456', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-15', 1),
(26, 1, 'vgbhhhgg .', 'hhhh@ghhjj.com', '+55855588555555', '22466658', '', '', 0, '0', 'Sunday, Nov 19', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-19', 1),
(27, 1, 'vilmar .', 'vilmarjralves@gmail.com', '+5551998065159', 'Ju061187', '', '', 0, '0', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(28, 1, 'huda .', 'qw@g.com', '+5525802580255', '123456', '', '', 0, '0', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(29, 1, 'yuiio .', 'qw@g.com', '++5556565656565', '123456', '', '', 0, '0', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(30, 1, 'tyui .', 'qwer@g.com', '++5511111111111', '123456', '', '', 0, '0', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(31, 1, 'we .', 'qwe@g.com', '++5566666666666', '123456', '', '', 0, '0', 'Monday, Nov 20', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-20', 1),
(32, 1, 'Apporio devices .', '', '+5556526885896', '', '', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111976537605501585996', 'Apporio devices', 'apporiodevices@gmail.com', 'null', '', '', 0, 0, '2', 0, '', 3, '2017-11-21', 1),
(33, 1, 'new .', 'sd@g.com', '++5536936936936', '123456', '', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-21', 1),
(34, 1, 'vzusuhs .', 'zvha@v.com', '++556464649764', '123456', '', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-21', 1),
(35, 1, 'usman .', 'tu@g.com', '++5544444444444', '123456', '', '', 0, '0', 'Tuesday, Nov 21', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-21', 1),
(36, 1, 'pujasri .', 'po@g.com', '++5577777777777', '123456', '', '', 0, '0', 'Wednesday, Nov 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.375', 0, '', 1, '2017-11-22', 1),
(37, 1, 'Apporio Singh', '', '+5500000000000', '', '', '', 0, '0', 'Wednesday, Nov 22', '', 0, 0, 0, 0, 0, '510600265969946', 'demo6619@gmail.com', 'http://graph.facebook.com/510600265969946/picture?type=large', 'Apporio', 'Singh', '', '', '', '', '', '', 0, 0, '5', 0, '', 2, '2017-11-22', 1),
(38, 1, 'vilmar .', 'vilmarjralves@gmail.com', '++5551998035159', 'Ju061187', '', '', 0, '0', 'Wednesday, Nov 22', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-22', 1),
(39, 1, 'uhyfgi .', 'ttt@g.com', '+559355696', '123456', 'http://apporio.org.com/IDE/ide/uploads/1511512947637.jpg', '', 0, '0', 'Friday, Nov 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'e5Z5EqSgfF0:APA91bFIUUG1gntLGmL1DWR4Fucgass4-Jn5M-XS6EvSftccYeoj-LuC5rtwkOwv5f27QSyiPGCJ7JUom6F7qRfb0d52Snf5w1G-o7bj0Jxjtm7oarGpF_CWRc6ZDkyDyHzWRfs-1l3Y', 0, 'e659dbb5fa6025fb', 0),
(2, 2, 'dsrDliqMzkw:APA91bE-p3p5uJEkA0iaZdaL0J2mboieS0vYkRftnFCvO-E4dmvsSrlJM2I8dWSS5biZ91mNklGDf30g91cr2iTHsapH-cF0DaHeDDIyS4ozSoYBEl8rHxBOzvdxV4gxjEcrqDK-dTXq', 0, 'cfd109c5896012d9', 0),
(3, 1, 'fwSBSzaH9_Y:APA91bH1Fzkh39OT4qYSH0z3HoA0Otipaepb2Vlm__LbxfW9Sluy7xCHzFh0rPODSUP8oOS2GI0Ema5_D1CUKQtaxfODiwj78Pe85Hgb_0nGn7bMGsOI5M7pyipmFiK-9mWycCQLs0x7', 0, 'c06c40037632ed39', 1),
(4, 4, 'fSTuHFpj4Lw:APA91bHcwe4HnJQD33gd7BhYB4b7-70E4X1FeETqnUsGdZknllQadmCtGVqfie8muO5qgHLHxJHFv-6aCjc4r3tyKkqJbtibsXc8JdwXPYEXj_T-FFJgh-PfY2b69VrByArFB0I_a3vl', 0, '9f1891df4b8e4e77', 1),
(5, 11, 'e1Xpz58KBRw:APA91bFQ2v-pSnnnqscMNRYWbmJQRz1lKHZP5vKOgixnmQg63B-Zl1Xp5af0kdFHaWCN-ruh-yJTtFvM0IysEDTA0xbUMhFjrZAwxgBoBb3NP4zjjwM36yPNoosQyhRT85lkGC9zFk2J', 0, '12a0a2c9bfedcff0', 1),
(6, 16, '7D68A35CF2611189C76B48582AEFE764FF41E7D8E789D8D7C3D822D3703EE7EB', 1, 'F1863BA1-CF43-47B3-B3F6-AB00E49ADFC5', 1),
(7, 12, 'D769657DD28F3047B56E1C50B78C4F9CBDB527B2DE52763674200933A3025CC2', 1, 'E04DA5BC-B7B9-45CA-9AB7-019B3A53D29A', 1),
(8, 11, '485F957ED2DC29BAE8FAC11E303B294117D8A21EDFB4C0C9C6A8024F004207F3', 1, '51F031BF-CAF6-46DE-8E1A-70E21EBB8938', 1),
(9, 9, 'DA71F466BBFC2E7396DD8F80EC8C8B03EE43B5F38BBE89D5DADB68A0104E0737', 1, 'D5FB4C3D-F6CE-4201-9F2B-F251F73E62B8', 1),
(10, 17, '6907DFFAA2BD2C7F768AFD8468F401DB1B131FFE4E64E173E23AC4983FFC228A', 1, '0AA5E854-D461-4223-B385-640E14A3506F', 1),
(11, 36, 'du4_BBOF6eI:APA91bETvFIZjqrmj38IPkITN1PdDf9YEwzfFLRVXW2C6Qrk0W4F-Yazxq_Bs2yM-n9U3ukf6bin9PnHkRwpNoaYXSFxD_hUO5KHRGatj7P3pAjV4qKjGK46Tr27wQXH6LbVSW3m9KjA', 0, 'a8eede1a231915ec', 1),
(12, 39, 'cOkEWa--V30:APA91bEituIJTbBkdri4UzuFku4g5MtdDz8pTJ3WvQPdSDY0zz7Br3ioIqV8V0RnVarLyZHFsRjVtfXBgqPqOzCFj6Gn-Ehfi6TxMB2bLCtL0_ZPEyaZjFsNo5xusxYzfPGjhbVeR6rD', 0, 'affa394d918054c9', 1),
(13, 23, 'cd65OaF7rXU:APA91bGOcEInsXp4cSJSQ_nlUv_UB-F1PJenHCnMPTZx0ssh-3gw6oHWOqMLFw7pPX-rBTqhuagrFm_6rttWsnti-rbWZu9CBO_6Xb2DzSYGCCezHeJxSlyi4clvISZ6CcoTFFNrPg1C', 0, 'a4594951bbeaaafa', 0),
(14, 25, '232D97BEEB8F7A622DA7F3F7060D559F15CE31C185EF747CDC4444682092F92B', 1, 'F372C9D4-C52D-4BF1-A314-0B8C470B0A94', 1),
(15, 26, 'e3YPQez0ZQk:APA91bFZ8YpGm2-BJBwH7dX7p49_RT2gI-qALGy8IJE2Ccngv7bpx_lCTmaH9n0Lze0NmSd90_9bWMxfO1EhQUMUNGqUr_8x-FLHJEn-EuPNwTNp264qOs79MT5NNqgOQ9ivHyfTYJFH', 0, 'eff54ff52339245c', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `content`, `image`, `link`) VALUES
(1, 'Cabs for Every Pocket', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', 'webstatic/img/ola-article/why-ola-1.jpg', ''),
(2, 'Secure and Safer Rides', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'webstatic/img/ola-article/why-ola-3.jpg', ''),
(3, 'Apporio Select', 'A membership program with Ola that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', 'webstatic/img/ola-article/why-ola-2.jpg', ''),
(4, '\r\nIn Cab Entertainment', 'Play music, watch videos and a lot more with Ola Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', 'webstatic/img/ola-article/why-ola-9.jpg', ''),
(5, 'Cashless Rides', 'Now go cashless and travel easy. Simply recharge your Apporio money or add your credit/debit card to enjoy hassle free payments.', 'webstatic/img/ola-article/why-ola-5.jpg', ''),
(6, 'Share and Express', 'To travel with the lowest fares choose Apporio Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', 'webstatic/img/ola-article/why-ola-3.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
