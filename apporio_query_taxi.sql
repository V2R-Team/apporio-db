-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:21 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_query_taxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_setting`
--

CREATE TABLE `account_setting` (
  `acc_id` int(11) NOT NULL,
  `acc_site_title` varchar(255) NOT NULL,
  `acc_email` varchar(255) NOT NULL,
  `acc_url` varchar(255) NOT NULL,
  `acc_phone` varchar(255) NOT NULL,
  `acc_mobile` varchar(255) NOT NULL,
  `acc_img` varchar(255) NOT NULL,
  `acc_address` text NOT NULL,
  `acc_country` varchar(255) NOT NULL,
  `acc_state` varchar(255) NOT NULL,
  `acc_city` varchar(255) NOT NULL,
  `acc_zip` varchar(255) NOT NULL,
  `acc_map` varchar(255) NOT NULL,
  `acc_fb` varchar(255) NOT NULL,
  `acc_tw` varchar(255) NOT NULL,
  `acc_go` varchar(255) NOT NULL,
  `acc_dribble` varchar(255) NOT NULL,
  `acc_rss` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_setting`
--

INSERT INTO `account_setting` (`acc_id`, `acc_site_title`, `acc_email`, `acc_url`, `acc_phone`, `acc_mobile`, `acc_img`, `acc_address`, `acc_country`, `acc_state`, `acc_city`, `acc_zip`, `acc_map`, `acc_fb`, `acc_tw`, `acc_go`, `acc_dribble`, `acc_rss`) VALUES
(1, 'Realty Singh', 'bhagyashree@wscubetech.com', '', '1234', '12345', 'img/.png', 'aafhff', 'India', 'Rajasthan', 'Jodhpur', '342001', 'aaaffhhf', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://accounts.google.com/', 'https://dribbble.com/', 'http://www.rss.org/');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`) VALUES
(1, 'infolabs', '', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '0', 'andhrasrdh@gmail.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced develo', 'india', 'India', 'Haryana', 'delhi', 110001, 'na', 'https://www.facebook.com/INDIA', '', '', '', '', '', 1),
(13, 'Demo', 'User', 'apporio', '', '', 'demo', '', 'demo@demo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '15', 0),
(14, 'KESHAV', 'GOYAL', 'keshavgoyallll', '', '', '123456', '', 'keshavgoyal5@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '16', 0),
(15, 'Abinash', 'Satyapriya', 'abinash', '', '', 'abinash', '', 'abinash.satyapriya@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '16', 0),
(17, 'test', 'test', 'test123', '', '', 'test123', '', 'test@fh.hjh', '', '', '', '', '', 0, '', '', '', '', '', '', '16', 0),
(18, 'ZZZ', 'HHH', 'ZH', '', '', 'king7878', '', 'paktimes@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '15', 0),
(19, 'aa', 'bb', 'aa', '', '', 'king7878', '', 'a.a.a.@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(1, 'Changed my mind', '', 0, 1, 1),
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(5, 'Driver ask to cancel', '', 0, 1, 1),
(6, 'Driver too far', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', 3, 1),
(3, 'Nano', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', 1, 1),
(8, 'Alto', '', '', '', 8, 1),
(12, 'Harley', '', '', '', 2, 1),
(11, 'Audi Q7', '', '', '', 4, 1),
(20, 'nissan', '', '', '', 3, 1),
(16, 'Korola', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', 5, 1),
(19, 'Eco Sport', '', '', '', 28, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', 1),
(3, 'MINI', '', 'MINI', 'uploads/car/editcar_3.png', 1),
(4, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_4.png', 1),
(5, 'SUV', '', 'SUV', 'uploads/car/editcar_5.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) NOT NULL,
  `city_longitude` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `city_name_arabic` varchar(255) NOT NULL,
  `city_name_french` varchar(255) NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `language`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(3, 'Dummy City', '', '', '', '', '', '', '', 1),
(56, 'Gurugram', '', '', 'Indian', 'Miles', 'Hindi', '', '', 1),
(57, 'Delhi', '', '', '', '', '', '', '', 1),
(58, 'Dhaka', '', '', '', '', '', '', '', 1),
(59, 'Pune', '', '', '', '', '', '', '', 1),
(60, 'D.C.', '', '', '', '', '', '', '', 1),
(61, 'Pune', '', '', '', '', '', '', '', 1),
(62, 'Frankfurt', '', '', '', '', '', '', '', 1),
(63, 'Paris', '', '', '', '', '', '', '', 1),
(64, 'KÃ¸benhavn', '', '', '', '', '', '', '', 1),
(65, 'Tirana', '', '', '', '', '', '', '', 1),
(66, 'Leiden', '', '', '', '', '', '', '', 1),
(67, 'Nairobi', '', '', '', '', '', '', '', 1),
(68, 'Aguascalientes', '', '', '', '', '', '', '', 1),
(69, 'Thailand', '', '', '', '', '', '', '', 1),
(70, 'Karachi', '', '', '', '', '', '', '', 1),
(71, 'Florida', '', '', '', '', '', '', '', 1),
(74, 'Al Khurtum', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(4, 'erwr', 'werw', 'werew', 'werwe'),
(5, 'iasgiusag', 'iugiui@ygcy.com', 'ccytcy', 'ytcytcyt'),
(6, 'iasgiusag', 'iugiui@ygcy.com', 'ccytcy', 'ytcytcyt'),
(7, 'sasa', 'sasssasa@sasa.com', 'yyuf', 'ytfyu'),
(8, 'ojhoucahgsiucgsaicaiugu', 'igiygiugiuguoi@gigiug.com', 'cuycyu', 'yugyugi'),
(9, 'iuhiuhUHIU', 'HIUHHI@HIUHI.COM', 'YYUGYU', 'YGUY');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `iso` char(3) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`iso`, `name`) VALUES
('KRW', '(South) Korean Won'),
('AFA', 'Afghanistan Afghani'),
('ALL', 'Albanian Lek'),
('DZD', 'Algerian Dinar'),
('ADP', 'Andorran Peseta'),
('AOK', 'Angolan Kwanza'),
('ARS', 'Argentine Peso'),
('AMD', 'Armenian Dram'),
('AWG', 'Aruban Florin'),
('AUD', 'Australian Dollar'),
('BSD', 'Bahamian Dollar'),
('BHD', 'Bahraini Dinar'),
('BDT', 'Bangladeshi Taka'),
('BBD', 'Barbados Dollar'),
('BZD', 'Belize Dollar'),
('BMD', 'Bermudian Dollar'),
('BTN', 'Bhutan Ngultrum'),
('BOB', 'Bolivian Boliviano'),
('BWP', 'Botswanian Pula'),
('BRL', 'Brazilian Real'),
('GBP', 'British Pound'),
('BND', 'Brunei Dollar'),
('BGN', 'Bulgarian Lev'),
('BUK', 'Burma Kyat'),
('BIF', 'Burundi Franc'),
('CAD', 'Canadian Dollar'),
('CVE', 'Cape Verde Escudo'),
('KYD', 'Cayman Islands Dollar'),
('CLP', 'Chilean Peso'),
('CLF', 'Chilean Unidades de Fomento'),
('COP', 'Colombian Peso'),
('XOF', 'Communauté Financière Africaine BCEAO - Francs'),
('XAF', 'Communauté Financière Africaine BEAC, Francs'),
('KMF', 'Comoros Franc'),
('XPF', 'Comptoirs Français du Pacifique Francs'),
('CRC', 'Costa Rican Colon'),
('CUP', 'Cuban Peso'),
('CYP', 'Cyprus Pound'),
('CZK', 'Czech Republic Koruna'),
('DKK', 'Danish Krone'),
('YDD', 'Democratic Yemeni Dinar'),
('DOP', 'Dominican Peso'),
('XCD', 'East Caribbean Dollar'),
('TPE', 'East Timor Escudo'),
('ECS', 'Ecuador Sucre'),
('EGP', 'Egyptian Pound'),
('SVC', 'El Salvador Colon'),
('EEK', 'Estonian Kroon (EEK)'),
('ETB', 'Ethiopian Birr'),
('EUR', 'Euro'),
('FKP', 'Falkland Islands Pound'),
('FJD', 'Fiji Dollar'),
('GMD', 'Gambian Dalasi'),
('GHC', 'Ghanaian Cedi'),
('GIP', 'Gibraltar Pound'),
('XAU', 'Gold, Ounces'),
('GTQ', 'Guatemalan Quetzal'),
('GNF', 'Guinea Franc'),
('GWP', 'Guinea-Bissau Peso'),
('GYD', 'Guyanan Dollar'),
('HTG', 'Haitian Gourde'),
('HNL', 'Honduran Lempira'),
('HKD', 'Hong Kong Dollar'),
('HUF', 'Hungarian Forint'),
('INR', 'Indian Rupee'),
('IDR', 'Indonesian Rupiah'),
('XDR', 'International Monetary Fund (IMF) Special Drawing Rights'),
('IRR', 'Iranian Rial'),
('IQD', 'Iraqi Dinar'),
('IEP', 'Irish Punt'),
('ILS', 'Israeli Shekel'),
('JMD', 'Jamaican Dollar'),
('JPY', 'Japanese Yen'),
('JOD', 'Jordanian Dinar'),
('KHR', 'Kampuchean (Cambodian) Riel'),
('KES', 'Kenyan Schilling'),
('KWD', 'Kuwaiti Dinar'),
('LAK', 'Lao Kip'),
('LBP', 'Lebanese Pound'),
('LSL', 'Lesotho Loti'),
('LRD', 'Liberian Dollar'),
('LYD', 'Libyan Dinar'),
('MOP', 'Macau Pataca'),
('MGF', 'Malagasy Franc'),
('MWK', 'Malawi Kwacha'),
('MYR', 'Malaysian Ringgit'),
('MVR', 'Maldive Rufiyaa'),
('MTL', 'Maltese Lira'),
('MRO', 'Mauritanian Ouguiya'),
('MUR', 'Mauritius Rupee'),
('MXP', 'Mexican Peso'),
('MNT', 'Mongolian Tugrik'),
('MAD', 'Moroccan Dirham'),
('MZM', 'Mozambique Metical'),
('NAD', 'Namibian Dollar'),
('NPR', 'Nepalese Rupee'),
('ANG', 'Netherlands Antillian Guilder'),
('YUD', 'New Yugoslavia Dinar'),
('NZD', 'New Zealand Dollar'),
('NIO', 'Nicaraguan Cordoba'),
('NGN', 'Nigerian Naira'),
('KPW', 'North Korean Won'),
('NOK', 'Norwegian Kroner'),
('OMR', 'Omani Rial'),
('PKR', 'Pakistan Rupee'),
('XPD', 'Palladium Ounces'),
('PAB', 'Panamanian Balboa'),
('PGK', 'Papua New Guinea Kina'),
('PYG', 'Paraguay Guarani'),
('PEN', 'Peruvian Nuevo Sol'),
('PHP', 'Philippine Peso'),
('XPT', 'Platinum, Ounces'),
('PLN', 'Polish Zloty'),
('QAR', 'Qatari Rial'),
('RON', 'Romanian Leu'),
('RUB', 'Russian Ruble'),
('RWF', 'Rwanda Franc'),
('WST', 'Samoan Tala'),
('STD', 'Sao Tome and Principe Dobra'),
('SAR', 'Saudi Arabian Riyal'),
('SCR', 'Seychelles Rupee'),
('SLL', 'Sierra Leone Leone'),
('XAG', 'Silver, Ounces'),
('SGD', 'Singapore Dollar'),
('SKK', 'Slovak Koruna'),
('SBD', 'Solomon Islands Dollar'),
('SOS', 'Somali Schilling'),
('ZAR', 'South African Rand'),
('LKR', 'Sri Lanka Rupee'),
('SHP', 'St. Helena Pound'),
('SDP', 'Sudanese Pound'),
('SRG', 'Suriname Guilder'),
('SZL', 'Swaziland Lilangeni'),
('SEK', 'Swedish Krona'),
('CHF', 'Swiss Franc'),
('SYP', 'Syrian Potmd'),
('TWD', 'Taiwan Dollar'),
('TZS', 'Tanzanian Schilling'),
('THB', 'Thai Baht'),
('TOP', 'Tongan Paanga'),
('TTD', 'Trinidad and Tobago Dollar'),
('TND', 'Tunisian Dinar'),
('TRY', 'Turkish Lira'),
('UGX', 'Uganda Shilling'),
('AED', 'United Arab Emirates Dirham'),
('UYU', 'Uruguayan Peso'),
('USD', 'US Dollar'),
('VUV', 'Vanuatu Vatu'),
('VEF', 'Venezualan Bolivar'),
('VND', 'Vietnamese Dong'),
('YER', 'Yemeni Rial'),
('CNY', 'Yuan (Chinese) Renminbi'),
('ZRZ', 'Zaire Zaire'),
('ZMK', 'Zambian Kwacha'),
('ZWD', 'Zimbabwe Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `document_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_type`
--

CREATE TABLE `document_type` (
  `document_type_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_type`
--

INSERT INTO `document_type` (`document_type_id`, `document_name`, `merchant_id`, `status`) VALUES
(1, 'license', 1, 1),
(2, 'Insurance', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `driver_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `driver_id`, `amount`, `distance`, `tot_time`, `payment_status`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.42088233528142', '77.03551355749369', '28.42095752634667', '77.0357422158122', 'Tikri, Sector 48,Gurugram, Haryana 122001,', 'Bellevue Towers Road, Central Park II, Sector 48, Gurugram, Haryana 122004, India, null, null, null', '10:13:51 PM', '10:13:56 PM', '10:14:01 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 0, 1, 0, '0000-00-00'),
(2, 2, '28.412052468868172', '77.04336974769832', '28.412071341929867', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India, null, null, null', '12:42:12 PM', '12:42:20 PM', '12:42:26 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 1, 1, 1, '0000-00-00'),
(3, 6, '28.412052468868172', '77.04336974769832', '28.412090214988226', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India, null, null, null', '02:51:34 PM', '02:51:38 PM', '02:51:42 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 1, 1, 1, '0000-00-00'),
(4, 7, '', '', '', '', '', '', '02:53:21 PM', '', '', '0', '0', '0', 1, '', '', '', 0, 0, 0, '0000-00-00'),
(5, 8, '', '', '', '', '', '', '02:54:09 PM', '', '', '0', '0', '0', 1, '', '', '', 0, 0, 0, '0000-00-00'),
(6, 11, '28.412052468868172', '77.04336974769832', '28.412416364496657', '77.0435357093811', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India, null, null, null', '03:23:50 PM', '03:23:59 PM', '03:24:03 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 1, 1, 1, '0000-00-00'),
(7, 13, '28.412052468868172', '77.04336974769832', '28.412061905399437', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:24:56 PM', '06:25:03 PM', '06:25:07 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 1, 0, 1, '0000-00-00'),
(8, 19, '28.412052468868172', '77.04336974769832', '28.412061905399437', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:29:27 PM', '08:29:33 PM', '08:29:37 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 0, 1, 0, '0000-00-00'),
(9, 20, '28.412052468868172', '77.04336974769832', '28.41200705554962', '77.04326916486025', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:31:10 PM', '08:31:15 PM', '08:31:20 PM', '0', '0.00', '00.00', 1, '100.00', '0.00', '0', 1, 1, 1, '0000-00-00'),
(10, 22, '28.412054828001068', '77.04328458756208', '28.412139756750417', '77.0432909578085', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:48:18 PM', '12:48:20 PM', '12:48:24 PM', '0', '0.00', '00.00', 12, '178.00', '0.00', '0', 0, 1, 0, '0000-00-00'),
(11, 23, '28.412057776917123', '77.0432835817337', '28.412050994410066', '77.04325709491968', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:36:42 PM', '01:36:48 PM', '01:36:55 PM', '0', '0.00', '00.00', 4, '178.00', '0.00', '0', 1, 1, 1, '0000-00-00'),
(12, 32, '', '', '', '', '', '', '02:07:34 PM', '', '', '0', '0', '0', 4, '', '', '', 0, 0, 0, '0000-00-00'),
(13, 39, '-5.710251380485966', '-78.80996480584145', '-5.70971059450736', '-78.80741368979216', 'Calle Diego Palomino 1794,JaÃ©n,', 'Calle Diego Palomino 1381, JaÃ©n, PerÃº, null', '06:18:19 PM', '06:18:22 PM', '06:18:26 PM', '0', '0.00', '0.00', 2, '0.00', '0.00', '0', 1, 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `rc` text NOT NULL,
  `insurance` text NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` varchar(255) NOT NULL,
  `reject_rides` varchar(255) NOT NULL,
  `cancelled_rides` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `driver_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `rc`, `insurance`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `driver_admin_status`) VALUES
(1, 'ankit', 'ankit@gmail.com', '9835424567', '', '123456', '2HEOWvkqTv7eoMxK', 'c3z4n9r-yfE:APA91bEzGD6hzF1Gkyo0KN6i4WU9NoALy-8TxTc6HC9yPwIqOkeiEFTdhxH7FK4uUb2lKk9ARVGQBJsJdA9Db8Qt0SNtxduLIsW4EfYEedLlXYQEZAu69S10HklomqV2oKAMfcPrmFVy', 2, '4.8', 2, 12, 'sdfg', 56, 'Saturday, Jul 1', 'uploads/driver/1498919192license_1.jpg', 'uploads/driver/1498919192rc_1.jpg', 'uploads/driver/1498919192insurance_1.jpg', '', '', '', '0.00', '0.00', '0.00', '28.41207222660456', '77.04337008297443', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India, null, null, null', '10:55', 'Saturday, Jul 15, 2017', '7', '1', '3', 1, 0, 1, 2, 0, 1),
(2, 'julio', 'abcj_0306@hotmail.com', '921476525', '', '111111111', 'orYoormf4V5q2ODb', 'dOCssho4nVk:APA91bGHFUZyQ0wr9abfVO5kUg1waiOxJ-v0SF-jbNyvFu2zPOgAGm83FN314zzg4peUXj0g4cQCKz0kBq8Txx4eXJlgSudPcKQf-qQ96_53-1sT94xzMZ2rwtvny3wii9suxyHHP4fW', 2, '', 3, 20, 'm3p465', 70, 'Tuesday, Jul 4', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-5.709696582772558', '-78.80741368979216', 'Calle Diego Palomino 1381, JaÃ©n, PerÃº, null', '01:03', 'Friday, Jul 28, 2017', '1', '', '', 1, 0, 1, 1, 0, 1),
(3, 'sonu', 'sonu@gmail.com', '8547693258', '', '123456', 'IgnjsfwlglxlG8LW', 'cJYakQTL-70:APA91bEBR3sahrXo-r12s9L2Gm5jF1fDWqRorgRSrFdV1h-Ro4Mm-u4wp-JZspAct471bgw20hvz4WYNj507P9wQhC8fng1QEKyQ0y3l-yTW8GoXjOC2-8bghl0r-T7KhJW0dKmadMR2', 2, '', 5, 18, 'fghb', 56, 'Thursday, Jul 6', 'uploads/driver/1499346844license_3.jpg', 'uploads/driver/1499346844rc_3.jpg', 'uploads/driver/1499346844insurance_3.jpg', '', '', '', '0.00', '0.00', '0.00', '28.41209109966277', '77.04337008297443', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India, null, null, null', '18:44', 'Thursday, Jul 6, 2017', '', '', '', 1, 0, 1, 2, 0, 1),
(4, 'abcd', 'abcd@gmail.com', '9825456985', '', '123456', 'tMoU3supOMcU8hxA', 'e8T4IA2Wxw8:APA91bGreKcC-j-i8NlWo6ysYqXMWWAtwaImc59EihYhn95WLQYrU8fcRF2UCnBq_DIlj-_hjJ1xS0prKHbDKgZsj9UtSfRwNWJwiizsT-aCRmEaWcQ8_OM6x3r0jZb0rUawy3QckhZi', 2, '3.5', 4, 11, 'yffyfy', 56, 'Friday, Jul 7', 'uploads/driver/1499673896license_4.jpg', 'uploads/driver/1499673896rc_4.jpg', 'uploads/driver/1499673896insurance_4.jpg', '', '', '', '0.00', '0.00', '0.00', '28.412098177058724', '77.04325709491968', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:49', 'Friday, Jul 14, 2017', '1', '1', '3', 2, 0, 2, 2, 0, 1),
(5, 'saini', 'saini@gmail.com', '9835486952', '', '123456', 'PyzSOjofLCU04dkW', 'eLD28RrQ4-0:APA91bF6h_3yk4TTyQ6Qn5qLbBFofRVML0-c3tm9V9NJ4h29VAIRJsI--UxmpZosFl83Pecv8PRZCebQ5GgoVXgcZ6jNkmeBoZzrLeVQTIcfWHaIt-4adNg-GiQK5WrdKYlHtHZLmBzu', 2, '', 3, 20, 'gdhhd', 56, 'Friday, Jul 7', 'uploads/driver/1499432591license_5.jpg', 'uploads/driver/1499432591rc_5.jpg', 'uploads/driver/1499432591insurance_5.jpg', '', '', '', '0.00', '0.00', '0.00', '28.41206043094149', '77.04325709491968', 'Not yet fetched', '06:38', 'Friday, Jul 7, 2017', '', '', '', 2, 0, 1, 2, 0, 1),
(6, 'hdhfh', 'gdhh@gmail.co', '9858698547', '', '123456', 'VL7hY8B7zzpyuePh', 'eLD28RrQ4-0:APA91bF6h_3yk4TTyQ6Qn5qLbBFofRVML0-c3tm9V9NJ4h29VAIRJsI--UxmpZosFl83Pecv8PRZCebQ5GgoVXgcZ6jNkmeBoZzrLeVQTIcfWHaIt-4adNg-GiQK5WrdKYlHtHZLmBzu', 2, '', 4, 11, 'gd', 56, 'Friday, Jul 7', 'uploads/driver/1499432976license_6.jpg', 'uploads/driver/1499432976rc_6.jpg', 'uploads/driver/1499432976insurance_6.jpg', '', '', '', '0.00', '0.00', '0.00', '28.412079304001782', '77.04325709491968', 'Not yet fetched', '07:02', 'Friday, Jul 7, 2017', '', '', '', 2, 0, 0, 2, 0, 1),
(7, 'hdhfh', 'gdhh11@gmail.co', '9958698547', '', '123456', 'M2RVcjinuVSxEdp0', '', 0, '', 4, 11, 'gd', 56, 'Friday, Jul 7', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', '', '', '', 1, 0, 0, 1, 0, 1),
(8, 'abcd', 'abcd11@gmail.com', '9999999988', 'uploads/driver/1499434162driver_8.png', '123456', 'mynUyQDWZLP9K19V', '', 0, '', 4, 11, '45sfhg', 56, 'Friday, Jul 7', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', '', '', '', 1, 0, 0, 1, 0, 1),
(9, 'gjgjh', 'ggggh@gmail.com', '9807580965', 'uploads/driver/1499434798driver_9.jpg', '123456', '5DRqepuaXxLl2A5Q', 'eLD28RrQ4-0:APA91bF6h_3yk4TTyQ6Qn5qLbBFofRVML0-c3tm9V9NJ4h29VAIRJsI--UxmpZosFl83Pecv8PRZCebQ5GgoVXgcZ6jNkmeBoZzrLeVQTIcfWHaIt-4adNg-GiQK5WrdKYlHtHZLmBzu', 2, '', 5, 18, 'yruru', 56, 'Friday, Jul 7', 'uploads/driver/1499434393license_9.jpg', 'uploads/driver/1499434393rc_9.jpg', 'uploads/driver/1499434393insurance_9.jpg', '', '', '', '0.00', '0.00', '0.00', '28.412098177058724', '77.04325709491968', 'Not yet fetched', '07:32', 'Friday, Jul 7, 2017', '', '', '', 2, 0, 0, 2, 0, 1),
(10, 'hjjhjh', 'yuuuy@gmail.com', '9852470896', '', '123456', '6k4FpTegsdep5Psk', 'eLD28RrQ4-0:APA91bF6h_3yk4TTyQ6Qn5qLbBFofRVML0-c3tm9V9NJ4h29VAIRJsI--UxmpZosFl83Pecv8PRZCebQ5GgoVXgcZ6jNkmeBoZzrLeVQTIcfWHaIt-4adNg-GiQK5WrdKYlHtHZLmBzu', 2, '', 4, 11, 'hcgugi', 56, 'Friday, Jul 7', 'uploads/driver/1499436274license_10.jpg', 'uploads/driver/1499436274rc_10.jpg', 'uploads/driver/1499436274insurance_10.jpg', '', '', '', '0.00', '0.00', '0.00', '28.412107613585917', '77.04325709491968', 'Not yet fetched', '07:47', 'Friday, Jul 7, 2017', '', '', '', 2, 0, 0, 2, 0, 1),
(11, 'gd ga h', 'gdhs@gmail.com', '8547258695', '', '123456', 'ftBluPW63Ws8AHZR', '', 0, '', 4, 11, 'gxhdh', 56, 'Friday, Jul 7', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '20:00', 'Friday, Jul 14, 2017', '', '', '', 1, 0, 0, 1, 0, 1),
(12, 'qwery', 'query@gmail.com', '7997976666', 'uploads/driver/1499521250driver_12.jpg', 'aggzgz', 'wuBTPid1WseoQKX9', 'cjX_9x89-dg:APA91bH1H1Fb14jaSkK1vQLiWhP6CAfoN3cDPgHqi_-aRE5HSIC4Xd_7l_l1nTPygf0n1R_kQ95kQ1V5-7icT3kcZQOhGe635Q2DA6SDUgsORAlr5ZU2dzZ_k2AuJyNxOwx23YzqEc_M', 2, '', 4, 11, 'vabaH', 56, 'Saturday, Jul 8', 'uploads/driver/1499520572license_12.jpg', 'uploads/driver/1499520572rc_12.jpg', 'uploads/driver/1499520572insurance_12.jpg', '', '', '', '0.00', '0.00', '0.00', '28.412107318694453', '77.04328492283821', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '16:22', 'Monday, Jul 10, 2017', '1', '', '', 1, 0, 2, 2, 0, 1),
(13, 'fernando', 'nfernandozv@gmail.com', '988927919', '', '999228860', '1HI6mFvtVr0wNfWn', 'cuWpWcetmaY:APA91bEpAg3KKz8B_Y3YtVMFHYS4fKSLI_QJ5N-T2s59rcsqFHV2D8bjkcKXKTEPl3sajypQlEw1yYtnBErpdRQ07Dw4MViW6bhLrcVLmjZV5ZrjEYjZhqpgOwYlDSTrV-R0M63kxdGf', 2, '', 3, 20, '3253', 62, 'Tuesday, Jul 11', '', '', '', '', '', '', '0.00', '0.00', '0.00', '-5.7275586', '-78.7961728', 'Not yet fetched', '02:51', 'Tuesday, Jul 11, 2017', '', '', '', 1, 0, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Apporio Infolabs Pvt. Ltd. is an ISO certified</h2>\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n</div>\r\n</h1>\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n</div>\r\n</div>\r\n</h2>\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n</div>\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img class=\"alignnone size-full wp-image-434\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\" alt=\"iso_apporio\" width=\"232\" height=\"163\"></div>\r\n</div>\r\n</h2>\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Marketing :</h3>\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n</h2>\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">For more information, you can catch us on :</h3>\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n</h2>\r\n<a>\r\n</a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', '<h2 class=\"apporioh2\" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\" style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" rgb(121,=\"\" 121,=\"\" 121);\"=\"\">Apporio Infolabs Pvt. Ltd. is an ISO certified</h2><h1 style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" color:=\"\" rgb(121,=\"\" 121,=\"\" 121);\"=\"\"><p class=\"product_details\" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\" style=\"padding: 10px 0px;\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p><p class=\"product_details\" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\" style=\"margin-top: 0px; padding: 10px 0px;\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p><div class=\"row\" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51);\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div></div></h1><h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51);\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\" style=\"padding: 10px 0px;\">1. Native Android Application on Android Studio<br>2. Native iOS Application on Objective-C<br>3. Native iOS Application on Swift<br>4. Titanium Appcelerator<br>5. Iconic Framework</p></div></div></h2><h3 class=\"apporioh3\" style=\"margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important;\">Web Application :</h3><h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><div class=\"row\" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51);\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p></div><br></div></h2><h3 class=\"apporioh3\" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\" style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" rgb(121,=\"\" 121,=\"\" 121);\"=\"\">Marketing :</h3><h2 class=\"apporioh2\" style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><p class=\"product_details\" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\" style=\"padding: 10px 0px;\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p></h2><h3 class=\"apporioh3\" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\" style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" rgb(121,=\"\" 121,=\"\" 121);\"=\"\">For more information, you can catch us on :</h3><h2 class=\"apporioh2\" style=\"font-family: \" source=\"\" sans=\"\" pro\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><p class=\"product_details\" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\" style=\"padding: 10px 0px;\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p></h2>', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 2, 2, '1', 'Cash', 'Android', '100', '03 Jul, 2017', 'Done'),
(2, 3, 2, '1', 'Cash', 'Android', '100', '03 Jul, 2017', 'Done'),
(3, 6, 2, '1', 'Cash', 'Android', '100', '03 Jul, 2017', 'Done'),
(4, 7, 2, '1', 'Cash', 'Android', '100', '03 Jul, 2017', 'Done'),
(5, 9, 2, '1', 'Cash', 'Android', '100', '03 Jul, 2017', 'Done'),
(6, 11, 2, '1', 'Cash', 'Android', '178', '10 Jul, 2017', 'Done'),
(7, 13, 3, '1', 'Cash', 'Android', '0', '12 jul., 2017', 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `car_type_id`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(4, 3, 1, 0, 0, 0, 0, '4', '100', '30', '1', '10', '1', '10'),
(3, 56, 1, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(5, 3, 2, 0, 0, 0, 0, '4', '75', '20', '3', '12', '2', '15'),
(6, 3, 3, 0, 0, 0, 0, '1', '30', '1', '4', '1', '1', '1'),
(7, 3, 4, 0, 0, 0, 0, '4', '200', '40', '2', '15', '2', '13'),
(8, 3, 5, 0, 0, 0, 0, '5', '75', '14', '1', '11', '1', '15'),
(19, 57, 1, 0, 0, 0, 0, '1', '200', '2', '1', '2', '1', '1'),
(12, 56, 2, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 3, 0, 0, 0, 0, '4', '80', '16', '1', '10', '1', '15'),
(14, 56, 4, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 5, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(21, 71, 2, 0, 0, 0, 0, '100', '4', '10', '5', '1', '5', '5');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message` text NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message`, `push_app`) VALUES
(1, 'Test', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rating_customer`
--

CREATE TABLE `rating_customer` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_customer`
--

INSERT INTO `rating_customer` (`rating_id`, `ride_id`, `user_id`, `driver_id`, `rating_star`, `comment`) VALUES
(1, 2, 2, 1, '4.5', ''),
(2, 6, 2, 1, '4.5', ''),
(3, 11, 2, 1, '5.0', ''),
(4, 13, 2, 1, '5.0', ''),
(5, 20, 2, 1, '5.0', ''),
(6, 23, 2, 4, '3.5', '');

-- --------------------------------------------------------

--
-- Table structure for table `rating_driver`
--

CREATE TABLE `rating_driver` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_driver`
--

INSERT INTO `rating_driver` (`rating_id`, `ride_id`, `driver_id`, `user_id`, `rating_star`, `comment`) VALUES
(1, 1, 1, 2, '4.0', 'Static comment'),
(2, 2, 1, 2, '4.0', 'Static comment'),
(3, 6, 1, 2, '5.0', 'Static comment'),
(4, 11, 1, 2, '4.5', 'Static comment'),
(5, 19, 1, 2, '4.0', 'Static comment'),
(6, 20, 1, 2, '5.0', 'Static comment'),
(7, 22, 12, 2, '5.0', 'Static comment'),
(8, 23, 4, 2, '3.5', 'Static comment');

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 3, 3, 1),
(4, 4, 3, 1),
(5, 5, 4, 1),
(6, 6, 4, 1),
(7, 7, 4, 1),
(8, 8, 4, 1),
(9, 9, 4, 1),
(10, 10, 4, 1),
(11, 11, 4, 1),
(12, 12, 4, 1),
(13, 13, 4, 1),
(14, 14, 4, 1),
(15, 15, 4, 1),
(16, 16, 5, 1),
(17, 17, 2, 1),
(18, 18, 6, 1),
(19, 19, 6, 1),
(20, 20, 7, 1),
(21, 21, 6, 1),
(22, 22, 7, 1),
(23, 23, 7, 1),
(24, 24, 7, 1),
(25, 25, 7, 1),
(26, 26, 7, 1),
(27, 27, 7, 1),
(28, 28, 7, 1),
(29, 29, 7, 1),
(30, 30, 7, 1),
(31, 31, 7, 1),
(32, 32, 7, 1),
(33, 33, 6, 1),
(34, 34, 6, 1),
(35, 35, 7, 1),
(36, 36, 7, 1),
(37, 37, 7, 1),
(38, 39, 8, 1),
(39, 40, 8, 1),
(40, 41, 8, 1),
(41, 44, 9, 1),
(42, 1, 1, 1),
(43, 2, 1, 1),
(44, 3, 1, 1),
(45, 4, 1, 1),
(46, 5, 1, 1),
(47, 6, 1, 1),
(48, 7, 1, 1),
(49, 8, 1, 1),
(50, 9, 1, 1),
(51, 10, 1, 1),
(52, 11, 1, 1),
(53, 12, 1, 1),
(54, 13, 1, 1),
(55, 14, 1, 1),
(56, 15, 1, 1),
(57, 16, 1, 1),
(58, 17, 1, 1),
(59, 18, 1, 1),
(60, 19, 1, 1),
(61, 20, 1, 1),
(62, 21, 12, 1),
(63, 22, 12, 1),
(64, 23, 4, 1),
(65, 24, 4, 1),
(66, 25, 4, 1),
(67, 26, 4, 1),
(68, 27, 4, 1),
(69, 28, 4, 1),
(70, 29, 4, 1),
(71, 30, 4, 1),
(72, 31, 4, 1),
(73, 32, 4, 1),
(74, 33, 4, 1),
(75, 34, 4, 1),
(76, 35, 4, 1),
(77, 39, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 4, 1),
(2, 28, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 2, '', '28.42088233528142', '77.03551355749369', 'Tikri, Sector 48,Gurugram, Haryana 122001,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Dimanche, 2 Juillet', '22:13:34', '10:14:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.42088233528142,77.03551355749369&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(2, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '12:42:01', '12:42:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(3, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '14:48:28', '02:48:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(4, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:49:21', '02:49:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 4, 0, 1, 0, 1),
(5, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:49:34', '02:50:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 9, 8, 1, 0, 1),
(6, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:50:34', '02:51:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(7, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:53:11', '02:53:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 9, 8, 1, 0, 1),
(8, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:54:00', '02:54:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 9, 8, 1, 0, 1),
(9, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:54:31', '02:56:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 1, 0, 1),
(10, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 3 Juillet', '14:57:56', '03:01:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 3, 1, 0, 1),
(11, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.413388909238044', '77.04210642725229', '1102, Sohna Road,Block S, Sispal Vihar, Sector 47,Gurugram, Haryana 122004,', 'Lundi, 3 Juillet', '15:23:11', '03:24:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.413388909238044,77.04210642725229&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(12, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '17:37:03', '05:37:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(13, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '18:24:20', '06:25:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(14, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '18:25:39', '06:25:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 0, 0, 1, 0, 1),
(15, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '18:27:16', '06:27:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(16, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '20:23:06', '08:23:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '20:26:34', '08:26:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(18, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '20:27:28', '08:27:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(19, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '20:28:28', '08:29:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(20, 2, '', '28.412052468868172', '77.04336974769832', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 3 Juillet', '20:30:43', '08:31:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412052468868172,77.04336974769832&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 7, 0, 1, 0, 1),
(21, 2, '', '28.412053943326224', '77.04328391700983', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 10 Juillet', '12:44:34', '12:44:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412053943326224,77.04328391700983&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(22, 2, '', '28.412054828001068', '77.04328458756208', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 10 Juillet', '12:47:31', '12:48:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412054828001068,77.04328458756208&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 4, 1, 7, 0, 1, 0, 1),
(23, 2, '', '28.412057776917123', '77.0432835817337', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 10 Juillet', '13:36:22', '01:36:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412057776917123,77.0432835817337&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 7, 0, 1, 0, 1),
(24, 2, '', '28.412057776917123', '77.0432835817337', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.524578658081488', '77.20661502331495', 'Pramod Mahajan Marg,Block A, Block D, Saket,New Delhi, Delhi 110017,', 'Lundi, 10 Juillet', '13:37:36', '01:37:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412057776917123,77.0432835817337&markers=color:red|label:D|28.524578658081488,77.20661502331495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(25, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:38:40', '01:38:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 0, 0, 1, 0, 1),
(26, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:39:50', '01:39:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 0, 0, 1, 0, 1),
(27, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:41:13', '01:41:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(28, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:42:04', '01:42:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 4, 0, 1, 0, 1),
(29, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:42:18', '01:43:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(30, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.535516056746474', '77.39102661609651', 'Dadri Main Road,Sector - 106,Noida, Uttar Pradesh 201304,', 'Lundi, 10 Juillet', '13:43:44', '01:44:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.535516056746474,77.39102661609651&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(31, 2, '', '28.412059841158307', '77.04328291118145', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.565491391388207', '77.39108830690384', 'Sector 77,Noida, Uttar Pradesh,', 'Lundi, 10 Juillet', '13:45:08', '01:54:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412059841158307,77.04328291118145&markers=color:red|label:D|28.565491391388207,77.39108830690384&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(32, 2, '', '28.412058366700307', '77.04328693449497', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 10 Juillet', '14:06:55', '03:01:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412058366700307,77.04328693449497&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(33, 2, '', '28.412058366700307', '77.04328693449497', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.447657591990623', '77.07504864782095', '264, Bhagwan Mahaveer Marg,Indira Colony, Sector 44,Gurugram, Haryana 122003,', 'Lundi, 10 Juillet', '15:02:11', '03:17:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412058366700307,77.04328693449497&markers=color:red|label:D|28.447657591990623,77.07504864782095&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(34, 2, '', '28.41204155787781', '77.04325709491968', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 10 Juillet', '15:20:06', '03:22:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.41204155787781,77.04325709491968&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(35, 2, '', '28.41204155787781', '77.04325709491968', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', '28.459269417552733', '77.07241907715797', 'Sector 29,Gurugram, Haryana 122022,', 'Lundi, 10 Juillet', '15:25:02', '03:26:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.41204155787781,77.04325709491968&markers=color:red|label:D|28.459269417552733,77.07241907715797&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 4, 1, 9, 8, 1, 0, 1),
(36, 3, '', '-5.709722604565479', '-78.80741737782955', 'Calle Diego Palomino 1381,JaÃ©n,', '-5.698988514763737', '-78.81219238042831', '4 de Junio 109,JaÃ©n,', 'Mercredi, Juillet 12', '18:14:08', '06:14:08 PM', '', 'Mercredi, Juillet 12', '07:43', 0, 2, 2, 1, 0, 1, 0, 1),
(37, 3, '', '-5.709722604565479', '-78.80741737782955', 'Calle Diego Palomino 1381,JaÃ©n,', '-5.698988514763737', '-78.81219238042831', '4 de Junio 109,JaÃ©n,', 'Mercredi, Juillet 12', '18:14:45', '06:14:45 PM', '', 'Mercredi, Juillet 12', '07:44', 0, 3, 2, 1, 0, 1, 0, 1),
(38, 3, '', '-5.710251380485966', '-78.80996480584145', 'Calle Diego Palomino 1794,JaÃ©n,', '-5.703587767392745', '-78.81217662245035', 'Orellana 1078,JaÃ©n,', 'Mercredi, Juillet 12', '18:16:51', '06:16:51 PM', '', 'Mercredi, Juillet 12', '07:46', 0, 3, 2, 1, 0, 1, 0, 1),
(39, 3, '', '-5.710251380485966', '-78.80996480584145', 'Calle Diego Palomino 1794,JaÃ©n,', '-5.70971059450736', '-78.80741368979216', 'Calle Diego Palomino 1381,JaÃ©n,', 'Mercredi, 12 Juillet', '18:17:24', '06:18:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|-5.710251380485966,-78.80996480584145&markers=color:red|label:D|-5.70971059450736,-78.80741368979216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 7, 0, 1, 0, 1),
(40, 3, '', '-7.147539137382344', '-78.52223016321659', 'Avenida VÃ­a de Evitamiento Norte 350,Cajamarca,', '-7.183748241614434', '-78.49445756524803', 'Avenida Industrial 249,Cajamarca,', 'Tuesday, Jul 25', '00:31:47', '12:31:47 AM', '', 'Monday, Jul 24', '14:01', 0, 2, 2, 1, 0, 1, 0, 1),
(41, 3, '', '-7.155758691330081', '-78.52078277617693', 'Pisagua 565,Cajamarca,', '-7.177864391517007', '-78.5041993483901', 'Jr. Santa Anita 289,Cajamarca,', 'Friday, Jul 28', '01:02:15', '01:02:15 AM', '', 'Thursday, Jul 27', '14:31', 0, 2, 2, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_track`
--

CREATE TABLE `ride_track` (
  `ride_track_id` int(11) NOT NULL,
  `ride_id` bigint(11) NOT NULL,
  `driver_id` bigint(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `driver_lat` text NOT NULL,
  `driver_long` text NOT NULL,
  `driver_location` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `role_permission` longtext NOT NULL,
  `role_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `role_permission`, `role_status`) VALUES
(15, 'Demo User', '{\"user_view\":\"1\",\"user_delete\":\"0\",\"driver_view\":\"1\",\"driver_delete\":\"0\",\"transaction_view\":\"1\",\"transaction_delete\":\"0\",\"ride_view\":\"1\",\"ride_delete\":\"0\",\"city_add\":\"0\",\"city_view\":\"1\",\"city_delete\":\"0\",\"city_update\":\"0\",\"carmodel_add\":\"0\",\"carmodel_view\":\"1\",\"carmodel_delete\":\"0\",\"carmodel_update\":\"0\",\"cartype_add\":\"0\",\"cartype_view\":\"1\",\"cartype_delete\":\"0\",\"cartype_update\":\"0\",\"coupon_add\":\"1\",\"coupon_view\":\"1\",\"coupon_update\":\"0\",\"coupon_delete\":\"0\",\"ratecard_add\":\"0\",\"ratecard_view\":\"1\",\"ratecard_update\":\"0\",\"ratecard_delete\":\"0\",\"view_now\":\"1\",\"delete_now\":\"0\",\"view_later\":\"1\",\"delete_later\":\"0\",\"account\":\"1\",\"about\":\"1\",\"call\":\"1\",\"terms\":\"1\",\"role_add\":\"0\",\"role_view\":\"1\",\"role_update\":\"0\",\"role_delete\":\"0\",\"subadmin_add\":\"1\",\"subadmin_view\":\"1\",\"subadmin_update\":\"0\",\"subadmin_delete\":\"0\"}', 1),
(16, 'Dispatcher ', '{\"user_view\":\"1\",\"user_delete\":\"0\",\"driver_view\":\"1\",\"driver_delete\":\"0\",\"transaction_view\":\"0\",\"transaction_delete\":\"0\",\"ride_view\":\"1\",\"ride_delete\":\"0\",\"city_add\":\"0\",\"city_view\":\"1\",\"city_delete\":\"0\",\"city_update\":\"0\",\"carmodel_add\":\"0\",\"carmodel_view\":\"0\",\"carmodel_delete\":\"0\",\"carmodel_update\":\"0\",\"cartype_add\":\"0\",\"cartype_view\":\"0\",\"cartype_delete\":\"0\",\"cartype_update\":\"0\",\"coupon_add\":\"0\",\"coupon_view\":\"0\",\"coupon_update\":\"0\",\"coupon_delete\":\"0\",\"ratecard_add\":\"0\",\"ratecard_view\":\"0\",\"ratecard_update\":\"0\",\"ratecard_delete\":\"0\",\"view_now\":\"0\",\"delete_now\":\"0\",\"view_later\":\"0\",\"delete_later\":\"0\",\"account\":\"1\",\"about\":\"1\",\"call\":\"1\",\"terms\":\"1\",\"role_add\":\"0\",\"role_view\":\"0\",\"role_update\":\"0\",\"role_delete\":\"0\",\"subadmin_add\":\"0\",\"subadmin_view\":\"0\",\"subadmin_update\":\"0\",\"subadmin_delete\":\"0\"}', 1),
(18, 'Joint Pay', '{\"user_view\":\"1\",\"user_delete\":\"1\",\"driver_view\":\"1\",\"driver_delete\":\"1\",\"transaction_view\":\"1\",\"transaction_delete\":\"1\",\"ride_view\":\"1\",\"ride_delete\":\"1\",\"city_add\":\"1\",\"city_view\":\"1\",\"city_delete\":\"0\",\"city_update\":\"0\",\"carmodel_add\":\"1\",\"carmodel_view\":\"1\",\"carmodel_delete\":\"0\",\"carmodel_update\":\"0\",\"cartype_add\":\"1\",\"cartype_view\":\"1\",\"cartype_delete\":\"0\",\"cartype_update\":\"0\",\"coupon_add\":\"1\",\"coupon_view\":\"1\",\"coupon_update\":\"0\",\"coupon_delete\":\"0\",\"ratecard_add\":\"1\",\"ratecard_view\":\"1\",\"ratecard_update\":\"0\",\"ratecard_delete\":\"0\",\"view_now\":\"1\",\"delete_now\":\"1\",\"view_later\":\"1\",\"delete_later\":\"1\",\"account\":\"1\",\"about\":\"0\",\"call\":\"1\",\"terms\":\"0\",\"role_add\":\"1\",\"role_view\":\"1\",\"role_update\":\"0\",\"role_delete\":\"0\",\"subadmin_add\":\"1\",\"subadmin_view\":\"1\",\"subadmin_update\":\"0\",\"subadmin_delete\":\"0\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 2, 'Yogesh', 'Yadav@gmail.com', 'ygg usqgiuds\r\n', 'ygg usqgiuds\r\n'),
(2, 0, 'sasa', 'sasssasa@sasa.com', 'yyuf', 'ytfyu');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `register_date`, `device_id`, `flag`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `status`) VALUES
(1, 'Apporio lalit .', 'apporiolalit@gmail.com', '+919540956147', '', '', '', 'eOgzSG8Fkp0:APA91bHHHQsTzLY8WnvGAoj-aQSvmGUxFXdRTbughYX_1hwXrSPZr_YmunL1815uIviUtxU5hXEH-saAu8Y15fleN4r4MYi24TY2xykFHeheESFjnpl6cp-KRRf25stHhVuxvd52spu6', 2, '', 0, 0, 0, 0, 0, '', '', '', '', '', '112863616453781319776', 'Apporio lalit', 'apporiolalit@gmail.com', 'google user image', '', '', 0, 0, '', 1),
(2, 'mku .', '', '+2498285469069', '123456', '', 'Sunday, Jul 2', 'dqdCJQ7G8u0:APA91bFjqqLBS_pOWXleMp4pbVPb89Lb13OrdS77511YfKpUIjyXbJujll3UxKF1HO3DHZybtB9_NHD0sv42Iaq3KjqKuwMvMU1Rjy5BCoS_HqryZbPAuYGp57zcxu2RIyEi1HzUMjsf', 2, '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.375', 1),
(3, 'Julio Cesar', 'abcj_0306@hotmail.com', '+51931476525', '', '', '', 'd8wUbNwiFBM:APA91bG06SUqeguxa5LCy93IFDWbZkjtS9WmQgxNpIM0o5v96Zie_MrOQQRBme1P8FWkejGlcaoGkMRQfS7dK8xBTRAwE2gUj9Vv9P6zQoidEDd6t8XunLMaEpXpLbh2rI3AwpPZXTND', 2, '', 0, 0, 0, 0, 0, '1979622012268863', 'abcj_0306@hotmail.com', 'facebook imgae url need to send', 'Julio', 'Cesar', '', '', '', '', '', '', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '1', '1.0', '1'),
(2, 'Android', 'Driver', '', '1', '1.0', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_setting`
--
ALTER TABLE `account_setting`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`iso`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`document_type_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rating_customer`
--
ALTER TABLE `rating_customer`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `rating_driver`
--
ALTER TABLE `rating_driver`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `ride_track`
--
ALTER TABLE `ride_track`
  ADD PRIMARY KEY (`ride_track_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_setting`
--
ALTER TABLE `account_setting`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_type`
--
ALTER TABLE `document_type`
  MODIFY `document_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rating_customer`
--
ALTER TABLE `rating_customer`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rating_driver`
--
ALTER TABLE `rating_driver`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `ride_track`
--
ALTER TABLE `ride_track`
  MODIFY `ride_track_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
