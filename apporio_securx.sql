-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:22 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_securx`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL,
  `job_payment` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `paid_ammount` varchar(255) NOT NULL,
  `penalty` varchar(255) NOT NULL,
  `remark` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `account_details`
--

CREATE TABLE `account_details` (
  `id` int(11) NOT NULL,
  `installer_id` varchar(255) NOT NULL,
  `account_no` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `branch_address` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_password` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_email`, `admin_password`) VALUES
(1, 'apporio@infolabs.com', '8b1a9953c4611296a827abf8c47804d7');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `business_name` varchar(100) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `phone` varchar(55) NOT NULL,
  `phone1` varchar(20) NOT NULL,
  `email` varchar(55) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `business_name`, `first_name`, `last_name`, `phone`, `phone1`, `email`, `address`, `pincode`, `city`, `state`) VALUES
(1, 'Aamir', 'Aamir', 'Brar', '9654032955', '', 'yogeshkumar2491@gmail.com', 'spaze it park', '123456', 'Gurugram', 'Haryana');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employeeNumber` int(11) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `officeCode` varchar(10) NOT NULL,
  `file_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `jobTitle` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `holyday`
--

CREATE TABLE `holyday` (
  `holiday_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL,
  `holiday_startdate` date NOT NULL,
  `holiday_enddate` date NOT NULL,
  `holiday_starttime` text NOT NULL,
  `holiday_endtime` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holyday`
--

INSERT INTO `holyday` (`holiday_id`, `installer_id`, `holiday_startdate`, `holiday_enddate`, `holiday_starttime`, `holiday_endtime`) VALUES
(4, 10, '2017-04-21', '2017-04-21', '10:16', '10:15'),
(5, 10, '2017-04-21', '2017-04-21', '10:16', '18:30'),
(6, 4, '2017-04-21', '2017-04-24', '13:37', '13:37'),
(7, 5, '2017-04-22', '2017-05-22', '10:43', '10:43'),
(8, 4, '2017-04-25', '2017-04-30', '10:07', '10:07'),
(9, 4, '2017-05-01', '2017-04-25', '10:00', '12:00'),
(11, 1, '2017-04-25', '2017-04-26', '12:09', '12:10'),
(12, 2, '2017-04-25', '2017-04-27', '12:22', '12:22');

-- --------------------------------------------------------

--
-- Table structure for table `installers`
--

CREATE TABLE `installers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `phone` varchar(55) NOT NULL,
  `password` varchar(55) NOT NULL,
  `address` varchar(255) NOT NULL,
  `google_address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `rating` varchar(255) NOT NULL,
  `installer_image` text NOT NULL,
  `installer_latitude` text NOT NULL,
  `installer_longitude` text NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `account_number` varchar(200) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `branch_address` varchar(255) NOT NULL,
  `ifsc_code` varchar(50) NOT NULL,
  `sole_proreitor` varchar(255) DEFAULT NULL,
  `year_incarportion` varchar(255) DEFAULT NULL,
  `total_employee` varchar(255) DEFAULT NULL,
  `local_tax_reg` varchar(255) DEFAULT NULL,
  `central_reg_no` varchar(255) DEFAULT NULL,
  `roc_reg_no` varchar(255) DEFAULT NULL,
  `tin_no` varchar(255) DEFAULT NULL,
  `aadhar_card` varchar(255) NOT NULL,
  `aadhar_card_number` text NOT NULL,
  `pan_card` text NOT NULL,
  `pan_card_number` text NOT NULL,
  `voter_card` text NOT NULL,
  `voter_card_number` text NOT NULL,
  `driving_licence` text NOT NULL,
  `driving_licence_number` text NOT NULL,
  `installer_type` int(2) NOT NULL DEFAULT '2',
  `product_category_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '0',
  `active_deactive` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `installers`
--

INSERT INTO `installers` (`id`, `first_name`, `last_name`, `email`, `phone`, `password`, `address`, `google_address`, `city`, `state`, `flag`, `device_id`, `rating`, `installer_image`, `installer_latitude`, `installer_longitude`, `pincode`, `account_number`, `bank_name`, `branch_address`, `ifsc_code`, `sole_proreitor`, `year_incarportion`, `total_employee`, `local_tax_reg`, `central_reg_no`, `roc_reg_no`, `tin_no`, `aadhar_card`, `aadhar_card_number`, `pan_card`, `pan_card_number`, `voter_card`, `voter_card_number`, `driving_licence`, `driving_licence_number`, `installer_type`, `product_category_id`, `login_logout`, `active_deactive`, `detail_status`, `status`) VALUES
(2, 'Zggz', 'Zhzh', 'p@gmail.com', '9560049459', '1234567', 'xyzy-vczb,xyz,zuzu', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Gurugram', 'Haryana', 2, 'cyRbbzC8GUE:APA91bHttSVD3ZMA_-iZ-cRluzPFyKJlqcSEmfzMVctqfj1CBahnuHrgil5bIAaNru0c6WX9Xfi6x93gWgA2_NVWdlHM71s7J2ChJY1ZauSwbl1AbbQrlhdx27lnEyXw_yJUS73RmPBD', '', 'uploads/1492693438132.jpg', '28.4113312', '77.0437399', '122018', '7676', 'bxbz', 'hzhhzh', 'bzbz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/7557545454541492693474.jpg', '755754545454', 'uploads/vcxbdbhdhd1492693474.jpg', 'vcxbdbhdhd', 'uploads/xhxbhdhdhd1492693474.jpg', 'xhxbhdhdhd', 'uploads/tzgzhdbdbdbdbfbd1492693474.jpg', 'tzgzhdbdbdbdbfbd', 1, '1,2', 1, 1, 2, 1),
(3, 'Sudeep', 'Rajvanshi', 'sudeeprajvanshi123@gmail.com', '9643634980', '9582356543', 'd276-sudeep,34,sudeep', 'Nala Road,Block E, Block C, New Ashok Nagar,New Delhi, Delhi 110096,', 'New Delhi', 'Delhi', 2, 'dSZ43PktGP0:APA91bEtOwao7ivMC-i1h59mtniAl6LuytaVEaZODB1T0wibaQmSs3UPtrECZC07VU6KksJVIbRFIwvmYC26zgNM9SvKaN-5lqOm-Ovm5YsoSV8na85kd9mqFi9kTdGHSBiFYEUl0j8C', '', 'uploads/1492751157100.jpg', '28.5932734', '77.3049244', '110096', '0000000000000000', 'oooooo', 'yhhdjjfgjfd', 'edbifgjd', 'Private Limited Company', '00', '08', 'ooooo', 'oooooo', 'ooooooo', 'oooooo', '', '', '', '', '', '', '', '', 2, '2', 0, 1, 2, 0),
(4, 'Anand', 'Nalwa', 'anand@securx.in', '8800895800', 'securx2016', '123-krishna plaza,4,4 floor', 'Road Number 4,Block B, Mahipalpur Village, Mahipalpur,New Delhi, Delhi 110076,', 'New Delhi', 'Delhi', 2, 'ehmHTFkfLDY:APA91bExgk88Jb-1qo5li3P5l93QOHfVeMEXxB-VlfsWtq9XW_fQjehwfl4fKW9sj9BKeeuEi0YlIXs8rdK9B23l5c1n5uvf8ZAGwAe7fjqQnvqXhmMfCEWlMN5rbdDKnqpT1R-_LNJ4', '', 'uploads/1492761790963.jpg', '28.5493461', '77.1305329', '110076', '123456789012', 'abcd', 'mahipalpur', 'ifsc1234', 'Private Limited Company', '5', '50', '22222223455668', '234455444444', '3344445555', '4444455544', 'uploads/1478523698581492761973.jpg', '147852369858', 'uploads/asdfg1234h1492761973.jpg', 'asdfg1234h', 'uploads/44444444331492761973.jpg', '4444444433', 'uploads/dddd4443333334331492761973.jpg', 'dddd444333333433', 2, '1,2,3,5,6,7', 0, 1, 3, 1),
(5, 'Alok', 'Vaid', 'alok.vaid@securx.in', '8800895831', 'aloksecurx7863', 'road no. 4-krishna plaza,9 ,securx systems pvt ltd', 'Road Number 4,Block B, Mahipalpur Village, Mahipalpur,New Delhi, Delhi 110076,', 'New Delhi', 'Delhi', 2, 'fP8KokIHyy4:APA91bEjDEwCApGr0RmsTgJVPp3iBG-Q3UBwZ2reGCEHabSk7Rf2t1Luj_UozccSCi2_QK2U5BtyHErwg3YJ950bOvSJG2ImahddFd70agtORTmRbH_5d1E4FvrQHidMwODRqiVJ7pQh', '', 'uploads/1492764246170.jpg', '28.5493461', '77.1305329', '110076', '0000000000000000', 'icici', 'okhla', 'icic0000716', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/0000000000001492764328.jpg', '000000000000', 'uploads/00000000001492764328.jpg', '0000000000', 'uploads/00000000001492764328.jpg', '0000000000', 'uploads/00000000000000001492764328.jpg', '0000000000000000', 1, '1,2,3,5,6,7,8', 0, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `installer_type`
--

CREATE TABLE `installer_type` (
  `id` int(11) NOT NULL,
  `type` varchar(44) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `installer_type`
--

INSERT INTO `installer_type` (`id`, `type`) VALUES
(1, 'company'),
(2, 'installer');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `installer_id` varchar(255) NOT NULL,
  `job_id` varchar(255) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `invoice_image` varchar(255) NOT NULL,
  `invoice_time` varchar(255) NOT NULL,
  `invoice_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_start_date` varchar(12) NOT NULL,
  `job_end_date` text NOT NULL,
  `job_title` varchar(33) NOT NULL,
  `job_no` varchar(33) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `job_estimated_hours` varchar(20) NOT NULL,
  `job_description` varchar(255) NOT NULL,
  `job_tools` varchar(255) NOT NULL,
  `job_spl_instruction` varchar(2000) NOT NULL,
  `job_address` text NOT NULL,
  `number_installer` int(11) NOT NULL,
  `geo_address` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `job_payment` varchar(255) NOT NULL,
  `invoice_image` text NOT NULL,
  `job_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_start_date`, `job_end_date`, `job_title`, `job_no`, `start_time`, `end_time`, `job_estimated_hours`, `job_description`, `job_tools`, `job_spl_instruction`, `job_address`, `number_installer`, `geo_address`, `latitude`, `longitude`, `client_id`, `job_payment`, `invoice_image`, `job_status`) VALUES
(2, '2017-04-25', '2017-04-26', 'About Us', '123', '13:15', '22:15', '55', 'text', '', '', 'spaze it park,123456,Gurugram,Haryana', 2, 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4138566', '77.0421785', 1, '432143', '', 0),
(3, '2017-05-05', '2017-05-06', 'About Us', '123', '14:10', '14:10', '55', 'text', '', '', 'spaze it park,123456,Gurugram,Haryana', 12, 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.4138566', '77.0421785', 1, '432143', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_reports`
--

CREATE TABLE `job_reports` (
  `job_report_id` int(11) NOT NULL,
  `job_report` varchar(255) NOT NULL,
  `job_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_schedulle`
--

CREATE TABLE `job_schedulle` (
  `job_schedulle_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL,
  `team_leader` int(11) NOT NULL DEFAULT '0',
  `installer_status` int(11) NOT NULL,
  `job_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_schedulle`
--

INSERT INTO `job_schedulle` (`job_schedulle_id`, `job_id`, `installer_id`, `team_leader`, `installer_status`, `job_status`) VALUES
(2, 2, 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `location_name` varchar(55) NOT NULL,
  `lat` varchar(55) NOT NULL,
  `long` varchar(55) NOT NULL,
  `pin` varchar(44) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location_name`, `lat`, `long`, `pin`) VALUES
(1, 'Laxmi nagar', '2121', '31231', ''),
(2, 'Ganesh Nagar', '364589236', '23453245', ''),
(3, 'GorakhPur', '8643896', '42342.234', '221234'),
(4, 'Behram Pur', '645896.43', '98645868.56686', '665433');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `description`) VALUES
(1, 'About Us', 'installer provider'),
(2, 'Apporio infolabs', '+919560506619'),
(3, 'Terms and Conditions', 'SecurX');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `product_category`, `status`) VALUES
(1, 'Validation Engineer', 1),
(2, 'CCTV Engineer', 1),
(3, 'Electronics Technician', 1),
(5, 'Electrician-Apprentice', 1),
(6, 'Field Installation', 1),
(7, 'Cable Technician', 1),
(8, 'camera', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `rating_id` int(11) NOT NULL,
  `installer_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `rating_comments` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`rating_id`, `installer_id`, `job_id`, `rating`, `rating_comments`) VALUES
(2, 1, 5, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(11) NOT NULL,
  `date` varchar(10) NOT NULL,
  `installer_id` varchar(5) NOT NULL,
  `job_id` varchar(5) NOT NULL,
  `job_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `date`, `installer_id`, `job_id`, `job_status`) VALUES
(1, '8-1-2017', '94', '1', 1),
(2, '5-1-2017', '94', '2', 2),
(3, '5-1-2017', '94', '3', 3),
(4, '6-1-2017', '94', '4', 4),
(5, '1-1-2017', '25', '8', 0),
(6, '7-11-2016', '94', '7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fname` varchar(44) NOT NULL,
  `lname` varchar(44) NOT NULL,
  `email` varchar(44) NOT NULL,
  `password` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fname`, `lname`, `email`, `password`) VALUES
(1, 'samir', 'goel', 'samir@apporio.com', '123456'),
(2, 'Rahul', 'Kashayap', 'rahul@gmail.com', '123456'),
(3, 'edfwdewqer', 'qwrwqerqwerqwr', 'qwerqwerqwrwqrqw@gmail.com', '123456'),
(4, 'xcvv', 'xvx', 'xcvx@gmail.com', '123456'),
(5, 'kartik', 'Goel', 'kartickjnefckb@gmail.com', '123456'),
(6, 'wefwfwfef', 'crcr3d3d3', 'de3d3de3', 'ed3ed3d3d'),
(7, 'aair', 'Mohammad', 'aamir@gmail.com', '123456'),
(8, 'Aamir', 'Mohammad', 'aamir@apporio.com', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `account_details`
--
ALTER TABLE `account_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employeeNumber`);

--
-- Indexes for table `holyday`
--
ALTER TABLE `holyday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `installers`
--
ALTER TABLE `installers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installer_type`
--
ALTER TABLE `installer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `job_reports`
--
ALTER TABLE `job_reports`
  ADD PRIMARY KEY (`job_report_id`);

--
-- Indexes for table `job_schedulle`
--
ALTER TABLE `job_schedulle`
  ADD PRIMARY KEY (`job_schedulle_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `account_details`
--
ALTER TABLE `account_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employeeNumber` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holyday`
--
ALTER TABLE `holyday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `installers`
--
ALTER TABLE `installers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `installer_type`
--
ALTER TABLE `installer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_reports`
--
ALTER TABLE `job_reports`
  MODIFY `job_report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_schedulle`
--
ALTER TABLE `job_schedulle`
  MODIFY `job_schedulle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
