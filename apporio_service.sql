-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:23 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_name` varchar(255) NOT NULL,
  `address_full` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `lattitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `address_status` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_name`, `address_full`, `state`, `city`, `pincode`, `lattitude`, `longitude`, `landmark`, `nickname`, `address_status`, `user_id`, `session_id`) VALUES
(1, 'London United Kingdom', 'London, United Kingdom', 'England', 'London', 'No zipcode available', '51.5073509', '-0.1277583', '', '', 1, 1, ''),
(2, 'New', 'New York, NY, United States', 'New York', 'New York', 'No zipcode available', '40.7127837', '-74.0059413', '', '', 1, 1, '1'),
(3, 'new ofc', '4688, spaze new, 49, Sohna Road,Vatika City, Block W, Sector 49,Gurugram, Haryana 122101,', 'Haryana', 'Gurugram', '122001', '28.4061228', '77.0452125', '', '', 1, 2, ''),
(4, 'New York, NY, United States', 'New York, NY, United States', 'New York', 'New York', 'No zipcode available', '40.7127837', '-74.0059413', '', '', 1, 1, ''),
(5, 'Malibu', 'JMD Megapolis, Malibu Town, Gurgaon, Haryana, India', 'Haryana', 'Gurgaon', '122018', '28.42004', '77.03962', '', '', 1, 3, ''),
(6, 'Gurgaon Haryana India', 'Gurgaon, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 4, ''),
(7, 'ofccc', '4688, spazeee, Vatika Business Park, 3rd Floor, Block 2, State Highway 13,Vatika City, Block W, Sector 49,Gurugram, Haryana 122108,', 'Haryana', 'Gurugram', '12222', '28.4059483', '77.0445237', 'Sector 49', '', 1, 2, ''),
(8, 'cuucu', 'cftcyg, gccf, B/7,Pocket C, Nirvana Country, Sector 50,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '686886', '28.4251596', '77.0563306', 'cuucu', '', 1, 5, ''),
(9, 'Øvre Årdal Norway', 'Øvre Årdal, Norway', 'Sogn og Fjordane', 'Øvre Årdal', 'No zipcode available', '61.3088576', '7.8028434', '', '', 1, 6, ''),
(10, 'jfufuf', '344, hxuxyd, 68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '686838', '28.4113312', '77.0437399', 'jfufuf', '', 1, 7, ''),
(11, 'gdgsh', 'gshsh, gsgsg, 183-A,Block C, Sushant Lok Phase I, Sector 43,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '87646', '28.455934', '77.079495', 'gdgsh', '', 1, 8, ''),
(12, 'Heritage', 'JMD Regent Square, Heritage City, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122002', '28.4813175', '77.085001', '', '', 1, 9, ''),
(13, '', '', '', '', '', '', '', '', '', 1, 10, ''),
(27, '1002, Sispal Vihar Internal Road, Sispal Vihar, Sector 49 Gurugram, Haryana 122002', '1002, Sispal Vihar Internal Road, Sispal Vihar, Sector 49 Gurugram, Haryana 122002', '', '', '', '', '', '', '', 1, 13, ''),
(14, 'Huda Metro Station, Delhi, India', 'Huda Metro Station, Delhi, India', 'Delhi', 'Gurugram', '122007', '28.4592693', '77.0724192', '', '', 1, 9, ''),
(15, 'Gujarat India', 'Gujarat, India', 'Gujarat', 'No city available', 'No zipcode available', '22.258652', '71.1923805', '', '', 1, 9, ''),
(16, 'Shimogyo Ward, Kyoto Prefecture, Japan', 'Shimogyo Ward, Kyoto Prefecture, Japan', 'Kyoto Prefecture', 'Kyoto', 'No zipcode available', '34.9876552', '135.7555334', '', '', 1, 9, ''),
(17, 'Huda Panipat Haryana India', 'Huda, Panipat, Haryana, India', 'Haryana', 'Panipat', 'No zipcode available', '29.3622905', '76.9931074', '', '', 1, 9, ''),
(18, 'Øvre Slottsgate Oslo Norge', 'Øvre Slottsgate, Oslo, Norge', 'Oslo', 'Oslo', 'Inget postnummer tilgjengelig', '59.9121825', '10.7415371', '', '', 1, 11, ''),
(19, 'shilpa', '772, 772, 773,LIG Colony, Sector 31,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4535468', '77.0530491', 'shilpa', '', 1, 12, ''),
(20, '68, Plaza Street, Block S, Uppal Southend, Sector 49 Gurugram, Haryana 122018', '68, Plaza Street, Block S, Uppal Southend, Sector 49 Gurugram, Haryana 122018', 'Haryana', 'Gurugram', '122002', '28.4813175', '77.085001', '', '', 1, 9, ''),
(21, 'West Java Indonesia', 'West Java, Indonesia', 'West Java', 'No city available', 'No zipcode available', '-7.090911', '107.668887', '', '', 1, 9, ''),
(25, 'Hudson New York USA', 'Hudson, New York, USA', 'New York', 'Hudson', '12534', '42.2528649', '-73.790959', '', '', 1, 13, ''),
(26, '1002, Sispal Vihar Internal Road, Sispal Vihar, Sector 49 Gurugram, Haryana 122002', '1002, Sispal Vihar Internal Road, Sispal Vihar, Sector 49 Gurugram, Haryana 122002', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', '', '', 1, 13, ''),
(28, 'JMD Megapolis, Sector 48, Gurgaon, Haryana, India', 'JMD Megapolis, Sector 48, Gurgaon, Haryana, India', 'Haryana', 'Gurgaon', '122001', '28.42001', '77.03945', '', '', 1, 13, ''),
(29, 'Malibu', 'JMD Megapolis, Malibu Town, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', '', '', 1, 14, '');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_username` text NOT NULL,
  `admin_password` text NOT NULL,
  `admin_fname` text NOT NULL,
  `admin_lname` text NOT NULL,
  `admin_email` text NOT NULL,
  `admin_role` varchar(11) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_username`, `admin_password`, `admin_fname`, `admin_lname`, `admin_email`, `admin_role`, `admin_type`) VALUES
(1, 'apporio', '12345', 'abc', 'apporio', 'apporio@gmail.com', '', 1),
(2, 'app', '12345', 'abc', 'apporio', 'apporio@gmail.com', '7', 0);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(11, 'cus_AHpfxVl0MtKZQb', 38),
(49, 'cus_APJfcwO4aQT9mv', 4),
(48, 'cus_APJW9NcPUgSJzq', 3),
(46, 'cus_APJCrOAF2BKtRi', 1),
(47, 'cus_APJLtwYOzopu42', 1),
(50, 'cus_APK6OBWkDFEiIm', 2),
(51, 'cus_APLJenLKzqy7lu', 5),
(52, 'cus_APaZoIV7Cm7PNQ', 9),
(53, 'cus_APuN3TrYBgeKmD', 6),
(54, 'cus_AQ55b242LZpuJh', 10),
(55, 'cus_AQPsDlUwe0p1ze', 9),
(56, 'cus_AQdU8XAoSSeSXP', 12),
(57, 'cus_AQlFKb3ABlw8WL', 13);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `date` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sub_total` decimal(65,2) NOT NULL,
  `special_notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `category_id`, `product_id`, `quantity`, `address_id`, `time_id`, `date`, `sub_total`, `special_notes`, `status`, `session_id`) VALUES
(564, 11, 2, 47, 2, 18, 8, '2017-04-29', 1520.00, '', 1, '58e69a99d87bc'),
(563, 11, 2, 45, 3, 18, 8, '2017-04-29', 1935.00, '', 1, '58e69a99d87bc'),
(562, 11, 3, 23, 1, 0, 0, '', 619.00, '', 1, '58e69a99d87bc'),
(561, 11, 3, 21, 1, 0, 0, '', 599.00, '', 1, '58e69a99d87bc'),
(640, 2, 1, 1, 1, 0, 0, '', 50.00, '', 1, ''),
(641, 2, 1, 5, 1, 0, 0, '', 50.00, '', 1, ''),
(642, 2, 1, 29, 1, 0, 0, '', 95.00, '', 1, ''),
(643, 2, 1, 31, 1, 0, 0, '', 85.00, '', 1, ''),
(581, 9, 1, 29, 1, 0, 0, '', 95.00, '', 1, '58e733e9e0c86'),
(671, 0, 1, 29, 1, 0, 0, '', 95.00, '', 1, '58e793e71b302');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(21) NOT NULL,
  `category_id` int(21) NOT NULL,
  `category_name` text CHARACTER SET latin1 NOT NULL,
  `category_icon` text CHARACTER SET latin1 NOT NULL,
  `category_image` text CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL,
  `category_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `category_id`, `category_name`, `category_icon`, `category_image`, `language_id`, `category_status`) VALUES
(1, 1, 'Laundry', 'http://apporio.org/servicesapp/uploads/Laundry.png', 'http://apporio.org/servicesapp/uploads/laundry_(1)1.jpg', 1, 1),
(2, 2, 'Car Wash', 'http://apporio.org/servicesapp/uploads/Carwash.png', 'http://apporio.org/servicesapp/uploads/carwash.png', 1, 1),
(3, 3, 'House Cleaning', 'http://apporio.org/servicesapp/uploads/Cleaning.png', 'http://apporio.org/servicesapp/uploads/housecleaning.jpg', 1, 1),
(4, 4, 'Boat Wash', 'http://apporio.org/servicesapp/uploads/Boatwash.png', 'http://apporio.org/servicesapp/uploads/boatWash.png', 1, 1),
(43, 1, 'Klesvask', 'http://apporio.org/servicesapp/uploads/Laundry.png', 'http://apporio.org/servicesapp/uploads/laundry.jpg', 2, 1),
(44, 2, 'Bilvask', 'http://apporio.org/servicesapp/uploads/Carwash.png', 'http://apporio.org/servicesapp/uploads/carwash.png', 2, 1),
(45, 3, 'Boligvask', 'http://apporio.org/servicesapp/uploads/Cleaning.png', 'http://apporio.org/servicesapp/uploads/housecleaning.jpg', 2, 1),
(46, 4, 'Båtvask', 'http://apporio.org/servicesapp/uploads/Boatwash.png', 'http://apporio.org/servicesapp/uploads/boatWash.png', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `status`) VALUES
(1, 'English', 1),
(2, 'Norwegian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successfull', 1),
(2, 1, 'Pålogging vellykkede', 2),
(109, 25, 'Registration Successfully', 1),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Krev felt mangler', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'E-post-id eller passord er feil', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'Logg ut Vellykket', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Ingen registrering funnet', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Påmelding hell\n', 2),
(108, 24, 'Vis Underkategori', 2),
(17, 8, 'Email already exist', 1),
(18, 8, 'E-post allerede eksisterer', 2),
(107, 24, 'View SubCategory', 1),
(106, 23, 'tid Slot', 2),
(105, 23, 'Time Slot', 1),
(104, 22, 'Vis kategori', 2),
(103, 22, 'View Category', 1),
(94, 101, 'Bruker Adresse', 2),
(93, 101, 'User Address', 1),
(80, 14, 'Wrong Cart Id', 1),
(81, 14, 'Feil Handlekurv Id', 2),
(82, 12, 'No Category Found', 1),
(83, 12, 'Ingen Kategori Funnet', 2),
(84, 13, 'No Time Slot Found!!', 1),
(85, 13, 'No Time Slot Funnet !!', 2),
(91, 17, 'Order Id Not Found', 1),
(92, 17, 'Bestill Id Not Found', 2),
(79, 11, 'Feil adresse-ID', 2),
(76, 10, 'Adress Succesfully Deleted!!', 1),
(77, 10, 'Adresse hell Slettet !!', 2),
(78, 11, 'Wrong Address Id', 1),
(102, 21, 'Ingen Handlekurv', 2),
(101, 21, 'No Cart', 1),
(100, 20, 'vellykkede', 2),
(99, 20, 'Succesfull', 1),
(98, 19, 'Hell lagt i handlevognen', 2),
(97, 19, 'Succesfully Added To Cart', 1),
(96, 18, 'Adresse Lagt hell', 2),
(86, 9, 'Finner ingen adresse !!', 2),
(87, 15, 'Email already exist', 1),
(88, 15, 'E-post allerede eksisterer', 2),
(89, 16, 'No such Token', 1),
(90, 16, 'Ingen slik Token', 2),
(124, 33, 'E-post adresse Not Found !!', 2),
(123, 33, 'Email Address Not Found!!', 1),
(122, 32, 'Passord endret!!', 2),
(121, 32, 'Password Changed!!', 1),
(120, 31, 'feil brukernavn', 2),
(119, 31, 'Incorrect Username', 1),
(118, 30, 'Ingen bruker funnet', 2),
(117, 30, 'No User Found', 1),
(116, 29, 'Terms and Conditions', 1),
(115, 29, 'Vilkår og betingelser', 2),
(114, 27, 'Kontakt oss', 2),
(113, 27, 'Contact Us', 1),
(112, 26, 'Om oss', 2),
(111, 26, 'About Us', 1),
(110, 25, 'Registrering Vellykket', 2),
(95, 18, 'Address Added Succesfully', 1),
(74, 9, 'No Address Found!!', 1),
(125, 34, 'Succesful!!', 1),
(126, 34, 'Vellykket !!', 2),
(127, 35, 'Remove Succesfull', 1),
(128, 35, 'Fjern vellykkede', 2),
(129, 36, 'View Cart', 1),
(130, 36, 'Se handlekurv', 2),
(131, 37, 'Password Changed!!', 1),
(132, 37, 'Passord endret!!', 2),
(133, 38, 'Old Password Does Not Matched!!', 1),
(134, 38, 'Gammelt passord virker ikke matchede !!', 2),
(135, 39, 'User does not exit!!', 1),
(136, 39, 'Bruker does not exit !!', 2),
(137, 40, 'Profile updated', 1),
(138, 40, 'Profil oppdatert', 2),
(139, 41, 'Order Palced', 1),
(140, 41, 'Ordre plassert', 2),
(141, 42, 'No Order Found', 1),
(142, 42, 'Ingen Bestill Funnet', 2),
(143, 43, 'Order Details', 1),
(144, 43, 'Ordre detaljer', 2),
(145, 44, 'Deleted Successfully', 1),
(146, 44, 'slettet', 2),
(147, 45, 'Kortet Detaljer Lagrede vellykket', 2),
(148, 45, 'Card Details Saved Succesfully', 1),
(149, 46, 'Payment_Succesful', 1),
(150, 46, 'Payment_Succesful', 2),
(151, 47, 'Order Palced', 1),
(152, 47, 'Ordre plassert', 2),
(153, 48, 'Payment_Not_Succesful', 1),
(154, 48, 'Payment_Not_Succesful', 2),
(155, 49, 'No Order', 1),
(156, 49, 'Ingen Bestill', 1),
(157, 50, 'order_history', 1),
(158, 50, 'Ordrehistorikk', 2),
(159, 51, 'no_history_found', 1),
(160, 51, 'no_history_found', 2),
(161, 52, 'Cart Details', 1),
(162, 52, 'Handlevogn Detaljer', 2),
(163, 53, 'Datab added!!', 1),
(164, 53, 'Datab lagt !!', 2),
(165, 54, 'Cart Deleted!!', 1),
(166, 54, 'Handlekurv slettet !!', 2),
(167, 55, 'Succesfully Added', 1),
(168, 55, 'hell Lagd', 2),
(169, 56, 'Already Updated With Same Data', 1),
(170, 56, 'Allerede oppdatert med samme data', 2);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `user_id`, `session_id`) VALUES
(26, 6, ''),
(8, 1, ''),
(7, 1, ''),
(9, 3, ''),
(10, 4, ''),
(25, 9, ''),
(12, 5, ''),
(13, 5, ''),
(14, 4, ''),
(15, 9, ''),
(16, 9, ''),
(17, 5, ''),
(18, 5, ''),
(19, 5, ''),
(20, 6, ''),
(21, 6, ''),
(22, 6, ''),
(23, 10, ''),
(24, 6, ''),
(27, 6, ''),
(28, 6, ''),
(29, 12, ''),
(30, 13, ''),
(31, 13, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `address_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `special_notes` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`order_product_id`, `order_id`, `order_category_id`, `product_id`, `category_id`, `quantity`, `address_id`, `time_id`, `date`, `total`, `special_notes`, `session_id`) VALUES
(6, 6, 61, 8, 1, 2, 4, 5, '2017-04-11', 200.00, '', ''),
(5, 6, 63, 3, 3, 2, 1, 9, '2017-04-19', 140.00, 'Hello', ''),
(7, 6, 61, 5, 1, 2, 4, 5, '2017-04-11', 100.00, '', ''),
(8, 7, 71, 5, 1, 2, 4, 5, '2017-04-12', 100.00, 'Dffggh', ''),
(9, 7, 71, 1, 1, 2, 4, 5, '2017-04-12', 100.00, 'Dffggh', ''),
(10, 8, 81, 5, 1, 1, 4, 5, '2017-04-12', 50.00, 'Fjes', ''),
(11, 8, 81, 1, 1, 1, 4, 5, '2017-04-12', 50.00, 'Fjes', ''),
(12, 9, 91, 8, 1, 1, 5, 5, '2017-04-18', 100.00, 'Kdkfdk', ''),
(13, 9, 91, 5, 1, 1, 5, 5, '2017-04-18', 50.00, 'Kdkfdk', ''),
(14, 10, 101, 9, 1, 2, 6, 5, '2017-04-04', 1000.00, '', ''),
(15, 10, 101, 7, 1, 2, 6, 5, '2017-04-04', 200.00, '', ''),
(16, 10, 101, 6, 1, 2, 6, 5, '2017-04-04', 400.00, '', ''),
(17, 10, 101, 8, 1, 2, 6, 5, '2017-04-04', 200.00, '', ''),
(18, 10, 101, 1, 1, 2, 6, 5, '2017-04-04', 100.00, '', ''),
(19, 10, 101, 5, 1, 2, 6, 5, '2017-04-04', 100.00, '', ''),
(20, 11, 111, 5, 1, 9, 7, 5, '05-Apr-2017', 450.00, 'note', ''),
(21, 11, 112, 2, 2, 2, 3, 6, '12-Apr-2017', 40.00, 'noteesss', ''),
(22, 11, 111, 1, 1, 2, 7, 5, '05-Apr-2017', 100.00, 'note', ''),
(23, 12, 124, 4, 4, 4, 8, 11, '14 Apr 2017', 40.00, 'yu', ''),
(24, 12, 121, 8, 1, 2, 8, 5, '15 Apr 2017', 200.00, 'guu', ''),
(25, 12, 121, 5, 1, 2, 8, 5, '15 Apr 2017', 100.00, 'guu', ''),
(26, 12, 121, 1, 1, 2, 8, 5, '15 Apr 2017', 100.00, 'guu', ''),
(27, 13, 131, 8, 1, 3, 8, 5, '22 Apr 2017', 300.00, 'guu', ''),
(28, 13, 131, 5, 1, 2, 8, 5, '22 Apr 2017', 100.00, 'guu', ''),
(29, 13, 131, 1, 1, 2, 8, 5, '22 Apr 2017', 100.00, 'guu', ''),
(30, 14, 141, 8, 1, 1, 6, 5, '2017-04-12', 100.00, 'Dkfkfk', ''),
(31, 14, 141, 5, 1, 1, 6, 5, '2017-04-12', 50.00, 'Dkfkfk', ''),
(32, 14, 141, 1, 1, 1, 6, 5, '2017-04-12', 50.00, 'Dkfkfk', ''),
(33, 15, 151, 7, 1, 1, 12, 7, '2017-04-19', 100.00, 'fdssb', ''),
(34, 15, 151, 1, 1, 3, 12, 7, '2017-04-19', 150.00, 'fdssb', ''),
(35, 15, 151, 5, 1, 1, 12, 7, '2017-04-19', 50.00, 'fdssb', ''),
(36, 16, 161, 5, 1, 2, 12, 5, '2017-04-20', 100.00, 'ffd', ''),
(37, 16, 161, 1, 1, 2, 12, 5, '2017-04-20', 100.00, 'ffd', ''),
(38, 17, 171, 5, 1, 2, 8, 5, '6 Apr 2017', 100.00, 'gdhdh', ''),
(39, 17, 171, 1, 1, 2, 8, 5, '6 Apr 2017', 100.00, 'gdhdh', ''),
(40, 18, 182, 2, 2, 2, 8, 2, '04-Apr-2017', 40.00, 'vzhzb', ''),
(41, 18, 181, 8, 1, 1, 8, 5, '6 Apr 2017', 100.00, 'hshsh', ''),
(42, 18, 181, 5, 1, 2, 8, 5, '6 Apr 2017', 100.00, 'hshsh', ''),
(43, 18, 181, 1, 1, 2, 8, 5, '6 Apr 2017', 100.00, 'hshsh', ''),
(44, 19, 193, 3, 3, 4, 8, 9, '21 Apr 2017', 280.00, 'tdye', ''),
(45, 20, 201, 1, 1, 2, 9, 7, '2017-04-28', 100.00, 'Ok', ''),
(46, 21, 213, 3, 3, 1, 9, 9, '05-apr.-2017', 70.00, 'fin', ''),
(47, 21, 211, 1, 1, 2, 9, 5, '28. apr. 2017', 100.00, 'bra', ''),
(48, 22, 221, 1, 1, 2, 9, 5, '2017-04-20', 100.00, '', ''),
(49, 23, 231, 5, 1, 2, 13, 1, '05-Apr-2017', 100.00, 'test', ''),
(50, 23, 231, 1, 1, 1, 13, 1, '05-Apr-2017', 50.00, 'test', ''),
(51, 24, 241, 1, 1, 2, 9, 5, '22. apr. 2017', 100.00, 'ok', ''),
(52, 24, 243, 3, 3, 3, 9, 9, '29. apr. 2017', 210.00, '', ''),
(53, 25, 252, 45, 2, 1, 12, 6, '2017-04-14', 645.00, '', ''),
(54, 25, 251, 29, 1, 1, 12, 7, '2017-04-27', 95.00, '', ''),
(55, 26, 262, 2, 2, 1, 9, 6, '2017-04-15', 20.00, 'Ok', ''),
(56, 26, 263, 3, 3, 3, 9, 9, '2017-04-26', 210.00, 'Nei', ''),
(57, 26, 261, 1, 1, 2, 9, 5, '2017-05-19', 100.00, '', ''),
(58, 27, 273, 23, 3, 1, 9, 9, '06-apr.-2017', 619.00, 'nei', ''),
(59, 27, 272, 45, 2, 1, 9, 6, '30. apr. 2017', 645.00, 'ok', ''),
(60, 28, 282, 47, 2, 2, 0, 0, '', 1520.00, '', ''),
(61, 28, 283, 21, 3, 6, 9, 9, '07-apr.-2017', 3594.00, '', ''),
(62, 28, 282, 45, 2, 2, 0, 0, '', 1290.00, '', ''),
(63, 28, 281, 31, 1, 2, 9, 5, '29. apr. 2017', 170.00, 'jk', ''),
(64, 28, 281, 29, 1, 3, 9, 5, '29. apr. 2017', 285.00, 'jk', ''),
(65, 29, 293, 23, 3, 5, 19, 9, '08-Apr-2017', 3095.00, '1234', ''),
(66, 29, 293, 21, 3, 5, 19, 9, '08-Apr-2017', 2995.00, '1234', ''),
(67, 29, 292, 47, 2, 5, 19, 6, '07-Apr-2017', 3800.00, '1111', ''),
(68, 29, 292, 45, 2, 5, 19, 6, '07-Apr-2017', 3225.00, '1111', ''),
(69, 29, 291, 31, 1, 5, 19, 5, '07-Apr-2017', 425.00, '1111', ''),
(70, 29, 291, 29, 1, 5, 19, 5, '07-Apr-2017', 475.00, '1111', ''),
(71, 30, 302, 45, 2, 1, 26, 6, '2017-04-21', 645.00, 'Hello', ''),
(72, 30, 301, 29, 1, 1, 25, 7, '2017-04-13', 95.00, '', ''),
(73, 30, 301, 37, 1, 2, 25, 7, '2017-04-13', 500.00, '', ''),
(74, 31, 311, 37, 1, 3, 28, 5, '2017-04-19', 750.00, 'Jhvdvddv', ''),
(75, 31, 311, 31, 1, 1, 28, 5, '2017-04-19', 85.00, 'Jhvdvddv', ''),
(76, 31, 311, 29, 1, 1, 28, 5, '2017-04-19', 95.00, 'Jhvdvddv', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` text CHARACTER SET latin1 NOT NULL,
  `desc` text CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `title`, `desc`, `email`, `language_id`) VALUES
(1, 1, 'About Us', 'VASKMIN is a convenient way to get done cleaning of housing, laundry and dry cleaning of textiles, car washing and care of leisure boats. We offer a simple and user-friendly process where our services include that we come straight to you for pickup and delivery of textiles, car or boat.\r\n\r\nWe are proud to have relationships with the most serious players in each of the service categories that are offered, no matter where in Norway we are represented. You can count on us to keep our time appointments, prices quoted and that your assets are safeguarded in the best possible way.     ', '', 1),
(2, 1, 'Om oss', 'VASKMIN er en praktisk måte å få unnagjort renhold av bolig, vask og rens av tekstiler, bilvask og pleie av fritidsbåter. Vi tilbyr en enkel og brukervennlig prosess der våre tjenester inkluderer at vi kommer rett til deg for henting og levering av tekstiler, bil eller båt. \r\n\r\nVi er stolte over å ha samarbeid med de mest seriøse aktører i hver av de tjenestekategorier som tilbys, uansett hvor i Norge vi er representert. Du kan stole på at vi holder våre tidsavtaler, oppgitte priser og at dine eiendeler blir ivaretatt på best mulig måte.\r\n\r\nHvordan VASKMIN fungerer:\r\n\r\n1) Du laster ned appen, sett opp kontoen din og velger dine tjenester og preferanser for henting og levering av dine tekstiler, din bil eller båt eller tid for utføring av tjenester i din bolig. Du velger en eller flere tjenester, legger disse i din handlekurv og utfører betaling.\r\n\r\n2) Vi mottar din bestilling og dobbeltsjekker at tidspunktet du har valgt kan bookes inn i vårt system. Hvis alternative tider må vurderes av våre underleverandører grunnet mangel på kapasitet og tilgjengelighet, blir du kontaktet av oss med informasjon før endelig ordre blir fastsatt.\r\n\r\n3) En av våre medarbeidere henter dine tekstiler, din bil eller din båt til avtalt tid og leverer når oppdraget er utført. Har du bestilt renhold av bolig så kommer en eller flere profesjonelle renholdere for å utføre oppdraget til det tidspunkt som er avtalt.\r\n\r\nAndre fordeler inkluderer:\r\n- Alltid rask og trygg levering til avtalt tid\r\n- Tilpass din bestilling med egne notater til oss ved bestilling\r\n- Alltid konkurransedyktige priser\r\n- Samme prisnivå, uansett sted og fylke\r\n\r\nHver VASKMIN ordre er støttet av vår servicegaranti som lover:\r\n\r\n- Vi er alltid presis og holder våre avtaler, ingen ordre blir misligholdt\r\n- Vi garanterer kvalitet i alle ledd og følger alle lover og regler\r\n- Saklighet og profesjonalitet utøves av alle servicemedarbeidere\r\n- Vaskmin DA driver IKKE en forretningsmodel innenfor delingsøkonomien\r\n\r\nRegistrer deg som bruker i VASKMIN i dag, vi kan avlaste deg fra mange av dine daglige gjøremål.\r\n\r\nVASKMIN er ved oppstart tilgjengelig på Sunnmøre, men vi utvider fortløpende til nye kommuner og fylker. Ønsker du at vi kommer til ditt område? Send oss en kort melding på: support@vaskmin.no\r\n\r\n\r\n   ', '', 2),
(3, 2, 'Keshav Goyal', '004797521288', 'support@vaskmin.no', 1),
(4, 2, 'Keshav Goyal', '004797521288', 'support@vaskmin.no', 2),
(5, 3, 'Terms and Conditions', 'For all mobile applications that require registration or a login, you agree to complete the initial registration process according to go limo App requirements stated on the registration page, and to provide go limo with accurate, complete and updated information as required for the initial registration process, including, but not limited to, your legal name, billing and delivery address, email address, and appropriate telephone contact numbers. \r\n', '', 1),
(6, 3, 'Vilkår og betingelser', 'For alle mobile applikasjoner som krever registrering eller innlogging, samtykker du til å fullføre den innledende registreringsprosessen ifølge gå limo App kravene på registreringssiden, og for å gi slipp limo med nøyaktig, fullstendig og oppdatert informasjon som er nødvendig for den første registreringen , inkludert, men ikke begrenset til, din juridiske navn, fakturering og leveringsadresse, e-postadresse, og passende telefon kontakt tall.', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `pay_id` int(21) NOT NULL,
  `order_id` int(21) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(21) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`pay_id`, `order_id`, `payment_id`, `status`) VALUES
(5, 22, 'ch_19xFm9IuSX6GbxlatxcMRPi5', 0),
(4, 21, 'ch_19xFkbIuSX6GbxlaUti7CghC', 0),
(6, 22, 'ch_19xFmTIuSX6GbxlaIIBZ5CWf', 0),
(7, 49, 'ch_19xG6tIuSX6Gbxla8JbqNGkQ', 0),
(8, 50, 'ch_19xGD1IuSX6GbxlajqtPT1Vf', 0),
(9, 50, 'ch_19xGDDIuSX6GbxlatpxfG31I', 0),
(10, 50, 'ch_19xGEeIuSX6Gbxla6NGxFNuz', 0),
(11, 50, 'ch_19xGEqIuSX6GbxlaXgTFm1Fa', 0),
(12, 58, 'ch_19xGrcIuSX6GbxlaAGOxNEpY', 0),
(13, 58, 'ch_19xGrhIuSX6GbxlaEtygRkdV', 0),
(14, 58, 'ch_19xGrxIuSX6GbxlaG5LyZl07', 0),
(15, 58, 'ch_19xGsGIuSX6GbxlaDrm3nFI9', 0),
(16, 40, 'ch_1A0mZhIuSX6GbxlaqKmc1l1G', 1),
(17, 41, 'ch_1A0pL8IuSX6GbxlaB795Rx8S', 1),
(18, 1, 'ch_1A0pN2IuSX6Gbxlawm0rzocs', 1),
(19, 2, 'ch_1A0qTcIuSX6GbxlaDpL23E4m', 1),
(20, 1, 'ch_1A0qXcIuSX6GbxlaYiWsgwpz', 1),
(21, 2, 'ch_1A1VGkIuSX6Gbxlaz6pVxfq0', 1),
(22, 3, 'ch_1A1VfAIuSX6GbxlapMzcFYQL', 1),
(23, 5, 'ch_1A1VreIuSX6GbxlaOWQd7Y3K', 1),
(24, 1, 'ch_1A1VsoIuSX6GbxlaWOpoLbmT', 1),
(25, 2, 'ch_1A1VwPIuSX6GbxlaKAyRmfJE', 1),
(26, 3, 'ch_1A1tJIIuSX6GbxlabRPqbp9h', 1),
(27, 5, 'ch_1A2JgcIuSX6GbxlaTuUKQv0U', 1),
(28, 6, 'ch_1A4UZaIuSX6GbxlaDmAnysrn', 1),
(29, 7, 'ch_1A4UiiIuSX6GbxlawCYLqTAv', 1),
(30, 8, 'ch_1A4UjRIuSX6GbxlaMBBKebfm', 1),
(31, 9, 'ch_1A4UtqIuSX6Gbxlad4XG4wzq', 1),
(32, 10, 'ch_1A4V2QIuSX6GbxlaSSzca95z', 1),
(33, 11, 'ch_1A4VSNIuSX6GbxlaGda2imYs', 1),
(34, 12, 'ch_1A4WdQIuSX6GbxlaC4l9l27u', 1),
(35, 13, 'ch_1A4WkdIuSX6GbxlaRgIDNATp', 1),
(36, 14, 'ch_1A4jjGIuSX6Gbxla9GcmGQDS', 1),
(37, 15, 'ch_1A4lPaIuSX6GbxlaM6kiPrF4', 1),
(38, 16, 'ch_1A4lWVIuSX6GbxlaRgefPYsG', 1),
(39, 17, 'ch_1A4t50IuSX6GbxlaJlCqFlrw', 1),
(40, 18, 'ch_1A4t9TIuSX6GbxlazjUmQcSo', 1),
(41, 19, 'ch_1A4tCXIuSX6GbxlaL22bUqwv', 1),
(42, 20, 'ch_1A54Z3IuSX6GbxlaPI6Wzpb7', 1),
(43, 21, 'ch_1A54jlIuSX6Gbxlavl2xiZA6', 1),
(44, 22, 'ch_1A5DlIIuSX6GbxlawA5oJcBo', 1),
(45, 23, 'ch_1A5EvoIuSX6GbxlaBvSPwARO', 1),
(46, 24, 'ch_1A5GEFIuSX6Gbxlar2FoU5Pc', 1),
(47, 25, 'ch_1A5UObIuSX6GbxlajcdXqbwg', 1),
(48, 26, 'ch_1A5hClIuSX6Gbxlabhbnve2r', 1),
(49, 27, 'ch_1A5hy5IuSX6Gbxla7nm1pMSN', 1),
(50, 28, 'ch_1A5ikHIuSX6Gbxla1hIsAgp0', 1),
(51, 29, 'ch_1A5mDlIuSX6GbxlaR63ucaAs', 1),
(52, 30, 'ch_1A5tixIuSX6GbxlaURo4IfjL', 1),
(53, 31, 'ch_1A5u2aIuSX6GbxlaPnz6RPsS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pro_id` int(11) NOT NULL,
  `product_id` int(21) NOT NULL,
  `product_name` text CHARACTER SET latin1 NOT NULL,
  `product_image` text CHARACTER SET latin1 NOT NULL,
  `product_description` text CHARACTER SET latin1 NOT NULL,
  `product_unit` text CHARACTER SET latin1 NOT NULL,
  `product_price` decimal(15,2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pro_id`, `product_id`, `product_name`, `product_image`, `product_description`, `product_unit`, `product_price`, `category_id`, `subcategory_id`, `language_id`, `product_status`) VALUES
(1, 1, 'Product1', 'http://apporio.org/servicesapp/uploads/alarm1.jpg', 'By means of the mobile otect premises', '2', 50.00, 1, 1, 1, 0),
(2, 2, 'Product2', 'http://apporio.org/servicesapp/uploads/childrens.jpg', 'This is a list of cities ', '4', 20.00, 2, 2, 1, 0),
(3, 3, 'Hjemrenhold', 'http://apporio.org/servicesapp/uploads/enven.jpeg', 'Komplett  rengjøring av bolig. Pris/t', '1', 599.00, 3, 3, 1, 0),
(4, 4, 'Product4', 'http://apporio.org/servicesapp/uploads/iphone.jpeg', 'Transparent Flexible Soft ', '10', 10.00, 4, 4, 1, 0),
(5, 5, 'Product5', 'http://apporio.org/servicesapp/uploads/pro.jpeg', 'It usually takes 15-35 days ', '10', 50.00, 1, 1, 1, 0),
(6, 6, 'Product6', 'http://apporio.org/servicesapp/uploads/children.jpg', 'wooly green small fashion ', '12', 200.00, 1, 5, 1, 0),
(7, 7, 'Product7', 'http://apporio.org/servicesapp/uploads/childrens.jpg', 'wooly green ', '20', 100.00, 1, 5, 1, 0),
(8, 8, 'Shirts', 'http://apporio.org/servicesapp/uploads/children-s-clothing-20937495.jpg', 'Best In Class.....', '20', 100.00, 1, 1, 1, 0),
(9, 9, 'Jeans', 'http://apporio.org/servicesapp/uploads/children-s-sweater-20937472.jpg', 'Jeans Wash And Iron', '5', 500.00, 1, 5, 1, 0),
(14, 1, 'Produkt1', 'http://apporio.org/servicesapp/uploads/alarm.jpg', 'Ved hjelp av de mobile otect lokalene', '2', 50.00, 1, 1, 2, 0),
(15, 2, 'Produkt2', 'http://apporio.org/servicesapp/uploads/childrens.jpg', 'Dette er en liste over byer', '4', 20.00, 2, 2, 2, 0),
(16, 3, 'Produkt3', 'http://apporio.org/servicesapp/uploads/enven.jpeg', 'Superior JBL lyd', '1', 70.00, 3, 3, 2, 0),
(17, 4, 'Produkt4', 'http://apporio.org/servicesapp/uploads/iphone.jpeg', 'Transparent Fleksibel Soft', '10', 10.00, 4, 4, 2, 0),
(18, 5, 'Produkt5', 'http://apporio.org/servicesapp/uploads/pro.jpeg', 'Det tar vanligvis 15-35 dager', '10', 50.00, 1, 1, 2, 0),
(19, 6, 'Produkt6', 'http://apporio.org/servicesapp/uploads/children.jpg', 'ullen grønn liten mote', '12', 200.00, 1, 5, 2, 0),
(20, 7, 'Produkt7', 'http://apporio.org/servicesapp/uploads/childrens.jpg', 'ullen grønn', '20', 100.00, 1, 5, 2, 0),
(21, 8, 'Skjorter', 'http://apporio.org/servicesapp/uploads/children-s-clothing-20937495.jpg', 'Best i klassen....', '20', 100.00, 1, 1, 2, 0),
(22, 9, 'jeans', 'http://apporio.org/servicesapp/uploads/children-s-sweater-20937472.jpg', 'Jeans vaske og stryke', '5', 500.00, 1, 5, 2, 0),
(25, 19, 'Home cleaning', 'http://apporio.org/servicesapp/uploads/hjemrenhold.jpg', 'full cleaning av home. Price/h', '1', 599.00, 3, 3, 1, 0),
(26, 19, 'Hjemrenhold', 'http://apporio.org/servicesapp/uploads/hjemrenhold.jpg', 'Komplett rengjøring av bolig. Pris/t', '1', 599.00, 3, 3, 2, 0),
(27, 21, 'Home Cleaning', 'http://apporio.org/servicesapp/uploads/hjemrenhold1.jpg', 'Full home cleaning service\r\nPrice/h', '1', 599.00, 3, 11, 1, 1),
(28, 21, 'Hjemrenhold', 'http://apporio.org/servicesapp/uploads/hjemrenhold1.jpg', 'Komplett rengjøring av bosted\r\nPris/t', '1', 599.00, 3, 11, 2, 1),
(29, 23, 'Showcase Cleaning', 'http://apporio.org/servicesapp/uploads/visningsvask.jpg', 'Home sale showcase cleaning', '1', 619.00, 3, 11, 1, 1),
(30, 23, 'Visningsvask', 'http://apporio.org/servicesapp/uploads/visningsvask.jpg', 'Komplett vask av salgsobjekt. Pris/t', '1', 619.00, 3, 11, 2, 1),
(31, 25, 'Move cleaning', 'http://apporio.org/servicesapp/uploads/flyttevask.png', 'Home move cleaning\r\nPrice /m2', '1', 82.00, 3, 11, 1, 1),
(32, 25, 'Flyttevask', 'http://apporio.org/servicesapp/uploads/flyttevask.png', 'Flyttevask ved bytte av bolig\r\nPris /kvm', '1', 82.00, 3, 11, 2, 1),
(33, 27, 'Staircase cleaning', 'http://apporio.org/servicesapp/uploads/trappevask.jpg', 'Cleaning of high-rise staircase\r\nPrice /h', '1', 669.00, 3, 11, 1, 1),
(34, 27, 'Trappevask', 'http://apporio.org/servicesapp/uploads/trappevask.jpg', 'Trappevask for boligblokker\r\nPris /t', '1', 669.00, 3, 11, 2, 1),
(35, 29, 'Shirt', 'http://apporio.org/servicesapp/uploads/Skjorte.jpg', 'Washing of mens shirt\r\nPrice /unit', '1', 95.00, 1, 13, 1, 1),
(36, 29, 'Skjorte', 'http://apporio.org/servicesapp/uploads/Skjorte.jpg', 'Vask av herreskjorte\r\nPris /enhet', '1', 95.00, 1, 13, 2, 1),
(37, 31, 'Tie', 'http://apporio.org/servicesapp/uploads/Slips.png', 'Dry cleaning of tie for shirt\r\nPrice /unit', '1', 85.00, 1, 13, 1, 1),
(38, 31, 'Slips', 'http://apporio.org/servicesapp/uploads/Slips.png', 'Rens av slips til skjorte\r\nPris /enhet', '1', 85.00, 1, 13, 2, 1),
(39, 33, 'Blazer', 'http://apporio.org/servicesapp/uploads/Blazer.png', 'Washing of gents blazer\r\nPrice /unit', '1', 210.00, 1, 13, 1, 1),
(40, 33, 'Blazer', 'http://apporio.org/servicesapp/uploads/Blazer.png', 'Vask av herreblazer\r\nPris /enhet', '1', 210.00, 1, 13, 2, 1),
(41, 35, 'Jacket', 'http://apporio.org/servicesapp/uploads/Jakke.png', 'Washing of gents jacket\r\nPrice /unit', '1', 280.00, 1, 13, 1, 1),
(42, 35, 'Jakke', 'http://apporio.org/servicesapp/uploads/Jakke.png', 'Vask av herrejakke\r\nPrice /enhet', '1', 280.00, 1, 13, 2, 1),
(43, 37, 'Dress', 'http://apporio.org/servicesapp/uploads/Kjole_enkel.png', 'Washing of ladies simple dress', '1', 250.00, 1, 15, 1, 1),
(44, 37, 'Kjole Enkel', 'http://apporio.org/servicesapp/uploads/Kjole_enkel.png', 'Vask av enkel damekjole. Pris /enhet', '1', 250.00, 1, 15, 2, 1),
(45, 39, 'Overcoat', 'http://apporio.org/servicesapp/uploads/Frakk.png', 'Washing of ladies overcoat. Price /unit', '1', 295.00, 1, 15, 1, 1),
(46, 39, 'Frakk', 'http://apporio.org/servicesapp/uploads/Frakk.png', 'Vask av damefrakk. Pris /enhet', '1', 295.00, 1, 15, 2, 1),
(47, 41, 'Cardigan', 'http://apporio.org/servicesapp/uploads/Cardigan.png', 'Washing of ladies cardigan. Price /unit', '1', 140.00, 1, 15, 1, 1),
(48, 41, 'Cardigan', 'http://apporio.org/servicesapp/uploads/Cardigan.png', 'Vask av damebluse. Pris /enhet', '1', 140.00, 1, 15, 2, 1),
(49, 43, 'Skirt', 'http://apporio.org/servicesapp/uploads/Skjørt.png', 'Washing of ladies skirt.\r\nPrice /unit', '1', 165.00, 1, 15, 1, 0),
(50, 43, 'Skjørt', 'http://apporio.org/servicesapp/uploads/Skjørt.png', 'Vask av dameskjørt\r\nPris /enhet', '1', 165.00, 1, 15, 2, 0),
(51, 45, 'Standard wash in/out medium car', 'http://apporio.org/servicesapp/uploads/medium_standard.png', 'Please se FAQ for package details\r\nPrice /car', '1', 645.00, 2, 17, 1, 1),
(52, 45, 'Standard medium bil', 'http://apporio.org/servicesapp/uploads/medium_standard.png', 'Vask ut-/innvendig. Se FAQ for detaljer. Pris /bil', '1', 645.00, 2, 17, 2, 1),
(53, 47, 'Standard large car', 'http://apporio.org/servicesapp/uploads/stor_standard.png', 'Was in/out. See FAQ for details. Price /car', '1', 760.00, 2, 17, 1, 1),
(54, 47, 'Standard stor bil', 'http://apporio.org/servicesapp/uploads/stor_standard.png', 'Vask ut-/innvendig. Se FAQ for detaljer. Pris /bil', '1', 760.00, 2, 17, 2, 1),
(55, 49, 'Pro medium car', 'http://apporio.org/servicesapp/uploads/medium_pro.jpg', 'Wash in/out. see FAQ for details. Price /car', '1', 855.00, 2, 17, 1, 1),
(56, 49, 'Pro medium bil', 'http://apporio.org/servicesapp/uploads/medium_pro.jpg', 'Vask ut-/innvendig. Se FAQ for detaljer. Pris /bil', '1', 855.00, 2, 17, 2, 1),
(57, 51, 'Pro large car', 'http://apporio.org/servicesapp/uploads/stor_pro.png', 'Wash in/out. See FAQ for details. Price /car', '1', 925.00, 2, 17, 1, 1),
(58, 51, 'Pro stor bil', 'http://apporio.org/servicesapp/uploads/stor_pro.png', 'Vask ut-/innvendig. Se FAQ for detaljer. Pris /bil', '1', 925.00, 2, 17, 2, 1),
(59, 53, 'Boat lift', 'http://apporio.org/servicesapp/uploads/4.png', 'Boat lift service\r\nPrice /tons', '1', 79.00, 4, 19, 1, 1),
(60, 53, 'Båtløft', 'http://apporio.org/servicesapp/uploads/4.png', 'Båtløft tjenesten\r\nPris /tonn', '1', 79.00, 4, 19, 2, 1),
(61, 55, 'Boat Transport', 'http://apporio.org/servicesapp/uploads/henting.jpg', 'Boat transport service\r\nPrice /hour', '1', 2600.00, 4, 19, 1, 1),
(62, 55, 'Båttransport ', 'http://apporio.org/servicesapp/uploads/henting.jpg', 'Båttransport Tjenesten\r\nPris /time', '1', 2600.00, 4, 19, 2, 1),
(63, 57, 'Pressure wash', 'http://apporio.org/servicesapp/uploads/1.png', 'Pressure wash of bottom\r\nPrice /feet\r\n', '1', 79.00, 4, 19, 1, 1),
(64, 57, 'Spyling av groe', 'http://apporio.org/servicesapp/uploads/1.png', 'Vask og skrap av skjell, groe og løst bunnstoff. Pris /fot', '1', 79.00, 4, 19, 2, 1),
(65, 59, 'Teak treatment', 'http://apporio.org/servicesapp/uploads/2.jpg', 'Protection and refurbishment of old wood\r\nPrice /hour', '1', 1300.00, 4, 19, 1, 1),
(66, 59, 'Teakbehandling', 'http://apporio.org/servicesapp/uploads/2.jpg', 'Beskyttelse og forskjønnelse av edle tresorter\r\nPris /time', '1', 1300.00, 4, 19, 2, 1),
(67, 61, 'Polish', 'http://apporio.org/servicesapp/uploads/3.png', 'We perform wash, scrub and polish\r\nPrice /hour', '1', 1300.00, 4, 19, 1, 1),
(68, 61, 'Polering', 'http://apporio.org/servicesapp/uploads/3.png', 'Vi utfører grundig vasks, rubbing og polering.\r\nPris /time', '1', 1300.00, 4, 19, 2, 1),
(69, 63, 'Bottom treatment', 'http://apporio.org/servicesapp/uploads/bunn.png', 'bottom treatment incl oils\r\nPrice /feet', '1', 235.00, 4, 19, 1, 1),
(70, 63, 'Bunnsmøring', 'http://apporio.org/servicesapp/uploads/bunn.png', 'Vi utfører bunnsmøring inkludert bunnstoff\r\nPris /fot', '1', 235.00, 4, 19, 2, 1),
(74, 65, 'Nedvask', 'http://apporio.org/servicesapp/uploads/nedvask.jpg', 'Nedvask av bolig/hytte/kontor\r\nPris /kvm', '1', 82.00, 3, 11, 2, 1),
(73, 65, 'Downwash', 'http://apporio.org/servicesapp/uploads/nedvask.jpg', 'We do a complete downwash\r\nPrice /m2', '1', 82.00, 3, 11, 1, 1),
(75, 67, 'Floor shine', 'http://apporio.org/servicesapp/uploads/skuring.jpg', 'We treat floor area\r\nPrice /m2', '1', 82.00, 3, 11, 1, 1),
(76, 67, 'Skuring & boning', 'http://apporio.org/servicesapp/uploads/skuring.jpg', 'Vi behandler gulvarealer\r\nPris /kvm', '1', 82.00, 3, 11, 2, 1),
(77, 69, 'Appliances', 'http://apporio.org/servicesapp/uploads/hvitevarer.jpg', 'Wash of appliances\r\nPrice /unit', '1', 325.00, 3, 11, 1, 1),
(78, 69, 'Hvitevarer', 'http://apporio.org/servicesapp/uploads/hvitevarer.jpg', 'Vask av hvitevarer\r\nPris /enhet', '1', 325.00, 3, 11, 2, 1),
(79, 71, 'Building', 'http://apporio.org/servicesapp/uploads/bygg.png', 'Cleaning of new building\r\nPrice /m2', '1', 69.00, 3, 11, 1, 1),
(80, 71, 'Byggrengjøring', 'http://apporio.org/servicesapp/uploads/bygg.png', 'Komplett nybygg rengjøring\r\nPris /kvm', '1', 69.00, 3, 11, 2, 1),
(81, 73, 'Window wash', 'http://apporio.org/servicesapp/uploads/vinduspuss.png', 'Window cleaning\r\nPrice /hour', '1', 669.00, 3, 11, 1, 1),
(82, 73, 'Vinduspuss', 'http://apporio.org/servicesapp/uploads/vinduspuss.png', 'Rengjøring av vinduer\r\nPris /time', '1', 669.00, 3, 11, 2, 1),
(83, 75, 'Trash can cleaning', 'http://apporio.org/servicesapp/uploads/avfallsdunk.png', 'Washing of trash can\r\nPrice /unit', '1', 135.00, 3, 11, 1, 1),
(84, 75, 'Søppeldunk', 'http://apporio.org/servicesapp/uploads/avfallsdunk.png', 'Rengjøring av avfallsdunk\r\nPris /enhet', '1', 135.00, 3, 11, 2, 1),
(85, 77, 'Carpet cleaning', 'http://apporio.org/servicesapp/uploads/tepperens.jpg', 'Carpet cleaning\r\nPrice /m2', '1', 65.00, 3, 11, 1, 1),
(86, 77, 'Tepperens', 'http://apporio.org/servicesapp/uploads/tepperens.jpg', 'Komplett tepperens\r\nPris /kvm', '1', 65.00, 3, 11, 2, 1),
(87, 79, 'Garage and padio', 'http://apporio.org/servicesapp/uploads/garasje.png', 'We clean garage and padio\r\nPrice /hour', '1', 669.00, 3, 11, 1, 1),
(88, 79, 'Terrasse & garasje', 'http://apporio.org/servicesapp/uploads/garasje.png', 'Terrasse- og garasjevask\r\nPris /time', '1', 669.00, 3, 11, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `cat_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `category_id` int(21) NOT NULL,
  `subcategory_name` text CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL,
  `subcategory_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`cat_id`, `subcategory_id`, `category_id`, `subcategory_name`, `language_id`, `subcategory_status`) VALUES
(1, 1, 1, 'Laundry1', 1, 0),
(2, 2, 2, 'Car Wash1', 1, 0),
(3, 3, 3, 'House Cleaning1', 1, 0),
(4, 4, 4, 'Boat Wash1', 1, 0),
(5, 5, 1, 'Laundry2', 1, 0),
(18, 1, 1, 'vaskeri1', 2, 0),
(21, 11, 3, 'Vaskmin Cleaning Service', 1, 1),
(20, 2, 2, 'Bilvask1', 2, 0),
(17, 3, 3, 'Hus rengjring1', 2, 0),
(19, 4, 4, 'båt Wash1', 2, 0),
(16, 5, 1, 'vaskeri2', 2, 0),
(22, 11, 3, 'Vaskmin Renholdstjenester', 2, 1),
(23, 13, 1, 'Mens', 1, 1),
(24, 13, 1, 'Herre', 2, 1),
(25, 15, 1, 'Ladies', 1, 1),
(26, 15, 1, 'Dame', 2, 1),
(27, 17, 2, 'Car wash services', 1, 1),
(28, 17, 2, 'Vask av kjøretøy', 2, 1),
(29, 19, 4, 'Vaskmin Boat Wash Services', 1, 1),
(30, 19, 4, 'Vaskmin Båtvasktjenester', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE `time_slot` (
  `time_slot_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `time_slot_from` text CHARACTER SET latin1 NOT NULL,
  `time_slot_to` text CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`time_slot_id`, `category_id`, `time_slot_from`, `time_slot_to`, `status`) VALUES
(1, 1, '10:00', '11:00', 1),
(2, 2, '11:00', '12:00', 1),
(3, 3, '21:00', '22:00', 1),
(11, 4, '13:00', '14:00', 1),
(5, 1, '14:00', '15:00', 1),
(8, 2, '15:00', '16:00', 1),
(7, 1, '16:00', '17:00', 1),
(6, 2, '13:00', '14:00', 1),
(9, 3, '13:00', '14:00', 1),
(10, 3, '16:00', '17:00', 1),
(4, 4, '13:00', '14:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` text CHARACTER SET latin1 NOT NULL,
  `last_name` text CHARACTER SET latin1 NOT NULL,
  `email` text CHARACTER SET latin1 NOT NULL,
  `password` text CHARACTER SET latin1 NOT NULL,
  `phone` text CHARACTER SET latin1 NOT NULL,
  `profile_pic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `online_offline` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `profile_pic`, `online_offline`, `status`) VALUES
(1, 'Gurgaon', 'Gurgaon', 'qwerty@gmail.com', '111111', 'Gurgaon', '', 1, 1),
(2, 'lalit', 'kumar', 'lalit@apporio.com', '123', '99106233', '', 1, 1),
(3, 'ggg', 'ggg', 'ggg@gmail.con', '111111', '1111111111', '', 1, 1),
(4, 'demo', 'Atul', 'demoatul@gmail.com', '123456', '8524568524', '', 1, 1),
(5, 'demo', 'ankit', 'demoankit@gmail.com', '123456', '85241785', 'http://apporio.org/servicesapp/uploads/IMG_20170404_203219.jpg', 1, 1),
(6, 'cip', 'pater', 'post@com.com', 'pass123', '786555', 'http://apporio.org/servicesapp/uploads/file4.jpeg', 1, 1),
(7, 'jcjcj', 'jchch', 'hxhh@gmail.com', '123', '6886838', 'http://apporio.org/servicesapp/uploads/IMG_20170404_120632.jpg', 1, 1),
(8, 'demo', 'navneet', 'demonavneet@gmail.com', '123456', '52874568', 'http://apporio.org/servicesapp/uploads/IMG_20170404_114755.jpg', 1, 1),
(9, 'apporio', 'singh', 'apporio@gmail.com', '123456', '8888888888', 'http://apporio.org/servicesapp/uploads/file5.jpeg', 1, 1),
(10, 'Gurgaon', 'Gurgaon', 'lalit123@gmail.com', '123', 'Gurgaon', '', 1, 1),
(11, 'cris', 'pat', 'cristi@com.com', 'pass123', '8566', '', 1, 1),
(12, 'shilpa', 'goel', 'hello@apporio.com', '123456', '81399369', 'http://apporio.org/servicesapp/uploads/IMG_20170407_071829.jpg', 1, 1),
(13, 'Shivani', 'Jain', 'shivanisethi23_92@yahoo.com', '123456', '8888888888', 'http://apporio.org/servicesapp/uploads/file6.jpeg', 1, 1),
(14, 'Shivani', 'Jain', 'ss@gmail.com', '111111', '1111111111', '', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`time_slot_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=672;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(21) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `pay_id` int(21) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `time_slot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
