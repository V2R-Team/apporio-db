-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:23 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_services`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `address_name` varchar(255) NOT NULL,
  `address_full` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `lattitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `address_status` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `address_name`, `address_full`, `state`, `city`, `pincode`, `lattitude`, `longitude`, `landmark`, `nickname`, `address_status`, `user_id`, `session_id`) VALUES
(4, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 2, ''),
(3, 'Uppal Southend', 'Adarsh Street,Block W, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4071568', '77.0489649', 'Uppal Southend', '', 1, 0, '58d36f470747a'),
(5, 'Gurugram Haryana India', 'Gurugram, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 3, ''),
(6, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 0, '58d372322cb13'),
(7, 'Sector 49', '381,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4102135', '77.0482225', 'Sector 49', '', 1, 4, ''),
(8, 'Sector 49', 'Adarsh Street,D Block, Block W, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4050592', '77.0501555', 'Sector 49', '', 1, 0, '58d374259c331'),
(9, 'South City I', '372, Plaza Street,Block S, South City I, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4118569', '77.0473646', 'South City I', '', 1, 0, '58d378a572329'),
(10, 'Sector 49', '107, Sankalp Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4099', '77.045079', 'Sector 49', '', 1, 5, ''),
(11, 'Block S', '5-9, Block C Uppal Southland Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113009', '77.0482088', 'Block S', '', 1, 0, '58d37d930742b'),
(12, 'Sector 48', 'JMD Megapolis, Sector 48, Gurugram, Haryana, India', 'Haryanaa', 'Gurugramm', '122001', '28.42001', '77.03945', '', 'nick', 1, 6, ''),
(13, 'Sector 48', 'H 1201,Sohna Road,Park View City 1, Tatvam Villas, Dhani, Sector 48,Gurugram, Haryana 122103,', 'Haryana', 'Gurugram', '122103', '28.4060281', '77.0426509', 'Sector 48', '', 1, 0, '58d37e243dcb4'),
(14, 'Malibu Town', 'JMD Megapolis, Malibu Town, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', 'hone', '', 1, 0, '58d395c319e65'),
(17, 'Road Shivajinagar', 'JM Road, Shivajinagar, Pune, Maharashtra, India', 'Maharashtra', 'Pune', 'No zipcode available', '18.522246', '73.8476594', '', '', 1, 0, '58d3c84a88f0e'),
(18, 'Place Boston', 'jm Curley, Temple Place, Boston, MA, United States', 'Massachusetts', 'Boston', '02111', '42.355195', '-71.0623678', '', '', 1, 0, '58d3c84a88f0e'),
(19, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 0, '58d3d4a8b2bfa'),
(20, 'Sector 49', '206, Samadhan Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.408558', '77.047347', 'Sector 49', '', 1, 0, '58d3d837c6197'),
(21, 'Sector 39', '1, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 39,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4410263', '77.0394185', 'Sector 39', '', 1, 0, '58d3eafe18e3e'),
(22, 'Sector 38', '222, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 38,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4347666', '77.0469718', 'Sector 38', '', 1, 0, '58d3ef986f487'),
(23, 'Sector 39', '1, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 39,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4347874', '77.0485835', 'Sector 39', '', 1, 7, ''),
(24, 'Sector 38', '222, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 38,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4347666', '77.0469718', 'Sector 38', '', 1, 0, '58d3f5dbb8df6'),
(25, 'Muhammadpur', 'Housing Board Road,Muhammadpur, Sector 39,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4394219', '77.0460924', 'Muhammadpur', '', 1, 8, ''),
(26, 'Sector 38', '222, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 38,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4347666', '77.0469718', 'Sector 38', '', 1, 0, '58d3f9470ac4d'),
(27, 'Muhammadpur', 'Housing Board Road,Muhammadpur, Sector 39,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4394219', '77.0460924', 'Muhammadpur', '', 1, 0, '58d3fc4b7e657'),
(28, 'Muhammadpur', 'Housing Board Road,Muhammadpur, Sector 39,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4394219', '77.0460924', 'Muhammadpur', '', 1, 9, ''),
(29, 'Sector 41', 'P-98,South City I, Sector 41,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122022', '28.4564117', '77.0569271', 'Sector 41', '', 1, 0, '58d47efb215b8'),
(30, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 0, '58d3fe0bbcc67'),
(31, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 10, ''),
(32, 'Sector 38', '222, CH Baktawar Singh Road,Medicity, Islampur Colony, Sector 38,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4347666', '77.0469718', 'Sector 38', '', 1, 0, '58d5309d1e791'),
(33, 'Sector 31', 'Sector 31,Gurugram, Haryana 122003,', 'Haryana', 'Gurugram', '122003', '28.4520176', '77.0496235', 'Sector 31', '', 1, 10, ''),
(34, 'Sector 31', '773,LIG Colony, Sector 31,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4535468', '77.0530491', 'Sector 31', '', 1, 11, ''),
(35, 'Vijay Vihar', '108, Silokara Road,Vijay Vihar, Jal Vayu Vihar, Sector 30,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4590669', '77.0531109', 'Vijay Vihar', '', 1, 0, '58d6b2ed8e5cf'),
(36, 'Sector 31', '773,LIG Colony, Sector 31,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4535468', '77.0530491', 'Sector 31', '', 1, 12, ''),
(37, 'Sector 41', '28,Block C, South City I, Sector 41,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122022', '28.4604139', '77.0611563', 'Sector 41', '', 1, 12, ''),
(38, 'Malibu Town', 'JMD Megapolis, Malibu Town, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', '', '', 1, 0, '58d89fc69a25c'),
(39, 'Sector 48', 'JMD Megapolis, Sector 48, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122001', '28.42001', '77.03945', '', '', 1, 13, ''),
(40, 'Sector 33', 'JMD Garden, Sector 33, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.4299008', '77.0321359', '', '', 1, 0, '58d8a262bccd6'),
(41, 'Road Shivajinagar', 'JM Road, Shivajinagar, Pune, Maharashtra, India', 'Maharashtra', 'Pune', 'No zipcode available', '18.522246', '73.8476594', '', '', 1, 14, ''),
(42, 'Malibu Town', 'JMD Megapolis, Malibu Town, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', '', '', 1, 15, ''),
(43, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 0, '58d6af7b3dfab'),
(44, 'Sector 48', 'JMD Megapolis, Sector 48, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122001', '28.42001', '77.03945', '', '', 1, 0, '58d8a7c42dd61'),
(45, 'Sector 48', 'JMD Megapolis, Sector 48, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122001', '28.42001', '77.03945', '', '', 1, 16, ''),
(46, '2', '2', '101', '101', '101', '101', '101', '', '', 1, 101, ''),
(116, 'asdb', 'gwgwhw, auwhhw, South City Road,South City II, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '444', '28.4163561', '77.0484339', 'asdb', '', 1, 42, ''),
(104, 'Sector 45', '1881,Block A, Greenwood City, Sector 45,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '', '28.4495516', '77.0678061', 'Sector 45', '', 1, 6, ''),
(49, 'Sector 48', 'JMD Megapolis, Sector 48, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122001', '28.42001', '77.03945', '', '', 1, 17, ''),
(50, 'Road Shivajinagar', 'JM Road, Shivajinagar, Pune, Maharashtra, India', 'Maharashtra', 'Pune', 'No zipcode available', '18.522246', '73.8476594', '', '', 1, 18, ''),
(51, 'Huda Metro', 'Huda Metro Station, Delhi, India', 'Delhi', 'Gurugram', '122007', '28.4592693', '77.0724192', '', '', 1, 19, ''),
(52, 'Gurugram Haryana India', 'Gurugram, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 20, ''),
(53, 'Gurugram Haryana India', 'Gurugram, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 21, ''),
(54, 'Sector 4', '19,Sector 4,Gurugram, Haryana 122001,', 'Haryana', 'Gurugram', '122001', '28.4742304', '77.0087818', 'Sector 4', '', 1, 22, ''),
(55, 'Phase II', '86A, Carterpuri Road,Phase II, Ashok Vihar, Sector 3,Gurgaon Rural, Haryana 122006,', 'Haryana', 'Gurgaon Rural', '122006', '28.487915', '77.019624', 'Phase II', '', 1, 22, ''),
(56, 'Sector 5', 'HUDA Road,Part- 6, Sector 5,Gurugram, Haryana 122006,', 'Haryana', 'Gurugram', '122006', '28.4789088', '77.0219216', 'Sector 5', '', 1, 22, ''),
(57, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 22, ''),
(58, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 22, ''),
(59, 'South City II', 'C-5, South City Road,South City II, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4145568', '77.0479761', 'South City II', '', 1, 22, ''),
(60, 'Sector 49', '79,South City II, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.415799', '77.050804', 'Sector 49', '', 1, 22, ''),
(61, 'South City II', '136, South City Road,South City II, Sector 50,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.418943', '77.052509', 'South City II', '', 1, 22, ''),
(62, 'Huda Metro', 'Huda Metro Station, Delhi, India', 'Delhi', 'Gurugram', '122007', '28.4592693', '77.0724192', '', '', 1, 16, ''),
(63, 'Gurgaon Haryana India', 'Gurgaon, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 23, ''),
(64, 'New Delhi Delhi India', 'New Delhi, Delhi, India', 'Delhi', 'New Delhi', 'No zipcode available', '28.6139391', '77.2090212', 'Benz', 'bxjbz', 1, 23, ''),
(117, 'vebe', 'svvs, wgwg, 68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '464', '28.4113312', '77.0437399', 'vebe', '', 1, 43, ''),
(115, 'hfhd', 'hchc, hcuf, 978, Jharsa Road,Jharsa, Durga Colony, Sector 39,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122001', '28.4442614', '77.0536057', 'hfhd', '', 1, 41, ''),
(68, '2', '2', '101', '101', '', '101', '101', '', '', 1, 101, ''),
(69, 'test', 'test', 'haryana', 'gurugram', '', '28.4113312', '77.0437399', '', '', 1, 1, ''),
(70, 'test', 'test', 'haryana', 'gurugram', 'null', '28.4113312', '77.0437399', '', '', 1, 1, ''),
(71, 'test', 'test', 'haryana', 'gurugram', '', '28.4113312', '77.0437399', '', '', 1, 1, ''),
(114, 'anshul', '123, b block, 1129, Sector 46 Road,Medicity, Islampur Village, Sector 38,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122001', '28.4392292', '77.0400394', 'anshul', '', 1, 40, ''),
(113, 'aman', '456, b block, 1346,Huda Colony, Sector 46,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122001', '28.4313958', '77.0560809', 'aman', '', 1, 39, ''),
(112, 'ydhdh', 'vdvdh, gdd, Fatehpur, Haryana 122018,', 'Haryana', 'Fatehpur', '865656', '28.4222469', '77.0492504', 'ydhdh', '', 1, 38, ''),
(107, 'Block S', '467space itSai Dham Road,Vatika City, Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '123456', '28.4073292', '77.0461935', 'Block S', '', 1, 36, ''),
(106, 'Sector 47', 'ya gdhfor gdh21, Cedar Drive,Malibu Town, Sector 47,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '88889', '28.4233229', '77.0493642', 'Sector 47', '', 1, 35, ''),
(108, 'Sector 49', '467space itVatika City, Gardenia Street,Vatika City, Block W, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '123456', '28.4051098', '77.0474187', 'Sector 49', '', 1, 36, ''),
(109, 'ofc', '467spazeGolf Course Extension Road,Sector 69,Gurugram, Haryana 122004,', 'Haryana', 'Gurugram', '123456', '28.4013654', '77.0404013', 'Sector 69', '', 1, 36, ''),
(110, 'sachin', 'gdhehhedhhdhydheh\n\n\n\n\n\n, b block\n\n\n, Orange Drive,Malibu Town, Sector 47,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122001', '28.4249057', '77.0507845', 'sachin', '', 1, 37, ''),
(111, 'Tingstadvika 6035 Fiskarstrand Norway', 'Tingstadvika, 6035 Fiskarstrand, Norway', 'Møre og Romsdal', 'No city available', '6035', '62.4415952', '6.259142', '', '', 1, 34, ''),
(82, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '122018', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 24, ''),
(83, 'Sector 47', '40, Vikas Marg,Malibu Town, Sector 47,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '', '28.422983', '77.051881', 'Sector 47', '', 1, 24, ''),
(84, 'Huda Metro', 'Huda Metro Station, Delhi, India', 'Delhi', 'Gurugram', '122007', '28.4592693', '77.0724192', 'cv', 'fg', 1, 3, ''),
(85, 'Uppal Southend', '68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '', '28.4113312', '77.0437399', 'Uppal Southend', '', 1, 24, ''),
(86, 'Sector 50', 'B - 77, Nirvana Central Road,Pocket C, Mayfield Garden, Sector 50,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '', '28.4242685', '77.0582394', 'Sector 50', '', 1, 24, ''),
(91, 'Oegstgeest Netherlands', 'Oegstgeest, Netherlands', 'South Holland', 'Oegstgeest', 'No zipcode available', '52.1862262', '4.4748104', '', '', 1, 28, ''),
(89, 'Øvre Årdal Norway', 'Øvre Årdal, Norway', 'Sogn og Fjordane', 'Øvre Årdal', 'No zipcode available', '61.3088576', '7.8028434', '', '', 1, 27, ''),
(90, 'Sector 49', 'Vatika City, Gardenia Street,Vatika City, Block W, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '', '28.4051098', '77.0474187', 'Sector 49', '', 1, 26, ''),
(92, 'Örebro Sweden', 'Örebro, Sweden', 'Örebro County', 'Örebro', 'No zipcode available', '59.2752626', '15.2134105', '', '', 1, 28, ''),
(93, 'Huda', 'Huda Metro Station, Delhi, India', 'Delhi', 'Gurugram', '122007', '28.4592693', '77.0724192', '', '', 1, 29, ''),
(95, 'New Delhi Delhi India', 'New Delhi, Delhi, India', 'Delhi', 'New Delhi', 'No zipcode available', '28.6139391', '77.2090212', '', '', 1, 29, ''),
(96, 'Sector 49', 'C-122, Sohna - Gurgaon Road,Tatvam Villas, Uppal Southend, Sector 49,Gurugram, Haryana 122004,', 'Haryana', 'Gurugram', '122004', '28.4077308', '77.0419733', '', '', 1, 30, ''),
(97, 'Storgata 1 Alesund Norway', 'Storgata 1, Alesund, Norway', 'Møre og Romsdal', 'Alesund', '6002', '62.4717219', '6.1599138', '', '', 1, 31, ''),
(98, 'Malibu', 'JMD Megapolis, Malibu Town, Gurugram, Haryana, India', 'Haryana', 'Gurugram', '122018', '28.42004', '77.03962', '', '', 1, 32, ''),
(102, 'Sector 43', 'Sector 43 Service Road,Block C, Sushant Lok Phase I, Sector 43,Gurugram, Haryana 122009,', 'Haryana', 'Gurugram', '', '28.4501481', '77.0776578', 'Sector 43', '', 1, 33, ''),
(103, 'Øvre Årdal Norway', 'Øvre Årdal, Norway', 'Sogn og Fjordane', 'Øvre Årdal', 'No zipcode available', '61.3088576', '7.8028434', '', '', 1, 34, ''),
(118, 'whhw', 'svvs, wggw, 68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '464', '28.4113312', '77.0437399', 'whhw', '', 1, 44, ''),
(119, 'ankit', '457, b block, 1478, Arya Samaj Road,Residency Green, Sector 46,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122001', '28.4382722', '77.0533181', 'ankit', '', 1, 45, ''),
(120, 'dhhdhf', 'jfuf, hdhdd, 192,Indira Colony, Indira Colony 1, Sector 52,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122001', '28.4382638', '77.0694245', 'Sector 52', '', 1, 45, ''),
(121, 'ankit', '122A, b block, 1418,Huda Colony, Sector 46,Gurugram, Haryana 122022,', 'Haryana', 'Gurugram', '122001', '28.429229', '77.0576092', 'ankit', '', 1, 46, ''),
(122, 'Gurgaon Haryana India', 'Gurgaon, Haryana, India', 'Haryana', 'Gurugram', 'No zipcode available', '28.4594965', '77.0266383', '', '', 1, 47, ''),
(123, 'bsbs', 'agga, whwhwyw, 68, Plaza Street,Block S, Uppal Southend, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '464', '28.4113312', '77.0437399', 'bsbs', '', 1, 48, ''),
(124, 'bsbz', 'agsh, aha, 165,South City II, Sector 49,Gurugram, Haryana 122018,', 'Haryana', 'Gurugram', '7667', '28.4159865', '77.0470856', 'Sector 49', '', 1, 48, '');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_username` text NOT NULL,
  `admin_password` text NOT NULL,
  `admin_fname` text NOT NULL,
  `admin_lname` text NOT NULL,
  `admin_email` text NOT NULL,
  `admin_role` varchar(11) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_username`, `admin_password`, `admin_fname`, `admin_lname`, `admin_email`, `admin_role`, `admin_type`) VALUES
(1, 'apporio', '12345', 'abc', 'apporio', 'apporio@gmail.com', '', 1),
(2, 'app', '12345', 'abc', 'apporio', 'apporio@gmail.com', '7', 0);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, 'dgfdsgdf', 2),
(2, 'cus_ALBTLyBmrHDXrJ', 4),
(3, 'cus_ALCxwUhuEsoK9p', 3),
(4, 'cus_ALEmbENYBQ6pRh', 7),
(5, 'cus_ALFF1ca2I7WDUM', 8),
(6, 'cus_ALFhC38nvBxQKS', 9),
(7, 'cus_ALwMhQp1uKmDgZ', 6),
(8, 'cus_AM0eeFrEFzFP77', 10),
(9, 'cus_AM0s06EvFC7ROX', 11),
(10, 'cus_AM1RxWwW1VQObI', 12),
(11, 'cus_AMZvDwC2XnqfAB', 16),
(13, 'cus_AMdiAEav3Wu6ET', 19),
(14, 'cus_AMfHPfnmWcEabQ', 21),
(15, 'cus_AMhUiUlQFlflxX', 23),
(16, 'cus_AMjCcwa4DHClwP', 24),
(17, 'cus_AMlRNl52LPVjxP', 25),
(18, 'cus_AN35rXjsJfOYbk', 27),
(19, 'cus_AN36sBLlX3u4zF', 27),
(20, 'cus_AN3Hrqrl1p6OnS', 26),
(21, 'cus_AN3PKiSO8fGwcq', 28),
(23, 'cus_AN3i9xVJEjpp47', 29),
(24, 'cus_AN4FRep4Upmvbf', 29),
(25, 'cus_AN5lpQNHo7awob', 32),
(26, 'cus_ANMOYdljoNt8jS', 33),
(27, 'cus_ANPT2q39EE2Uu8', 34),
(28, 'cus_ANoactou66GtiE', 1),
(29, 'cus_ANoebYf0GVIKfl', 35),
(30, 'cus_ANqhxOcXUXCvnA', 37),
(31, 'cus_AO62SD7KIP1Z4l', 38),
(32, 'cus_AO6KXUpmO3F66M', 36),
(33, 'cus_AOA5alJpIASUuQ', 39),
(34, 'cus_AOEKQD93TYPvQK', 45),
(35, 'cus_AOSxHqaHnRl9lW', 47),
(36, 'cus_AOSzmGKg6ygpye', 46),
(37, 'cus_AOZZBW3s6F2kpL', 48);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `date` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sub_total` decimal(65,2) NOT NULL,
  `special_notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `category_id`, `product_id`, `quantity`, `address_id`, `time_id`, `date`, `sub_total`, `special_notes`, `status`, `session_id`) VALUES
(596, 100, 4, 4, 12, 0, 0, '', 100.00, '', 1, '100'),
(1122, 0, 1, 1, 2, 0, 0, '', 100.00, '', 1, '58dfb05c0499c');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(21) NOT NULL,
  `category_id` int(21) NOT NULL,
  `category_name` text CHARACTER SET latin1 NOT NULL,
  `category_icon` text CHARACTER SET latin1 NOT NULL,
  `category_image` text CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL,
  `category_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `category_id`, `category_name`, `category_icon`, `category_image`, `language_id`, `category_status`) VALUES
(1, 1, 'Laundry', 'http://apporio.org/servicesapp/uploads/Laundry.png', 'http://apporio.org/servicesapp/uploads/laundry.jpg', 1, 1),
(2, 2, 'Car Wash', 'http://apporio.org/servicesapp/uploads/Carwash.png', 'http://apporio.org/servicesapp/uploads/carwash.jpg', 1, 1),
(3, 3, 'House Cleaning', 'http://apporio.org/servicesapp/uploads/Cleaning.png', 'http://apporio.org/servicesapp/uploads/housecleaning.jpg', 1, 1),
(4, 4, 'Boat Wash', 'http://apporio.org/servicesapp/uploads/Boatwash.png', 'http://apporio.org/servicesapp/uploads/boatWash.jpg', 1, 1),
(44, 5, 'Klæsvask', '', 'http://apporio.org/servicesapp/uploads/vaskeri.jpg', 2, 1),
(43, 5, 'Lundry', '', 'http://apporio.org/servicesapp/uploads/vaskeri.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `status`) VALUES
(1, 'English', 1),
(2, 'Norwegian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successfull', 1),
(2, 1, 'Vellykket innlogging\n', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Bruker inaktiv', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Krev felt mangler', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'E-post-id eller passord er feil', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'Logg ut Vellykket', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Ingen registrering funnet', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Påmelding hell\n', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'Telefonnummer allerede eksisterer', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'E-post allerede eksisterer', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `user_id`, `session_id`) VALUES
(1, 4, ''),
(2, 4, ''),
(3, 3, ''),
(4, 4, ''),
(5, 4, ''),
(6, 4, ''),
(7, 3, ''),
(8, 8, ''),
(9, 9, ''),
(10, 6, ''),
(11, 10, ''),
(12, 11, ''),
(13, 12, ''),
(14, 12, ''),
(15, 16, ''),
(16, 6, ''),
(24, 19, ''),
(42, 26, ''),
(25, 21, ''),
(26, 21, ''),
(27, 16, ''),
(28, 23, ''),
(29, 23, ''),
(30, 6, ''),
(31, 3, ''),
(32, 3, ''),
(33, 24, ''),
(34, 24, ''),
(35, 24, ''),
(36, 25, ''),
(37, 25, ''),
(38, 25, ''),
(39, 3, ''),
(41, 100, ''),
(43, 26, ''),
(44, 28, ''),
(45, 28, ''),
(46, 28, ''),
(47, 26, ''),
(48, 29, ''),
(49, 29, ''),
(50, 29, ''),
(51, 29, ''),
(52, 16, ''),
(53, 32, ''),
(54, 3, ''),
(55, 3, ''),
(56, 3, ''),
(57, 33, ''),
(58, 33, ''),
(59, 33, ''),
(60, 33, ''),
(61, 34, ''),
(62, 6, ''),
(63, 6, ''),
(64, 6, ''),
(65, 6, ''),
(66, 34, ''),
(67, 1, ''),
(68, 35, ''),
(69, 35, ''),
(70, 37, ''),
(71, 37, ''),
(72, 38, ''),
(73, 36, ''),
(74, 39, ''),
(75, 3, ''),
(76, 45, ''),
(77, 45, ''),
(78, 47, ''),
(79, 46, ''),
(80, 46, ''),
(81, 48, ''),
(82, 48, ''),
(83, 34, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `address_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `total` decimal(21,2) NOT NULL DEFAULT '0.00',
  `special_notes` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`order_product_id`, `order_id`, `order_category_id`, `product_id`, `category_id`, `quantity`, `address_id`, `time_id`, `date`, `total`, `special_notes`, `session_id`) VALUES
(1, 1, 0, 1, 1, 2, 7, 1, '23-Mar-2017', 100.00, '', ''),
(2, 1, 0, 5, 1, 1, 7, 1, '23-Mar-2017', 50.00, '', ''),
(3, 2, 0, 2, 2, 2, 7, 2, '23-Mar-2017', 40.00, '', ''),
(4, 3, 0, 5, 1, 1, 5, 5, '2017-03-24', 50.00, '', ''),
(5, 3, 0, 1, 1, 1, 5, 5, '2017-03-24', 50.00, '', ''),
(6, 4, 0, 5, 1, 1, 19, 1, '23-Mar-2017', 50.00, '', ''),
(7, 4, 0, 1, 1, 1, 19, 1, '23-Mar-2017', 50.00, '', ''),
(8, 5, 0, 1, 1, 1, 7, 5, '24-Mar-2017', 50.00, '', ''),
(9, 6, 0, 1, 1, 1, 20, 1, '23-Mar-2017', 50.00, '', ''),
(10, 6, 0, 5, 1, 1, 20, 1, '23-Mar-2017', 50.00, '', ''),
(11, 7, 0, 2, 2, 1, 5, 2, '2017-03-24', 20.00, '', ''),
(12, 8, 0, 1, 1, 1, 24, 1, '23-Mar-2017', 50.00, '', ''),
(13, 8, 0, 5, 1, 1, 24, 1, '23-Mar-2017', 50.00, '', ''),
(14, 9, 0, 5, 1, 1, 27, 1, '23-Mar-2017', 50.00, '', ''),
(15, 9, 0, 1, 1, 1, 27, 1, '23-Mar-2017', 50.00, '', ''),
(16, 10, 0, 1, 1, 1, 32, 1, '29-Mar-2017', 50.00, '', ''),
(17, 11, 0, 5, 1, 4, 33, 5, '25-Mar-2017', 200.00, '', ''),
(18, 11, 0, 1, 1, 3, 33, 5, '25-Mar-2017', 150.00, '', ''),
(19, 12, 0, 2, 2, 5, 34, 6, '25-Mar-2017', 100.00, '', ''),
(20, 13, 0, 2, 2, 1, 35, 2, '25-Mar-2017', 20.00, '', ''),
(21, 14, 0, 1, 1, 1, 37, 5, '25-Mar-2017', 50.00, '', ''),
(22, 15, 0, 1, 1, 1, 44, 7, '2017-03-31', 50.00, '', ''),
(23, 16, 0, 1, 1, 1, 47, 1, '27-Mar-2017', 50.00, '', ''),
(24, 16, 0, 5, 1, 1, 47, 1, '27-Mar-2017', 50.00, '', ''),
(25, 16, 0, 2, 2, 1, 48, 2, '27-Mar-2017', 20.00, '', ''),
(26, 17, 0, 1, 3, 12, 46, 1, '12/04/2017', 100.00, '', ''),
(27, 18, 0, 1, 2, 12, 46, 1, '12/04/2017', 100.00, '', ''),
(31, 21, 0, 4, 4, 12, 46, 1, '12/04/2017', 100.00, '', ''),
(30, 21, 0, 1, 1, 12, 46, 1, '12/04/2017', 100.00, '', ''),
(32, 22, 0, 1, 1, 12, 46, 1, '12/04/2017', 100.00, '', ''),
(33, 23, 0, 4, 4, 12, 0, 0, '', 100.00, '', ''),
(34, 24, 0, 4, 4, 1, 51, 11, '2017-03-28', 10.00, '', ''),
(35, 24, 0, 8, 1, 1, 51, 5, '2017-03-31', 100.00, '', ''),
(36, 24, 0, 2, 2, 1, 51, 8, '2017-03-28', 20.00, '', ''),
(37, 24, 0, 3, 3, 2, 51, 9, '2017-03-30', 140.00, '', ''),
(38, 25, 0, 8, 1, 1, 53, 7, '2017-03-29', 100.00, '', ''),
(39, 25, 0, 1, 1, 1, 53, 7, '2017-03-29', 50.00, '', ''),
(40, 25, 0, 5, 1, 1, 53, 7, '2017-03-29', 50.00, '', ''),
(41, 26, 0, 3, 3, 2, 53, 10, '2017-03-30', 140.00, '', ''),
(42, 26, 0, 2, 2, 3, 53, 8, '2017-03-28', 60.00, '', ''),
(43, 27, 0, 8, 1, 12, 45, 5, '2017-03-29', 1200.00, '', ''),
(44, 28, 0, 8, 1, 5, 64, 5, '2017-03-29', 500.00, '', ''),
(45, 28, 0, 5, 1, 4, 64, 5, '2017-03-29', 200.00, '', ''),
(46, 28, 0, 1, 1, 3, 64, 5, '2017-03-29', 150.00, '', ''),
(47, 29, 0, 4, 4, 1, 63, 9, '2017-03-29', 10.00, '', ''),
(48, 29, 0, 2, 2, 1, 64, 6, '2017-03-30', 20.00, '', ''),
(49, 29, 0, 3, 3, 2, 63, 9, '2017-03-31', 140.00, '', ''),
(50, 30, 0, 5, 1, 1, 47, 1, '27-Mar-2017', 50.00, '', ''),
(51, 30, 0, 1, 1, 1, 47, 1, '27-Mar-2017', 50.00, '', ''),
(52, 31, 0, 2, 2, 2, 5, 6, '2017-03-30', 40.00, '', ''),
(53, 32, 0, 2, 2, 4, 5, 2, '2017-03-29', 80.00, '', ''),
(54, 33, 0, 2, 2, 13, 83, 2, '29 Mar 2017', 260.00, '', ''),
(55, 33, 0, 3, 3, 3, 86, 9, '28 Mar 2017', 210.00, '', ''),
(56, 34, 0, 3, 3, 2, 82, 9, '29 Mar 2017', 140.00, '', ''),
(57, 35, 0, 3, 3, 5, 83, 9, '27-Mar-2017', 350.00, '', ''),
(58, 35, 0, 2, 2, 4, 83, 2, '27-Mar-2017', 80.00, '', ''),
(59, 36, 0, 3, 3, 1, 87, 10, '2017-04-21', 70.00, '', ''),
(60, 36, 0, 9, 1, 1, 87, 5, '2017-05-25', 500.00, '', ''),
(61, 36, 0, 1, 1, 2, 87, 5, '2017-05-25', 100.00, '', ''),
(62, 37, 0, 3, 3, 1, 87, 9, '2017-03-30', 70.00, '', ''),
(63, 37, 0, 4, 4, 1, 87, 11, '2017-03-31', 10.00, '', ''),
(64, 37, 0, 2, 2, 1, 87, 6, '2017-03-31', 20.00, '', ''),
(65, 38, 0, 3, 3, 2, 87, 9, '2017-05-26', 140.00, '', ''),
(66, 38, 0, 5, 1, 1, 87, 1, '2017-03-30', 50.00, '', ''),
(67, 38, 0, 8, 1, 1, 87, 1, '2017-03-30', 100.00, '', ''),
(68, 39, 0, 2, 2, 19, 84, 8, '2017-03-30', 380.00, '', ''),
(69, 39, 0, 9, 1, 2, 84, 7, '2017-03-30', 1000.00, '', ''),
(70, 39, 0, 7, 1, 3, 84, 7, '2017-03-30', 300.00, '', ''),
(71, 39, 0, 6, 1, 3, 84, 7, '2017-03-30', 600.00, '', ''),
(72, 39, 0, 8, 1, 3, 84, 7, '2017-03-30', 300.00, '', ''),
(73, 39, 0, 5, 1, 4, 84, 7, '2017-03-30', 200.00, '', ''),
(74, 39, 0, 1, 1, 3, 84, 7, '2017-03-30', 150.00, '', ''),
(75, 39, 0, 3, 3, 1, 84, 9, '2017-03-31', 70.00, '', ''),
(76, 40, 404, 4, 4, 12, 46, 1, '12/04/2018', 100.00, '', ''),
(77, 41, 412, 2, 2, 12, 46, 1, '12/04/2018', 100.00, '', ''),
(78, 42, 423, 3, 3, 2, 90, 3, '30-Mar-2017', 140.00, '', ''),
(79, 42, 421, 1, 1, 2, 90, 1, '28-Mar-2017', 100.00, '', ''),
(80, 43, 431, 5, 1, 1, 90, 5, '30-Mar-2017', 50.00, '', ''),
(81, 43, 431, 1, 1, 1, 90, 5, '30-Mar-2017', 50.00, '', ''),
(82, 44, 441, 1, 1, 2, 91, 5, '2017-03-29', 100.00, 'Hello', ''),
(83, 45, 451, 1, 1, 1, 0, 0, '', 50.00, '', ''),
(84, 45, 451, 8, 1, 1, 0, 0, '', 100.00, '', ''),
(85, 46, 461, 7, 1, 1, 92, 5, '2017-03-30', 100.00, 'Fdfhh', ''),
(86, 46, 461, 1, 1, 1, 92, 5, '2017-03-30', 50.00, 'Fdfhh', ''),
(87, 46, 461, 8, 1, 1, 92, 5, '2017-03-30', 100.00, 'Fdfhh', ''),
(88, 47, 471, 1, 1, 1, 90, 1, '28-Mar-2017', 50.00, 'test', ''),
(89, 47, 471, 5, 1, 1, 90, 1, '28-Mar-2017', 50.00, 'test', ''),
(90, 48, 481, 1, 1, 1, 93, 5, '2017-03-30', 50.00, '', ''),
(91, 49, 493, 3, 3, 2, 93, 9, '2017-03-30', 140.00, '', ''),
(92, 49, 491, 9, 1, 1, 93, 5, '2017-03-29', 500.00, '', ''),
(93, 49, 491, 7, 1, 3, 93, 5, '2017-03-29', 300.00, '', ''),
(94, 49, 491, 6, 1, 2, 93, 5, '2017-03-29', 400.00, '', ''),
(95, 50, 501, 5, 1, 1, 95, 5, '2017-03-29', 50.00, '', ''),
(96, 50, 503, 3, 3, 1, 93, 9, '2017-03-30', 70.00, '', ''),
(97, 50, 501, 1, 1, 1, 95, 5, '2017-03-29', 50.00, '', ''),
(98, 51, 513, 3, 3, 5, 95, 9, '2017-03-29', 350.00, '', ''),
(99, 52, 521, 5, 1, 1, 45, 5, '2017-03-29', 50.00, 'Hello', ''),
(100, 52, 521, 1, 1, 1, 45, 5, '2017-03-29', 50.00, 'Hello', ''),
(101, 53, 531, 1, 1, 1, 98, 5, '2017-03-30', 50.00, '', ''),
(102, 53, 531, 8, 1, 1, 98, 5, '2017-03-30', 100.00, '', ''),
(103, 54, 541, 1, 1, 1, 84, 7, '2017-03-31', 50.00, '', ''),
(104, 55, 551, 1, 1, 1, 84, 5, '2017-03-31', 50.00, '', ''),
(105, 55, 553, 3, 3, 1, 5, 9, '2017-03-31', 70.00, '', ''),
(106, 56, 562, 2, 2, 1, 5, 6, '2017-03-31', 20.00, 'Jxjsjsa', ''),
(107, 56, 564, 4, 4, 3, 84, 11, '2017-03-31', 30.00, '', ''),
(108, 56, 561, 7, 1, 2, 5, 5, '2017-03-30', 200.00, '', ''),
(109, 56, 561, 5, 1, 1, 5, 5, '2017-03-30', 50.00, '', ''),
(110, 57, 572, 2, 2, 2, 100, 8, '31-Mar-2017', 40.00, 'ghggg', ''),
(111, 58, 582, 2, 2, 4, 100, 6, '04-Apr-2017', 80.00, '', ''),
(112, 58, 583, 3, 3, 3, 101, 9, '03-Apr-2017', 210.00, 's', ''),
(113, 59, 591, 5, 1, 3, 102, 5, '29-Mar-2017', 150.00, 'fgsghd', ''),
(114, 59, 591, 1, 1, 2, 102, 5, '29-Mar-2017', 100.00, 'fgsghd', ''),
(115, 60, 603, 3, 3, 4, 102, 9, '31-Mar-2017', 280.00, '', ''),
(116, 61, 614, 4, 4, 4, 103, 11, '2017-03-31', 40.00, 'Bext boat', ''),
(117, 61, 612, 2, 2, 1, 103, 8, '2017-03-31', 20.00, 'Take car', ''),
(118, 61, 613, 3, 3, 1, 103, 9, '2017-04-07', 70.00, 'Was good', ''),
(119, 62, 622, 2, 2, 1, 104, 6, '30-Mar-2017', 20.00, '', ''),
(120, 62, 621, 5, 1, 2, 104, 5, '30-Mar-2017', 100.00, 'test', ''),
(121, 62, 621, 1, 1, 2, 104, 5, '30-Mar-2017', 100.00, 'test', ''),
(122, 63, 634, 4, 4, 3, 104, 11, '31-Mar-2017', 30.00, '', ''),
(123, 63, 632, 2, 2, 9, 104, 6, '29-Mar-2017', 180.00, 'hdhd', ''),
(124, 64, 642, 2, 2, 5, 104, 6, '30-Mar-2017', 100.00, 'sss', ''),
(125, 65, 654, 4, 4, 3, 104, 11, '29-Mar-2017', 30.00, 'cff', ''),
(126, 66, 662, 2, 2, 2, 103, 6, '31. mar. 2017', 40.00, 'ok', ''),
(127, 66, 661, 1, 1, 3, 103, 5, '28. mai 2017', 150.00, 'good', ''),
(128, 67, 673, 3, 3, 2, 70, 10, '30-Mar-2017', 140.00, 'fsg', ''),
(129, 67, 671, 1, 1, 3, 71, 5, '31-Mar-2017', 150.00, 'fsfsgd', ''),
(130, 68, 682, 2, 2, 2, 106, 6, '31-Mar-2017', 40.00, 'mibeeeer', ''),
(131, 68, 683, 3, 3, 2, 106, 3, '01-Apr-2017', 140.00, 'gdhd', ''),
(132, 69, 692, 2, 2, 3, 106, 6, '30-Mar-2017', 60.00, 'fgdg', ''),
(133, 69, 693, 3, 3, 3, 106, 9, '31-Mar-2017', 210.00, 'ggdh', ''),
(134, 70, 701, 1, 1, 2, 110, 5, '28-Apr-2017', 100.00, '', ''),
(135, 70, 701, 5, 1, 3, 110, 5, '28-Apr-2017', 150.00, '', ''),
(136, 71, 712, 2, 2, 3, 110, 2, '31-Mar-2017', 60.00, '', ''),
(137, 72, 722, 2, 2, 4, 112, 6, '01-Apr-2017', 80.00, '', ''),
(138, 73, 731, 1, 1, 1, 108, 5, '31-Mar-2017', 50.00, 'test', ''),
(139, 73, 731, 5, 1, 1, 108, 5, '31-Mar-2017', 50.00, 'test', ''),
(140, 74, 741, 1, 1, 3, 113, 5, '1 Apr 2017', 150.00, 'gsh', ''),
(141, 74, 741, 5, 1, 3, 113, 5, '1 Apr 2017', 150.00, 'gsh', ''),
(142, 75, 751, 1, 1, 1, 5, 1, '2017-04-01', 50.00, '', ''),
(143, 75, 751, 5, 1, 2, 5, 1, '2017-04-01', 100.00, '', ''),
(144, 75, 751, 8, 1, 2, 5, 1, '2017-04-01', 200.00, '', ''),
(145, 75, 753, 3, 3, 7, 5, 3, '2017-04-01', 490.00, 'XBZFhfhfh', ''),
(146, 76, 762, 2, 2, 2, 119, 6, '01-Apr-2017', 40.00, 'ankit', ''),
(147, 77, 771, 1, 1, 3, 120, 5, '01-Apr-2017', 150.00, 'xhxyd', ''),
(148, 77, 771, 5, 1, 3, 120, 5, '01-Apr-2017', 150.00, 'xhxyd', ''),
(149, 77, 772, 2, 2, 4, 120, 6, '01-Apr-2017', 80.00, '', ''),
(150, 78, 781, 8, 1, 2, 122, 5, '2017-04-15', 200.00, '', ''),
(151, 78, 781, 5, 1, 2, 122, 5, '2017-04-15', 100.00, '', ''),
(152, 78, 781, 1, 1, 2, 122, 5, '2017-04-15', 100.00, '', ''),
(153, 79, 791, 5, 1, 2, 121, 5, '8 Apr 2017', 100.00, 'hsh', ''),
(154, 79, 791, 1, 1, 2, 121, 5, '8 Apr 2017', 100.00, 'hsh', ''),
(155, 80, 802, 2, 2, 2, 121, 8, '8 Apr 2017', 40.00, 'gsg', ''),
(156, 80, 801, 5, 1, 2, 121, 5, '22 Apr 2017', 100.00, 'vdvdh', ''),
(157, 80, 801, 1, 1, 2, 121, 5, '22 Apr 2017', 100.00, 'vdvdh', ''),
(158, 81, 811, 1, 1, 1, 124, 1, '01-Apr-2017', 50.00, '', ''),
(159, 81, 811, 8, 1, 5, 124, 1, '01-Apr-2017', 500.00, '', ''),
(160, 81, 811, 5, 1, 5, 124, 1, '01-Apr-2017', 250.00, '', ''),
(161, 82, 821, 1, 1, 2, 124, 1, '01-Apr-2017', 100.00, '', ''),
(162, 83, 831, 8, 1, 2, 103, 5, '28. apr. 2017', 200.00, 'nei', ''),
(163, 83, 831, 5, 1, 3, 103, 5, '28. apr. 2017', 150.00, 'nei', ''),
(164, 83, 831, 1, 1, 4, 103, 5, '28. apr. 2017', 200.00, 'nei', ''),
(165, 83, 834, 4, 4, 2, 103, 11, '27. mai 2017', 20.00, 'ja', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` text CHARACTER SET latin1 NOT NULL,
  `desc` text CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `title`, `desc`, `email`, `language_id`) VALUES
(1, 1, 'About Us', 'For a remarkable about page, all you need to do is figure out your company\'s unique identity, and then share it with the world. Easy, right? Of course it\'s not easy. That said, the \"About Us\" page is one of the most important pages on your website, and it can\'t go neglected. It also happens to be one of the most commonly overlooked pages, which is why you should make it stand out         ', '', 1),
(2, 1, 'Om oss', 'For en bemerkelsesverdig om side, er alt du trenger å gjøre å finne ut bedriftens unike identitet, og deretter dele det med verden. Enkelt, ikke sant? Selvfølgelig er det ikke lett. Når det er sagt, \"Om oss\" siden er en av de viktigste sidene på nettstedet ditt, og det kan ikke gå neglisjert. Det skjer også å være en av de mest oversett sidene, noe som er grunnen til at du bør gjøre det skiller seg ut', '', 2),
(3, 2, 'Keshav Goyal', '004797521288', 'support@vaskmin.no', 1),
(4, 2, 'Keshav Goyal', '004797521288', 'support@vaskmin.no', 2),
(5, 3, 'Terms and Conditions', 'For all mobile applications that require registration or a login, you agree to complete the initial registration process according to go limo App requirements stated on the registration page, and to provide go limo with accurate, complete and updated information as required for the initial registration process, including, but not limited to, your legal name, billing and delivery address, email address, and appropriate telephone contact numbers. \r\n', '', 1),
(6, 3, 'Vilkår og betingelser', 'For alle mobile applikasjoner som krever registrering eller innlogging, samtykker du til å fullføre den innledende registreringsprosessen ifølge gå limo App kravene på registreringssiden, og for å gi slipp limo med nøyaktig, fullstendig og oppdatert informasjon som er nødvendig for den første registreringen , inkludert, men ikke begrenset til, din juridiske navn, fakturering og leveringsadresse, e-postadresse, og passende telefon kontakt tall.', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `pay_id` int(21) NOT NULL,
  `order_id` int(21) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(21) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`pay_id`, `order_id`, `payment_id`, `status`) VALUES
(1, 1, 'ch_1A0PUCIuSX6Gbxla8aghHx9H', 0),
(2, 2, 'ch_1A0Q7VIuSX6GbxlayUOE9uDr', 0),
(3, 3, 'ch_1A0Q86IuSX6Gbxla4oqQ2EGM', 0),
(4, 5, 'ch_1A0QhNIuSX6GbxlaZwiXY2BS', 0),
(5, 6, 'ch_1A0Qj8IuSX6GbxlaY5Vjtkb0', 0),
(6, 3, 'ch_1A0V6mIuSX6GbxlaZN6UNmjo', 0),
(7, 1, 'ch_1A0Ve6IuSX6Gbxlaj6cWViA8', 0),
(8, 2, 'ch_1A0VfIIuSX6Gbxlaxo5vsFC4', 0),
(9, 1, 'ch_1A0WJvIuSX6GbxlaDvycYzER', 0),
(10, 2, 'ch_1A0WKiIuSX6GbxlakWtFHTtN', 0),
(11, 3, 'ch_1A0WXdIuSX6Gbxla7oaf3GQt', 0),
(12, 4, 'ch_1A0WaCIuSX6Gbxlav8IN3sMO', 0),
(13, 5, 'ch_1A0WfeIuSX6GbxlaHfp1BrO0', 0),
(14, 6, 'ch_1A0WjhIuSX6GbxlamnwAoNEO', 0),
(15, 7, 'ch_1A0X8AIuSX6GbxlaVSDPV0OA', 0),
(16, 8, 'ch_1A0YlNIuSX6GbxlaeYKi2Gdb', 0),
(17, 9, 'ch_1A0ZC0IuSX6GbxlafZ9W0uGP', 0),
(18, 10, 'ch_1A1EUIIuSX6GbxlaJF2GQyVr', 1),
(19, 11, 'ch_1A1IdJIuSX6GbxlasokoWGVV', 1),
(20, 12, 'ch_1A1IqiIuSX6GbxlarDtaoUj9', 1),
(21, 13, 'ch_1A1JP2IuSX6GbxlaVP5ANtcg', 1),
(22, 14, 'ch_1A1JQeIuSX6GbxlaOol7jICx', 1),
(23, 15, 'ch_1A1qlvIuSX6GbxlaCuZHHhUD', 1),
(24, 16, 'ch_1A1rsbIuSX6Gbxla1jDlp7Df', 1),
(25, 17, 'ch_1A1tJXIuSX6GbxlaV07N0xuB', 1),
(26, 18, 'ch_1A1tKUIuSX6GbxlalFvLh4Cx', 1),
(27, 19, 'ch_1A1uPuIuSX6GbxlaCk7sRMcZ', 1),
(28, 21, 'ch_1A1uZBIuSX6GbxlaSGZQF9Z3', 1),
(29, 22, 'ch_1A1ugBIuSX6GbxlamRlHKlsF', 1),
(30, 23, 'ch_1A1usdIuSX6GbxlaVcWDyTBh', 1),
(31, 24, 'ch_1A1uuBIuSX6GbxlajxxWCS5J', 1),
(32, 25, 'ch_1A1vxVIuSX6GbxlaBzUKX961', 1),
(33, 26, 'ch_1A1wAvIuSX6GbxlaLsu11KZj', 1),
(34, 27, 'ch_1A1xBdIuSX6GbxlaIJvChjQw', 1),
(35, 28, 'ch_1A1y5XIuSX6Gbxlam572PBzu', 1),
(36, 29, 'ch_1A1y9xIuSX6Gbxla9z8r8OzV', 1),
(37, 30, 'ch_1A1z3xIuSX6GbxlaMRsHKkKl', 1),
(38, 31, 'ch_1A1zZKIuSX6GbxlanPyJCjg2', 1),
(39, 32, 'ch_1A1zc6IuSX6GbxlaoksKL4uc', 1),
(40, 33, 'ch_1A1zkSIuSX6GbxlaSucvTFem', 1),
(41, 34, 'ch_1A1zlEIuSX6GbxlarwXypRtq', 1),
(42, 35, 'ch_1A1znRIuSX6Gbxla4eJgNXV2', 1),
(43, 36, 'ch_1A21uuIuSX6GbxlaPsofJmR8', 1),
(44, 37, 'ch_1A232pIuSX6GbxlaWA0LEQUd', 1),
(45, 38, 'ch_1A23N9IuSX6Gbxla9AjUViKT', 1),
(46, 39, 'ch_1A2Ey0IuSX6GbxlalnN54Hc3', 1),
(47, 40, 'ch_1A2J48IuSX6GbxlaFfcRfDNs', 1),
(48, 41, 'ch_1A2J6YIuSX6GbxlaOP4u1ea3', 1),
(49, 42, 'ch_1A2JArIuSX6GbxlaVDQkb02C', 1),
(50, 43, 'ch_1A2JD1IuSX6GbxlaV0IGKNID', 1),
(51, 44, 'ch_1A2JLLIuSX6Gbxlam3shsxWi', 1),
(52, 45, 'ch_1A2JN9IuSX6Gbxla2LzpCV6b', 1),
(53, 46, 'ch_1A2JNyIuSX6Gbxla1s2JYG1C', 1),
(54, 47, 'ch_1A2JRBIuSX6GbxlaTOHl5Nrv', 1),
(55, 48, 'ch_1A2JYWIuSX6Gbxla2mavosSg', 1),
(56, 49, 'ch_1A2JaHIuSX6GbxlaJERmopC2', 1),
(57, 50, 'ch_1A2K65IuSX6GbxlaQKDwoih4', 1),
(58, 51, 'ch_1A2K7GIuSX6GbxlamY7oV57R', 1),
(59, 52, 'ch_1A2LPYIuSX6GbxlaQMn0AjaK', 1),
(60, 53, 'ch_1A2LZuIuSX6GbxlaJmLCg3RM', 1),
(61, 54, 'ch_1A2ZFvIuSX6GbxlalXwXBuJo', 1),
(62, 55, 'ch_1A2bccIuSX6GbxlaldF2qD9R', 1),
(63, 56, 'ch_1A2beoIuSX6GbxlavDJX9CDG', 1),
(64, 57, 'ch_1A2bg9IuSX6GbxlamN1IXL9Z', 1),
(65, 58, 'ch_1A2bhlIuSX6GbxlabckpPQ8L', 1),
(66, 59, 'ch_1A2brIIuSX6GbxlaHm1p1Dsg', 1),
(67, 60, 'ch_1A2buEIuSX6Gbxlavq8YuJ28', 1),
(68, 61, 'ch_1A2efKIuSX6Gbxla6YdNtjG2', 1),
(69, 62, 'ch_1A2hmbIuSX6GbxlaCThK1hSI', 1),
(70, 63, 'ch_1A2hqUIuSX6Gbxla23ikiqxp', 1),
(71, 64, 'ch_1A2htoIuSX6GbxlaoBclJKX5', 1),
(72, 65, 'ch_1A2hxLIuSX6Gbxlam6XLAydK', 1),
(73, 66, 'ch_1A2oaAIuSX6GbxlaTCH1VxzU', 1),
(74, 67, 'ch_1A32wxIuSX6GbxlaCXnvE0Cm', 1),
(75, 68, 'ch_1A331DIuSX6GbxlaT1KEWOgX', 1),
(76, 69, 'ch_1A333KIuSX6GbxlaKHpSVhXg', 1),
(77, 70, 'ch_1A350YIuSX6GbxlagN9gdYVX', 1),
(78, 71, 'ch_1A3JeCIuSX6Gbxla2NoZNO67', 1),
(79, 72, 'ch_1A3JqsIuSX6GbxlaAaJyCuuI', 1),
(80, 73, 'ch_1A3K8IIuSX6GbxlaNzUx3N1i', 1),
(81, 74, 'ch_1A3NlqIuSX6Gbxlac3p2RZpI', 1),
(82, 75, 'ch_1A3PZZIuSX6Gbxlaby2KE6g6', 1),
(83, 76, 'ch_1A3RrTIuSX6Gbxlaso2q0aIB', 1),
(84, 77, 'ch_1A3RwrIuSX6GbxlaZwyVett2', 1),
(85, 78, 'ch_1A3g1pIuSX6Gbxlav7OLEUze', 1),
(86, 79, 'ch_1A3g3iIuSX6GbxlaNKTqC6OS', 1),
(87, 80, 'ch_1A3g4xIuSX6GbxlaNfKdUqWT', 1),
(88, 81, 'ch_1A3mQSIuSX6GbxlafASiPWCx', 1),
(89, 82, 'ch_1A3myVIuSX6GbxlaJNGCmj0i', 1),
(90, 83, 'ch_1A4K6AIuSX6Gbxlax9RGfBCj', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pro_id` int(11) NOT NULL,
  `product_id` int(21) NOT NULL,
  `product_name` text CHARACTER SET latin1 NOT NULL,
  `product_image` text CHARACTER SET latin1 NOT NULL,
  `product_description` text CHARACTER SET latin1 NOT NULL,
  `product_unit` text CHARACTER SET latin1 NOT NULL,
  `product_price` decimal(15,2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pro_id`, `product_id`, `product_name`, `product_image`, `product_description`, `product_unit`, `product_price`, `category_id`, `subcategory_id`, `language_id`, `product_status`) VALUES
(1, 1, 'Product1', 'http://apporio.org/servicesapp/uploads/alarm.jpg', 'By means of the mobile otect premises', '2', 50.00, 1, 1, 0, 1),
(2, 2, 'Product2', 'http://apporio.org/servicesapp/uploads/aakrati.jpeg', 'This is a list of cities ', '4', 20.00, 2, 2, 0, 1),
(3, 3, 'Product3', 'http://apporio.org/servicesapp/uploads/enven.jpeg', 'Superior JBL sound', '1', 70.00, 3, 3, 0, 1),
(4, 4, 'Product4', 'http://apporio.org/servicesapp/uploads/iphone.jpeg', 'Transparent Flexible Soft ', '10', 10.00, 4, 4, 0, 1),
(5, 5, 'Product5', 'http://apporio.org/servicesapp/uploads/pro.jpeg', 'It usually takes 15-35 days ', '10', 50.00, 1, 1, 0, 1),
(6, 6, 'Product6', 'http://apporio.org/servicesapp/uploads/children.jpg', 'wooly green small fashion ', '12', 200.00, 1, 5, 0, 1),
(7, 7, 'Product7', 'http://apporio.org/servicesapp/uploads/childrens.jpg', 'wooly green ', '20', 100.00, 1, 5, 0, 1),
(8, 8, 'Shirts', 'http://apporio.org/servicesapp/uploads/children-s-clothing-20937495.jpg', 'Best In Class.....', '20', 100.00, 1, 1, 0, 1),
(9, 9, 'Jeans', 'http://apporio.org/servicesapp/uploads/children-s-sweater-20937472.jpg', 'Jeans Wash And Iron', '5', 500.00, 1, 5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `cat_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `category_id` int(21) NOT NULL,
  `subcategory_name` text CHARACTER SET latin1 NOT NULL,
  `language_id` int(11) NOT NULL,
  `subcategory_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`cat_id`, `subcategory_id`, `category_id`, `subcategory_name`, `language_id`, `subcategory_status`) VALUES
(1, 1, 1, 'Laundry1', 1, 1),
(2, 2, 2, 'Car Wash1', 1, 1),
(3, 3, 3, 'House Cleaning1', 1, 1),
(4, 4, 4, 'Boat Wash1', 1, 1),
(5, 5, 1, 'Laundry2', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE `time_slot` (
  `time_slot_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `time_slot_from` text CHARACTER SET latin1 NOT NULL,
  `time_slot_to` text CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`time_slot_id`, `category_id`, `time_slot_from`, `time_slot_to`, `status`) VALUES
(1, 1, '10:00', '11:00', 1),
(2, 2, '11:00', '12:00', 1),
(3, 3, '21:00', '22:00', 1),
(11, 4, '13:00', '14:00', 1),
(5, 1, '14:00', '15:00', 1),
(8, 2, '15:00', '16:00', 1),
(7, 1, '16:00', '17:00', 1),
(6, 2, '13:00', '14:00', 1),
(9, 3, '13:00', '14:00', 1),
(10, 3, '16:00', '17:00', 1),
(4, 4, '13:00', '14:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` text CHARACTER SET latin1 NOT NULL,
  `last_name` text CHARACTER SET latin1 NOT NULL,
  `email` text CHARACTER SET latin1 NOT NULL,
  `password` text CHARACTER SET latin1 NOT NULL,
  `phone` text CHARACTER SET latin1 NOT NULL,
  `profile_pic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `online_offline` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `phone`, `profile_pic`, `online_offline`, `status`) VALUES
(1, 'lalit', 'kumar', 'lalit@apporio.com', '1234', '9910623373', '', 1, 1),
(2, 'abc', 'xyz', 'yogesh@apporio.com', '123', '123', 'http://apporio.org/servicesapp/uploads/car.png', 1, 1),
(3, 'shivani', 'j', 'shivani@gmail.com', '111111', '1111111111', 'http://apporio.org/servicesapp/uploads/file25.jpeg', 1, 1),
(4, 'Lalit', 'kumar', 'lalit1@apporio.com', '1234', '9887988730', '', 1, 1),
(5, 'diw', 'jguf', 'diw@gmail.com', '123', '8575554457474', '', 1, 1),
(6, 'lalit', 'apporio', 'abc@apporio.com', '123', '9910623373', 'http://apporio.org/servicesapp/uploads/IMG_20170329_185010.jpg', 1, 1),
(7, 'test', 'final', 'testfinal@apporio.com', '123', '9887123456', '', 1, 1),
(8, 'test', 'final', 'tet@apporio.com', '123', '98871234789', '', 1, 1),
(9, 'test', 'apporio', 'test23@gmail.com', '123', '9887302132', '', 1, 1),
(10, 'txctctcy', 'xtfyyc', 'qqqq@gmail.com', '111111', '2345677888', '', 1, 1),
(11, 'shilpa', 'goel', 'goel@gmail.com', '123456', '8130039030', '', 1, 1),
(12, 'demo', 'demo', 'd@g.com', '111111', '7777788888', '', 1, 1),
(13, 'reena', 'jain', 'reena@gmail.com', '111111', '1111111111', '', 1, 1),
(14, 'john', 'smith', 'john@gmail.com', '111111', '1111111111', '', 1, 1),
(15, 'xxx', 'xxx', 'xxx@gmail.com', '111111', '1111111111', '', 1, 1),
(16, 'yyy', 'yyy', 'yyy@gmail.com', '111111', '1111111111', 'http://apporio.org/servicesapp/uploads/file17.jpeg', 1, 1),
(17, 'zzz', 'zzz', 'zzz@gmail.com', '111111', '1111111111', 'http://apporio.org/servicesapp/uploads/file14.jpeg', 1, 1),
(26, 'yogesh', 'yadav', 'yogeshkumar2491@gmail.com', '123', '9050969416', 'http://apporio.org/servicesapp/uploads/IMG_20170328_183321.jpg', 1, 1),
(18, 'ttt', 'ttt', 'ttt@gmail.com', '111111', '1111111111', '', 1, 1),
(19, 'hello', 'hello', 'hello@apporio.com', '123456', '1111111111', 'http://apporio.org/servicesapp/uploads/file15.jpeg', 1, 1),
(20, 'aaa', 'aaa', 'aaa@gnail.com', '111111', '4444444444', '', 1, 1),
(21, 'chugging', 'uffuf', 'assas@gmail.com', '123456', '8888528526', 'http://apporio.org/servicesapp/uploads/file16.jpeg', 1, 1),
(22, 'apporio', 'apporio', 'apporio@gmail.com', 'password', '81330022222', '', 1, 1),
(23, 'ancdef', 'abcdef', 'abcdef@gmail.com', '1234567', '9825475869', '', 1, 1),
(24, 'ankit', 'kumar@g.com', 'kumar@g.com', '1234567', '9867897678', '', 1, 1),
(25, 'ciprian', 'pater', 'post@post.com', 'pass', '985566', '', 1, 1),
(35, 'abcd', 'ssss', 'abcd@gmail.com', '123456', '98356789', 'http://apporio.org/servicesapp/uploads/IMG_20170330_184553.jpg', 1, 1),
(27, 'herbal', 'Jain', 'Jain@gmail.com', '111111', '888444666', '', 1, 1),
(28, 'Rohan', 'Jain', 'rohan@gmail.com', '111111', '1212121212', '', 1, 1),
(29, 'Ankit', 'shah', 'ankit@gmail.com', '1234567', '8586895479', 'http://apporio.org/servicesapp/uploads/file22.jpeg', 1, 1),
(30, 'yogesh', 'kumar', 'yogesh1@apporio.com', '123', '917393629', 'http://apporio.org/servicesapp/uploads/IMG_20170328_185948.jpg', 1, 1),
(31, 'tarzan', 'tarzin', 'test@test.no', 'leon666', '11122233', '', 1, 1),
(32, 'Apporio', 'Apporio', 'qqq@gmail.com', '111111', '4545454545', '', 1, 1),
(33, 'ankit', 'kumar', 'kumar@gmail.com', '123456', '9835425645', '', 1, 1),
(34, 'ciprian', 'pateerrr', 'pater@com.com', 'pass123', '98869885', 'http://apporio.org/servicesapp/uploads/file24.jpeg', 1, 1),
(36, 'lalit', 'kumar', 'address@gmail.com', '123', '22222222', 'http://apporio.org/servicesapp/uploads/IMG_20170331_154934.jpg', 1, 1),
(37, 'sachin', 'tendulkar', 'tendulkar@gmail.com', '123456', '88887799', 'http://apporio.org/servicesapp/uploads/IMG_20170330_204717.jpg', 1, 1),
(38, 'vxgxh', 'tdhd', 'anki@gmail.com', '123456', 'gdhhdhhd', '', 1, 1),
(39, 'stev', 'smith', 'smith@gmail.com', '123456', '87654567', '', 1, 1),
(40, 'ankit', 'gupta', 'gupta@gmail.com', '123456', '55566786', '', 1, 1),
(41, 'xhdgxh', 'xhhfhx', 'abgf@gmail.com', '123456', '55567890', '', 1, 1),
(42, 'shilpa', 'demo', 'demo@ymail.com', 'password', '81300390', '', 1, 1),
(43, 'ssj', 'shsh', 'apporio111@gmail.com', 'password', '82828727', '', 1, 1),
(44, 'shghs', 'whwh', 'svbsbs@gmail.com', '111111', '82883883', '', 1, 1),
(45, 'ankit', 'demo', 'demo@gmail.com', '123456', '98675645', '', 1, 1),
(46, 'ankit', 'g', 'ankittips2@gmail.com', '123456', '98354245', '', 1, 1),
(48, 'hhssh', 'abab', 'w@gmail.com', '111111', '88796646', 'http://apporio.org/servicesapp/uploads/IMG_20170401_190739.jpg', 1, 1),
(47, 'atul', 'jain', 'jain1@gmail.com', '123456', '8568523698', '', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`time_slot_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1135;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(21) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `pay_id` int(21) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `time_slot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
