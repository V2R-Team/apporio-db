-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:23 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_supertaxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`) VALUES
(1, 'Super Taxi', 'uploads/logo/logo_5a1cfa9af0328.png', 'hello@apporio.com', 'Istanbul, Turkey', 'AIzaSyAAg7jPvZlzw5hLJQkAzm2dqpzEfRhCIWE ', '41.0082376', '28.9783589');

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(1, 'cus_BdovrdqjauA1zl', 18);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'Mercedes Vito', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) CHARACTER SET utf32 NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(12, 'Sari', '', '', 'uploads/car/car_12.png', '6784', 1, 1),
(13, 'Turkuaz', '', '', 'uploads/car/editcar_13.png', '8777', 1, 1),
(14, 'VIP', '', '', 'uploads/car/car_14.png', '6625', 1, 1),
(15, 'Vito', '', '', 'uploads/car/car_15.png', '7240', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#8377', 'Miles', '', '', 1),
(120, 'Istanbul', '41.0082376', '28.9783589', '&#;', 'Km', '', '', 1),
(121, 'London', '51.5073509', '-0.1277583', 'Â£', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-09-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-13', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'MANISH', '12', 1, 12, '2017-09-21', '2017-10-21', 'Percentage', 1),
(109, 'MAN', '200', 12, 12, '2017-09-16', '2017-10-27', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377'),
(4, 'YTL', '&#;', '&#;');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM'),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM'),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM'),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM'),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM'),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM'),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM'),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM'),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM'),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM'),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM'),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM'),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM'),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM'),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM'),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM'),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM'),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM'),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM'),
(20, 2, 'tgg', 'tgg', '6666666666', 'gv', 'Saturday, Sep 16, 03:57 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 11, '28.4120710010094', '77.0432192803599', '28.4120651042774', '77.0431658276324', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:03:19 PM', '02:03:30 PM', '02:03:41 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(2, 14, '28.4120852658904', '77.0431683146629', '28.4120359326743', '77.0432331766324', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:17:26 PM', '02:18:41 PM', '02:20:34 PM', '1', '10.00', '00.00', '0.00', '0.00', '0.00', 3, '110', '4.40', '105.60', '100.00', '0.00', '0.00 Miles', '0.0', '1', '110', 1, '', 1, 1, '0000-00-00'),
(3, 16, '', '', '', '', '', '', '02:25:26 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 3, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(4, 18, '28.4120174630934', '77.043117464122', '28.4120380188344', '77.0432202471831', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:32:04 PM', '02:32:42 PM', '02:38:05 PM', '0', '0.00', '60.00', '0.00', '0.00', '0.00', 3, '160', '6.40', '153.60', '100.00', '0.00', '0.00 Miles', '0.0', '5', '160', 1, '', 1, 1, '0000-00-00'),
(5, 19, '28.4121438133174', '77.0431979224896', '28.4121629461779', '77.0432023830539', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:42:31 PM', '02:42:40 PM', '02:59:22 PM', '0', '0.00', '225.00', '0.00', '0.00', '0.00', 3, '325', '13.00', '312.00', '100.00', '0.00', '0.00 Miles', '0.0', '16', '325', 1, '', 1, 1, '0000-00-00'),
(6, 20, '', '', '', '', '', '', '10:43:20 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 3, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(7, 38, '28.4121673', '77.0431843', '28.41218', '77.0431382', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:05:32 AM', '07:05:49 AM', '07:06:08 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(8, 39, '28.4121808', '77.0431688', '28.4121814', '77.0431628', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:08:40 AM', '07:09:04 AM', '07:09:15 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(9, 55, '28.4120894', '77.0434034', '28.4121856', '77.0431223', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:26:02 AM', '08:26:15 AM', '08:26:27 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(10, 56, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:31:00 AM', '08:31:12 AM', '08:31:26 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 0, 0, '0000-00-00'),
(11, 57, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:41:53 AM', '08:41:54 AM', '08:41:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 0, 0, '0000-00-00'),
(12, 58, '', '', '', '', '', '', '08:42:35 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 11, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(13, 60, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:57:03 AM', '08:57:11 AM', '08:57:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Miles', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(14, 61, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '08:58:47 AM', '08:58:51 AM', '08:58:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '200', 11, '0.00', '0.00', '0.00', '178.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 0, '0000-00-00'),
(15, 62, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:00:46 AM', '09:00:50 AM', '09:00:56 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(16, 63, '28.4120894', '77.0434034', '28.4121856', '77.0431223', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '09:01:42 AM', '09:01:45 AM', '09:01:50 AM', '0', '0.00', '00.00', '0.00', '0.00', '200', 10, '0.00', '0.00', '0.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 0, '0000-00-00'),
(17, 65, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '11:00:16 AM', '11:00:45 AM', '11:00:53 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 10, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(18, 66, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '12:55:18 PM', '12:55:56 PM', '12:56:19 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(19, 67, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '12:57:22 PM', '12:57:59 PM', '12:58:04 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(20, 75, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '06:50:39 AM', '06:51:06 AM', '06:51:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(21, 76, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:04:52 AM', '07:04:56 AM', '07:04:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(22, 77, '28.4120894', '77.0434034', '28.4120894', '77.0434034', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:11:33 AM', '07:11:36 AM', '07:11:38 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(23, 78, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:17:56 AM', '07:18:00 AM', '07:18:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(24, 79, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:38:51 AM', '07:38:54 AM', '07:38:59 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(25, 80, '28.4120894', '77.0434034', '28.4121856', '77.0431223', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '07:42:27 AM', '07:42:30 AM', '07:43:05 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(26, 81, '28.4120894', '77.0434034', '28.4120894', '77.0434034', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:43:46 AM', '07:43:48 AM', '07:43:54 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(27, 83, '28.4120632114153', '77.0432878228259', '28.4120288204518', '77.0432450909577', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:36:25 PM', '12:36:33 PM', '12:36:54 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(28, 84, '28.4121364636191', '77.0432474852221', '19.0759837', '72.8776559', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '192, CST Rd, Friends Colony, Hallow Pul, Kurla, Mumbai, Maharashtra 400070, India', '12:42:22 PM', '12:42:27 PM', '12:42:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(29, 91, '', '', '', '', '', '', '09:40:41 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 20, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(30, 94, '51.4007158438222', '-0.268297512084496', '51.400727033663', '-0.268281670287501', '18 S Ln W, New Malden KT3 5AQ, UK', '18 S Ln W, New Malden KT3 5AQ, UK', '12:10:41 PM', '12:12:34 PM', '12:13:00 PM', '1', '0.00', '00.00', '0.00', '0.00', '0.00', 21, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(31, 98, '28.4120862028282', '77.043301410665', '28.3881906911472', '77.0804475992918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Unnamed Road, Sector 64, Gurugram, Haryana 122005, India', '12:58:57 PM', '12:59:02 PM', '12:59:14 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(32, 99, '28.4120773587245', '77.0432640376134', '28.4123116782811', '77.0541093125939', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'C-5, Rosewood City Rd, Rosewood City, Ghasola, Sector 49, Gurugram, Haryana 122018, India', '01:26:35 PM', '01:26:39 PM', '01:26:50 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 0, '0000-00-00'),
(33, 100, '28.4120830598839', '77.0432726944649', '28.4120804661682', '77.0432668501098', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:53:24 PM', '01:53:29 PM', '01:53:34 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 1, '0000-00-00'),
(34, 104, '51.360055348861', '-0.373267777363918', '51.3600798310002', '-0.373252057799067', 'Portsmouth Rd, Esher KT10, UK', 'Portsmouth Rd, Esher KT10, UK', '02:19:47 PM', '02:19:57 PM', '02:20:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(35, 110, '51.4007016364964', '-0.268270689994347', '51.4007483419584', '-0.26824977233604', '18 S Ln W, New Malden KT3 5AQ, UK', '18 S Ln W, New Malden KT3 5AQ, UK', '05:14:57 PM', '05:15:07 PM', '05:16:20 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 21, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(36, 113, '51.4006877225371', '-0.268218806013715', '51.4007384749608', '-0.268253087997686', '18 S Ln W, New Malden KT3 5AQ, UK', '18 S Ln W, New Malden KT3 5AQ, UK', '05:29:14 PM', '05:29:43 PM', '05:38:19 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 21, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '8', '100', 1, '', 1, 1, '0000-00-00'),
(37, 117, '51.3685992661209', '-0.367233604474955', '51.3686078158398', '-0.367156484205809', '102-106 High St, Esher KT10, UK', '102-106 High St, Esher KT10, UK', '10:33:57 AM', '10:34:14 AM', '10:34:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 0, 1, '0000-00-00'),
(38, 118, '', '', '', '', '', '', '10:40:51 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 20, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(39, 120, '51.3987689624783', '-0.305602855260021', '41.0993141692931', '29.003589327514', '10 Uxbridge Rd, Kingston upon Thames KT1 2LL, UK', 'Huzur Mh., KaÄŸÄ±thane Barbaros Cd No:84, 34396 ÅžiÅŸli/Ä°stanbul, Turkey', '12:40:38 PM', '12:40:44 PM', '02:40:07 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '59', '100', 1, '', 1, 0, '0000-00-00'),
(40, 121, '41.0959006843864', '29.005515134415', '41.0847078996784', '29.0080483317755', 'Huzur Mh., Ä°nÃ¶nÃ¼ Cd. No:2, 34396 ÅžiÅŸli/Ä°stanbul, Turkey', 'Konaklar Mahallesi, Ã‡Ä±nar Sk. 178 A, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '02:59:35 PM', '03:02:04 PM', '05:42:12 PM', '2', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '330.326308816311', '40', '100', 1, '', 1, 1, '0000-00-00'),
(41, 122, '41.079505407329', '29.0192569373903', '41.0474409885112', '29.0321368995688', 'Levent Mahallesi, Levent Cd. No:43, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'BoÄŸaziÃ§i KÃ¶prÃ¼sÃ¼, Ä°stanbul, Turkey', '05:44:45 PM', '05:44:53 PM', '09:01:36 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '43', '100', 1, '', 1, 1, '0000-00-00'),
(42, 123, '41.0219227881732', '29.0482279820795', '40.9615315707126', '29.1120774641555', 'Altunizade Mahallesi, Ä°stanbul Ã‡evre Yolu, 34662 ÃœskÃ¼dar/Ä°stanbul, Turkey', 'Ä°Ã§erenkÃ¶y Mahallesi, BahÃ§elerarasÄ± Sk. No:37, 34752 AtaÅŸehir/Ä°stanbul, Turkey', '09:03:43 AM', '09:03:47 AM', '10:26:08 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '8422.37475751257', '22', '100', 1, '', 1, 1, '0000-00-00'),
(43, 124, '40.9616361636627', '29.1120581280427', '41.0049175989888', '29.0452672757345', 'Ä°Ã§erenkÃ¶y Mahallesi, BahÃ§elerarasÄ± Sk. No:37, 34752 AtaÅŸehir/Ä°stanbul, Turkey', 'AcÄ±badem, AcÄ±badem Cd. No:107, 34718 KadÄ±kÃ¶y/Ä°stanbul, Turkey', '10:27:40 AM', '10:27:45 AM', '11:22:32 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '7396.02163212784', '54', '100', 1, '', 1, 0, '0000-00-00'),
(44, 125, '41.0659972572616', '29.0177831712827', '41.0670794551103', '29.0172030565735', 'LevazÄ±m Mahallesi, KarayollarÄ± 17. BÃ¶lge Md., 34340 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'LevazÄ±m Mahallesi, Koru SokaÄŸÄ± No:5, 34340 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '02:30:23 PM', '02:30:44 PM', '02:31:24 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '79.9865129746789', '0', '100', 1, '', 1, 1, '0000-00-00'),
(45, 126, '41.0654693768363', '29.0000544392725', '41.0654489905886', '29.0000356806581', 'Fulya Mahallesi, Ali Sami Yen Sk. No:19, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', 'Fulya Mahallesi, Ali Sami Yen Sk. No:19, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '08:42:47 AM', '08:42:56 AM', '08:44:51 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(46, 127, '28.4121070408678', '77.0432627095188', '28.4112488861673', '77.0369274169207', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'BCD Rd, Vipul World, Sector 48, Gurugram, Haryana 122004, India', '10:32:14 AM', '10:32:17 AM', '10:32:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(47, 128, '28.4120985607999', '77.0432655648869', '28.4088419351916', '77.0485842972994', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'S - 298, Samadhan St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:33:08 AM', '10:33:12 AM', '10:33:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(48, 129, '28.4120802479933', '77.0432743260075', '28.4050111077273', '77.0612369477749', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Golf Course Ext Rd, Badshahpur, Sector 65, Gurugram, Haryana 122018, India', '11:10:45 AM', '11:10:53 AM', '11:10:57 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(49, 135, '28.4120808370208', '77.0432511408973', '28.4088286646658', '77.0501835644245', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '16, Block C Uppal Southland St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:25:28 AM', '11:25:31 AM', '11:25:35 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(50, 136, '28.4121233326435', '77.043291831534', '28.4083293967885', '77.0320645719767', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Spaze Privy Internal Road, Tatvam Villas, Dhani, Sector 72, Gurugram, Haryana 122004, India', '12:47:35 PM', '12:47:39 PM', '12:47:43 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 22, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(51, 137, '51.3988132433172', '-0.305648520165044', '51.3686079062402', '-0.367420832046903', '10 Uxbridge Rd, Kingston upon Thames KT1 2LL, UK', '94 High St, Esher KT10 9QJ, UK', '01:14:02 PM', '01:15:15 PM', '01:26:22 PM', '1', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '110.5', '11.05', '99.45', '110.50', '0.00', '6.05 Km', '5425.87584148916', '11', '110.5', 1, '', 1, 1, '0000-00-00'),
(52, 139, '28.4120632291389', '77.0432770937655', '28.4120632291389', '77.0432770937655', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:59:23 PM', '01:59:26 PM', '01:59:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 25, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 0, 1, '0000-00-00'),
(53, 140, '51.4137027986826', '-0.287652422482279', '51.4135657324557', '-0.287202816541297', '204 A308, Kingston upon Thames KT2, UK', '204 London Rd, Kingston upon Thames KT2 6QP, UK', '08:29:40 PM', '08:30:13 PM', '08:30:24 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(54, 141, '41.0440712696008', '29.0164069396125', '41.0441465580293', '29.0167999319526', 'YÄ±ldÄ±z Mh., Ã‡Ä±raÄŸan Cd. No:32, 34349 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'YÄ±ldÄ±z Mh., Ã‡Ä±raÄŸan Cd. No:32, 34349 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '07:32:42 AM', '07:32:48 AM', '07:32:58 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(55, 142, '41.079127788143', '29.0201751207556', '41.0792240900395', '29.0201292689557', 'Levent Mahallesi, GÃ¼vercin Sk. No:35, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'Levent Mahallesi, GÃ¼vercin Sk. No:37, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '12:53:03 PM', '12:53:25 PM', '12:53:35 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(56, 143, '41.0791007080267', '29.0196890108748', '41.0789886116342', '29.0201852541508', 'Levent Mahallesi, Krizantem Sk. No:34, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'Levent Mahallesi, GÃ¼vercin Sk. No:33, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '05:06:02 PM', '05:06:08 PM', '05:07:53 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, '', 1, 0, '0000-00-00'),
(57, 144, '41.0774285830837', '29.020220789052', '41.0774405913873', '29.0202681015976', 'Levent Mahallesi, Krizantem Sk. No:12, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', 'Levent Mahallesi, Krizantem Sk. No:12, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '01:37:34 PM', '01:37:40 PM', '01:38:01 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(58, 145, '41.0787166087875', '29.0119550505172', '41.0787074709307', '29.0119365291301', 'Esentepe Mahallesi, Ecza SokaÄŸÄ± No:1, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', 'Esentepe Mahallesi, Ecza SokaÄŸÄ± No:1, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '07:43:20 PM', '07:43:29 PM', '07:43:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 20, '100', '10.00', '90.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 4, 'Anurag', 'anurag@apporio.com', '9876543210', '', 'akhil1010', 'R9YRb9C7taqi0Rg6', '0', '0', '', '4EDCD7460FFDDC9BCB4EE80BCC45B8F80403275F525D4E1B65F2AA239A982999', 1, '', 2, 3, '1346', 56, 'Thursday, Sep 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120406529433', '77.0432423119157', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '01:59', 'Thursday, Sep 14, 2017', 0, 2, 0, 2, 0, 2, 2, 0, '2017-09-14', 1, '', '2017-09-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 2, 3),
(2, 0, 4, 'dissed', 'dddddd34@t.com', '6767676767', '', '123456', 'xxSrnrIZKvUAEWDp', '96', '4', '', '4EDCD7460FFDDC9BCB4EE80BCC45B8F80403275F525D4E1B65F2AA239A982999', 1, '4.5', 2, 3, 'DL-10S-5677', 56, 'Thursday, Sep 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121175493162', '77.0428222867075', '1, Sohna Road, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122001', '14:16', 'Thursday, Sep 14, 2017', 1, 0, 0, 2, 0, 2, 2, 0, '2017-09-14', 1, '', '2017-09-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(3, 0, 4, 'Amit', 'asdfghjkl@gmail.com', '9876543211', 'uploads/driver/1505396336driver_3.jpg', '1234567890', 'zdPgJ63cqTMBtbEq', '763.2', '31.8', '', '4EDCD7460FFDDC9BCB4EE80BCC45B8F80403275F525D4E1B65F2AA239A982999', 1, '4.83333333333', 2, 3, '1234', 56, 'Thursday, Sep 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120893477994', '77.0432488220125', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '08:25', 'Friday, Sep 22, 2017', 5, 1, 1, 2, 0, 2, 2, 0, '2017-09-14', 1, '', '2017-09-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(4, 0, 5, 'AShish', 'ashish@apporio.com', '7550646597', '', '12345678', '8giOQgc9G3Vmk8Z0', '285', '15', '', 'B744E8BBD90224DD8EDD7F28423A18E6175CAB9CC33887E315C4A59F8059C2F4', 1, '2', 3, 31, '1233456678', 56, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120812063683', '77.0432708939325', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:45', 'Thursday, Nov 30, 2017', 3, 0, 0, 1, 0, 2, 2, 0, '2017-09-15', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(5, 0, 4, 'vishal', 'vishal@gmail.com', '9034903490', '', '123456', 'VTYqWgApfEgSJr3b', '0', '0', '', '', 0, '', 2, 3, '1212', 56, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-27', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(6, 0, 4, 'vishal', 'vishal99@gmail.com', '9034903434', '', '123456', 'Zdpf65K4N9XK2oMc', '0', '0', '', 'cfPAihyr2_U:APA91bHcfdiujzv5suE8Fzd8hh9qOZvIW-qZZZRaQUWbeLmdJ2X7MUEzG7Cj7gdWWXF5WhJcX9TVNL9MFpHMC4_ofvNqsqrmX0HrIsvosk4PBnng3j7sH-r7Nzu895CrJvMG7flHINAc', 2, '', 2, 3, '4242', 56, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.41219', '77.0431572', '----', '07:01', 'Saturday, Sep 16, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-15', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 2, 3),
(7, 0, 5, 'glykylylymlylu', 'fjtjtj@gkyk.hjk', '2525523535', '', 'eeyeyeyyyr', 'sAJ0o4N9Cv6dhyPn', '0', '0', '', '617CECF36F0BCE8714928EB9E6DF1D475A332BD753C57665FACA338D5836A1ED', 1, '', 3, 20, 'hrdur45566', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '06:23', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-27', 0, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 3, 1, 0),
(8, 0, 5, 'Aman', 'aman@gmail.com', '8845676567', '', '12345678', 'VWbUYHpFaWJtBwqw', '0', '0', '', '617CECF36F0BCE8714928EB9E6DF1D475A332BD753C57665FACA338D5836A1ED', 1, '', 3, 20, 'RT233444', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '06:37', '', 0, 0, 0, 2, 0, 2, 1, 0, '', 0, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(9, 0, 5, 'New User', 'new@gmail.com', '9876789876', '', '123456789', 'y1N6XaepI2MUomJI', '0', '0', '', '617CECF36F0BCE8714928EB9E6DF1D475A332BD753C57665FACA338D5836A1ED', 1, '', 3, 20, 'ED127658', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:32', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-10-03', 0, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'Your Are Reject By Admin', 3, 1, 0),
(10, 0, 4, 'ankit sharma', 'ankit@gmail.com', '9999999999', 'uploads/driver/1505544431driver_10.jpg', '123456', 'VKAFXLYJrQeyAgrh', '384', '16', '', 'cfPAihyr2_U:APA91bGjxnQpzukOEvJ9XCx7Ve1Zh0-do1VluK_DHT9cPxnESBIWfu8TGtwspsEjRNTay1t_qvLGbRFjRzP0gUG3E451JJb5ikjCL-Vfwe6Korxjr6o51K0zvTVZNPiq6pTMmYIT4XEA', 2, '2', 2, 3, '2121', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120894', '77.0434034', '----', '12:46', 'Saturday, Sep 16, 2017', 5, 2, 1, 2, 0, 2, 2, 0, '2017-09-16', 1, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(11, 0, 6, 'samir', 'samir@apporio.com', '9650923089', 'uploads/driver/1505546954driver_11.jpg', '123456', 'XRighjFflfrdiW3D', '836.6', '53.4', '', 'cfPAihyr2_U:APA91bGjxnQpzukOEvJ9XCx7Ve1Zh0-do1VluK_DHT9cPxnESBIWfu8TGtwspsEjRNTay1t_qvLGbRFjRzP0gUG3E451JJb5ikjCL-Vfwe6Korxjr6o51K0zvTVZNPiq6pTMmYIT4XEA', 2, '2.8', 4, 29, 'DL7687', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121856', '77.0431223', '----', '08:59', 'Saturday, Sep 16, 2017', 6, 1, 0, 2, 0, 2, 2, 0, '2017-09-16', 1, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(12, 0, 4, 'praveen', 'praveen@gmail.com', '9696969696', '', '123456', 'HPi2wILZ5XCrLg8Y', '864', '36', '', 'cZdWkKmqb30:APA91bEF1VDrpgIlkmekSjHYWwDSl1nf0cpyQsGmjL-G-_b4rwvZWxMHE_kH67YZo8Z66JRAeT6BF0YGQJ8yyHXNBFPU-nGc_MkMvxeTkD5DSpkIT2pAatSAvxpOcrOzPm3iPwYdY4Nv', 2, '2.125', 2, 3, 'Hr2121', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121856', '77.0431223', '----', '08:21', 'Monday, Sep 18, 2017', 10, 0, 0, 2, 0, 2, 2, 0, '2017-09-16', 1, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(13, 0, 5, 'New ', 'newemail@gmail.com', '7550567878', '', '12345678', 'sBXd4K0LGkDfHlx2', '0', '0', '', '66A1056FCFA8B6D329B637840AF1F15620852DBCA9D5D5D266A5F9B02753CCFA', 1, '', 3, 20, '12345578', 56, 'Monday, Sep 18', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120171444713', '77.0431878077903', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '05:48', 'Tuesday, Sep 19, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-18', 1, '', '2017-09-18', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(14, 0, 5, 'Hana', 'supertaksi.istanbul@gmail.com', '07415743544', '', 'supertaksi', 'dna2KGX2FB0TSAE9', '0', '0', '', 'DA6A39CA7A6D1A35256B8D438C7082B348BEF33B4350AD9A866E85300803E42B', 1, '', 3, 2, 'xxxxxx', 56, 'Tuesday, Sep 19', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '51.4094388904529', '-0.306338947266626', '8-9 Market Place , Kingston upon Thames KT1 1JT, UK', '18:07', 'Sunday, Nov 12, 2017', 0, 0, 0, 1, 0, 2, 2, 0, '2017-09-27', 1, '', '2017-09-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(15, 0, 5, 'new', 'new6@gmail.com', '1234567890', '', '12345678', 'S54NOhrDSCQPFEzc', '0', '0', '', '58A21ED35176DA6C6FA25EBA3A319D325D6FB1C15B2F59EAAAC544C391BF2D34', 1, '', 3, 20, '122344', 56, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '08:47', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-27', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(16, 0, 93, 'Oktay Ozdemir', 'Ceomobil1@gmail.com', '+905304190021', '', 'supertaxi', '', '0', '0', '', '', 0, '', 3, 20, '34 HC 2233', 120, 'Friday, Sep 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-22', 1, '', '2017-09-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0),
(17, 0, 10, 'AShish', 'new3@gmail.com', '7550656587', '', '12345678', '6ZcQD7eDU6VHNnHN', '0', '0', '', '58A21ED35176DA6C6FA25EBA3A319D325D6FB1C15B2F59EAAAC544C391BF2D34', 1, '', 2, 3, 'right', 120, 'Wednesday, Sep 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120642201724', '77.0432737829468', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:12', 'Wednesday, Oct 4, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-27', 1, '', '2017-09-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(18, 0, 10, 'Dummy', 'dummy@gmail.com', '9876564534', '', '123456', '', '0', '0', '', '', 0, '', 2, 3, '7788', 120, 'Thursday, Sep 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 0, 0, '', 1, '', '2017-09-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 3),
(21, 0, 10, 'Hana Won', 'hwon@roundsonme.com', '7792940066', 'uploads/driver/1507221906driver_21.jpg', 'Cocopops1990', '4sCnWFgvd4V27966', '270', '30', '', 'A3F5DC7E335CFBA2164F10E212A6CBEA2540C5E56D3D2E124FB6EFA48C09551D', 1, '2', 2, 3, 'KF15 YNK', 121, 'Wednesday, Oct 4', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '51.4008996806828', '-0.26842720207152', '14 South Lane West , New Malden KT3 5AQ, UK', '10:27', 'Wednesday, Nov 1, 2017', 3, 0, 0, 2, 0, 2, 2, 0, '2017-10-04', 1, '', '2017-10-04', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0),
(19, 0, 21, 'Test ', 'test@demo.com', '23232434', '', 'Yf2LD&smJi45TfTqOk', '', '0', '0', '', '', 0, '', 3, 2, 'fsj577', 120, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(20, 0, 10, 'Huseyin Sanli', 'hsanli@roundsonme.com', '7970985987', '', 'Hcs14055', 'NqrBe25HxbI6JMJQ', '1539.45', '171.05', '', 'FF66397AB002AE0D43698DE8F88252D6567EB3224D873641B0E4AAF31E37349B', 1, '2.75', 2, 3, 'Hcs1234567', 121, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.0787012521127', '29.0119349334246', 'Esentepe Mahallesi , Ecza SokaÄŸÄ± No:1, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '19:44', 'Tuesday, Nov 21, 2017', 17, 0, 2, 1, 0, 1, 2, 0, '2017-10-03', 1, '', '2017-10-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0),
(24, 0, 4, 'Ashish', 'ashish123@gmail.com', '1234568923', '', '12345678', 'uo9WhakWpumKTzTW', '0', '0', '', 'B35795FEA54564383E9655A8BAEA74E29B170AA77319471A698B1E84852F4B01', 1, '', 2, 3, 'add', 56, 'Friday, Nov 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '01:52', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(22, 0, 5, 'new5', 'new5@gmail.com', '7656787657', '', '12345678', 'hbvFqeTtRzcpQJE5', '475', '25', '', 'B9F2A9E88D39527C407E62759139CA28D811DBD702C4DD561C906A98AE96774C', 1, '3', 3, 20, 'wef123', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121233326435', '77.043291831534', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '12:47', 'Friday, Oct 27, 2017', 5, 0, 0, 1, 0, 1, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(23, 0, 10, 'Ozan Berk', 'ozanberkplt@gmail.com', '05537291209', '', 'ozan2491971', 'rTnbI9NnV1hMF6ql', '0', '0', '', '', 0, '', 3, 20, '1111111', 120, 'Wednesday, Oct 25', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-10-25', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(25, 0, 4, 'Ashish', 'ashish1234@gmail.com', '52456859', '', '12345678', 'sKk8VyegKBeAKUcf', '96', '4', '', 'B35795FEA54564383E9655A8BAEA74E29B170AA77319471A698B1E84852F4B01', 1, '2.5', 2, 3, 'add', 56, 'Friday, Nov 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120726079597', '77.0432356389712', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '11:11', 'Friday, Nov 10, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '2017-11-03', 1, '', '2017-11-03', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(26, 0, 4, 'tyy', 'tyu@hjj.ggh', '343345657', '', 'ryuu', 'MkiVn8aplehTaLUH', '0', '0', '', '023E291E9915E41724C91DE84172C9EA4955B2E29A4BEE86F420E50B2A5D37CB', 1, '', 2, 3, 'ryu', 56, 'Thursday, Nov 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '05:40', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(27, 0, 4, 'try', 'zxc@gmail.com', '2323345678', '', 'qwerty', 'jeij4dYgSVftdpwy', '0', '0', '', 'B744E8BBD90224DD8EDD7F28423A18E6175CAB9CC33887E315C4A59F8059C2F4', 1, '', 2, 3, 'err', 56, 'Tuesday, Nov 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '05:53', '', 0, 0, 0, 2, 0, 0, 3, 0, '', 0, '', '2017-11-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(28, 0, 4, '7ssuuss', 'shshzg@g.com', '979779778896', '', 'dhxhhxh', 'vVIMn3vR3vWRPLRd', '0', '0', '', '', 0, '', 2, 3, 'zvvzz', 56, 'Tuesday, Nov 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(29, 0, 7, 'aaaa', 'aaaaa@gmail.com', '05325421236', '', '12345678', 'S4Q1XJWMKaimZFqo', '0', '0', '', '', 0, '', 13, 41, '34tre21', 120, 'Thursday, Nov 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(30, 0, 7, 'Ozan Berk Polat', 'ibrahimplt1955@gmail.com', '05303257901', '', '2491971', 'a6dXj20qBSCYPvT0', '0', '0', '', '', 0, '', 13, 41, '34TCY76', 120, 'Thursday, Nov 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 2, '100', 1, '96.00', '4.00', '2017-09-14'),
(2, 3, '595', 3, '571.2', '23.8', '2017-09-14'),
(3, 10, '400', 5, '384', '16', '2017-09-16'),
(4, 11, '890', 6, '836.6', '53.4', '2017-09-16'),
(5, 12, '200', 2, '192', '8', '2017-09-16'),
(6, 12, '800', 8, '768', '32', '2017-09-18'),
(7, 3, '200', 2, '192', '8', '2017-09-18'),
(8, 21, '100', 1, '90.00', '10.00', '2017-10-04'),
(9, 4, '300', 3, '285', '15', '2017-10-04'),
(10, 20, '100', 1, '90.00', '10.00', '2017-10-05'),
(11, 21, '200', 2, '180', '20', '2017-10-05'),
(12, 20, '100', 1, '90.00', '10.00', '2017-10-11'),
(13, 20, '200', 2, '180', '20', '2017-10-18'),
(14, 20, '400', 4, '360', '40', '2017-10-19'),
(15, 20, '100', 1, '90.00', '10.00', '2017-10-20'),
(16, 20, '100', 1, '90.00', '10.00', '2017-10-21'),
(17, 22, '400', 4, '380', '20', '2017-10-24'),
(18, 22, '100', 1, '95.00', '5.00', '2017-10-27'),
(19, 20, '110.5', 1, '99.45', '11.05', '2017-10-31'),
(20, 25, '100', 1, '96.00', '4.00', '2017-11-03'),
(21, 20, '100', 1, '90.00', '10.00', '2017-11-14'),
(22, 20, '300', 3, '270', '30', '2017-11-20'),
(23, 20, '200', 2, '180', '20', '2017-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`, `ride_status`) VALUES
(1, 1, 10, 1, 1),
(2, 2, 11, 1, 1),
(3, 3, 88, 1, 0),
(4, 6, 23, 1, 0),
(5, 10, 65, 1, 1),
(6, 11, 61, 1, 1),
(7, 12, 88, 1, 0),
(8, 13, 87, 1, 0),
(9, 20, 147, 1, 1),
(10, 21, 120, 1, 1),
(11, 4, 103, 1, 1),
(12, 22, 136, 1, 1),
(13, 25, 139, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 3, 1, 'Monday', '11:35', '11:35', '12:35', '10:35', 1, '23'),
(18, 56, 1, 'Monday', '17:19', '18:20', '17:20', '18:20', 1, '55'),
(22, 56, 2, 'Night', '17:00', '23.45', '', '', 1, '99'),
(32, 112, 1, 'Sunday', '07:30', '09:30', '14:30', '18:0', 2, '1.2'),
(33, 112, 2, 'Night', '23:30', '06:30', '', '', 2, '1.5'),
(34, 3, 1, 'Wednesday', '14:28', '23:45', '14:29', '23:59', 1, '10'),
(37, 56, 1, 'Sunday', '08:30', '09:30', '10:30', '11:30', 2, '23'),
(38, 56, 1, 'Saturday', '12:31', '12:31', '12:31', '12:31', 2, '20'),
(39, 79, 1, 'Monday', '16:19', '20:19', '22:19', '01:19', 2, '10'),
(40, 56, 1, 'Thursday', '22:30', '23:45', '23:45', '23:45', 1, '100'),
(41, 56, 1, 'Tuesday', '09:13 PM', '10:14 PM', '10:14 PM', '11:14 PM', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:14:43', '02:14:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(2, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:14:56', '02:14:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(3, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:15:15', '02:15:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(4, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:15:29', '02:15:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(5, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:15:42', '02:15:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(6, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:15:51', '02:15:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(7, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:54:33', '08:54:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(8, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:54:39', '08:54:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(9, 9, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '', '', 'Set your drop point', 'Saturday, Sep 16', '10:59:38', '10:59:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(10, 4, '', '28.412091345946', '77.0432094824296', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Monday, Sep 18', '12:14:01', '12:14:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412091345946,77.0432094824296&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 4, 0, 1),
(11, 4, '', '28.41412671618', '77.0882139354944', 'Sector 62\nGurugram, Haryana 122102', '28.4060471257722', '77.0899204909801', 'Sector 62\nGurugram, Haryana 122102', 'Monday, Sep 18', '14:39:04', '02:39:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41412671618,77.0882139354944&markers=color:red|label:D|28.4060471257722,77.0899204909801&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(12, 4, '', '28.41412671618', '77.0882139354944', 'Sector 62\nGurugram, Haryana 122102', '28.4059527550631', '77.097452133894', 'Unnamed Road, Ullahawas, Baharampur Naya, Sector 61\nGurugram, Haryana 122005', 'Monday, Sep 18', '14:47:36', '02:47:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41412671618,77.0882139354944&markers=color:red|label:D|28.4059527550631,77.097452133894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(13, 15, '', '51.3525416507002', '-0.444656091352879', 'Linden Road\nWeybridge KT13 0QW, UK', '51.369487', '-0.365927', 'Esher', '', '12:14:35', '12:14:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|51.3525416507002,-0.444656091352879&markers=color:red|label:D|51.369487,-0.365927&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(14, 13, '', '28.40440506344', '77.0547627657652', '3/1, Emilia 1, Golf Course Extension Road, Vatika City, Block W, Sector 49\nGurugram, Haryana 122018', '28.3881906911472', '77.0804475992918', 'Unnamed Road, Sector 64\nGurugram, Haryana 122005', '', '12:58:04', '12:58:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.40440506344,77.0547627657652&markers=color:red|label:D|28.3881906911472,77.0804475992918&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(15, 14, '', '41.0647886795557', '28.999680429697', 'Fulya Mahallesi\nAli Sami Yen Sokak No:19, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '41.044', '29.002', 'BeÅŸiktaÅŸ', '', '08:15:49', '08:15:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.0647886795557,28.999680429697&markers=color:red|label:D|41.044,29.002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 4, 0, 1),
(16, 12, '', '41.0654538651145', '29.0000589986588', 'Fulya Mahallesi\nAli Sami Yen Sokak No:19, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '41.0967624', '28.9892494', 'Seyrantepe Mahallesi', '', '08:42:22', '08:42:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.0654538651145,29.0000589986588&markers=color:red|label:D|41.0967624,28.9892494&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 17, '', '28.4120840876556', '77.0432270682001', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4067050642235', '77.0516812428832', 'D Block\nGurugram, Haryana', '', '12:40:13', '12:40:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120840876556,77.0432270682001&markers=color:red|label:D|28.4067050642235,77.0516812428832&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(18, 17, '', '28.4120840876556', '77.0432270682001', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4067050642235', '77.0516812428832', 'D Block\nGurugram, Haryana', 'Friday, Oct 27', '12:40:19', '12:40:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120840876556,77.0432270682001&markers=color:red|label:D|28.4067050642235,77.0516812428832&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(19, 17, '', '28.4120840876556', '77.0432270682001', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4067050642235', '77.0516812428832', 'D Block\nGurugram, Haryana', 'Friday, Oct 27', '12:40:22', '12:40:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120840876556,77.0432270682001&markers=color:red|label:D|28.4067050642235,77.0516812428832&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(20, 18, '', '41.04167683471742', '28.989780060946938', 'Harbiye Mahallesi, Ä°TÃœ TaÅŸkÄ±ÅŸla KampÃ¼sÃ¼ MimarlÄ±k FakÃ¼ltesi, 34367 ÅžiÅŸli/Ä°stanbul, Turkey', '', '', 'Ä°niÅŸ NoktanÄ±zÄ± Belirleyin', '', '09:51:39', '09:51:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.04167683471742,28.989780060946938&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(21, 18, '', '41.04628837346394', '28.988996520638466', 'Harbiye Mahallesi, GÃ¼mÃ¼ÅŸ Sk. No:4, 34367 ÅžiÅŸli/Ä°stanbul, Turkey', '', '', 'Ä°niÅŸ NoktanÄ±zÄ± Belirleyin', '', '09:51:57', '09:51:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.04628837346394,28.988996520638466&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(22, 21, '', '51.3228122248912', '-0.603040439355762', '4 Kirkland Avenue\nWoking GU21 3QR, UK', '51.412537', '-0.2896408', 'KT2 6NU', '', '11:59:13', '11:59:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|51.3228122248912,-0.603040439355762&markers=color:red|label:D|51.412537,-0.2896408&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 2, 0, 1),
(23, 21, '', '51.3228122248912', '-0.603040439355762', '4 Kirkland Avenue\nWoking GU21 3QR, UK', '51.412537', '-0.2896408', 'KT2 6NU', '', '11:59:15', '11:59:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|51.3228122248912,-0.603040439355762&markers=color:red|label:D|51.412537,-0.2896408&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 2, 0, 1),
(24, 18, '', '41.07918940128279', '29.019446969032288', 'Levent Mahallesi, Levent Cd. No:48, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '', '', 'Ä°niÅŸ NoktanÄ±zÄ± Belirleyin', '', '11:14:22', '11:14:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.07918940128279,29.019446969032288&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 2, '1', 'Cash', 'Admin', '100', 'Thursday, Sep 14', '1'),
(2, 2, 3, '1', 'Cash', 'Admin', '110', 'Thursday, Sep 14', '1'),
(3, 4, 3, '1', 'Cash', 'Admin', '160', 'Thursday, Sep 14', '1'),
(4, 5, 3, '1', 'Cash', 'Admin', '325', 'Thursday, Sep 14', '1'),
(5, 7, 8, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(6, 8, 8, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(7, 9, 9, '1', 'Cash', 'Admin', '178', 'Saturday, Sep 16', '1'),
(8, 10, 9, '1', 'Cash', 'Admin', '178', 'Saturday, Sep 16', '1'),
(9, 11, 9, '1', 'Cash', 'Admin', '178', 'Saturday, Sep 16', '1'),
(10, 11, 9, '1', 'Cash', 'Admin', '178', 'Saturday, Sep 16', '1'),
(11, 13, 9, '1', 'Cash', 'Admin', '178', 'Saturday, Sep 16', '1'),
(12, 14, 9, '1', 'Cash', 'Admin', '0.00', 'Saturday, Sep 16', '1'),
(13, 15, 9, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(14, 16, 9, '1', 'Cash', 'Admin', '0.00', 'Saturday, Sep 16', '1'),
(15, 17, 9, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(16, 18, 9, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(17, 19, 9, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(18, 20, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(19, 20, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(20, 21, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(21, 22, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(22, 23, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(23, 24, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(24, 25, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(25, 26, 10, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(26, 27, 4, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(27, 28, 4, '1', 'Cash', 'Admin', '100', 'Monday, Sep 18', '1'),
(28, 30, 12, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '1'),
(29, 31, 13, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '1'),
(30, 32, 13, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '1'),
(31, 33, 4, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 4', '1'),
(32, 34, 12, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 5', '1'),
(33, 35, 15, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 5', '1'),
(34, 36, 15, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 5', '1'),
(35, 37, 15, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 11', '1'),
(36, 39, 14, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 18', '1'),
(37, 40, 12, '1', 'Cash', 'Admin', '100', 'Wednesday, Oct 18', '1'),
(38, 41, 12, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 19', '1'),
(39, 42, 12, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 19', '1'),
(40, 43, 12, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 19', '1'),
(41, 43, 12, '1', 'Cash', 'Admin', '100', 'Thursday, Oct 19', '1'),
(42, 44, 12, '1', 'Cash', 'Admin', '100', 'Friday, Oct 20', '1'),
(43, 45, 12, '1', 'Cash', 'Admin', '100', 'Saturday, Oct 21', '1'),
(44, 46, 16, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 24', '1'),
(45, 47, 16, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 24', '1'),
(46, 48, 16, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 24', '1'),
(47, 49, 17, '1', 'Cash', 'Admin', '100', 'Tuesday, Oct 24', '1'),
(48, 50, 17, '1', 'Cash', 'Admin', '100', 'Friday, Oct 27', '1'),
(49, 51, 14, '1', 'Cash', 'Admin', '110.5', 'Tuesday, Oct 31', '1'),
(50, 52, 20, '1', 'Cash', 'Admin', '100', 'Friday, Nov 3', '1'),
(51, 53, 21, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 14', '1'),
(52, 54, 12, '1', 'Cash', 'Admin', '100', 'Monday, Nov 20', '1'),
(53, 55, 12, '1', 'Cash', 'Admin', '100', 'Monday, Nov 20', '1'),
(54, 56, 12, '1', 'Cash', 'Admin', '100', 'Monday, Nov 20', '1'),
(55, 57, 12, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 21', '1'),
(56, 58, 12, '1', 'Cash', 'Admin', '100', 'Tuesday, Nov 21', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(12, 56, 'Miles', '&#8377', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '&#8377', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '&#8377', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(43, 120, 'Km', '&#;', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(44, 120, 'Km', '&#;', 3, 10, 0, 0, 0, 0, 0, 0, '2', '70', '10', '0', '0', '0', '0'),
(45, 120, 'Km', '&#;', 4, 10, 0, 0, 0, 0, 0, 0, '5', '60', '10', '0', '0', '0', '0'),
(46, 121, 'Km', 'Â£', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(47, 120, 'Km', '&#;', 7, 5, 0, 0, 0, 0, 0, 0, '5', '60', '10', '0', '0', '0', '0'),
(48, 120, 'Km', '&#;', 10, 15, 0, 0, 0, 0, 0, 0, '10', '60', '10', '0', '0', '0', '0'),
(49, 120, 'Km', '&#;', 11, 20, 0, 0, 0, 0, 0, 0, '15', '60', '15', '1', '1', '1', '1'),
(50, 120, 'Km', '&#;', 12, 4, 0, 0, 0, 0, 0, 0, '2', '10', '2', '5', '2', '0', '0'),
(51, 120, 'Km', '&#;', 13, 7, 0, 0, 0, 0, 0, 0, '2', '12', '2', '2', '2', '0', '0'),
(52, 120, 'Km', '&#;', 14, 10, 0, 0, 0, 0, 0, 0, '10', '20', '4', '0', '0', '0', '0'),
(53, 120, 'Km', '&#;', 15, 15, 0, 0, 0, 0, 0, 0, '20', '40', '4', '0', '0', '0', '0'),
(54, 56, 'Miles', '&#8377', 12, 10, 0, 0, 0, 0, 0, 0, '10', '10', '10', '10', '10', '0', '10');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'kjnx z', ',xz nlkx', 'uploads/notification/push_image_1.jpg', 'knlkXZ', 0, 0, '2017-09-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 1, '2017-09-14'),
(2, 2, 1, 1, '2017-09-14'),
(3, 3, 1, 1, '2017-09-14'),
(4, 4, 1, 1, '2017-09-14'),
(5, 5, 1, 1, '2017-09-14'),
(6, 6, 1, 0, '2017-09-14'),
(7, 7, 1, 1, '2017-09-14'),
(8, 8, 1, 1, '2017-09-14'),
(9, 9, 1, 0, '2017-09-14'),
(10, 10, 1, 1, '2017-09-14'),
(11, 11, 2, 1, '2017-09-14'),
(12, 12, 3, 0, '2017-09-14'),
(13, 13, 3, 0, '2017-09-14'),
(14, 14, 3, 1, '2017-09-14'),
(15, 15, 3, 0, '2017-09-14'),
(16, 16, 3, 1, '2017-09-14'),
(17, 18, 3, 1, '2017-09-14'),
(18, 19, 3, 1, '2017-09-14'),
(19, 20, 3, 1, '2017-09-15'),
(20, 21, 3, 0, '2017-09-15'),
(21, 22, 3, 0, '2017-09-16'),
(22, 22, 6, 0, '2017-09-16'),
(23, 23, 3, 0, '2017-09-16'),
(24, 23, 6, 0, '2017-09-16'),
(25, 24, 3, 0, '2017-09-16'),
(26, 25, 3, 0, '2017-09-16'),
(27, 26, 3, 0, '2017-09-16'),
(28, 27, 3, 0, '2017-09-16'),
(29, 28, 3, 0, '2017-09-16'),
(30, 29, 3, 0, '2017-09-16'),
(31, 30, 3, 0, '2017-09-16'),
(32, 31, 3, 0, '2017-09-16'),
(33, 32, 3, 0, '2017-09-16'),
(34, 33, 3, 0, '2017-09-16'),
(35, 34, 3, 0, '2017-09-16'),
(36, 35, 3, 0, '2017-09-16'),
(37, 36, 3, 0, '2017-09-16'),
(38, 37, 3, 0, '2017-09-16'),
(39, 38, 3, 0, '2017-09-16'),
(40, 38, 10, 1, '2017-09-16'),
(41, 39, 3, 0, '2017-09-16'),
(42, 39, 10, 1, '2017-09-16'),
(43, 40, 3, 0, '2017-09-16'),
(44, 40, 10, 0, '2017-09-16'),
(45, 40, 3, 0, '0000-00-00'),
(46, 41, 3, 0, '2017-09-16'),
(47, 41, 10, 1, '2017-09-16'),
(48, 42, 3, 0, '2017-09-16'),
(49, 43, 3, 0, '2017-09-16'),
(50, 43, 10, 0, '2017-09-16'),
(51, 44, 3, 0, '2017-09-16'),
(52, 44, 10, 0, '2017-09-16'),
(53, 45, 3, 0, '2017-09-16'),
(54, 45, 10, 0, '2017-09-16'),
(55, 46, 3, 0, '2017-09-16'),
(56, 46, 10, 0, '2017-09-16'),
(57, 47, 3, 0, '2017-09-16'),
(58, 47, 10, 0, '2017-09-16'),
(59, 48, 3, 0, '2017-09-16'),
(60, 48, 10, 0, '2017-09-16'),
(61, 49, 3, 0, '2017-09-16'),
(62, 49, 10, 0, '2017-09-16'),
(63, 50, 3, 0, '2017-09-16'),
(64, 51, 3, 0, '2017-09-16'),
(65, 52, 3, 0, '2017-09-16'),
(66, 53, 3, 0, '2017-09-16'),
(67, 54, 3, 0, '2017-09-16'),
(68, 55, 11, 1, '2017-09-16'),
(69, 56, 11, 1, '2017-09-16'),
(70, 57, 11, 1, '2017-09-16'),
(71, 58, 11, 1, '2017-09-16'),
(72, 59, 11, 0, '2017-09-16'),
(73, 60, 11, 1, '2017-09-16'),
(74, 61, 11, 1, '2017-09-16'),
(75, 62, 3, 0, '2017-09-16'),
(76, 62, 10, 1, '2017-09-16'),
(77, 63, 3, 0, '2017-09-16'),
(78, 63, 10, 1, '2017-09-16'),
(79, 64, 3, 0, '2017-09-16'),
(80, 64, 10, 0, '2017-09-16'),
(81, 64, 3, 0, '0000-00-00'),
(82, 65, 3, 0, '2017-09-16'),
(83, 65, 10, 1, '2017-09-16'),
(84, 66, 3, 0, '2017-09-16'),
(85, 66, 12, 1, '2017-09-16'),
(86, 67, 3, 0, '2017-09-16'),
(87, 67, 12, 1, '2017-09-16'),
(88, 68, 3, 0, '2017-09-16'),
(89, 69, 3, 0, '2017-09-16'),
(90, 70, 3, 0, '2017-09-16'),
(91, 71, 3, 0, '2017-09-16'),
(92, 72, 3, 0, '2017-09-16'),
(93, 73, 3, 0, '2017-09-16'),
(94, 74, 3, 0, '2017-09-16'),
(95, 75, 3, 0, '2017-09-18'),
(96, 75, 12, 1, '2017-09-18'),
(97, 76, 3, 0, '2017-09-18'),
(98, 76, 12, 1, '2017-09-18'),
(99, 77, 3, 0, '2017-09-18'),
(100, 77, 12, 1, '2017-09-18'),
(101, 78, 3, 0, '2017-09-18'),
(102, 78, 12, 1, '2017-09-18'),
(103, 79, 3, 0, '2017-09-18'),
(104, 79, 12, 1, '2017-09-18'),
(105, 80, 3, 0, '2017-09-18'),
(106, 80, 12, 1, '2017-09-18'),
(107, 81, 3, 0, '2017-09-18'),
(108, 81, 12, 1, '2017-09-18'),
(109, 83, 3, 1, '2017-09-18'),
(110, 83, 12, 0, '2017-09-18'),
(111, 84, 3, 1, '2017-09-18'),
(112, 84, 12, 0, '2017-09-18'),
(113, 85, 13, 0, '2017-09-18'),
(114, 86, 13, 0, '2017-09-18'),
(115, 87, 13, 0, '2017-09-18'),
(116, 88, 3, 0, '2017-09-18'),
(117, 88, 12, 0, '2017-09-18'),
(118, 91, 20, 1, '0000-00-00'),
(119, 92, 20, 0, '2017-10-04'),
(120, 93, 20, 0, '2017-10-04'),
(121, 94, 21, 1, '2017-10-04'),
(122, 94, 20, 0, '2017-10-04'),
(123, 98, 4, 1, '2017-10-04'),
(124, 99, 4, 1, '2017-10-04'),
(125, 100, 4, 1, '2017-10-04'),
(126, 101, 4, 1, '2017-10-04'),
(127, 102, 4, 1, '2017-10-04'),
(128, 103, 4, 1, '2017-10-04'),
(129, 104, 21, 0, '2017-10-05'),
(130, 104, 20, 1, '2017-10-05'),
(131, 105, 20, 0, '2017-10-05'),
(132, 106, 20, 0, '2017-10-05'),
(133, 107, 20, 0, '2017-10-05'),
(134, 108, 20, 0, '2017-10-05'),
(135, 109, 20, 0, '2017-10-05'),
(136, 110, 21, 1, '2017-10-05'),
(137, 110, 20, 0, '2017-10-05'),
(138, 111, 21, 0, '2017-10-05'),
(139, 111, 20, 0, '2017-10-05'),
(140, 112, 21, 0, '2017-10-05'),
(141, 112, 20, 0, '2017-10-05'),
(142, 113, 21, 1, '2017-10-05'),
(143, 113, 20, 0, '2017-10-05'),
(144, 114, 20, 0, '2017-10-05'),
(145, 115, 20, 0, '2017-10-09'),
(146, 117, 20, 1, '2017-10-11'),
(147, 118, 20, 1, '2017-10-11'),
(148, 119, 21, 0, '2017-10-12'),
(149, 120, 21, 0, '2017-10-12'),
(150, 120, 20, 1, '2017-10-12'),
(151, 121, 20, 1, '2017-10-18'),
(152, 122, 20, 1, '2017-10-18'),
(153, 123, 20, 1, '2017-10-19'),
(154, 124, 20, 1, '2017-10-19'),
(155, 125, 20, 1, '2017-10-20'),
(156, 126, 20, 1, '2017-10-21'),
(157, 127, 22, 1, '2017-10-24'),
(158, 128, 22, 1, '2017-10-24'),
(159, 129, 22, 1, '2017-10-24'),
(160, 130, 22, 0, '2017-10-24'),
(161, 131, 22, 0, '2017-10-24'),
(162, 132, 22, 0, '2017-10-24'),
(163, 133, 22, 0, '2017-10-24'),
(164, 134, 22, 0, '2017-10-24'),
(165, 135, 22, 1, '2017-10-24'),
(166, 136, 22, 1, '2017-10-27'),
(167, 137, 20, 1, '2017-10-31'),
(168, 138, 25, 0, '2017-11-03'),
(169, 139, 25, 1, '2017-11-03'),
(170, 140, 20, 1, '2017-11-14'),
(171, 141, 20, 1, '2017-11-20'),
(172, 142, 20, 1, '2017-11-20'),
(173, 143, 20, 1, '2017-11-20'),
(174, 144, 20, 1, '2017-11-21'),
(175, 145, 20, 1, '2017-11-21'),
(176, 146, 20, 0, '2017-11-22'),
(177, 147, 20, 0, '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 6, 1),
(2, 9, 1),
(3, 15, 3),
(4, 40, 10),
(5, 59, 11),
(6, 64, 10);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:32:01', '01:32:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(2, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:33:36', '01:34:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(3, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:35:20', '01:35:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(4, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:36:30', '01:37:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(5, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:37:56', '01:38:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(6, 2, '', '28.4121365129453', '77.0432312786579', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4435230474289', '77.1046280488372', 'Sector 54\nGurugram, Haryana', 'Thursday, Sep 14', '13:40:47', '01:40:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121365129453,77.0432312786579&markers=color:red|label:D|28.4435230474289,77.1046280488372&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(7, 2, '', '28.4121409363159', '77.0432399958372', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317243355028', '77.0592863112688', '1683, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '13:41:34', '01:41:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121409363159,77.0432399958372&markers=color:red|label:D|28.4317243355028,77.0592863112688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(8, 2, '', '28.4121409363159', '77.0432399958372', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317243355028', '77.0592863112688', '1683, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '13:47:03', '01:47:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121409363159,77.0432399958372&markers=color:red|label:D|28.4317243355028,77.0592863112688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(9, 2, '', '28.4121409363159', '77.0432399958372', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317243355028', '77.0592863112688', '1683, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '13:51:36', '01:51:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121409363159,77.0432399958372&markers=color:red|label:D|28.4317243355028,77.0592863112688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(10, 2, '', '28.4121409363159', '77.0432399958372', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317243355028', '77.0592863112688', '1683, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '13:54:47', '01:54:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121409363159,77.0432399958372&markers=color:red|label:D|28.4317243355028,77.0592863112688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(11, 2, '', '28.412033581865', '77.0432535699086', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4317243355028', '77.0592863112688', '1683, Huda Colony, Sector 46\nGurugram, Haryana 122022', 'Thursday, Sep 14', '14:03:02', '02:03:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412033581865,77.0432535699086&markers=color:red|label:D|28.4317243355028,77.0592863112688&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(12, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:16:08', '02:16:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(13, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:16:31', '02:16:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(14, 3, '', '28.4120755117849', '77.0431642590628', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4225306291328', '77.0594720542431', 'C-115, Pocket I, Nirvana Country, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:17:03', '02:20:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120755117849,77.0431642590628&markers=color:red|label:D|28.4225306291328,77.0594720542431&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(15, 3, '', '28.4462732377069', '77.0668236538768', '1012, Block C, Uday Nagar, Sector 45\nGurugram, Haryana 122022', '28.3984497197797', '76.9873015210032', 'Sector 76\nGurugram, Haryana 122004', 'Thursday, Sep 14', '14:22:47', '02:22:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4462732377069,77.0668236538768&markers=color:red|label:D|28.3984497197797,76.9873015210032&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(16, 3, '', '28.4122550592143', '77.0431793108582', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.441002148829', '77.0924394205213', 'Khatu Shyam Road, Parsvnath Exotica, Sector 53\nGurugram, Haryana 122003', 'Thursday, Sep 14', '14:24:52', '02:26:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122550592143,77.0431793108582&markers=color:red|label:D|28.441002148829,77.0924394205213&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-14'),
(17, 3, '', '28.4119698991836', '77.042869515717', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.4281355208051', '77.0578872039914', 'Satpaul Mittal Marg, Huda Colony, Sector 51\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:29:22', '02:29:22 PM', '', '2017-10-22', '03:00:09', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-14'),
(18, 3, '', '28.4119698991836', '77.042869515717', '1, Sohna Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122001', '28.5245787', '77.206615', 'Saket', 'Thursday, Sep 14', '14:31:35', '02:38:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4119698991836,77.042869515717&markers=color:red|label:D|28.4025357,77.0466729&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(19, 3, '', '28.4122715731119', '77.0432966575027', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4239477285181', '77.0582811534405', 'B - 77, Nirvana Central Road, Pocket C, Mayfield Garden, Sector 50\nGurugram, Haryana 122018', 'Thursday, Sep 14', '14:42:12', '02:59:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122715731119,77.0432966575027&markers=color:red|label:D|28.4239477285181,77.0582811534405&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-14'),
(20, 4, '', '28.4120411903502', '77.0432332569051', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Friday, Sep 15', '10:42:56', '10:43:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120411903502,77.0432332569051&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(21, 4, '', '28.4120551228927', '77.0432148501277', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Friday, Sep 15', '10:50:46', '10:50:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120551228927,77.0432148501277&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(22, 7, '', '28.41109141273272', '77.04340629279613', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Saturday, Sep 16', '05:54:22', '05:54:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41109141273272,77.04340629279613&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(23, 7, '', '28.41109141273272', '77.04340629279613', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '05:55:41', '05:55:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41109141273272,77.04340629279613&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(24, 7, '', '28.41109141273272', '77.04340629279613', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:00:22', '06:00:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41109141273272,77.04340629279613&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(25, 7, '', '28.41109141273272', '77.04340629279613', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:01:48', '06:01:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41109141273272,77.04340629279613&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(26, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:04:05', '06:04:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(27, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:04:24', '06:04:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(28, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:05:18', '06:05:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(29, 7, '', '28.412177797730642', '77.04314444214107', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:13:40', '06:13:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412177797730642,77.04314444214107&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(30, 7, '', '28.412177797730642', '77.04314444214107', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:14:42', '06:14:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412177797730642,77.04314444214107&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(31, 7, '', '28.412177797730642', '77.04314444214107', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:18:56', '06:18:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412177797730642,77.04314444214107&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(32, 7, '', '28.412177797730642', '77.04314444214107', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:20:49', '06:20:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412177797730642,77.04314444214107&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(33, 7, '', '28.41218546490328', '77.04312264919281', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:36:25', '06:36:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312264919281&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(34, 7, '', '28.41218546490328', '77.04312264919281', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:36:57', '06:36:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312264919281&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(35, 7, '', '28.410954286798447', '77.04317394644022', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:39:31', '06:39:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410954286798447,77.04317394644022&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(36, 7, '', '28.410954286798447', '77.04317394644022', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '06:44:22', '06:44:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410954286798447,77.04317394644022&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(37, 7, '', '28.410954286798447', '77.04317394644022', 'Unitech Anthea Floors, Gurgaon, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41218015686074', '77.04314846545458', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', 'Saturday, Sep 16', '06:56:26', '06:56:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.410954286798447,77.04317394644022&markers=color:red|label:D|28.41218015686074,77.04314846545458&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(38, 8, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '07:05:14', '07:06:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(39, 8, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.43548389999999', '77.037736', 'Islampur Village', 'Saturday, Sep 16', '07:08:12', '07:09:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.43548389999999,77.037736&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(40, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '07:41:05', '07:41:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(41, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '07:42:15', '07:43:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-09-16'),
(42, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '07:47:26', '07:47:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(43, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '07:47:45', '07:47:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(44, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '07:48:25', '07:48:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(45, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '07:49:51', '07:49:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(46, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '07:50:41', '07:50:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(47, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '07:54:22', '07:54:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(48, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '07:56:07', '07:56:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(49, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '07:59:22', '07:59:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(50, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:07:44', '08:07:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(51, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:08:50', '08:08:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(52, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:09:23', '08:09:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(53, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:11:10', '08:11:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(54, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:14:28', '08:14:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(55, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.41298432227401', '77.04317428171633', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:25:43', '08:26:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.41298432227401,77.04317428171633&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(56, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:29:39', '08:31:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(57, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:41:42', '08:41:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(58, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:42:21', '08:42:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(59, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:54:58', '08:54:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(60, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.410929515642998', '77.0449535921216', '90, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:56:45', '08:57:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.410929515642998,77.0449535921216&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(61, 9, 'MAN', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.40782068964974', '77.0386641472578', '5, Sohna Rd, Tatvam Villas, Dhani, Sector 48, Gurugram, Haryana 122018, India', 'Saturday, Sep 16', '08:58:40', '08:58:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.40782068964974,77.0386641472578&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(62, 9, '', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '09:00:39', '09:00:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(63, 9, 'MAN', '28.41218133642578', '77.04313438385726', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4089123', '77.3177894', 'Faridabad', 'Saturday, Sep 16', '09:01:33', '09:01:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218133642578,77.04313438385726&markers=color:red|label:D|28.4089123,77.3177894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(64, 4, '', '28.4121922474017', '77.0432292670012', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '0.0', '0.0', 'No drop off point', 'Saturday, Sep 16', '10:33:20', '10:33:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121922474017,77.0432292670012&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(65, 9, '', '28.412086676290038', '77.04341098666191', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '10:59:55', '11:00:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412086676290038,77.04341098666191&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(66, 9, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '12:55:06', '12:56:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(67, 9, '', '28.416468083867503', '77.04350721091032', 'B-505, Sispal Vihar Internal Rd, Block S, Sispal Vihar, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '12:57:13', '12:58:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.416468083867503,77.04350721091032&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(68, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:02:29', '01:02:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(69, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:02:34', '01:02:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(70, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:02:41', '01:02:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(71, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:02:52', '01:02:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(72, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:03:01', '01:03:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(73, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:03:05', '01:03:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(74, 9, '', '28.455784117262986', '77.07385875284673', '201, Sector 43 Service Rd, PWO Appartments, Sector 43, Gurugram, Haryana 122003, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:03:15', '01:03:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.455784117262986,77.07385875284673&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(75, 10, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '', '', 'senindamlanoktasiniayarlayin', 'Monday, Sep 18', '06:50:10', '06:51:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(76, 10, '', '28.412089330313684', '77.043403275311', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '07:04:33', '07:04:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412089330313684,77.043403275311&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(77, 10, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '07:11:21', '07:11:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(78, 10, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '07:17:47', '07:18:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(79, 10, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '07:38:42', '07:38:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(80, 10, '', '28.412220262064515', '77.04196192324162', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '', '', 'senindamlanoktasiniayarlayin', 'Monday, Sep 18', '07:42:10', '07:43:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412220262064515,77.04196192324162&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(81, 10, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Sep 18', '07:43:35', '07:43:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(82, 4, '', '28.4121356282711', '77.0432272553444', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4161555113266', '77.0569601655006', '96, Block G, South City II, Sector 50\nGurugram, Haryana 122022', 'Monday, Sep 18', '08:29:35', '08:29:35 AM', '', '2017-11-10', '12:59:18', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(83, 4, '', '28.4120712852704', '77.0432977153434', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.409485701145', '77.0558504015207', 'A33/49, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', 'Monday, Sep 18', '12:35:51', '12:36:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120712852704,77.0432977153434&markers=color:red|label:D|28.409485701145,77.0558504015207&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(84, 4, '', '28.4120704778399', '77.0432716731663', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '19.0759837', '72.8776559', 'Mumbai', 'Monday, Sep 18', '12:41:42', '12:42:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120704778399,77.0432716731663&markers=color:red|label:D|19.0759837,72.8776559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-18'),
(85, 4, '', '28.41412671618', '77.0882139354944', 'Sector 62\nGurugram, Haryana 122102', '28.4059527550631', '77.097452133894', 'Unnamed Road, Ullahawas, Baharampur Naya, Sector 61\nGurugram, Haryana 122005', 'Monday, Sep 18', '14:47:56', '02:47:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41412671618,77.0882139354944&markers=color:red|label:D|28.4059527550631,77.097452133894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(86, 4, '', '28.41412671618', '77.0882139354944', 'Sector 62\nGurugram, Haryana 122102', '28.4059527550631', '77.097452133894', 'Unnamed Road, Ullahawas, Baharampur Naya, Sector 61\nGurugram, Haryana 122005', 'Monday, Sep 18', '14:49:07', '02:49:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41412671618,77.0882139354944&markers=color:red|label:D|28.4059527550631,77.097452133894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(87, 4, '', '28.41412671618', '77.0882139354944', 'Sector 62\nGurugram, Haryana 122102', '28.4059527550631', '77.097452133894', 'Unnamed Road, Ullahawas, Baharampur Naya, Sector 61\nGurugram, Haryana 122005', 'Monday, Sep 18', '14:49:36', '02:49:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41412671618,77.0882139354944&markers=color:red|label:D|28.4059527550631,77.097452133894&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(88, 9, '', '28.42744380789514', '77.03092496842146', 'B4, Sector 33, Gurugram, Haryana 122022, India', '', '', 'Set your drop point', 'Monday, Sep 18', '16:40:29', '04:40:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.42744380789514,77.03092496842146&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-18'),
(89, 11, '', '28.4102188169301', '77.0588259771466', 'Nirvana Country, Sector 50\nGurugram, Haryana', '28.406604795965', '77.0707866176963', 'Sector 65\nGurugram, Haryana 122102', 'Tuesday, Sep 19', '06:21:16', '06:21:16 AM', '', '2017-10-05', '09:51:02', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-19');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(90, 11, '', '28.4102188169301', '77.0588259771466', 'Nirvana Country, Sector 50\nGurugram, Haryana', '28.406604795965', '77.0707866176963', 'Sector 65\nGurugram, Haryana 122102', 'Tuesday, Sep 19', '06:26:01', '06:26:01 AM', '', '2017-09-19', '10:55:41', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-09-19'),
(91, 12, '123456', '51.41233', '-0.30068900000003396', 'Kingston upon Thames, United Kingdom', '51.5073509', '-0.12775829999998223', 'London, United Kingdom', 'Tuesday, Oct 3', '21:39:55', '09:47:13 PM', '', '', '', 20, 2, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-10-03'),
(92, 14, '', '51.4007811367941', '-0.268090479075909', '14 South Lane West\nNew Malden KT3 5AQ, UK', '51.5300365', '-0.4466335', '8 Uxbridge Rd', 'Wednesday, Oct 4', '10:11:48', '10:11:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4007811367941,-0.268090479075909&markers=color:red|label:D|51.5300365,-0.4466335&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(93, 14, '', '51.4007811367941', '-0.268090479075909', '14 South Lane West\nNew Malden KT3 5AQ, UK', '51.3990873', '-0.3071923', 'KT1 2LL', 'Wednesday, Oct 4', '10:15:40', '10:15:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4007811367941,-0.268090479075909&markers=color:red|label:D|51.3990873,-0.3071923&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(94, 12, '', '51.3986887191932', '-0.305407048617052', '6 Uxbridge Road\nKingston upon Thames KT1 2LL, UK', '51.4947186', '-0.1436198', 'Victoria Station', 'Wednesday, Oct 4', '12:10:19', '12:13:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3986887191932,-0.305407048617052&markers=color:red|label:D|51.4947186,-0.1436198&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(95, 15, '', '51.3525416507002', '-0.444656091352879', 'Linden Road\nWeybridge KT13 0QW, UK', '51.369487', '-0.365927', 'Esher', 'Wednesday, Oct 4', '12:14:25', '12:25:21 PM', '', '2017-10-04', '15:30:10', 0, 2, 2, 1, 2, 0, 4, 1, 0, 1, 1, '2017-10-04'),
(96, 14, '', '51.4007428589677', '-0.268292650580406', '18 South Lane West\nNew Malden KT3 5AQ, UK', '51.4095954', '-0.3053353', 'O Neills Irish Bar', 'Wednesday, Oct 4', '12:25:44', '12:25:44 PM', '', '2017-10-04', '14:25:39', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(97, 15, '', '51.3525542850889', '-0.444656439653246', 'Linden Road\nWeybridge KT13 0QW, UK', '51.3984768800709', '-0.306013561785221', '3-15 Uxbridge Road\nKingston upon Thames KT1, UK', 'Wednesday, Oct 4', '12:26:19', '12:26:19 PM', '', '2017-10-04', '12:26:11', 0, 2, 2, 1, 1, 0, 0, 4, 0, 1, 1, '2017-10-04'),
(98, 13, '', '28.40440506344', '77.0547627657652', '3/1, Emilia 1, Golf Course Extension Road, Vatika City, Block W, Sector 49\nGurugram, Haryana 122018', '28.3881906911472', '77.0804475992918', 'Unnamed Road, Sector 64\nGurugram, Haryana 122005', 'Wednesday, Oct 4', '12:58:40', '12:59:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.40440506344,77.0547627657652&markers=color:red|label:D|28.3881906911472,77.0804475992918&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(99, 13, '', '28.4119153441782', '77.0433375611901', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4123116782811', '77.0541093125939', 'C-5, Rosewood City Road, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Oct 4', '13:26:24', '01:26:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4119153441782,77.0433375611901&markers=color:red|label:D|28.4123116782811,77.0541093125939&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(100, 4, '', '28.4124986389428', '77.0457864180207', '359, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4131403184512', '77.0505929365754', '24, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Oct 4', '13:53:06', '01:53:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4124986389428,77.0457864180207&markers=color:red|label:D|28.4131403184512,77.0505929365754&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-04'),
(101, 649, '', '28.4134582525103', '77.0455266669322', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.6139391', '77.2090212', 'New Delhi', 'Wednesday, Oct 4', '14:09:04', '02:09:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4134582525103,77.0455266669322&markers=color:red|label:D|28.6139391,77.2090212&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(102, 649, '', '28.4134582525103', '77.0455266669322', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.6139391', '77.2090212', 'New Delhi', 'Wednesday, Oct 4', '14:10:44', '02:10:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4134582525103,77.0455266669322&markers=color:red|label:D|28.6139391,77.2090212&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(103, 649, '', '28.4134582525103', '77.0455266669322', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.6139391', '77.2090212', 'New Delhi', 'Wednesday, Oct 4', '14:11:51', '02:12:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4134582525103,77.0455266669322&markers=color:red|label:D|28.6139391,77.2090212&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 2, 3, 0, 0, 1, 0, 1, 1, '2017-10-04'),
(104, 12, '', '51.398702764654', '-0.305522417384322', '6 Uxbridge Road\nKingston upon Thames KT1 2LL, UK', '51.3910093', '-0.3323722', 'High Street', 'Thursday, Oct 5', '13:05:13', '02:20:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.398702764654,-0.305522417384322&markers=color:red|label:D|51.3910093,-0.3323722&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(105, 15, '', '51.3674334071672', '-0.367889106273651', '1 Belvedere Close\nEsher KT10 9LF, UK', '51.3601340172933', '-0.356887355446815', '43 Arbrook Lane\nEsher KT10 9ES, UK', 'Thursday, Oct 5', '15:16:27', '03:16:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3674334071672,-0.367889106273651&markers=color:red|label:D|51.3601340172933,-0.356887355446815&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(106, 15, '', '51.3674334071672', '-0.367889106273651', '1 Belvedere Close\nEsher KT10 9LF, UK', '51.3601340172933', '-0.356887355446815', '43 Arbrook Lane\nEsher KT10 9ES, UK', 'Thursday, Oct 5', '15:19:36', '03:19:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3674334071672,-0.367889106273651&markers=color:red|label:D|51.3601340172933,-0.356887355446815&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(107, 15, '', '51.3758364489119', '-0.361264385282993', 'Esher\nUK', '51.3607591446666', '-0.356773026287556', '43 Arbrook Lane\nEsher KT10 9EG, UK', 'Thursday, Oct 5', '15:23:25', '03:23:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3758364489119,-0.361264385282993&markers=color:red|label:D|51.3607591446666,-0.356773026287556&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(108, 14, '', '51.4008334288548', '-0.268299356102943', '14 South Lane West\nNew Malden KT3 5AQ, UK', '51.3684218', '-0.3673108', 'KT10 9QL', 'Thursday, Oct 5', '15:30:56', '03:30:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4008334288548,-0.268299356102943&markers=color:red|label:D|51.3684218,-0.3673108&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(109, 15, '', '51.3758364489119', '-0.361264385282993', 'Esher\nUK', '51.3607591446666', '-0.356773026287556', '43 Arbrook Lane\nEsher KT10 9EG, UK', 'Thursday, Oct 5', '16:50:10', '04:50:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3758364489119,-0.361264385282993&markers=color:red|label:D|51.3607591446666,-0.356773026287556&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(110, 15, '', '51.3982739760876', '-0.305464044213295', '3 Uxbridge Road\nKingston upon Thames KT1 2LH, UK', '51.3607591446666', '-0.356773026287556', '43 Arbrook Lane\nEsher KT10 9EG, UK', 'Thursday, Oct 5', '17:12:22', '05:16:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3982739760876,-0.305464044213295&markers=color:red|label:D|51.3607591446666,-0.356773026287556&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(111, 15, '', '51.4040482299221', '-0.272299200296402', '205-217 Kingston Road\nNew Malden KT3 3SY, UK', '51.3983434237379', '-0.305731929838657', '3-15 Uxbridge Road\nKingston upon Thames KT1, UK', 'Thursday, Oct 5', '17:25:07', '05:25:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4040482299221,-0.272299200296402&markers=color:red|label:D|51.3983434237379,-0.305731929838657&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(112, 15, '', '51.4040482299221', '-0.272299200296402', '205-217 Kingston Road\nNew Malden KT3 3SY, UK', '51.3983434237379', '-0.305731929838657', '3-15 Uxbridge Road\nKingston upon Thames KT1, UK', 'Thursday, Oct 5', '17:26:36', '05:26:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4040482299221,-0.272299200296402&markers=color:red|label:D|51.3983434237379,-0.305731929838657&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-05'),
(113, 15, '', '51.4040482299221', '-0.272299200296402', '205-217 Kingston Road\nNew Malden KT3 3SY, UK', '51.3983434237379', '-0.305731929838657', '3-15 Uxbridge Road\nKingston upon Thames KT1, UK', 'Thursday, Oct 5', '17:27:51', '05:38:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4040482299221,-0.272299200296402&markers=color:red|label:D|51.3983434237379,-0.305731929838657&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 21, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-05'),
(114, 15, '', '51.4041210152546', '-0.242143794894218', 'Kingston Bypass\nLondon SW20, UK', '51.3993909827311', '-0.229081101715565', '2 Crossway\nLondon SW20, UK', 'Thursday, Oct 5', '18:17:46', '06:17:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4041210152546,-0.242143794894218&markers=color:red|label:D|51.3993909827311,-0.229081101715565&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 4, 0, 1, 1, '2017-10-05'),
(115, 14, '', '51.4007764030727', '-0.268344869837415', '18 South Lane West\nNew Malden KT3 5AQ, UK', '51.3684218', '-0.3673108', 'KT10 9QL', 'Monday, Oct 9', '11:22:29', '11:22:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4007764030727,-0.268344869837415&markers=color:red|label:D|51.3684218,-0.3673108&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-09'),
(116, 13, '', '28.4121046646713', '77.0432597771287', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4081412491575', '77.0486456528306', 'Sai Dham Road, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 10', '07:53:15', '07:53:15 AM', '', '2017-10-10', '12:23:06', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(117, 15, '', '51.3684477623269', '-0.366852616412636', 'Dawes Court\nEsher KT10 9QU, UK', '51.3792783874866', '-0.359288603067398', '132 Lower Green Road\nEsher KT10, UK', 'Wednesday, Oct 11', '10:32:37', '10:34:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3684477623269,-0.366852616412636&markers=color:red|label:D|51.3792783874866,-0.359288603067398&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(118, 15, '', '51.3685204142656', '-0.366861176255425', '98 High Street\nEsher KT10, UK', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 11', '10:40:11', '12:40:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3685204142656,-0.366861176255425&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 9, 0, 8, 1, 0, 1, 1, '2017-10-11'),
(119, 14, '', '51.3988008970398', '-0.305474437773228', '6 Uxbridge Road\nKingston upon Thames KT1 2LL, UK', '51.3684218', '-0.3673108', 'KT10 9QL', 'Thursday, Oct 12', '12:39:53', '12:39:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3988008970398,-0.305474437773228&markers=color:red|label:D|51.3684218,-0.3673108&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-12'),
(120, 14, '', '51.3988008970398', '-0.305474437773228', '6 Uxbridge Road\nKingston upon Thames KT1 2LL, UK', '51.3684218', '-0.3673108', 'KT10 9QL', 'Thursday, Oct 12', '12:40:23', '02:40:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3988008970398,-0.305474437773228&markers=color:red|label:D|51.3684218,-0.3673108&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(121, 12, '', '41.0969944752193', '29.0045741200447', 'Huzur Mahallesi\nKaÄŸÄ±thane Barbaros Caddesi 58-60, 34396 ÅžiÅŸli/Ä°stanbul, Turkey', '41.0797528', '29.0194493', 'Krizantem Sokak', 'Wednesday, Oct 18', '14:59:11', '05:42:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0969944752193,29.0045741200447&markers=color:red|label:D|41.0797528,29.0194493&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(122, 12, '', '41.0797079670974', '29.018988880342', 'Levent Mahallesi\nSÃ¼lÃ¼nlÃ¼ Sokak No:2, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '40.961398', '29.1136762', 'Kosifler Oto BostancÄ± BMW Yetkili SatÄ±cÄ± ve Yetkili Servisi', 'Wednesday, Oct 18', '17:44:08', '09:01:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0797079670974,29.018988880342&markers=color:red|label:D|41.0697072,28.9950702&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(123, 12, '', '41.0424040762985', '29.0373824223935', 'Kuzguncuk Mahallesi\nBoÄŸaziÃ§i KÃ¶prÃ¼sÃ¼, ÃœskÃ¼dar/Ä°stanbul, Turkey', '40.973051', '29.05655', 'Kosifler BMW dealer Auto Baghdad Street', 'Thursday, Oct 19', '09:03:20', '10:26:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0424040762985,29.0373824223935&markers=color:red|label:D|40.973051,29.05655&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-19'),
(124, 12, '', '40.9616820971615', '29.1121636220737', 'Ä°Ã§erenkÃ¶y Mahallesi\nSerin Sokak 1 A, 34752 AtaÅŸehir/Ä°stanbul, Turkey', '41.0697072', '28.9950702', 'MecidiyekÃ¶y Mahallesi', 'Thursday, Oct 19', '10:27:20', '11:22:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|40.9616820971615,29.1121636220737&markers=color:red|label:D|41.0697072,28.9950702&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-19'),
(125, 12, '', '41.0701505833828', '29.0153059735894', 'Nisbetiye Mahallesi\nBÃ¼yÃ¼kdere Caddesi No:100, 34340 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '41.0636561830733', '29.0020525082946', 'Fulya Mahallesi\nProfesÃ¶r Doktor BÃ¼lent Tarcan Sokak No:14, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', 'Friday, Oct 20', '14:29:41', '02:31:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0701505833828,29.0153059735894&markers=color:red|label:D|41.0636561830733,29.0020525082946&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-20'),
(126, 12, '', '41.0654332418477', '29.0000215891581', 'Fulya Mahallesi\nAli Sami Yen Sokak No:19, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '41.0967624', '28.9892494', 'Seyrantepe Mahallesi', 'Saturday, Oct 21', '08:40:55', '08:44:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0654332418477,29.0000215891581&markers=color:red|label:D|41.0967624,28.9892494&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-21'),
(127, 16, '', '28.4120981770587', '77.0432788878679', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4112488861673', '77.0369274169207', 'BCD Road, Vipul World, Sector 48\nGurugram, Haryana 122004', 'Tuesday, Oct 24', '10:31:57', '10:32:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120981770587,77.0432788878679&markers=color:red|label:D|28.4112488861673,77.0369274169207&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(128, 16, '', '28.4121070408678', '77.0432627095188', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4088419351916', '77.0485842972994', 'S - 298, Samadhan Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '10:32:56', '10:33:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121070408678,77.0432627095188&markers=color:red|label:D|28.4088419351916,77.0485842972994&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(129, 16, '', '28.4120852145145', '77.0432571985539', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4050111077273', '77.0612369477749', 'Sector 65\nGurugram, Haryana 122102', 'Tuesday, Oct 24', '11:09:50', '11:10:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120852145145,77.0432571985539&markers=color:red|label:D|28.4050111077273,77.0612369477749&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(130, 16, '', '28.4120878878432', '77.0432661565797', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4078224590701', '77.050089687109', 'Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:14:34', '11:14:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120878878432,77.0432661565797&markers=color:red|label:D|28.4078224590701,77.050089687109&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(131, 17, '', '28.4120977759048', '77.0432738065112', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089837822632', '77.0512131974101', '21, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:16:37', '11:16:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120977759048,77.0432738065112&markers=color:red|label:D|28.4089837822632,77.0512131974101&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(132, 17, '', '28.4120977759048', '77.0432738065112', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089837822632', '77.0512131974101', '21, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:18:06', '11:18:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120977759048,77.0432738065112&markers=color:red|label:D|28.4089837822632,77.0512131974101&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(133, 17, '', '28.4120977759048', '77.0432738065112', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089837822632', '77.0512131974101', '21, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:21:00', '11:21:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120977759048,77.0432738065112&markers=color:red|label:D|28.4089837822632,77.0512131974101&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(134, 17, '', '28.4120977759048', '77.0432738065112', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089837822632', '77.0512131974101', '21, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:23:21', '11:23:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120977759048,77.0432738065112&markers=color:red|label:D|28.4089837822632,77.0512131974101&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-10-24'),
(135, 17, '', '28.4121125080565', '77.0432312575714', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4088286646658', '77.0501835644245', '16, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Tuesday, Oct 24', '11:25:14', '11:25:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121125080565,77.0432312575714&markers=color:red|label:D|28.4088286646658,77.0501835644245&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(136, 17, '', '28.4120851611743', '77.0432440917815', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4083293967885', '77.0320645719767', 'Dhani, Sector 72\nGurugram, Haryana', 'Friday, Oct 27', '12:47:25', '12:47:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120851611743,77.0432440917815&markers=color:red|label:D|28.4083293967885,77.0320645719767&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 22, 3, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(137, 14, '', '51.3987021898781', '-0.305425389392399', '6 Uxbridge Road\nKingston upon Thames KT1 2LL, UK', '51.3684218', '-0.3673108', 'KT10 9QL', 'Tuesday, Oct 31', '13:11:01', '01:26:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.3987021898781,-0.305425389392399&markers=color:red|label:D|51.3684218,-0.3673108&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(138, 20, '', '28.4120492250603', '77.0432138442993', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4137855327134', '77.0492434501648', 'C-5, South City Road, South City II, Sector 49\nGurugram, Haryana 122018', 'Friday, Nov 3', '13:57:46', '01:57:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120492250603,77.0432138442993&markers=color:red|label:D|28.4137855327134,77.0492434501648&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 2, 0, 0, 1, 0, 1, 1, '2017-11-03'),
(139, 20, '', '28.4120492250603', '77.0432138442993', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4137855327134', '77.0492434501648', 'C-5, South City Road, South City II, Sector 49\nGurugram, Haryana 122018', 'Friday, Nov 3', '13:59:11', '01:59:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120492250603,77.0432138442993&markers=color:red|label:D|28.4137855327134,77.0492434501648&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 25, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-03'),
(140, 21, '', '51.4136551593367', '-0.287702465158034', '204 London Road\nKingston upon Thames KT2, UK', '51.3230117', '-0.6031871', 'GU21 3QZ', 'Tuesday, Nov 14', '20:29:27', '08:30:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|51.4136551593367,-0.287702465158034&markers=color:red|label:D|51.3230117,-0.6031871&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-14'),
(141, 12, '', '41.0440181532104', '29.0163389766422', 'YÄ±ldÄ±z Mahallesi\nÃ‡Ä±raÄŸan Caddesi No:32, 34349 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '41.0797528', '29.0194493', 'Krizantem Sokak', 'Monday, Nov 20', '07:32:16', '07:32:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0440181532104,29.0163389766422&markers=color:red|label:D|41.0797528,29.0194493&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(142, 12, '', '41.0780028', '29.0207138', 'GÃ¼vercin Sokak', '41.0439752', '29.0161433', 'Ciragan Palace Kempinski', 'Monday, Nov 20', '12:52:26', '12:53:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0780028,29.0207138&markers=color:red|label:D|41.0439752,29.0161433&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(143, 12, '', '41.0790753180528', '29.0199897163425', 'Levent Mahallesi\nGÃ¼vercin Sokak No:35, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '40.9829888', '28.8104425', 'Istanbul Ataturk Airport', 'Monday, Nov 20', '17:05:43', '05:07:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0790753180528,29.0199897163425&markers=color:red|label:D|40.9829888,28.8104425&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(144, 12, '', '41.0775682177489', '29.0203992649234', 'Levent Mahallesi\nKrizantem Sokak No:14, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '41.0439752', '29.0161433', 'Ciragan Palace Kempinski', 'Tuesday, Nov 21', '13:37:01', '01:38:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0775682177489,29.0203992649234&markers=color:red|label:D|41.0439752,29.0161433&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(145, 12, '', '41.0787241210014', '29.0119604664774', 'Esentepe Mahallesi\nEcza SokaÄŸÄ± No:1, 34394 ÅžiÅŸli/Ä°stanbul, Turkey', '41.0797528', '29.0194493', 'Krizantem Sokak', 'Tuesday, Nov 21', '19:42:36', '07:43:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.0787241210014,29.0119604664774&markers=color:red|label:D|41.0797528,29.0194493&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 20, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(146, 18, '', '41.07910953796907', '29.019496254622936', 'Levent Mahallesi, Krizantem Sk. No:33, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '41.0797528', '29.01944929999999', 'Krizantem Sokak', 'Wednesday, Nov 22', '09:28:32', '09:28:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.07910953796907,29.019496254622936&markers=color:red|label:D|41.0797528,29.01944929999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-22'),
(147, 18, '', '41.07910953796907', '29.019496254622936', 'Levent Mahallesi, Krizantem Sk. No:33, 34330 BeÅŸiktaÅŸ/Ä°stanbul, Turkey', '41.0797528', '29.01944929999999', 'Krizantem Sokak', 'Wednesday, Nov 22', '09:28:36', '09:28:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.07910953796907,29.019496254622936&markers=color:red|label:D|41.0797528,29.01944929999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 1),
(10, 56, 4, 1),
(9, 56, 3, 1),
(8, 56, 2, 1),
(11, 120, 1, 1),
(12, 120, 2, 1),
(13, 120, 3, 1),
(14, 121, 1, 1),
(15, 121, 2, 1),
(16, 121, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1505392123document_image_13.jpg', '2018-01-24', 2),
(2, 1, 2, 'uploads/driver/1505392141document_image_12.jpg', '2017-12-28', 2),
(3, 1, 4, 'uploads/driver/1505392152document_image_14.jpg', '2017-12-28', 2),
(4, 2, 3, 'uploads/driver/1505394126document_image_23.jpg', '2017-10-19', 2),
(5, 2, 2, 'uploads/driver/1505394135document_image_22.jpg', '2017-11-29', 2),
(6, 2, 4, 'uploads/driver/1505394144document_image_24.jpg', '2017-10-25', 2),
(7, 3, 3, 'uploads/driver/1505394732document_image_33.jpg', '2017-12-28', 2),
(8, 3, 2, 'uploads/driver/1505394745document_image_32.jpg', '2017-12-27', 2),
(9, 3, 4, 'uploads/driver/1505394757document_image_34.jpg', '2017-11-30', 2),
(10, 4, 3, 'uploads/driver/1505471103document_image_43.jpg', '2017-09-21', 2),
(11, 4, 2, 'uploads/driver/1505471113document_image_42.jpg', '2017-09-27', 2),
(12, 4, 4, 'uploads/driver/1505471121document_image_44.jpg', '2017-09-26', 2),
(13, 5, 3, 'uploads/driver/1505480825document_image_53.jpg', '15-9-2017', 2),
(14, 5, 2, 'uploads/driver/1505480977document_image_52.jpg', '15-9-2017', 2),
(15, 5, 4, 'uploads/driver/1505481012document_image_54.jpg', '15-9-2017', 2),
(16, 6, 3, 'uploads/driver/1505481836document_image_63.jpg', '15-9-2017', 2),
(17, 6, 2, 'uploads/driver/1505481847document_image_62.jpg', '15-9-2017', 2),
(18, 6, 4, 'uploads/driver/1505481856document_image_64.jpg', '15-9-2017', 2),
(19, 10, 3, 'uploads/driver/1505541824document_image_103.jpg', '16-9-2017', 2),
(20, 10, 2, 'uploads/driver/1505541833document_image_102.jpg', '16-9-2017', 2),
(21, 10, 4, 'uploads/driver/1505541841document_image_104.jpg', '16-9-2017', 2),
(22, 11, 3, 'uploads/driver/1505546596document_image_113.jpg', '22-9-2017', 2),
(23, 11, 2, 'uploads/driver/1505546614document_image_112.jpg', '29-9-2017', 2),
(24, 11, 4, 'uploads/driver/1505546626document_image_114.jpg', '28-9-2017', 2),
(25, 12, 3, 'uploads/driver/1505562465document_image_123.jpg', '24-2-2018', 2),
(26, 12, 2, 'uploads/driver/1505562477document_image_122.jpg', '10-4-2018', 2),
(27, 12, 4, 'uploads/driver/1505562488document_image_124.jpg', '26-8-2018', 2),
(28, 13, 3, 'uploads/driver/1505739278document_image_133.jpg', '2017-09-18', 2),
(29, 13, 2, 'uploads/driver/1505739283document_image_132.jpg', '2017-09-18', 2),
(30, 13, 4, 'uploads/driver/1505739288document_image_134.jpg', '2017-09-18', 2),
(31, 14, 2, 'uploads/driver/1505815478document_image_142.jpg', '30-9-2017', 2),
(32, 14, 3, 'uploads/driver/1505815503document_image_143.jpg', '30-9-2017', 2),
(33, 14, 4, 'uploads/driver/1505815517document_image_144.jpg', '30-9-2017', 2),
(34, 15, 4, 'uploads/driver/1506078737document_image_154.jpg', '2017-09-27', 2),
(35, 15, 3, 'uploads/driver/1506078748document_image_153.jpg', '2017-09-27', 2),
(36, 15, 2, 'uploads/driver/1506078759document_image_152.jpg', '2017-09-28', 2),
(37, 16, 4, '', '', 1),
(38, 16, 3, '', '', 1),
(39, 16, 2, '', '', 1),
(40, 17, 1, 'uploads/driver/1506498516document_image_171.jpg', '2017-09-29', 2),
(41, 17, 2, 'uploads/driver/1506498523document_image_172.jpg', '2017-09-29', 2),
(42, 17, 3, 'uploads/driver/1506498531document_image_173.jpg', '2017-09-29', 2),
(43, 18, 3, 'uploads/driver/15066074350Screenshot_2017-09-13-20-31-31-275_com.alakowe.driver_20170913_203223276.jpgdocument_image_18.jpg', '2023-12-31', 2),
(44, 18, 2, 'uploads/driver/15066074351Screenshot_2017-09-13-20-31-31-275_com.alakowe.driver_20170913_203223276.jpgdocument_image_18.jpg', '2023-12-31', 2),
(45, 18, 1, 'uploads/driver/15066074352Screenshot_2017-09-13-20-31-31-275_com.alakowe.driver_20170913_203223276.jpgdocument_image_18.jpg', '2025-12-31', 2),
(46, 19, 3, 'uploads/driver/150701967201501245673949.jpgdocument_image_19.jpg', '', 2),
(47, 19, 2, 'uploads/driver/150701967211500517914588.jpgdocument_image_19.jpg', '', 2),
(48, 19, 1, 'uploads/driver/150701967221500517928231.jpgdocument_image_19.jpg', '', 2),
(49, 22, 4, 'uploads/driver/1508836315document_image_224.jpg', '2017-10-25', 2),
(50, 22, 3, 'uploads/driver/1508836325document_image_223.jpg', '2017-10-26', 2),
(51, 22, 2, 'uploads/driver/1508836333document_image_222.jpg', '2017-10-26', 2),
(52, 23, 1, 'uploads/driver/1508927098document_image_231.jpg', '31-10-2017', 1),
(53, 23, 2, 'uploads/driver/1508927116document_image_232.jpg', '31-10-2017', 1),
(54, 23, 3, 'uploads/driver/1508927134document_image_233.jpg', '31-10-2017', 1),
(55, 25, 4, 'uploads/driver/1509717240document_image_254.jpg', '2017-11-16', 2),
(56, 25, 3, 'uploads/driver/1509717248document_image_253.jpg', '2017-11-22', 2),
(57, 25, 2, 'uploads/driver/1509717256document_image_252.jpg', '2017-11-23', 2),
(58, 26, 4, 'uploads/driver/1511415457document_image_264.jpg', '2017-11-28', 1),
(59, 26, 3, 'uploads/driver/1511415465document_image_263.jpg', '2017-11-28', 1),
(60, 26, 2, 'uploads/driver/1511415472document_image_262.jpg', '2017-11-28', 1),
(61, 27, 4, 'uploads/driver/1511848362document_image_274.jpg', '2017-11-29', 1),
(62, 27, 3, 'uploads/driver/1511848369document_image_273.jpg', '2017-11-29', 1),
(63, 27, 2, 'uploads/driver/1511848381document_image_272.jpg', '2017-11-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-14 13:31:28', '2017-09-14 13:59:25', '0 Hours 54 Minutes', 0, 54, '2017-09-14'),
(2, 2, '2017-09-14 14:02:51', '2017-09-14 14:10:36', '0 Hours 7 Minutes', 0, 7, '2017-09-14'),
(3, 3, '2017-09-14 14:16:03', '', '', 0, 0, '2017-09-14'),
(4, 4, '2017-09-15 11:26:31', '2017-09-15 15:02:28', '3 Hours 35 Minutes', 3, 35, '2017-09-15'),
(5, 6, '2017-09-16 05:58:56', '2017-09-16 06:22:37', '0 Hours 26 Minutes', 0, 26, '2017-09-15'),
(6, 6, '2017-09-16 06:36:20', '2017-09-16 07:01:40', '0 Hours 25 Minutes', 0, 25, '2017-09-16'),
(7, 10, '2017-09-16 09:11:19', '2017-09-16 11:28:08', '2 Hours 136 Minutes', 2, 136, '2017-09-16'),
(8, 11, '2017-09-16 08:54:50', '2017-09-16 08:59:31', '0 Hours 24 Minutes', 0, 24, '2017-09-16'),
(9, 12, '2017-09-16 12:54:32', '2017-09-16 12:58:38', '0 Hours 4 Minutes', 0, 4, '2017-09-16'),
(10, 12, '2017-09-18 06:49:57', '', '', 0, 0, '2017-09-18'),
(11, 13, '2017-09-18 14:47:53', '2017-09-18 14:47:50', '0 Hours 54 Minutes', 0, 54, '2017-09-18'),
(12, 17, '2017-09-27 08:50:37', '2017-10-04 12:12:47', '3 Hours 22 Minutes', 3, 22, '2017-09-27'),
(13, 20, '2017-10-03 21:28:08', '2017-10-05 15:55:36', '18 Hours 27 Minutes', 18, 27, '2017-10-03'),
(14, 21, '2017-10-04 12:05:37', '2017-10-05 13:10:43', '1 Hours 5 Minutes', 1, 5, '2017-10-04'),
(15, 4, '2017-10-04 13:26:03', '2017-10-04 14:16:13', '0 Hours 77 Minutes', 0, 77, '2017-10-04'),
(16, 20, '2017-10-05 15:55:40', '2017-11-14 20:29:02', '4 Hours 33 Minutes', 4, 33, '2017-10-05'),
(17, 21, '2017-10-05 17:45:12', '2017-10-11 10:37:23', '16 Hours 89 Minutes', 16, 89, '2017-10-05'),
(18, 21, '2017-10-12 12:39:23', '2017-10-31 10:55:40', '22 Hours 16 Minutes', 22, 16, '2017-10-12'),
(19, 22, '2017-10-24 10:24:56', '', '', 0, 0, '2017-10-24'),
(20, 22, '2017-10-27 12:46:27', '', '', 0, 0, '2017-10-27'),
(21, 25, '2017-11-03 13:55:32', '', '', 0, 0, '2017-11-03'),
(22, 20, '2017-11-14 20:29:05', '', '', 0, 0, '2017-11-14');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver'),
(108, 1, 'tr', 'GÄ°RÄ°Åž BAÅžARILI'),
(109, 2, 'tr', 'KULLANICI AKTÄ°F DEÄžÄ°L'),
(110, 3, 'tr', 'DOLDURULMASI GEREKEN ALANLAR EKSÄ°K'),
(111, 4, 'tr', 'E-POSTA VEYA PAROLA GEÃ‡ERSÄ°Z'),
(112, 5, 'tr', 'Ã‡IKIÅž BAÅžARILI'),
(113, 6, 'tr', 'KAYIT BULUNAMADI'),
(114, 7, 'tr', 'KAYIT BAÅžARILI'),
(115, 8, 'tr', 'TELEFON NUMARASI KULLANILMAKTADIR'),
(116, 9, 'tr', 'E-POSTA ADRESÄ° KULLANILMAKTADIR'),
(117, 10, 'tr', 'EKSÄ°K KAYIT BELGESÄ°'),
(118, 11, 'tr', 'EKSÄ°K EHLÄ°YET/RUHSAT BELGESÄ°'),
(119, 12, 'tr', 'EKSÄ°K SÄ°GORTA BELGESÄ°'),
(120, 13, 'tr', 'PAROLA BAÅžARIYLA DEÄžÄ°ÅžTÄ°RÄ°LDÄ°'),
(121, 14, 'tr', 'ESKÄ° PAROLA '),
(122, 15, 'tr', 'GEÃ‡ERSÄ°Z KUPON KODU'),
(123, 16, 'tr', 'KUPON BAÅžARIYLA UYGULANDI'),
(124, 17, 'tr', 'KULLANICI BULUNAMADI'),
(125, 18, 'tr', 'GÃœNCELLEME BAÅžARILI'),
(126, 19, 'tr', 'TELEFON NUMARASI KULLANILMAKTADIR'),
(127, 20, 'tr', 'Ã‡EVRÄ°MÄ°Ã‡Ä°'),
(128, 21, 'tr', 'Ã‡EVRÄ°MDIÅžI'),
(129, 22, 'tr', 'TEK KULLANIMLIK PAROLA ONAY Ä°Ã‡Ä°N TELEFONA GÃ–NDERÄ°LDÄ°'),
(130, 23, 'tr', 'DEÄžERLENDÄ°RME BAÅžARILI'),
(131, 24, 'tr', 'E-POSTA BAÅžARIYLA GÃ–NDERÄ°LDÄ°'),
(132, 25, 'tr', 'REZERVASYON KABUL EDÄ°LDÄ°'),
(133, 26, 'tr', 'SÃœRÃœCÃœ KONUMA VARDI'),
(134, 27, 'tr', 'YOLCULUK BAÅžARIYLA Ä°PTAL EDÄ°LDÄ°'),
(135, 28, 'tr', 'YOLCULUK SONLANDIRILDI'),
(136, 29, 'tr', 'YOLCULUK REZERVASYONU BAÅžARILI'),
(137, 30, 'tr', 'YOLCULUK BAÅžARIYLA REDDEDÄ°LDÄ°'),
(138, 31, 'tr', 'YOLCULUK BAÅžLADI'),
(139, 32, 'tr', 'YENÄ° YOLCULUK Ä°Ã‡Ä°N YER AYRILDI'),
(140, 33, 'tr', 'YOLCULUK MÃœÅžTERÄ° TARAFINDAN Ä°PTAL EDÄ°LDÄ°'),
(141, 34, 'tr', 'REZERVASYON KABUL EDÄ°LDÄ°'),
(142, 35, 'tr', 'REZERVASYON REDDEDÄ°LDÄ°'),
(143, 36, 'tr', 'REZERVASYON SÃœRÃœCÃœ TARAFINDAN Ä°PTAL EDÄ°LDÄ°');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 11, 2, 4.5, '', 2, 4.5, '', '2017-09-14'),
(2, 14, 3, 5, '', 3, 5, '', '2017-09-14'),
(3, 18, 3, 5, '', 3, 5, '', '2017-09-14'),
(4, 19, 3, 4.5, '', 3, 4, '', '2017-09-14'),
(5, 38, 8, 0, '', 10, 5, '', '2017-09-16'),
(6, 7, 8, 4, '', 10, 0, '', '2017-09-16'),
(7, 8, 8, 5, '', 10, 0, '', '2017-09-16'),
(8, 39, 8, 0, '', 10, 5, '', '2017-09-16'),
(9, 55, 9, 0, '', 11, 5, '', '2017-09-16'),
(10, 9, 9, 5, 'jzbsh', 11, 0, '', '2017-09-16'),
(11, 10, 9, 4, '', 11, 0, '', '2017-09-16'),
(12, 60, 9, 0, '', 11, 5, '', '2017-09-16'),
(13, 13, 9, 5, '', 11, 0, '', '2017-09-16'),
(14, 62, 9, 0, '', 10, 5, '', '2017-09-16'),
(15, 65, 9, 0, '', 10, 5, '', '2017-09-16'),
(16, 17, 9, 5, '', 10, 0, '', '2017-09-16'),
(17, 66, 9, 0, '', 12, 4, '', '2017-09-16'),
(18, 20, 10, 4, '', 12, 0, '', '2017-09-18'),
(19, 80, 10, 0, '', 12, 4, '', '2017-09-18'),
(20, 25, 10, 4.5, '', 12, 0, '', '2017-09-18'),
(21, 81, 10, 0, '', 12, 5, '', '2017-09-18'),
(22, 83, 4, 0, '', 3, 2, '', '2017-09-18'),
(23, 84, 4, 0, '', 3, 3, '', '2017-09-18'),
(24, 98, 13, 4, '', 4, 5, '', '2017-10-04'),
(25, 100, 13, 0, '', 4, 5, 'ffff', '2017-10-04'),
(26, 104, 12, 5, '', 20, 5, '', '2017-10-05'),
(27, 110, 15, 0, '', 21, 5, '', '2017-10-05'),
(28, 113, 15, 4, '', 21, 5, '', '2017-10-05'),
(29, 117, 15, 4, '', 20, 0, '', '2017-10-11'),
(30, 118, 15, 0, '', 20, 5, '', '2017-10-11'),
(31, 120, 14, 0, '', 20, 5, '', '2017-10-18'),
(32, 121, 12, 5, '', 20, 5, '', '2017-10-18'),
(33, 122, 12, 4.5, '', 20, 5, '', '2017-10-19'),
(34, 123, 12, 0, '', 20, 5, '', '2017-10-19'),
(35, 124, 12, 0, '', 20, 5, '', '2017-10-19'),
(36, 125, 12, 5, '', 20, 5, '', '2017-10-20'),
(37, 126, 12, 0, '', 20, 5, '', '2017-10-21'),
(38, 127, 16, 3.5, '', 22, 2.5, 'New', '2017-10-24'),
(39, 128, 16, 2.5, 'This', 22, 3, '', '2017-10-24'),
(40, 129, 16, 3, '', 22, 3, '', '2017-10-24'),
(41, 135, 17, 3, 'New comm', 22, 2, 'Sf', '2017-10-24'),
(42, 136, 17, 3, '', 22, 3, '', '2017-10-27'),
(43, 137, 14, 5, '', 20, 5, '', '2017-10-31'),
(44, 139, 20, 2.5, '', 25, 0, '', '2017-11-03'),
(45, 140, 21, 0, '', 20, 4.5, '', '2017-11-14'),
(46, 141, 12, 5, '', 20, 5, '', '2017-11-20'),
(47, 142, 12, 5, '', 20, 5, '', '2017-11-20'),
(48, 143, 12, 0, '', 20, 5, '', '2017-11-20'),
(49, 144, 12, 0, '', 20, 5, '', '2017-11-21'),
(50, 145, 12, 0, '', 20, 4.5, '', '2017-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 2, 1, 1),
(2, 1, 2, 1, 2),
(3, 1, 2, 1, 3),
(4, 1, 2, 1, 4),
(5, 1, 2, 1, 5),
(6, 1, 2, 0, 6),
(7, 1, 2, 1, 7),
(8, 1, 2, 1, 8),
(9, 1, 2, 0, 9),
(10, 1, 2, 1, 10),
(11, 1, 2, 2, 11),
(12, 1, 3, 0, 12),
(13, 1, 3, 0, 13),
(14, 1, 3, 3, 14),
(15, 1, 3, 0, 15),
(16, 1, 3, 3, 16),
(17, 1, 3, 0, 17),
(18, 1, 3, 3, 18),
(19, 1, 3, 3, 19),
(20, 1, 4, 3, 20),
(21, 1, 4, 0, 21),
(22, 1, 7, 0, 22),
(23, 1, 7, 0, 23),
(24, 1, 7, 0, 24),
(25, 1, 7, 0, 25),
(26, 1, 7, 0, 26),
(27, 1, 7, 0, 27),
(28, 1, 7, 0, 28),
(29, 1, 7, 0, 29),
(30, 1, 7, 0, 30),
(31, 1, 7, 0, 31),
(32, 1, 7, 0, 32),
(33, 1, 7, 0, 33),
(34, 1, 7, 0, 34),
(35, 1, 7, 0, 35),
(36, 1, 7, 0, 36),
(37, 1, 7, 0, 37),
(38, 1, 8, 10, 38),
(39, 1, 8, 10, 39),
(40, 1, 9, 0, 40),
(41, 1, 9, 10, 41),
(42, 1, 9, 0, 42),
(43, 1, 9, 0, 43),
(44, 1, 9, 0, 44),
(45, 1, 9, 0, 45),
(46, 1, 9, 0, 46),
(47, 1, 9, 0, 47),
(48, 1, 9, 0, 48),
(49, 1, 9, 0, 49),
(50, 1, 9, 0, 50),
(51, 1, 9, 0, 51),
(52, 1, 9, 0, 52),
(53, 1, 9, 0, 53),
(54, 1, 9, 0, 54),
(55, 1, 9, 11, 55),
(56, 1, 9, 11, 56),
(57, 1, 9, 11, 57),
(58, 1, 9, 11, 58),
(59, 1, 9, 0, 59),
(60, 1, 9, 11, 60),
(61, 1, 9, 11, 61),
(62, 1, 9, 10, 62),
(63, 1, 9, 10, 63),
(64, 1, 4, 0, 64),
(65, 1, 9, 10, 65),
(66, 1, 9, 12, 66),
(67, 1, 9, 12, 67),
(68, 1, 9, 0, 68),
(69, 1, 9, 0, 69),
(70, 1, 9, 0, 70),
(71, 1, 9, 0, 71),
(72, 1, 9, 0, 72),
(73, 1, 9, 0, 73),
(74, 1, 9, 0, 74),
(75, 1, 10, 12, 75),
(76, 1, 10, 12, 76),
(77, 1, 10, 12, 77),
(78, 1, 10, 12, 78),
(79, 1, 10, 12, 79),
(80, 1, 10, 12, 80),
(81, 1, 10, 12, 81),
(82, 1, 4, 0, 82),
(83, 1, 4, 3, 83),
(84, 1, 4, 3, 84),
(85, 1, 4, 0, 85),
(86, 1, 4, 0, 86),
(87, 1, 4, 0, 87),
(88, 1, 9, 0, 88),
(89, 1, 11, 0, 89),
(90, 1, 11, 0, 90),
(91, 1, 12, 20, 91),
(92, 1, 14, 0, 92),
(93, 1, 14, 0, 93),
(94, 1, 12, 21, 94),
(95, 1, 15, 0, 95),
(96, 1, 14, 0, 96),
(97, 1, 15, 0, 97),
(98, 1, 13, 4, 98),
(99, 1, 13, 4, 99),
(100, 1, 4, 4, 100),
(101, 1, 649, 4, 101),
(102, 1, 649, 4, 102),
(103, 1, 649, 4, 103),
(104, 1, 12, 20, 104),
(105, 1, 15, 0, 105),
(106, 1, 15, 0, 106),
(107, 1, 15, 0, 107),
(108, 1, 14, 0, 108),
(109, 1, 15, 0, 109),
(110, 1, 15, 21, 110),
(111, 1, 15, 0, 111),
(112, 1, 15, 0, 112),
(113, 1, 15, 21, 113),
(114, 1, 15, 0, 114),
(115, 1, 14, 0, 115),
(116, 1, 13, 0, 116),
(117, 1, 15, 20, 117),
(118, 1, 15, 20, 118),
(119, 1, 14, 0, 119),
(120, 1, 14, 20, 120),
(121, 1, 12, 20, 121),
(122, 1, 12, 20, 122),
(123, 1, 12, 20, 123),
(124, 1, 12, 20, 124),
(125, 1, 12, 20, 125),
(126, 1, 12, 20, 126),
(127, 1, 16, 22, 127),
(128, 1, 16, 22, 128),
(129, 1, 16, 22, 129),
(130, 1, 16, 0, 130),
(131, 1, 17, 0, 131),
(132, 1, 17, 0, 132),
(133, 1, 17, 0, 133),
(134, 1, 17, 0, 134),
(135, 1, 17, 22, 135),
(136, 1, 17, 22, 136),
(137, 1, 14, 20, 137),
(138, 1, 20, 0, 138),
(139, 1, 20, 25, 139),
(140, 1, 21, 20, 140),
(141, 1, 12, 20, 141),
(142, 1, 12, 20, 142),
(143, 1, 12, 20, 143),
(144, 1, 12, 20, 144),
(145, 1, 12, 20, 145);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'vishal garg .', 'vishal@gmail.com', '+918950368358', '123456', '', '', 0, '0', 'Thursday, Sep 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-14', 1),
(2, 1, 'AKshay .', 'akshay@rt.com', '+916767676767', '123456', '', '', 0, '0', 'Thursday, Sep 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.5', 0, '', 1, '2017-09-14', 1),
(3, 1, 'Amit .', 'lkjhgfdsa@gmail.com', '+911234567890', '9876543211', 'http://apporio.org/SuperTaxi/supertaxi/uploads/swift_file65.jpeg', '', 0, '0', 'Thursday, Sep 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.66666666667', 0, '', 1, '2017-09-14', 1),
(4, 1, 'Ashish Apporio ', 'ashish@gmail.com', '+917550646597', '12345678', '', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104065945547688247074', 'Ashish Apporio', 'ashish@apporio.com', 'https://lh3.googleusercontent.com/-mP_gq66X3bs/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf41-C9NDj4nR9zS0rY2mJbN4co1KA/s400/photo.jpg', '', '', 0, 1, '2.5', 0, '', 1, '2017-09-15', 1),
(5, 1, 'AShish .', 'ashish@apporio.com', '+917005183557', '12345678', '', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-15', 1),
(6, 1, 'gfhggfj .', 'ghdhgfgh@gmail.com', '+919865787667', 'ergeryeryhdhh', '', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-15', 1),
(7, 1, 'ashish .', 'ashish@gmail.com', '+919813226951', '123456', '', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-09-16', 1),
(8, 1, 'satish .', 'satish@gmail.com', '+919034903490', '123456', 'http://apporio.org/SuperTaxi/supertaxi/uploads/1505543271192.jpg', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '2.5', 0, '', 1, '2017-09-16', 1),
(9, 1, 'Apporio Infoleb .', '', '+919898989898', '', '', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '106611368472904297378', 'Apporio Infoleb', 'infolebapporio@gmail.com', 'null', '', '', 0, 0, '2.66666666667', 0, '', 3, '2017-09-16', 1),
(10, 1, 'Apporio devices .', '', '+919991617237', '', '', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111976537605501585996', 'Apporio devices', 'apporiodevices@gmail.com', 'null', '', '', 0, 0, '2.25', 0, '', 3, '2017-09-16', 1),
(11, 1, 'AShish .', 'Newemail1@gmail.com', '+915676545679', '12345678', '', '', 0, '0', 'Monday, Sep 18', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-18', 1),
(12, 1, 'Huseyin Sanli ', '', '+447970985987', '', '', '', 0, '0', 'Saturday, Sep 23', '', 0, 0, 0, 0, 0, '10159012604515696', '10159012604515696@facebook.com', 'https://graph.facebook.com/10159012604515696/picture?width=640&height=640', 'Huseyin Sanli', '', '', '', '', '', '', '', 0, 0, '4.95833333333', 0, '', 2, '2017-09-23', 1),
(13, 1, 'shilpa .', 'sg@g.com', '+918130039030', 'qwerty', '', '', 0, '0', 'Wednesday, Sep 27', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-09-27', 1),
(14, 1, 'Hana Won ', 'hwon@roundsonme.com', '+447792940066', '', 'http://apporio.org/SuperTaxi/supertaxi/uploads/swift_file66.jpeg', '', 0, '0', 'Wednesday, Sep 27', '', 0, 0, 0, 0, 0, '', '', '', '', '', '110004814777066113989', 'Hana Won', 'hwon@roundsonme.com', 'https://lh6.googleusercontent.com/-6ojOqAmmZrw/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf6YT_XfZb82KiwXlgJuGsIw1ePnkA/s400/photo.jpg', '', '', 0, 0, '5', 0, '', 3, '2017-09-27', 1),
(15, 1, 'Jon .', 'jonathan.twydell_satterly@skytouch.ae', '+4407921284566', 'supertaxi291166@', '', '', 0, '0', 'Wednesday, Oct 4', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111915504320230966825', 'J T-S', 'sup3rtaxi@gmail.com', 'https://lh3.googleusercontent.com/-Th7ed4DJK1k/AAAAAAAAAAI/AAAAAAAAAAA/ACnBePZiY9xj4-lsmdb41fZ_YLDXpF8oWQ/s400/photo.jpg', '', '', 0, 1, '3.75', 0, '', 3, '2017-10-04', 1),
(16, 1, 'Ashish Apporio ', '', '+91342345678', '', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104065945547688247074', 'Ashish Apporio', 'ashish@apporio.com', 'https://lh3.googleusercontent.com/-mP_gq66X3bs/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf41-C9NDj4nR9zS0rY2mJbN4co1KA/s400/photo.jpg', '', '', 0, 0, '2.83333333333', 0, '', 3, '2017-10-24', 1),
(17, 1, 'Ashish Apporio ', '', '+91453456787', '', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104065945547688247074', 'Ashish Apporio', 'ashish@apporio.com', 'https://lh3.googleusercontent.com/-mP_gq66X3bs/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf41-C9NDj4nR9zS0rY2mJbN4co1KA/s400/photo.jpg', '', '', 0, 0, '2.5', 0, '', 3, '2017-10-24', 1),
(18, 1, 'Ozan Berk Polat .', 'ozanberkplt@gmail.com', '+905537291209', 'ozan2491971', '', '', 0, '500', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-24', 1),
(19, 1, 'bzbsn .', 'shzhzjz@gmail.com', '+91979779779797', '123456', '', '', 0, '0', 'Thursday, Nov 2', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-02', 1),
(20, 1, 'Ashish .', 'ashish@gmail.com', '+917550646590', '12345678', '', '', 0, '0', 'Friday, Nov 3', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-03', 1),
(21, 1, 'Curtis White ', 'curtisjwhite@hotmail.com', '+4407734252903', '', 'http://apporio.org/SuperTaxi/supertaxi/uploads/swift_file67.jpeg', '', 0, '0', 'Sunday, Nov 5', '', 0, 0, 0, 0, 0, '10155845672852229', '10155845672852229@facebook.com', 'https://graph.facebook.com/10155845672852229/picture?width=640&height=640', 'Curtis White', '', '', '', '', '', '', '', 0, 0, '4.5', 0, '', 2, '2017-11-05', 1),
(22, 1, 'Ashish Apporio ', '', '+91655666666', '', '', '', 0, '0', 'Tuesday, Nov 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104065945547688247074', 'Ashish Apporio', 'ashish@apporio.com', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/s400/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-11-28', 1),
(23, 1, 'zhhzh .', 'zhzhzh@g.com', '+917989899779', 'dhdbdnbdbd', '', '', 0, '0', 'Tuesday, Nov 28', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 10, 'cLaJikTQpgw:APA91bFkYRKIGB4xCeyXjVTxos3q4a2U6m_FXPyX5GwFDNYqFGMSaLcs9cWrVecfXMUY5VZqKl50fuIAGqhJ9hoBEGw1Z14PBehEjAxp-xZI9yNf6a1_3br22JbLOSvXLsXt2ssjF57Y', 0, 'c06c40037632ed39', 1),
(2, 4, '5ECAF9E2D6AD6A6D2E5FA5599CD52E414C5FE57342CEA088E4CF3AA58B228452', 1, '3E453AEB-2E0E-4314-9C97-EA3359190850', 1),
(3, 9, 'c-YtrQZggFg:APA91bGsuIDGg634CQLSZPqSGVwnpIRqhA1-BEarXg6IMzkmm7tk3GGRsmfNA5h7JN1ZdvL44daXaI6THTPP1LubvLqEaWLIjOcpwcnId8neqlSxPZXlDvs2pMyDqDd1TYJtmcSwMkq-', 0, 'e659dbb5fa6025fb', 1),
(4, 11, '3A6D436DA63BAC849FB771B0ED7F0A02958FC452736F8781CCCCBBB8AD278F07', 1, 'BC9D4F52-D536-4051-A422-24E4BA89FC2A', 1),
(5, 4, 'eK0e-qhPLJo:APA91bGtoKPIfLAoJ9ccTdTs8xU5r3O2Nll4plXJZ6wz101UY-QKhz9LggPgxuqhacWaOw-tGpr-Uqmy5fPw6P_Oc0MrPcvsJbQV3TQqLskgXUoI1lmqS1lCWJ_CBhqufDxECn-oAJcQ', 0, '96ac6773bae674b8', 1),
(6, 12, '93C2712125D4B812DA279F14C5B299889CE49A30F7BE66F64C4F6444842218CE', 1, 'F9B3C17B-B500-4DA4-81D1-48A44F9E3AF3', 1),
(7, 12, 'A5E7AA3C8E3FB5A721659A4E7302B9BA72933448A1FD008BD5BE3E5EED4DAEE9', 1, '3A932DAD-6A56-472B-A376-8C18FAE0729A', 1),
(8, 14, '9858E110D0EA7764BD6F503209B392C236715A436778BBC131028EAB4A42865C', 1, '4B4612E4-FA0D-47A5-B4E0-2BFD550E8F67', 1),
(9, 12, '017BF16CAB74055D53CFAF1F10067A01D33B07D54D50AEC00B41980C81448896', 1, 'EEFA2ED4-DFD6-4DA7-B798-A2E6B1245A36', 1),
(10, 15, 'BEBBEFE047F3AEAEB26E29E8C341387581E12E9F3E21E6173964D79F026BC586', 1, 'E551B7A2-1109-4FDD-A922-C653D1EA5D8B', 1),
(11, 4, '7eba6f12589ea1d618359dc10d5633e031aae26a50023e531f712659975a7013', 1, '5BACAE9E-BD9A-446F-BF3E-250AD118B9AA', 1),
(12, 20, '10137C5833BDF94FF4AB3D2D49E0BBA64702327DD0C670F2C53BC40956D57A53', 1, '7E0FC7E6-4A15-4C6B-BACD-AF6564A0B419', 1),
(13, 3, 'cwcJeMgKEf0:APA91bHRMD9mmgk_xrBR1JClergixI8VeHNPEk6vOqXh8D8OhrcbVHtf_0MB6bEepjguKzDa159Nw1nAXL81gd794UGnFcgi2XYs0Rm5YT29bEw-npzDfBb0EBVtMVpSyx6iYtUloyNS', 0, '5950e80b8fb1cbeb', 1),
(14, 18, 'csIqwaGvWr4:APA91bE5eeip3y2KS69vlEwZjB9gbou3l2lDYd8LugcRP7VcI24l1_4BHPdRkbR9RUh-FRufEWh2UTJjuJCDB0DyYV0fkWgdZPdLEJoncfiTTKCBhTIm7MibN2KsFBZ38-Obb0OgI72s', 0, '8790716c199871f2', 1),
(15, 15, '7eba6f12589ea1d618359dc10d5633e031aae26a50023e531f712659975a7013', 1, '1609B676-BB04-4952-B187-581D6CF7332E', 1),
(16, 23, 'copj-6NSqeg:APA91bHtxGt8USgw3dBWNJucco-jcFSUCjUck-w9Haalf0tzDFIDaLgxHEpoQYiQkkc3--74qKFkHFm4cdH1rbzPJL1iuompckwgYe8UwEn8LrWSF51bASfjPlKoLks-ndajsC-txmBB', 0, '2ba9bf317a129dea', 1),
(17, 21, '5612CB8359513F03B27AEB7237DDE32F25CAE84AD8501ED46CCDC4198273D2E4', 1, '1E0137F0-4D08-41C7-918D-52B098EAB2C4', 1),
(18, 18, 'ff8nDI0SqW8:APA91bFUZiicylu7ZEfcwkpxPmhw7IWYorFqn54pnkhbkMB91k1rtuQn3BaT6Oaza5LAPenzOsZDPc7Uk5Eponlt0iKv9jL-OMXDOw_7nzIqLKrESuMoXgdxepencvkoceZgC_03QtTf', 0, 'c95db9c9fc370a86', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
