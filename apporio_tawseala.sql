-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:23 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_tawseala`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`) VALUES
(1, 'Tawseala', 'uploads/logo/logo_59bbca00a400b.png', 'hello@apporio.com', 'Khartoum, Sudan', 'AIzaSyDtDu--fwhDZxVQKnCSZODbTAyZrnw5tn4', '15.5006544', '32.5598994');

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', '', 1, 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.png', '', 1, 1),
(4, 'Mini', '', 'Mini', 'uploads/car/editcar_4.png', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#8377', 'Miles', '', '', 1),
(120, 'Khartoum', '15.5006544', '32.5598994', 'Â£', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-09-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-13', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377'),
(4, 'SDG', '&#163;', '&#163;');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM'),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM'),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM'),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM'),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM'),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM'),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM'),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM'),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM'),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM'),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM'),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM'),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM'),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM'),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM'),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM'),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM'),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM'),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 2, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '02:52:21 PM', '02:52:24 PM', '02:52:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(2, 4, '28.4122306', '77.0431947', '28.4122263', '77.0431878', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '03:25:10 PM', '03:25:15 PM', '03:25:19 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(3, 9, '', '', '', '', '', '', '06:35:04 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 3, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(4, 11, '28.412367892478', '77.043292885718', '28.4123036032807', '77.0433161874088', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:44:08 AM', '09:44:18 AM', '09:44:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(5, 17, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '01:18:32 PM', '01:18:35 PM', '01:18:38 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(6, 18, '28.4121158905592', '77.043208396134', '28.4120756528421', '77.0432970836056', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:28:58 PM', '01:29:02 PM', '01:29:13 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 4, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(7, 21, '28.4121856', '77.0431223', '28.4121856', '77.0431223', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '01:51:18 PM', '01:51:20 PM', '01:51:24 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(8, 22, '28.4120894', '77.0434034', '28.4120894', '77.0434034', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:52:41 PM', '01:52:45 PM', '01:52:47 PM', '0', '0.00', '00.00', '0', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `driver_gender` varchar(255) NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `driver_gender`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 4, 'mky', 'mk@gmail.com', '8285469069', 'uploads/driver/1505562750driver_1.jpg', '123456', 'A56nk6XwAHfCLYod', '', '576', '24', '', 'exLUwClJFP0:APA91bGeuGDUqhkiQg1_9fWMf8e-KISnWo-IHQU1KsIgQOCampNUvmKxg2_0FX7VKb191I-QnnkIww0Wn_CFhciSE6D-fgQ4S5rCEgbmo64QuxO-n6PyYPyrEzqaC323YK3aSZCUYPZo', 2, '1.8125', 2, 3, 'qer', 56, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121856', '77.0431223', '----', '02:00', 'Saturday, Sep 16, 2017', 7, 4, 0, 2, 0, 1, 2, 0, '2017-09-15', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(2, 0, 5, 'user', 'user@gmail.com', '1234567812', '', 'asdfghjkl', '8Z4gzep56C23Lfgt', '', '0', '0', '', '750D44B800C8733BC8DCF6C74A53DF4918DF9577DFCEE92F4B5A742FB3B700AD', 1, '', 3, 20, '9090', 56, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120397964933', '77.0432363205782', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '05:11', 'Friday, Sep 15, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '2017-09-15', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(3, 0, 10, 'mohame', 'mohalwali88@gmail.com', '0911818056', '', '123456', 'bznTMlUUiltppqYb', 'Male', '0', '0', '', 'dOlrBLLCO5c:APA91bEXxIgiJ5-ZohYgaEWLX6z_TC5xBcelnX_9IxKNOEzDd3bouXII5UlKmfv4QwHNYX8pnmpl2ixYJLT17kgVoiSZV6emU0GsP1ukv0n95iJrEP9S02atpfqIdv6AIG2dXWEW0ote', 2, '', 2, 3, '74094', 120, 'Friday, Sep 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '15.5311758', '32.5866262', '', '18:34', 'Friday, Sep 15, 2017', 0, 0, 1, 1, 0, 1, 2, 0, '2017-09-15', 1, '', '2017-09-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 9, 1, 9),
(4, 0, 5, 'testing', 'testing@gmail.com', '9393939393', '', 'qwerty', '06Yi2oe9tTcuUcgg', '1', '190', '10', '', '75669EC3EFAF863A7181FE568DC5A850C5F985FDDE1E212443AC2737151AED82', 1, '2', 3, 20, '7788', 56, 'Saturday, Sep 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.414054587371', '77.0456634073022', '1002, Sispal Vihar Internal Road, Block K, South City II, Sector 49 , Gurugram, Haryana 122018', '02:43', 'Saturday, Sep 16, 2017', 2, 0, 4, 1, 0, 1, 2, 0, '2017-09-16', 1, '', '2017-09-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(5, 0, 0, 'te', '', '', '', '', '', '', '0', '0', '', '', 0, '', 0, 0, '', 0, 'Tuesday, Oct 3', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 2, 0, '', 0, '', '0000-00-00', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '200', 2, '192', '8', '2017-09-15'),
(2, 4, '200', 2, '190', '10', '2017-09-16'),
(3, 1, '400', 4, '384', '16', '2017-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`, `ride_status`) VALUES
(1, 1, 22, 1, 1),
(2, 3, 9, 1, 1),
(3, 4, 20, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 3, 1, 'Monday', '11:35', '11:35', '12:35', '10:35', 1, '23'),
(18, 56, 1, 'Monday', '17:19', '18:20', '17:20', '18:20', 1, '55'),
(22, 56, 2, 'Night', '17:00', '23.45', '', '', 1, '99'),
(32, 112, 1, 'Sunday', '07:30', '09:30', '14:30', '18:0', 2, '1.2'),
(33, 112, 2, 'Night', '23:30', '06:30', '', '', 2, '1.5'),
(34, 3, 1, 'Wednesday', '14:28', '23:45', '14:29', '23:59', 1, '10'),
(37, 56, 1, 'Sunday', '08:30', '09:30', '10:30', '11:30', 2, '23'),
(38, 56, 1, 'Saturday', '12:31', '12:31', '12:31', '12:31', 2, '20'),
(39, 79, 1, 'Monday', '16:19', '20:19', '22:19', '01:19', 2, '10'),
(40, 56, 1, 'Thursday', '22:30', '23:45', '23:45', '23:45', 1, '100'),
(41, 56, 1, 'Tuesday', '09:13 PM', '10:14 PM', '10:14 PM', '11:14 PM', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 2, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 15', '14:04:18', '02:04:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '1'),
(2, 2, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '1'),
(3, 2, 3, '1', 'Cash', 'Admin', '100', 'Friday, Sep 15', '1'),
(4, 4, 6, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(5, 5, 7, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(6, 6, 9, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(7, 7, 7, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(8, 8, 7, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1'),
(9, 8, 7, '1', 'Cash', 'Admin', '100', 'Saturday, Sep 16', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(12, 56, 'Miles', '&#8377', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '&#8377', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '&#8377', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(43, 120, 'Km', 'Â£', 2, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(44, 120, 'Km', 'Â£', 3, 10, 0, 0, 0, 0, 0, 0, '6', '70', '10', '0', '0', '0', '0'),
(45, 120, 'Km', 'Â£', 4, 10, 0, 0, 0, 0, 0, 0, '3', '80', '10', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 0, '2017-09-15'),
(2, 2, 1, 1, '2017-09-15'),
(3, 3, 1, 0, '2017-09-15'),
(4, 4, 1, 1, '2017-09-15'),
(5, 5, 3, 1, '2017-09-15'),
(6, 6, 3, 1, '2017-09-15'),
(7, 7, 3, 1, '2017-09-15'),
(8, 8, 3, 1, '2017-09-15'),
(9, 9, 3, 1, '2017-09-15'),
(10, 10, 4, 0, '2017-09-16'),
(11, 11, 4, 1, '2017-09-16'),
(12, 12, 4, 1, '2017-09-16'),
(13, 13, 4, 1, '2017-09-16'),
(14, 14, 4, 1, '2017-09-16'),
(15, 15, 1, 0, '2017-09-16'),
(16, 16, 1, 0, '2017-09-16'),
(17, 17, 1, 1, '2017-09-16'),
(18, 18, 4, 1, '2017-09-16'),
(19, 19, 1, 0, '2017-09-16'),
(20, 20, 4, 1, '2017-09-16'),
(21, 21, 1, 1, '2017-09-16'),
(22, 22, 1, 1, '2017-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 1, 1),
(2, 15, 1),
(3, 16, 1),
(4, 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `user_message` varchar(6000) NOT NULL,
  `ride_estimated` varchar(255) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `user_message`, `ride_estimated`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 2, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 15', '14:11:12', '02:11:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, '', '', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(2, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 15', '14:52:09', '02:52:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, '', '115.44', 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-15'),
(3, 2, '', '28.4120296942791', '77.0432630920238', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4077139345661', '77.0341935753822', 'Dhani, Sector 72\nGurugram, Haryana', 'Friday, Sep 15', '15:02:25', '03:02:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120296942791,77.0432630920238&markers=color:red|label:D|28.4077139345661,77.0341935753822&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, '', '', 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(4, 3, '', '28.412382157181252', '77.04343881458044', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Sep 15', '15:24:44', '03:25:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412382157181252,77.04343881458044&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, '', '115.62', 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-15'),
(5, 10, '', '15.531175958677034', '32.586626037955284', 'Unnamed Road, Khartoum, Sudan', '', '', 'Set your drop point', 'Friday, Sep 15', '18:27:05', '06:27:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|15.531175958677034,32.586626037955284&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, '', '', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(6, 10, '', '15.531175958677034', '32.586626037955284', 'Unnamed Road, Khartoum, Sudan', '15.53815982432583', '32.57676590234041', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø¬ÙŠÙ„ÙŠØŒ Al Khurtum, Sudan', 'Friday, Sep 15', '18:27:52', '06:27:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|15.531175958677034,32.586626037955284&markers=color:red|label:D|15.53815982432583,32.57676590234041&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, '', '100.00', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(7, 10, '', '15.531175958677034', '32.586626037955284', 'Unnamed Road, Khartoum, Sudan', '15.53815982432583', '32.57676590234041', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø¬ÙŠÙ„ÙŠØŒ Al Khurtum, Sudan', 'Friday, Sep 15', '18:29:50', '06:29:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|15.531175958677034,32.586626037955284&markers=color:red|label:D|15.53815982432583,32.57676590234041&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, '', '100.00', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(8, 10, '', '15.531175958677034', '32.586626037955284', 'Unnamed Road, Khartoum, Sudan', '15.53815982432583', '32.57676590234041', 'Ø´Ø§Ø±Ø¹ Ø§Ù„Ø¬ÙŠÙ„ÙŠØŒ Al Khurtum, Sudan', 'Friday, Sep 15', '18:30:09', '06:30:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|15.531175958677034,32.586626037955284&markers=color:red|label:D|15.53815982432583,32.57676590234041&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, '', '100.00', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-15'),
(9, 4, '', '15.531172082275177', '32.58653551340103', 'Unnamed Road, Khartoum, Sudan', '', '', 'Set your drop point', 'Friday, Sep 15', '18:34:38', '06:38:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|15.531172082275177,32.58653551340103&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 2, 1, '', '', 1, 9, 0, 8, 1, 0, 1, 1, '2017-09-15'),
(10, 6, '', '28.4120769350201', '77.0431656623177', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '09:07:04', '09:07:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120769350201,77.0431656623177&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 'Hii', '114.40', 2, 1, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(11, 6, '', '28.4122456227003', '77.0434592664242', '322B, Sohna Road, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '09:43:25', '09:44:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122456227003,77.0434592664242&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, '', '114.02', 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(12, 6, '', '28.4121503728393', '77.0431903749704', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '09:48:28', '09:49:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121503728393,77.0431903749704&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 'Hhhh', '114.46', 2, 9, 0, 8, 1, 0, 1, 1, '2017-09-16'),
(13, 6, '', '28.4120393419758', '77.04327072145', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '10:07:04', '10:07:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120393419758,77.04327072145&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, '', '114.23', 2, 2, 0, 3, 1, 0, 1, 1, '2017-09-16'),
(14, 6, '', '28.4121287762364', '77.0432274120011', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '10:07:55', '10:08:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121287762364,77.0432274120011&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, '', '114.46', 2, 2, 0, 3, 1, 0, 1, 1, '2017-09-16'),
(15, 3, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.524578699999996', '77.206615', 'Saket', 'Saturday, Sep 16', '10:38:55', '10:38:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.524578699999996,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, '', '332.48', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(16, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:02:09', '01:02:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 'godd care', '115.44', 1, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(17, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:14:24', '01:18:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 'gracias', '115.44', 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(18, 9, '', '28.4122176997529', '77.0432277850019', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4151240153954', '77.0526263862848', '2, South City Road, Block K, South City II, Sector 49\nGurugram, Haryana 122003', 'Saturday, Sep 16', '13:28:08', '01:29:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122176997529,77.0432277850019&markers=color:red|label:D|28.4151240153954,77.0526263862848&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 'GaghahbababHbabab', '100.00', 2, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(19, 9, '', '28.4120968217295', '77.0432943944605', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089130062013', '77.0244242995977', 'Unnamed Road, Behrampur Village, Sector 71\nGurugram, Haryana 122004', 'Saturday, Sep 16', '13:30:14', '01:30:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120968217295,77.0432943944605&markers=color:red|label:D|28.4089130062013,77.0244242995977&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 'Ganahabbanah', '100.00', 2, 2, 0, 0, 1, 0, 1, 1, '2017-09-16'),
(20, 9, '', '28.4120968217295', '77.0432943944605', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4089130062013', '77.0244242995977', 'Unnamed Road, Behrampur Village, Sector 71\nGurugram, Haryana 122004', 'Saturday, Sep 16', '13:30:30', '02:27:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120968217295,77.0432943944605&markers=color:red|label:D|28.4089130062013,77.0244242995977&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 'BabbBabanabBsb', '100.00', 2, 2, 0, 3, 1, 0, 1, 1, '2017-09-16'),
(21, 7, '', '28.41218546490328', '77.04312231391668', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:51:02', '01:51:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41218546490328,77.04312231391668&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, '', '115.44', 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16'),
(22, 7, '', '28.412079304001782', '77.0434146746993', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Sep 16', '13:52:15', '01:52:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412079304001782,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 'bkjv', '114.97', 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'ggg');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(1, 3, 1, 1),
(2, 3, 2, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(8, 120, 1, 1),
(9, 120, 2, 1),
(10, 120, 3, 1),
(11, 120, 4, 1),
(12, 120, 5, 1),
(13, 120, 6, 1),
(14, 120, 7, 1),
(15, 120, 8, 1),
(16, 120, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-09-15 00.00.01 AM', '2017-09-15 02:29:13 PM', 1, '0', '0000-00-00', 0),
(2, '2017-09-15 02:30:13 PM', '2017-09-15 02:31:32 PM', 1, '0', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1505480806document_image_13.jpg', '6-9-2018', 2),
(2, 1, 2, 'uploads/driver/1505480819document_image_12.jpg', '13-4-2018', 2),
(3, 1, 4, 'uploads/driver/1505480834document_image_14.jpg', '15-2-2019', 2),
(4, 2, 3, 'uploads/driver/1505489144document_image_23.jpg', '2017-09-15', 2),
(5, 2, 2, 'uploads/driver/1505489152document_image_22.jpg', '2017-09-15', 2),
(6, 2, 4, 'uploads/driver/1505489160document_image_24.jpg', '2017-09-15', 2),
(7, 3, 1, 'uploads/driver/1505495902document_image_31.jpg', '15-9-2017', 2),
(8, 3, 2, 'uploads/driver/1505495914document_image_32.jpg', '22-9-2017', 2),
(9, 3, 3, 'uploads/driver/1505495926document_image_33.jpg', '15-9-2017', 2),
(10, 3, 4, 'uploads/driver/1505495939document_image_34.jpg', '15-9-2017', 2),
(11, 3, 5, 'uploads/driver/1505495950document_image_35.jpg', '23-9-2017', 2),
(12, 3, 6, 'uploads/driver/1505495962document_image_36.jpg', '15-9-2017', 2),
(13, 3, 7, 'uploads/driver/1505495972document_image_37.jpg', '22-9-2017', 2),
(14, 3, 8, 'uploads/driver/1505495984document_image_38.jpg', '15-9-2017', 2),
(15, 3, 10, 'uploads/driver/1505495997document_image_310.jpg', '15-9-2017', 2),
(16, 4, 3, 'uploads/driver/1505548764document_image_43.jpg', '2017-12-20', 2),
(17, 4, 2, 'uploads/driver/1505548797document_image_42.jpg', '2017-11-24', 2),
(18, 4, 4, 'uploads/driver/1505548810document_image_44.jpg', '2017-12-26', 2),
(19, 5, 3, 'uploads/driver/1505576696document_image_53.jpg', '21-9-2018', 1),
(20, 5, 2, 'uploads/driver/1505576707document_image_52.jpg', '29-9-2017', 1),
(21, 5, 4, 'uploads/driver/1505576720document_image_54.jpg', '29-9-2017', 1),
(22, 6, 3, 'uploads/driver/1505578513document_image_63.jpg', '16-9-2022', 1),
(23, 6, 2, 'uploads/driver/1505578545document_image_62.jpg', '29-9-2017', 1),
(24, 6, 4, 'uploads/driver/1505578557document_image_64.jpg', '29-9-2017', 1),
(25, 7, 3, 'uploads/driver/1505578857document_image_73.jpg', '16-9-2021', 1),
(26, 8, 3, 'uploads/driver/1505579108document_image_83.jpg', '29-9-2017', 1),
(27, 8, 2, 'uploads/driver/1505579121document_image_82.jpg', '29-9-2017', 1),
(28, 8, 4, 'uploads/driver/1505579132document_image_84.jpg', '29-9-2017', 1),
(29, 16, 3, 'uploads/driver/1505989732document_image_163.jpg', '21-2-2019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-15 14:10:37', '', '', 0, 0, '2017-09-15'),
(2, 2, '2017-09-15 17:11:02', '2017-09-15 17:11:29', '0 Hours 0 Minutes', 0, 0, '2017-09-15'),
(3, 3, '2017-09-15 18:26:14', '', '', 0, 0, '2017-09-15'),
(4, 4, '2017-09-16 09:03:18', '', '', 0, 0, '2017-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 2, 3, 3.5, 'vh', 1, 4.5, '', '2017-09-15'),
(2, 1, 3, 3.5, '', 1, 0, '', '2017-09-15'),
(3, 4, 3, 0, '', 1, 3, '', '2017-09-15'),
(4, 11, 6, 4, '', 4, 4.5, '', '2017-09-16'),
(5, 17, 7, 0, '', 1, 4.5, '', '2017-09-16'),
(6, 5, 7, 3.5, '', 1, 0, '', '2017-09-16'),
(7, 18, 9, 0, '', 4, 5, '', '2017-09-16'),
(8, 21, 7, 0, '', 1, 4.5, '', '2017-09-16'),
(9, 22, 7, 0, '', 1, 4, '', '2017-09-16'),
(10, 8, 7, 4, '', 1, 0, '', '2017-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 2, 0, 1),
(2, 1, 3, 1, 2),
(3, 1, 2, 0, 3),
(4, 1, 3, 1, 4),
(5, 1, 10, 3, 5),
(6, 1, 10, 3, 6),
(7, 1, 10, 3, 7),
(8, 1, 10, 3, 8),
(9, 1, 4, 3, 9),
(10, 1, 6, 4, 11),
(11, 1, 6, 4, 12),
(12, 1, 6, 4, 13),
(13, 1, 6, 4, 14),
(14, 1, 3, 0, 15),
(15, 1, 7, 0, 16),
(16, 1, 7, 1, 17),
(17, 1, 9, 4, 18),
(18, 1, 9, 0, 19),
(19, 1, 9, 4, 20),
(20, 1, 7, 1, 21),
(21, 1, 7, 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `user_gender` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `user_gender`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'vishal garg .', 'vishal@gmail.com', '+918950368358', '123456', '', '', '', 0, '0', 'Thursday, Sep 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-14', 1),
(2, 1, 'mfmf .', 'bk@gmail.com', '+919868698989', 'qbdjf', '', 'Female', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-15', 1),
(3, 1, 'man .', 'av@gmail.com', '+918285469069', '12345', 'http://apporio.org/tawseala/tawseala/uploads/1505559708536.jpg', 'Male', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '2.5', 0, '', 1, '2017-09-15', 1),
(4, 1, 'mohamed .', 'mohalwali88@gmail.com', '+249911818056', '123456', '', 'Male', '', 0, '0', 'Friday, Sep 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-15', 1),
(5, 1, 'Nikita .', 'nikita@apporio.com', '+919205361756', 'qwerty', '', '3', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-09-16', 1),
(6, 1, 'qwwe .', 'qwer@gmail.com', '+911234512345', 'asdfghjkl', '', '1', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.5', 0, '', 1, '2017-09-16', 1),
(7, 1, 'anurag .', 'anu@gmail.com', '+918285469011', '12345', '', 'Male', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.25', 0, '', 1, '2017-09-16', 1),
(8, 1, 'name .', 'name@gmail.com', '+911472583691', 'asdfghjkl', '', '23', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-09-16', 1),
(9, 1, 'enter .', 'enter@gmail.com', '+911234561234', 'zxcvbnm', '', '1', '', 0, '0', 'Saturday, Sep 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '2017-09-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'd090LatIv5A:APA91bEH_ub2yuOUnvIfAD2-LWDHaGsWu0e-eRVb0Xj5utZbOFKIDGCBIjlz_q_mxuAiKkGY-uVzl0k8JY8jU73VCxIfi4a2kZb5AZfEnAjrFZsWJx_3X_g7ly-4INcz-l6BLN37qlf7', 0, 'c06c40037632ed39', 1),
(2, 7, 'c-HuesVH0UA:APA91bFAJc_6QkPFbWxuMLwRMSpkHHa0-Y3GxTzWC7i7L_bNhp4Vsa4ODZx7j0OgEfJKmuv4wY6xAQG_8P6RF6Y5i9kEvvXziU2H1D_8z8Ks65y98ojSP6OeAm_PhD2eHgW54ao70R3d', 0, '32c87e564d926edc', 0),
(3, 8, 'CD689085CFA8F86E1AE315C31CECF7160DAF8AC3AA9C0ADF393A66CD973AD5FC', 1, '51F995B7-8F48-4E7D-BDE5-4528956975C5', 1),
(4, 4, 'fU5M3e3CXiQ:APA91bEQ_crgWYgcL-DpXV-H7L8qCHST6qYtec-iuEu0KUd0mDuXJYpkmmRzXcjkF4L5WOTRp4mTlVmw3KrkDV4704loNKOruHAgYAGoZHKGHamFf69jghLxbNkONC_2f5HlPvxdKhCT', 0, '6d55756ced76ed90', 1),
(5, 9, '1DF50A84BD4943E2953364CE384B1E2D2FFE64FED9E459A3F451469007D66807', 1, '48C0A158-2AC2-4D85-B691-28C023A46993', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
