-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_taxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_car_model`
--

CREATE TABLE `table_car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_model_description` longtext NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_car_model`
--

INSERT INTO `table_car_model` (`car_model_id`, `car_type_id`, `car_model_name`, `car_model_image`, `car_model_description`, `car_model_status`) VALUES
(1, 1, 'Creta', '', '', 1),
(2, 1, 'Nano', '', '', 1),
(3, 3, 'Audi Q7', '', '', 1),
(4, 1, 'Alto', '', '', 1),
(5, 2, 'Korola', '', '', 1),
(6, 3, 'BMW 350', '', '', 1),
(7, 2, 'TATA', '', '', 1),
(8, 1, 'Eco Sport', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_car_type`
--

CREATE TABLE `table_car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `car_type_description` varchar(255) NOT NULL,
  `car_type_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_car_type`
--

INSERT INTO `table_car_type` (`car_type_id`, `car_type_name`, `car_type_image`, `car_type_description`, `car_type_admin_status`) VALUES
(1, 'Basic', 'uploads/car_1.png', 'Basic', 1),
(2, 'Normal', 'uploads/car_1.png', 'Normal', 1),
(3, 'Luxury', 'uploads/car_1.png', 'Luxury', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_city`
--

CREATE TABLE `table_city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) NOT NULL,
  `city_longitude` varchar(255) NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_city`
--

INSERT INTO `table_city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `city_admin_status`) VALUES
(1, 'Gurugram', '28.4595', '77.0266', 1),
(2, 'Delhi', '28.7041', '77.1025', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `car_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `car_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`car_document_id`, `city_id`, `car_type_id`, `document_id`, `car_document_status`) VALUES
(1, 1, 1, 1, 1),
(2, 2, 2, 2, 1),
(3, 1, 1, 3, 1),
(4, 1, 1, 2, 1),
(5, 1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver`
--

CREATE TABLE `table_driver` (
  `driver_id` int(11) NOT NULL,
  `driver_firstname` varchar(255) NOT NULL,
  `driver_lastname` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_profile_image` varchar(255) NOT NULL,
  `driver_city_id` int(11) NOT NULL,
  `driver_car_type_id` int(11) NOT NULL,
  `driver_car_model_id` int(11) NOT NULL,
  `driver_car_number` varchar(255) NOT NULL,
  `driver_car_model_date` varchar(255) NOT NULL,
  `driver_car_image` text NOT NULL,
  `driver_car_number_plate_image` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `driver_bank_account_holder_number` varchar(255) NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_bank_ifsc_code` varchar(255) NOT NULL,
  `driver_online_offline` int(11) NOT NULL DEFAULT '2',
  `driver_last_online` varchar(255) NOT NULL,
  `driver_last_offline` varchar(255) NOT NULL,
  `driver_device_id` varchar(255) NOT NULL,
  `driver_device_flag` varchar(255) NOT NULL,
  `driver_phone_info` varchar(255) NOT NULL,
  `driver_token` varchar(255) NOT NULL,
  `driver_signup_date` varchar(255) NOT NULL,
  `driver_last_login` varchar(255) NOT NULL,
  `driver_login_logout` varchar(255) NOT NULL,
  `signup_step` int(11) NOT NULL DEFAULT '1',
  `driver_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `documnet_varification_status`) VALUES
(23, 38, 2, 'http://apporioinfolabs.com/Apporiotaxi_new/uploads/1498111946618.jpg', 1),
(22, 38, 1, 'http://apporioinfolabs.com/Apporiotaxi_new/uploads/1498111938039.jpg', 1),
(21, 38, 4, 'http://apporioinfolabs.com/Apporiotaxi_new/uploads/1498111926526.jpg', 1),
(20, 38, 3, 'http://apporioinfolabs.com/Apporiotaxi_new/uploads/1498111909742.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_fare`
--

CREATE TABLE `table_fare` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_fare`
--

INSERT INTO `table_fare` (`price_id`, `city_id`, `car_type_id`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(4, 3, 1, 0, 0, 0, 0, 0, 0, '4', '100', '30', '1', '10', '1', '10'),
(3, 56, 1, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(5, 3, 2, 0, 0, 0, 0, 0, 0, '4', '75', '20', '3', '12', '2', '15'),
(6, 3, 3, 0, 0, 0, 0, 0, 0, '1', '30', '1', '4', '1', '1', '1'),
(7, 3, 4, 0, 0, 0, 0, 0, 0, '4', '200', '40', '2', '15', '2', '13'),
(8, 3, 5, 0, 0, 0, 0, 0, 0, '5', '75', '14', '1', '11', '1', '15'),
(19, 57, 1, 0, 0, 0, 0, 0, 0, '1', '200', '2', '1', '2', '1', '1'),
(12, 56, 2, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 3, 0, 0, 0, 0, 0, 0, '4', '80', '16', '1', '10', '1', '15'),
(14, 56, 4, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 5, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(21, 71, 2, 0, 0, 0, 0, 0, 0, '100', '4', '10', '5', '1', '5', '5'),
(22, 57, 3, 10, 15, 10, 5, 10, 30, '4', '100', '10', '5', '1', '5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_booking`
--

CREATE TABLE `table_rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_booking_allocated`
--

CREATE TABLE `table_rental_booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_category`
--

CREATE TABLE `table_rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_rental_category`
--

INSERT INTO `table_rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_admin_status`) VALUES
(1, 'Apporio', '2', '40', 1),
(2, '1 Hour 10 km', '1', '10', 1),
(3, '2 Hour 20 km', '2', '20', 1),
(4, '4 Hour 40 km', '4', '40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_rentcard`
--

CREATE TABLE `table_rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_rentcard`
--

INSERT INTO `table_rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `rentcard_admin_status`) VALUES
(1, 56, 2, 1, '40', 1),
(2, 56, 3, 2, '423', 1),
(3, 56, 5, 2, '330', 1),
(4, 56, 2, 3, '221', 1),
(5, 56, 3, 1, '20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE `table_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_car_model`
--
ALTER TABLE `table_car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `table_car_type`
--
ALTER TABLE `table_car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `table_city`
--
ALTER TABLE `table_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`car_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver`
--
ALTER TABLE `table_driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_fare`
--
ALTER TABLE `table_fare`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `table_rental_booking`
--
ALTER TABLE `table_rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `table_rental_booking_allocated`
--
ALTER TABLE `table_rental_booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `table_rental_category`
--
ALTER TABLE `table_rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `table_rentcard`
--
ALTER TABLE `table_rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `table_user`
--
ALTER TABLE `table_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_car_model`
--
ALTER TABLE `table_car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `table_car_type`
--
ALTER TABLE `table_car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `table_city`
--
ALTER TABLE `table_city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `car_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver`
--
ALTER TABLE `table_driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `table_fare`
--
ALTER TABLE `table_fare`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `table_rental_booking`
--
ALTER TABLE `table_rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_booking_allocated`
--
ALTER TABLE `table_rental_booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_category`
--
ALTER TABLE `table_rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table_rentcard`
--
ALTER TABLE `table_rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_user`
--
ALTER TABLE `table_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
