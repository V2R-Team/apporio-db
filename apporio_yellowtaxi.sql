-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_yellowtaxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'rsdcdulax', 'rsdcdulax', 'rsdcdulax', '', '', 'rsdcdulax', '+56982428020', 'rsdcdulax@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(33, 'jesus ', 'pajares', 'jph', '', '', 'soloyolose', '992233551', 'jesuspajareshorna@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(34, 'alvin', 'wong', 'alvin123', '', '', '123456', '123123123123', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(35, 'alvin', 'wong', 'alvindispatcher', '', '', '123456', '1293012937', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(36, 'ANURAG', 'UPADHYAY', 'anurag@apporio.com', '', '', 'qwerty', '8874531856', 'anurag@apporio.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'YellowTaxi', 'uploads/logo/logo_5a01997f303be.jpg', 'yellowtaxibcn@gmail.com', 'Barcelona, EspaÃ±a', 'AIzaSyCmKRLpo7Rb2v4ujYwdoKwm3jZaPzlEEE0', '41.3850639', '2.1734035', 'yellow-181307');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1),
(13, 'yamaha', 'uploads/car/car_13.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1),
(43, 'a2', '', '', 'Mercedes', '', '', '', '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `cartype_big_image` varchar(255) NOT NULL DEFAULT '',
  `car_seats` int(21) NOT NULL DEFAULT '0',
  `car_longdescription` text CHARACTER SET utf8 NOT NULL,
  `car_longdescription_arabic` text CHARACTER SET utf8 NOT NULL,
  `car_description` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `car_description_arabic` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `cartype_big_image`, `car_seats`, `car_longdescription`, `car_longdescription_arabic`, `car_description`, `car_description_arabic`, `ride_mode`, `car_admin_status`) VALUES
(2, '4-Seater', '4 مقاعد', '4-Seater', 'uploads/car/editcar_2.png', '', 'webstatic/img/fleet-image/Yellow4.png', 4, 'The all too familiar 4-Seater, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'هاتشباك مألوفة جدا، ناقص الازعاج من الانتظار والمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Get an 4-Seater at your doorstep', 'الحصول على هاتشباك على عتبة داركم', 1, 1),
(3, '6-Seater', '6 مقاعد', '6-Seater', 'uploads/car/editcar_3.png', '', 'webstatic/img/fleet-image/Yellow6.png', 6, 'The all too familiar 6-Seater, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'لوكسوري مألوفة جدا، ناقص الازعاج من الانتظاروالمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Get an 6-Seater at your doorstep', 'الحصول على لوكسوري على عتبةداركم', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `currency_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 0, 'Gurugram', '', '', '&#8364;', 'EUR', '20AC', 'Miles', '', '', 1),
(128, 7, 'Barcelona', '41.3850639', '2.1734035', '&#8364;', 'EUR', '20AC', 'Km', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 30, 'k10', 1, 2, 1, 2, 'k10', 'Apporio Infolabs', 'k10', 'apporio@info.com2', '', 'k10', '', 'k10', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-30', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'Manual', '10', 10, 10, '2017-09-19', '2017-09-22', 'Nominal', 1),
(109, '213131', '10', 200, 0, 'NaN-NaN-NaN', 'NaN-NaN-NaN', 'Nominal', 1),
(110, 'p898', '100', 1, 1, '2017-09-25', '2017-09-25', 'Percentage', 1),
(111, '1514', '16', 0, 0, 'NaN-NaN-NaN', 'NaN-NaN-NaN', 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD '),
(7, 21, '&#8364;', '20AC', 'EUR');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `tip` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` float NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL DEFAULT '0.00',
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `tip`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 5, '28.4123421', '77.0434543', '28.4123424', '77.0434565', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '04:47:29 AM', '04:47:35 AM', '04:47:45 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 9, '500,55', '495.00', '5.00', 0, '0.00', '0.0', '', '', '500,55', 1, '', 1, 0, '0000-00-00'),
(2, 19, '28.4121000409151', '77.0433194566294', '28.4120343265204', '77.0432652499673', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:49:34 AM', '05:49:38 AM', '05:51:29 AM', '0', '0', '0,77 ', '0.00', '0.00', '0.00', '0.00', 14, '0.77', '0.00', '0.77', 0, '0.00', '0.0', '', '', '0.77', 1, '', 0, 1, '0000-00-00'),
(3, 24, '', '', '', '', '', '', '05:56:18 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 11, '0', '0', '0', 0, '0.00', '0.00', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(4, 26, '28.4120658554429', '77.0432638670493', '28.4072565379486', '77.0500035211444', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Adarsh St, Block W, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '06:04:54 AM', '06:05:00 AM', '06:05:09 AM', '0', '0', '0,77 ', '0.00', '0.00', '0.00', '0.00', 14, '0.77', '0.00', '0.77', 0, '0.00', '0.0', '', '', '0.77', 1, '', 0, 1, '0000-00-00'),
(5, 27, '41.4166776515613', '2.21517466482394', '41.4169258193447', '2.21522651664015', 'Carrer de Pere Moragues, 9, 08019 Barcelona, Spain', 'Carrer de Pere Moragues, 9, 08019 Barcelona, Spain', '08:32:55 AM', '08:33:00 AM', '08:33:12 AM', '0', '0', '45,28', '0.00', '0.00', '0.00', '0.00', 4, '45.28', '45.28', '0.00', 0, '0.00', '0.0', '', '', '45.28', 1, '', 1, 1, '0000-00-00'),
(6, 29, '41.4168029141587', '2.21524328110806', '41.4167794265686', '2.21532201026825', 'Carrer de Trapani, 7, 08019 Barcelona, Spain', 'Carrer de Trapani, 7, 08019 Barcelona, Spain', '08:40:02 AM', '08:40:08 AM', '08:40:23 AM', '0', '0', '7,45', '0.00', '0.00', '0.00', '0.00', 6, '7.45', '7.45', '0.00', 0, '0.00', '0.0', '', '', '7.45', 1, '', 1, 1, '0000-00-00'),
(7, 30, '', '', '', '', '', '', '09:31:45 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 4, '0', '50.00', '0.00', 0, '0.00', '0.00', '', '', '0.00', 1, '', 1, 1, '0000-00-00'),
(8, 31, '', '', '', '', '', '', '09:34:42 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 4, '0', '85.00', '0.00', 0, '0.00', '0.00', '', '', '0.00', 1, '', 1, 1, '0000-00-00'),
(9, 33, '', '', '', '', '', '', '09:40:26 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 6, '0', '69.00', '0.00', 0, '0.00', '0.00', '', '', '0.00', 1, '', 1, 1, '0000-00-00'),
(10, 35, '28.4123411', '77.0434545', '28.4123501', '77.0434378', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:45:31 AM', '09:45:35 AM', '09:46:06 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 11, '85,50', '81.00', '4.00', 0, '0.00', '0.0', '', '', '85,50', 1, '', 0, 0, '0000-00-00'),
(11, 36, '28.412354', '77.0434377', '28.4123454', '77.0434386', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:47:25 AM', '09:47:32 AM', '09:47:50 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 11, '85,50', '81.00', '4.00', 0, '0.00', '0.0', '', '', '85,50', 1, '', 1, 0, '0000-00-00'),
(12, 37, '28.4123394', '77.0434496', '28.412339', '77.0434464', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:54:55 AM', '09:55:04 AM', '09:55:20 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 11, '85,50', '81.00', '4.00', 0, '0.00', '0.0', '', '', '85,50', 1, '', 1, 0, '0000-00-00'),
(13, 38, '28.4123379', '77.0434458', '28.4123362', '77.043434', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '10:01:21 AM', '10:01:30 AM', '10:01:55 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 11, '85,50', '81.00', '4.00', 0, '0.00', '0.0', '', '', '85,50', 1, '', 1, 0, '0000-00-00'),
(14, 43, '41.4273039482032', '2.22637076501386', '41.4284573396309', '2.2273657991719', 'Carrer de la Torrassa, 84, 08930 Sant Adrià de Besòs, Barcelona, Spain', 'Carrer de la Torrassa, 84-88, 08930 Sant Adrià de Besòs, Barcelona, Spain', '11:34:28 AM', '11:34:39 AM', '11:34:59 AM', '0', '0', '52,80', '0.00', '0.00', '0.00', '0.00', 4, '52.80', '52.80', '0.00', 0, '0.00', '0.0', '', '', '52.80', 1, '', 1, 1, '0000-00-00'),
(15, 46, '41.4352378', '2.2372851', '41.4352241', '2.2372892', 'Carrer de la Indústria, 427, 08918 Badalona, Barcelona, Spain', 'Carrer de la Indústria, 427, 08918 Badalona, Barcelona, Spain', '12:04:20 PM', '12:04:24 PM', '12:04:34 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '85,85', '85.00', '0.00', 0, '0.00', '0.0', '', '', '85,85', 1, '', 1, 1, '0000-00-00'),
(16, 48, '41.4353065', '2.2373071', '41.4353065', '2.2373071', 'Carrer de la Indústria, 427, 08918 Badalona, Barcelona, Spain', 'Carrer de la Indústria, 427, 08918 Badalona, Barcelona, Spain', '12:06:38 PM', '12:06:42 PM', '12:06:53 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 6, '863,25', '863.00', '0.00', 0, '0.00', '0.0', '', '', '863,25', 1, '', 1, 1, '0000-00-00'),
(17, 49, '', '', '', '', '', '', '05:21:52 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 4, '0', '12.00', '0.00', 0, '0.00', '0.00', '', '', '0.00', 1, '', 1, 1, '0000-00-00'),
(18, 51, '41.4167456263286', '2.21536147346826', '41.4167246598394', '2.21546210352236', 'Carrer de Trapani, 7, 08019 Barcelona, Spain', 'Carrer de Trapani, 7, 08019 Barcelona, Spain', '08:25:43 AM', '08:25:51 AM', '08:26:06 AM', '0', '0', '52,80', '0.00', '0.00', '0.00', '0.00', 6, '52.80', '52.80', '0.00', 0, '0.00', '0.0', '', '', '52.80', 1, '', 1, 0, '0000-00-00'),
(19, 52, '41.416956', '2.2158202', '41.416926', '2.2158126', 'Carrer Galba, 2, 08019 Barcelona, Spain', 'Carrer Galba, 26, 08019 Barcelona, Spain', '08:28:44 AM', '08:28:57 AM', '08:29:28 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '74,55', '74.00', '0.00', 0, '0.00', '0.0', '', '', '74,55', 1, '', 1, 0, '0000-00-00'),
(20, 55, '41.3016804', '2.0719253', '41.3997704', '2.2103489', 'C-32B, 18, 08820 El Prat de Llobregat, Barcelona, Spain', 'Av. del Litoral, 86, 08005 Barcelona, Spain', '06:51:07 PM', '06:51:10 PM', '07:26:31 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '56,85', '56.00', '0.00', 0, '0.00', '18160.0', '', '', '56,85', 1, '', 1, 1, '0000-00-00'),
(21, 56, '41.407639893638', '2.21597785504546', '', '', 'Passeig del Taulat, 283, 08019 Barcelona, Spain', '', '07:45:14 PM', '07:46:09 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 4, '0', '0', '0', 0, '0.00', '0.00', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(22, 57, '28.4122073069629', '77.0432204508187', '28.4121756087983', '77.0432463735602', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '05:09:52 AM', '05:09:57 AM', '05:11:21 AM', '0', '0', ' 0.55', '0.00', '0.00', '0.00', '0.00', 14, '0.55', '0.00', '0.55', 0, '0.00', '0.0', '', '', '0.55', 1, '', 0, 1, '0000-00-00'),
(23, 59, '28.4122341214958', '77.0432597071509', '28.4124140053718', '77.0525586605072', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'B50, Rosewood City, Ghasola, Sector 49, Gurugram, Haryana 122018, India', '09:25:26 AM', '09:25:30 AM', '09:25:41 AM', '0', '0', '0,77 ', '0.00', '0.00', '0.00', '0.00', 14, '0.77', '0.00', '0.77', 0, '0.00', '0.0', '', '', '0.77', 1, '', 0, 1, '0000-00-00'),
(24, 61, '28.4121256864026', '77.0433252914421', '26.4115421387624', '80.4096456617117', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Greater Kailash Avenue, Greater Kailash, Jajmau, Kanpur, Uttar Pradesh 208008, India', '09:46:12 AM', '09:46:29 AM', '09:47:00 AM', '0', '0', '89,50 ', '0.00', '0.00', '0.00', '0.00', 18, '89.50', '85.50', '4.00', 0, '0.00', '0.0', '', '', '89.50', 1, '', 1, 1, '0000-00-00'),
(25, 67, '41.2883898', '2.0727636', '41.4172992', '1.9977297', 'Av. les Garrigues, 1-3, 08820 El Prat de Llobregat, Barcelona, Spain', 'Carrer Riera del Mig, 5, 08780 Pallejà, Barcelona, Spain', '03:28:07 PM', '03:28:38 PM', '03:56:50 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 6, '45,45', '45.00', '0.00', 0, '0.00', '22790.0', '', '', '45,45', 1, '', 1, 1, '0000-00-00'),
(26, 68, '41.4166769407665', '2.21524735569082', '41.4166604657952', '2.21528278318384', 'Carrer de Trapani, 2, 08019 Barcelona, Spain', 'Carrer de Trapani, 2, 08019 Barcelona, Spain', '12:45:19 AM', '12:45:23 AM', '12:47:29 AM', '0', '0', '10,52', '0.00', '0.00', '0.00', '0.00', 6, '10.52', '10.52', '0.00', 0, '0.00', '0.300398502506221', '', '', '10.52', 1, '', 1, 1, '0000-00-00'),
(27, 34, '41.4166855505263', '2.21528091491652', '', '', 'Carrer de Trapani, 2, 08019 Barcelona, Spain', '', '12:55:59 AM', '12:56:03 AM', '', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', 6, '0', '1.45', '0.00', 0, '0.00', '0.00', '', '', '0.00', 1, '', 1, 1, '0000-00-00'),
(28, 70, '28.4121141202053', '77.0433121491643', '28.4121116570187', '77.0433284825168', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:03:14 AM', '07:03:18 AM', '07:05:31 AM', '0', '0', ' 0.66', '0.00', '0.00', '0.00', '0.00', 18, '0.66', '0.00', '0.66', 0, '0.00', '0.0', '', '', '0.66', 1, '', 0, 1, '0000-00-00'),
(29, 72, '28.4120961967422', '77.0433069771767', '28.4109610693757', '77.0480844005942', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '5, Block C Uppal Southland St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:31:07 AM', '07:31:10 AM', '07:31:19 AM', '0', '0', ' 0.76', '0.00', '0.00', '0.00', '0.00', 18, '0.76', '0.00', '0.76', 0, '0.00', '10.00', '', '', '0.76', 1, '', 1, 1, '0000-00-00'),
(30, 73, '28.4121080707032', '77.0433145653355', '0.0', '0.0', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Address Not found', '07:36:37 AM', '07:36:41 AM', '07:36:49 AM', '0', '0', ' 0.67', '0.00', '0.00', '0.00', '0.00', 18, '0.67', '0.00', '0.67', 0, '0.00', '10.34658234658236', '', '', '0.67', 1, '', 1, 1, '0000-00-00'),
(31, 74, '41.4155118741388', '2.21624301653107', '41.3750490455184', '2.16852781489082', 'Carrer Joan de Palomar, 1, 08019 Barcelona, Spain', 'Av. del Para?lel, 76, 08001 Barcelona, Spain', '10:13:46 AM', '10:13:54 AM', '10:35:39 AM', '0', '0', '15,20', '0.00', '0.00', '0.00', '0.00', 6, '15.20', '15.20', '0.00', 0, '0.00', '6.76294259649574', '', '', '15.20', 1, '', 1, 0, '0000-00-00'),
(32, 28, '41.4166858497443', '2.21519042470568', '41.4166891265945', '2.215306750621', 'Carrer de Pere Moragues, 9, 08019 Barcelona, Spain', 'Carrer de Trapani, 2, 08019 Barcelona, Spain', '12:06:52 PM', '12:07:09 PM', '12:07:37 PM', '0', '0', '45,75', '0.00', '0.00', '0.00', '0.00', 6, '45.75', '45.75', '0.00', 0, '0.00', '0.0', '', '', '45.75', 1, '', 1, 1, '0000-00-00'),
(33, 77, '', '', '41.4268433', '2.2290646', '', 'Carrer de Ramon Viñas, 8, 08930 Sant Adrià de Besòs, Barcelona, Spain', '11:04:56 AM', '', '11:12:46 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '14,75', '14.00', '0.00', 0, '0.00', '2120.0', '', '', '14,75', 1, '', 1, 1, '0000-00-00'),
(34, 78, '28.4121556009743', '77.043299753602', '28.4160198663519', '77.0509188249707', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'D-Block, South City II, Sector 49, Gurugram, Haryana 122018, India', '12:33:54 PM', '12:33:59 PM', '12:34:09 PM', '0', '0', ' 5.66', '0.00', '0.00', '0.00', '0.00', 18, '5.66', '1.66', '4.00', 0, '0.00', '10.34658234658236', '', '', '5.66', 1, '', 0, 0, '0000-00-00'),
(35, 81, '41.3847802', '2.1775682', '41.3757346', '2.1781843', 'Via Laietana, 29, 08003 Barcelona, Spain', 'Ronda Litoral, 6, 08039 Barcelona, Spain', '02:47:31 PM', '02:47:34 PM', '02:53:19 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '12,15', '12.00', '0.00', 0, '0.00', '1200.0', '', '', '12,15', 1, '', 1, 1, '0000-00-00'),
(36, 82, '28.4109418570602', '77.0300921587426', '28.4109479070784', '77.0300867207467', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48, Gurugram, Haryana 122018, India', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48, Gurugram, Haryana 122018, India', '03:30:04 PM', '03:30:08 PM', '03:30:19 PM', '0', '0', ' 5.66', '0.00', '0.00', '0.00', '0.00', 18, '5.66', '1.66', '4.00', 0, '0.00', '10.34658234658236', '', '', '5.66', 1, '', 1, 1, '0000-00-00'),
(37, 83, '41.3802117', '2.1475617', '41.388331', '2.1645006', 'Carrer de Vilamarí, 110, 08015 Barcelona, Spain', 'Carrer de la Diputació, 236, 08007 Barcelona, Spain', '03:58:22 PM', '03:58:26 PM', '04:06:48 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 4, '10,25', '10.00', '0.00', 0, '0.00', '2530.0', '', '', '10,25', 1, '', 1, 1, '0000-00-00'),
(38, 87, '41.3871954203554', '2.19653203021375', '41.3824611676641', '2.18617632295574', 'Carrer de la Marina, 21, 08005 Barcelona, Spain', 'Carrer del Dr. Aiguader, 9, 08003 Barcelona, Spain', '04:47:17 PM', '04:48:03 PM', '04:53:01 PM', '0', '0', '9,25', '0.00', '0.00', '0.00', '0.00', 4, '9.25', '9.25', '0.00', 0, '0.00', '1.23988895296349', '', '', '9.25', 1, '', 1, 1, '0000-00-00'),
(39, 88, '41.4165783', '2.2155121', '41.4165783', '2.2155121', 'Carrer de Trapani, 4, 08019 Barcelona, Spain', 'Carrer de Trapani, 4, 08019 Barcelona, Spain', '08:05:49 AM', '08:06:05 AM', '08:06:36 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 6, '14,45', '14.00', '0.00', 0, '0.00', '0.0', '', '', '14,45', 1, '', 1, 0, '0000-00-00'),
(40, 89, '41.4165859562367', '2.21361992843396', '41.4124413472728', '2.21730268441127', 'Carrer de Cristóbal de Moura, 229, 08019 Barcelona, Spain', 'Carrer de Llull, 399, 08019 Barcelona, Spain', '09:54:24 AM', '09:54:44 AM', '10:02:58 AM', '0', '0', '12,50', '0.00', '0.00', '0.00', '0.00', 4, '12.50', '12.50', '0.00', 0, '0.00', '1.54839949567692', '', '', '12.50', 1, '', 1, 0, '0000-00-00'),
(41, 90, '41.4186636', '2.2166662', '41.2896223', '2.0708041', 'Carrer de Cristóbal de Moura, 259, 08019 Barcelona, Spain', 'Terminal T1, 08820 El Prat de Llobregat, Barcelona, Spain', '10:07:59 AM', '10:08:04 AM', '10:35:05 AM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 6, '25,85', '25.00', '0.00', 0, '0.00', '25640.0', '', '', '25,85', 1, '', 1, 1, '0000-00-00'),
(42, 91, '41.4891206735396', '2.08803764623642', '41.5031854498089', '2.12286906657319', 'B-30, 600, 08174 Cerdanyola del Vallès, Barcelona, Spain', 'Autopista del Mediterráneo & E-15 & AP-7 & B-30, 08193, Barcelona, Spain', '05:18:38 PM', '05:18:44 PM', '05:28:14 PM', '0', '0', '15,45', '0.00', '0.00', '0.00', '0.00', 4, '15.45', '15.45', '0.00', 0, '0.00', '8.22649565785307', '', '', '15.45', 1, '', 1, 1, '0000-00-00'),
(43, 92, '41.4345586', '2.2213042', '41.4345084', '2.2214749', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', 'Carrer de Covadonga, 18, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '08:37:03 PM', '08:37:20 PM', '08:38:18 PM', '0', '', '', '0.00', '0.00', '0.00', '0.00', 27, '12,50', '12.00', '0.00', 0, '0.00', '0.0', '', '', '12,50', 1, '', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `car_sheats` int(11) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `driver_category` int(21) NOT NULL DEFAULT '0',
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `car_sheats`, `driver_password`, `driver_token`, `driver_category`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_model_name`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 0, 'Yasir ', 'yasir786@hotmail.es', '667362763', '', 6, '20061983', 'KApf00xXYREI1gYD', 0, '0', '0', '', '156A29DAEAEBA9B736838A13B6671AAF0875792099BD659DF63AE21F6A92A84D', 1, '', 3, 1, 'ssangyoung ', '2255', 121, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '03:24', 'Tuesday, Nov 14, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 2, 1, 0),
(2, 0, 0, 'Irfan', 'yasirirfan198342@gmail.com', '602386391', '', 4, '20061983', 'aJ2Y1LTGtfKQrpot', 0, '0', '0', '', '', 0, '', 2, 1, 'ssangyong', '4469', 121, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0),
(3, 0, 5, 'shilpa', 'sg@g.com', '8130039030', '', 6, '123456', '0DtG4XuulXxf23xs', 0, '0', '0', '', 'dyD2Xvz3BwE:APA91bGhn38vYsd7Ea3Rmj_4qJvgAhIvt5Kjevhf83oUyae6fQ8S69_uk1lj7G_NT3hhriWNzclu8VK3r4RKf_cw-bfyklncX851SOmFF7dzv2Wqww_XOCE8X7nJS9ZBs9gvWsKjbNia', 2, '', 3, 1, '7788', '7899', 56, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123201', '77.0434362', '----', '15:00', 'Tuesday, Nov 14, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(4, 0, 0, 'irfan', 'taxibarcelona365@gmail.com', '602386392', '', 4, '20061983', 'qCHubbxLqhpCG0ub', 0, '0', '386.28', '', 'A2B668C5E56BB06E9BB5FA748549934EC2563AEB370FEFB5261F3983FC24A81C', 1, '0.6875', 2, 1, 'ssangyoung', '4469', 128, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.4833017464843', '2.15974583449825', '08110 Montcada i Reixac , Barcelona', '17:36', 'Wednesday, Nov 22, 2017', 14, 5, 4, 2, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(5, 0, 0, 'amjad iqbal', 'aiqbal755@gmail.com', '622882635', '', 6, '123456', 'urbxj3TB7Z1Lsedb', 0, '0', '0', '', '', 0, '', 3, 20, '', '4715jck', 128, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(6, 0, 0, 'joan pere ', 'joan25@gmail.com', '667362762', '', 6, '20061983', 'GaOo0YoplmuTODXj', 0, '0', '1078.72', '', 'eAIU1_kx6Ak:APA91bG0t8d5ZUH1_plkwIzwHS9rAeuQF_vyF55ItWpLL3rD0MOnGXZuufC9JL8TVH67IGM9mozGaVtiaROm7DK4UnmoysQwk1YIBlPPijsTn4-0gjk6OhIaHkFIC2uE7qDjAZN64A4q', 2, '0.769230769231', 3, 1, 'ssangyoung ', '1111', 128, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.2922305', '2.0534221', '', '10:54', 'Tuesday, Nov 21, 2017', 11, 0, 4, 2, 0, 1, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(7, 0, 5, 'ft', 'asdfgh@gmail.com', '567656786', '', 6, '123456789', 'LTH2UslnwTSGOzdy', 0, '0', '0', '', '7054BBE20EF744A04EB8D6ED1A72BA2016C032D51AE12E71E173826C85AA54BC', 1, '', 3, 1, 'rtyyu', 'err', 56, 'Tuesday, Nov 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120524716138', '77.0432184689011', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '04:45', 'Wednesday, Nov 15, 2017', 0, 0, 2, 2, 0, 2, 2, 0, '', 1, '', '2017-11-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(9, 0, 5, 'demo', 'demo88@g.com', '9865321478', '', 6, '123456', 'bK0h4aUPcquJPaIF', 0, '5', '495', '', 'dyD2Xvz3BwE:APA91bGhn38vYsd7Ea3Rmj_4qJvgAhIvt5Kjevhf83oUyae6fQ8S69_uk1lj7G_NT3hhriWNzclu8VK3r4RKf_cw-bfyklncX851SOmFF7dzv2Wqww_XOCE8X7nJS9ZBs9gvWsKjbNia', 2, '4.5', 3, 1, 'hhhh', 'vvv', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123407', '77.0434618', '----', '04:59', 'Wednesday, Nov 15, 2017', 1, 1, 0, 2, 1, 2, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(8, 0, 0, 'Muhammad Usman', 'uu2029706@gmail.com', '0034632161914', 'uploads/driver/1510954071driver_8.jpg', 4, 'usm6477', 'OfwWAfFWGya4DygK', 0, '0', '0', '', 'fkJF5VYHALE:APA91bHD0tIHL-r6Z2tC2UvZhyQ_W2QRYwCe4rnvOaeXkXiHFwtHQuFzTqjnSt9Pubf5FRNlBMb-EZ7KJYblrLJ8sJFobrfbTv971zaDo3wn2_pkyJpTT_Q2QseuiKWnc2ZGpMxrAa5m', 2, '3', 2, 3, '', 'HRB7382', 128, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.2913836', '2.0539373', '----', '11:12', 'Friday, Nov 17, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(10, 0, 5, 'yttty', 'dfg@gmail.com', '234567876', '', 6, 'qwerty', 'ChmZE119kDjfesls', 0, '0', '0', '', '7054BBE20EF744A04EB8D6ED1A72BA2016C032D51AE12E71E173826C85AA54BC', 1, '', 3, 1, 'dfgg', 'dry', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121039632326', '77.0432881530185', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '05:01', 'Wednesday, Nov 15, 2017', 0, 0, 1, 2, 0, 1, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(11, 0, 4, 'Manish', 'mani@gmail.com', '8285469069', '', 4, '123456', 'oMRPRvwqXFHwlwGD', 0, '16', '324', '', 'dSawav0tqEo:APA91bEaopuoXVGjnDUWv-iaQXyEI59KwffF2Ulq4-9z7wk21X60lMjISUULUUI5ZBD0eI_7AQP4hHJKM_yfEcoc50zZBiqFFqYEYWsYiq7JmrNV4bMFscdhSFNgxUUk1_7RnhANnuo7', 2, '2.75', 2, 1, 'qwrt', 'qefv', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123393', '77.0434517', '----', '10:04', 'Wednesday, Nov 15, 2017', 4, 0, 2, 2, 0, 1, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(12, 0, 4, 'uyyy', 'yyy@gmail.com', '123456789', '', 4, '12345678', 'beRmV2wwbuaivy2E', 0, '0', '0', '', '7054BBE20EF744A04EB8D6ED1A72BA2016C032D51AE12E71E173826C85AA54BC', 1, '', 2, 1, 'try', 'Tyumen', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412164501097', '77.0432390013174', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '05:34', 'Wednesday, Nov 15, 2017', 0, 0, 3, 2, 0, 2, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(13, 0, 5, 'Akshay', 'aks@gmail.com', '8285469066', '', 6, '123456', 'DqWMIK8yh7BBBhH6', 0, '0', '0', '', 'dSawav0tqEo:APA91bEaopuoXVGjnDUWv-iaQXyEI59KwffF2Ulq4-9z7wk21X60lMjISUULUUI5ZBD0eI_7AQP4hHJKM_yfEcoc50zZBiqFFqYEYWsYiq7JmrNV4bMFscdhSFNgxUUk1_7RnhANnuo7', 2, '', 3, 1, 'fegwh', 'vsj', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123045', '77.0434133', '----', '12:52', 'Monday, Nov 20, 2017', 0, 1, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(14, 0, 4, 'tt', 'asdf@gmail.com', '555534567', '', 4, 'qwerty', 'q2R6MAaIiJMsPKLb', 0, '2.86', '0', '', 'DD452E0736DE7871F95D06D07965D8D52BB191232547A859858D353A8544C66B', 1, '2.5', 2, 1, 'tt', 'try', 56, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122333488427', '77.0432066294022', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '09:25', 'Friday, Nov 17, 2017', 4, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(16, 0, 0, 'Adeem Azam', 'p.adeem@gmail.com', '632406973', '', 4, 'nimra4mi', 'aRAlIb0P7lX7tUJw', 0, '0', '0', '', '4122A330BB8996EDF1B057569463806D2DA76C1349D6F36362773F14F8AC9222', 1, '', 2, 1, 'toledo', '9505dms', 128, 'Thursday, Nov 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '10:08', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(15, 0, 0, 'muhammad rizwan', 'brown_eyes759@yahoo.com', '673035888', '', 4, '09091984Jm', 'V2n9NlGZ3A8df4wi', 0, '0', '0', '', 'DC0C4F29770F5E7BE1CCF8A26FF6C97C0CD27E864065681FEC629DF2049C87B1', 1, '', 2, 1, 'seat toledo 4', '2706 HWD', 128, 'Wednesday, Nov 15', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.4045520406602', '2.13001677766641', 'Carrer de Ganduxer, 143 , 08022 Barcelona', '11:27', 'Wednesday, Nov 22, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-15', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(17, 0, 0, 'ARAVINDA', 'aravindafernando77@gmail.com', '0779929565', '', 4, 'malshimal', 'NF0PspUK8kuzfMOK', 0, '0', '0', '', '', 0, '', 2, 1, 'nano', 'wp kq 9775', 128, 'Thursday, Nov 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(18, 0, 4, 'qwerty', 'qwerty@gmail.com', '887453185', '', 4, 'qwerty', '6C2kr97m7xraSt0j', 0, '14.09', '88.82', '', '1A316DE682C34C45867CD93BFA3B6AE1D2345E9C427E3CD46FDB3323402A61E0', 1, '2.8', 2, 1, '2009', 'qwerty', 56, 'Friday, Nov 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4109428239361', '77.0303678003588', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48 , Gurugram, Haryana 122018', '16:48', 'Monday, Nov 20, 2017', 6, 2, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(19, 0, 0, 'Nadeem Abbas shah', 'nadeembukhari47@yahoo.come', '632489128', '', 4, '01021975', 'SE98252ipjEeJKXN', 0, '0', '0', '', '', 0, '', 2, 43, '', 'seat toledo', 128, 'Friday, Nov 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(20, 0, 0, 'Nadeem Abbas Shah', 'nadeembukhari47@yahoo.com', '0034632489128', '', 4, '121975', 'MMmuGDfCMURzvOT7', 0, '0', '0', '', 'e41FLo46swc:APA91bEUF5DzDSnUgGD5YU2gXrFizoHDUufLgy_WfX0peOxpe7w1OxqI1l8G7qPoIfOlcgqa-ecb_7xCOh7t1iWdmojWBXtEiCYm4u8DWrel5Qm3hakrb8HrD3BANuM1QA-dgpypqS8_', 2, '', 2, 3, '', 'seat toledo 2680', 128, 'Friday, Nov 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.3812908', '2.1208757', '----', '10:52', 'Monday, Nov 27, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(21, 0, 0, 'Ahmad6033', 'Ahmadbcn6033@yahoo.com', '631909593', '', 4, '006033', 'C5oQOGHkUFwqtfm4', 0, '0', '0', '', '08ED4A47C4EBC0E1838020AC860315212BF0A0F393C759C65EB8BFA65F9E65C0', 1, '', 2, 1, 'Toyota Prius ', '6033', 128, 'Friday, Nov 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.45421842117', '2.19276253134216', 'Carrer del Torrent de la Perera, 46 , 08033 Barcelona, Spain', '09:41', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-11-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(26, 0, 0, 'iftikhar ahmed', 'pakspan@hotmail.com', '679742320', '', 4, 'bcn0786', 'QIZE4QYiVuGb9Nv2', 0, '0', '0', '', '', 0, '', 2, 1, 'nissan nv200', 'hnx9884', 128, 'Tuesday, Nov 21', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-21', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(22, 0, 0, 'ZIA UL HASSAN MALIK', 'ziamalik007@hotmail.com', '675616923', '', 6, '5386JJJ', 'o8gw8XYKkTcV0Pqq', 0, '0', '0', '', '5C18FB5951EFA81881F863CBD410865AB470BD37BAF3A546E4CA885EFBEF2D79', 1, '', 3, 1, 'FIAT SCUDO', '5386JJJ', 128, 'Sunday, Nov 19', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '04:54', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(23, 0, 0, 'hamza Azhar', 'hamziita_95@hotmail.com', '664774892', '', 6, 'hamza1212', '70eRvDBoxYcYttD1', 0, '0', '0', '', 'DCF9E1875446FC649029BEFAE184C24E99841AE308AE53AA080B54E6E37A95DF', 1, '', 3, 1, 'Volkswagen caddy', '9476 HXG', 128, 'Sunday, Nov 19', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '07:07', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-19', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(24, 0, 0, 'karnail Singh', 'soni_toni@yahoo.com', '632799484', '', 4, '444454', '5gwST0eRMvBPtRDH', 0, '0', '0', '', '', 0, '', 2, 1, 'skoda', '0418fhg', 128, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(25, 0, 0, 'wasim ', 'wasim031985@gmail.com', '672528326', '', 4, '01011981', 'JJqbRSVBnoUlVWpO', 0, '0', '0', '', 'EBE4F2465A83567C8A5C04A8D266EEF5E35A8ED84BEF122803C735584B182905', 1, '', 2, 1, 'Peugeot 5008', '5369JCW', 128, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '12:43', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(27, 0, 0, 'kashaf 10523', 'nazirmiankashif@yahoo.es', '670806352', '', 6, '123456', 'NjUX2Ow3XVvkjNCt', 0, '0', '12', '', 'eK2Ak6nN84s:APA91bH1AEUSvhZGer0J6je-njr5M6ItOpdS0q9SN07rFORlGtuOew0ueWhYl8ciGoky9BBDdId9jMF6ZOipv4XuPrS0NJwc58Gya-b4i4CIGgBT-tmhm5yPx1QXxdnLiJd64YC7ai7s', 2, '', 3, 1, 'caddy', '10523', 128, 'Thursday, Nov 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '41.4345211', '2.2214571', '----', '20:57', 'Thursday, Nov 23, 2017', 1, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(28, 0, 0, 'muhammad afzal', 'kamalibcn@yahoo.com', '678899914', '', 4, '453764000', 'kBWUQSzz2KSoG7zX', 0, '0', '0', '', '', 0, '', 2, 1, 'skoda', '9057htg', 128, 'Sunday, Nov 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 9, '500,55', 1, '5.00', '495.00', '2017-11-15'),
(2, 14, '1.54', 2, '1.54', '0', '2017-11-15'),
(3, 4, '183.08', 6, '0', '183.08', '2017-11-15'),
(4, 6, '870.45', 3, '0', '870.45', '2017-11-15'),
(5, 11, '340', 4, '16', '324', '2017-11-15'),
(6, 6, '52.80', 1, '0.00', '52.80', '2017-11-16'),
(7, 4, '130', 2, '0', '130', '2017-11-16'),
(8, 14, '1.32', 2, '1.32', '0', '2017-11-17'),
(9, 18, '89.50', 1, '4.00', '85.50', '2017-11-17'),
(10, 6, '55.52', 3, '0', '55.52', '2017-11-17'),
(11, 18, '2.09', 3, '2.09', '0', '2017-11-18'),
(12, 6, '15.20', 1, '0.00', '15.20', '2017-11-18'),
(13, 6, '45.75', 1, '0.00', '45.75', '2017-11-19'),
(14, 4, '45.25', 4, '0', '45.25', '2017-11-20'),
(15, 18, '11.32', 2, '8', '3.32', '2017-11-20'),
(16, 6, '39', 2, '0', '39', '2017-11-21'),
(17, 4, '12.50', 1, '0.00', '12.50', '2017-11-21'),
(18, 4, '15.45', 1, '0.00', '15.45', '2017-11-22'),
(19, 27, '12,50', 1, '0.00', '12.00', '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 4, 91, 1),
(2, 6, 90, 1),
(3, 3, 82, 1),
(4, 7, 4, 1),
(5, 9, 8, 1),
(6, 10, 9, 1),
(7, 11, 38, 1),
(8, 12, 16, 1),
(9, 13, 93, 1),
(10, 14, 82, 1),
(11, 18, 82, 1),
(12, 27, 92, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favourites_driver`
--

CREATE TABLE `favourites_driver` (
  `favourite_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `favourite_date_added` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favourites_driver`
--

INSERT INTO `favourites_driver` (`favourite_id`, `driver_id`, `user_id`, `favourite_date_added`) VALUES
(1, 9, 1, '2017-11-15'),
(2, 14, 3, '2017-11-15'),
(3, 14, 3, '2017-11-15'),
(4, 6, 1, '2017-11-15'),
(5, 4, 1, '2017-11-15'),
(6, 4, 5, '2017-11-16'),
(7, 18, 3, '2017-11-17'),
(8, 6, 5, '2017-11-18'),
(9, 27, 6, '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 1, '', '41.400473538742204', '2.145709879696369', 'Via Augusta, 134, 08006 Barcelona, Spain', '', '', 'Set your drop point', '', '16:56:43', '04:56:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.400473538742204,2.145709879696369&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(2, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:36:56', '11:36:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(3, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:37:01', '11:37:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(4, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:37:04', '11:37:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(5, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:37:19', '11:37:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(6, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:37:44', '11:37:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(7, 1, '', '41.4316442877678', '2.23008615993995', 'Carrer del Progrés, 380\n08918 Sant Adrià de Besòs, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:37:48', '11:37:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4316442877678,2.23008615993995&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(8, 1, '', '41.4345076528434', '2.23321560770273', 'Carrer del Progrés, 331\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:39:24', '11:39:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4345076528434,2.23321560770273&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(9, 1, '', '41.4345076528434', '2.23321560770273', 'Carrer del Progrés, 331\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:39:27', '11:39:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4345076528434,2.23321560770273&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(10, 1, '', '41.4345075679105', '2.23321574780775', 'Carrer del Progrés, 331\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:39:56', '11:39:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4345075679105,2.23321574780775&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(11, 1, '', '41.4375263673164', '2.23417471341803', 'Carrer de Guifré, 617\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '11:42:54', '11:42:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4375263673164,2.23417471341803&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(12, 1, '', '41.4364923669937', '2.23659720271826', 'Carrer de Tortosa, 167\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '12:02:19', '12:02:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4364923669937,2.23659720271826&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(13, 1, '', '41.4352954126584', '2.23726809024811', 'Carrer de la Indústria, 427\n08918 Badalona, Barcelona', '0.0', '0.0', 'No drop off point', '', '12:03:18', '12:03:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4352954126584,2.23726809024811&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(14, 5, '', '41.41718356958116', '2.2154932469129562', 'Carrer de Pere Moragues, 13, 08019 Barcelona, Spain', '', '', 'Set your drop point', '', '19:38:01', '07:38:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.41718356958116,2.2154932469129562&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(15, 1, '', '41.4165825134064', '2.21550886472354', 'Carrer de Trapani, 4\n08019 Barcelona', '0.0', '0.0', 'No drop off point', '', '00:34:42', '12:34:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4165825134064,2.21550886472354&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(16, 1, '', '41.4165825134064', '2.21550886472354', 'Carrer de Trapani, 4\n08019 Barcelona', '0.0', '0.0', 'No drop off point', '', '00:34:44', '12:34:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4165825134064,2.21550886472354&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 6, '', '41.43452977258575', '2.2214557975530624', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '18:35:01', '06:35:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.43452977258575,2.2214557975530624&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(18, 6, '', '41.4345760229318', '2.2215235233306885', 'Carrer de Covadonga, 14, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '20:22:59', '08:22:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.4345760229318,2.2215235233306885&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(19, 6, '', '41.434512428697495', '2.2214467450976367', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '20:25:13', '08:25:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.434512428697495,2.2214467450976367&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(20, 6, '', '41.43452022087973', '2.221454791724682', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '20:25:44', '08:25:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.43452022087973,2.221454791724682&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(21, 6, '', '41.43452122632255', '2.2214467450976367', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '20:26:09', '08:26:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.43452122632255,2.2214467450976367&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(22, 6, '', '41.43451971815833', '2.2214551270008087', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', '', 'Set your drop point', '', '20:26:19', '08:26:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|41.43451971815833,2.2214551270008087&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">The use of Yellow Taxi is totally free for the final user , in other words , for the taxi client, with the exception of those costs deriving from the use of their mobile data plan and those countries where law establishes the need of implementing an additional charge to the service for the use of this kind of application.&nbsp;<br><br>Yellow Taxi is an application especially designed for ios or Android smartphones. The use of Yellow Taxi not imply paying any kind of commission to the taxi driver offering the service.&nbsp;<br><br>Company Name :&nbsp;Taxi Digital-SL<br>Email :&nbsp; &nbsp;yellowtaxibcn@gmail.com<br>Address :&nbsp;&nbsp;C/ Xavier Nogues,47, Bajos&nbsp; código postal:08019&nbsp; Barcelona<br><br></h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', '<h2 class=\"apporioh2\" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\" style=\"font-family: Roboto; color: rgb(0, 0, 0);\">The use of Yellow Taxi is totally free for the final user , in other words , for the taxi client, with the exception of those costs deriving from the use of their mobile data plan and those countries where law establishes the need&nbsp;of&nbsp;implementing an additional charge to the service for the use of this kind of application.&nbsp;<br><br>Yellow Taxi is an application&nbsp;especially&nbsp;designed for ios or Android smartphones. The use of Yellow Taxi&nbsp;not imply&nbsp;paying any kind of commission to the taxi driver offering the service.&nbsp;<br><br>Company Name :&nbsp;Taxi Digital-SL<br>Email :&nbsp; &nbsp;yellowtaxibcn@gmail.com<br>Address :&nbsp;&nbsp;C/ Xavier Nogues,47, Bajos&nbsp; código postal:08019&nbsp; Barcelona<br></h2>', '', '', '', ''),
(4, 'verify', 'verify', 'qhja', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Android', '500,55', 'Anything', 'Anything'),
(2, 2, 3, '1', 'Cash', 'Ios', '0.77', '15 de noviembre de 2017', 'Done'),
(3, 4, 3, '1', 'Cash', 'Ios', '0.77', '15 de noviembre de 2017', 'Done'),
(4, 5, 1, '1', 'Cash', 'Ios', '45.28', '15 November 2017', 'Done'),
(5, 6, 1, '1', 'Cash', 'Ios', '7.45', '15 November 2017', 'Done'),
(6, 7, 1, '1', 'Cash', 'Ios', '0.00', '15 November 2017', 'Done'),
(7, 8, 1, '1', 'Cash', 'Ios', '0.00', '15 November 2017', 'Done'),
(8, 9, 1, '1', 'Cash', 'Ios', '0.00', '15 November 2017', 'Done'),
(9, 10, 4, '1', 'Cash', 'Android', '85,50', 'Anything', 'Anything'),
(10, 11, 4, '1', 'Cash', 'Android', '85,50', 'Anything', 'Anything'),
(11, 12, 4, '1', 'Cash', 'Android', '85,50', 'Anything', 'Anything'),
(12, 13, 4, '1', 'Cash', 'Android', '85,50', 'Anything', 'Anything'),
(13, 14, 1, '1', 'Cash', 'Android', '52.80', 'Anything', 'Anything'),
(14, 15, 1, '1', 'Cash', 'Ios', '85,85', '15 November 2017', 'Done'),
(15, 16, 1, '1', 'Cash', 'Ios', '863,25', '15 November 2017', 'Done'),
(16, 17, 1, '1', 'Cash', 'Ios', '0.00', '15 November 2017', 'Done'),
(17, 18, 5, '1', 'Cash', 'Android', '52.80', 'Anything', 'Anything'),
(18, 19, 5, '1', 'Cash', 'Android', '74,55', 'Anything', 'Anything'),
(19, 20, 1, '1', 'Cash', 'Ios', '56,85', '16 November 2017', 'Done'),
(20, 22, 3, '1', 'Cash', 'Ios', '0.55', '17 November 2017', 'Done'),
(21, 23, 3, '1', 'Cash', 'Ios', '0.77', '17 de noviembre de 2017', 'Done'),
(22, 24, 3, '1', 'Cash', 'Ios', '89.50', '17 de noviembre de 2017', 'Done'),
(23, 25, 1, '1', 'Cash', 'Ios', '45,45', '17 November 2017', 'Done'),
(24, 26, 1, '1', 'Cash', 'Ios', '10.52', '18 November 2017', 'Done'),
(25, 27, 1, '1', 'Cash', 'Ios', '0.00', '18 November 2017', 'Done'),
(26, 28, 3, '1', 'Cash', 'Ios', '0.66', '18 November 2017', 'Done'),
(27, 29, 3, '1', 'Cash', 'Ios', '0.76', '18 November 2017', 'Done'),
(28, 30, 3, '1', 'Cash', 'Ios', '0.67', '18 November 2017', 'Done'),
(29, 31, 5, '1', 'Cash', 'Android', '15.20', 'Anything', 'Anything'),
(30, 32, 1, '1', 'Cash', 'Ios', '45.75', '19 November 2017', 'Done'),
(31, 33, 1, '1', 'Cash', 'Ios', '14,75', '20 November 2017', 'Done'),
(32, 34, 3, '1', 'Cash', 'Ios', '5.66', '20 November 2017', 'Done'),
(33, 35, 5, '1', 'Cash', 'Ios', '12,15', '20 November 2017', 'Done'),
(34, 36, 3, '1', 'Cash', 'Ios', '5.66', '20 November 2017', 'Done'),
(35, 37, 3, '1', 'Cash', 'Ios', '10,25', '20 November 2017', 'Done'),
(36, 38, 3, '1', 'Cash', 'Ios', '9.25', '20 November 2017', 'Done'),
(37, 39, 5, '1', 'Cash', 'Android', '14,45', 'Anything', 'Anything'),
(38, 40, 5, '1', 'Cash', 'Android', '12.50', 'Anything', 'Anything'),
(39, 41, 3, '1', 'Cash', 'Ios', '25,85', '21 November 2017', 'Done'),
(40, 42, 3, '1', 'Cash', 'Ios', '15.45', '22 November 2017', 'Done'),
(41, 43, 6, '1', 'Cash', 'Android', '12,50', 'Anything', 'Anything');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Pay via App', '', '', 2),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(46, 121, 'Km', '7', 2, 0, 0, 0, 0, 0, 0, 0, '3', '90', '10', '0', '0', '0', '0'),
(47, 121, 'Km', '7', 3, 0, 0, 0, 0, 0, 0, 0, '2', '90', '10', '0', '0', '0', '0'),
(48, 124, 'Km', '6', 2, 5, 0, 0, 0, 0, 0, 0, '1', '100', '1', '12', '1', '1', '1'),
(49, 128, 'Km', '&#8364;', 2, 0, 0, 0, 0, 0, 0, 0, '3', '90', '10', '0', '0', '0', '0'),
(50, 128, 'Km', '&#8364;', 3, 0, 0, 0, 0, 0, 0, 0, '2', '90', '10', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Work star', 'Work start tomorrow ', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-11-14', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 0, 4, 0, '2017-11-14'),
(2, 0, 4, 0, '2017-11-14'),
(3, 0, 4, 0, '2017-11-14'),
(4, 0, 4, 0, '2017-11-14'),
(5, 0, 6, 0, '2017-11-14'),
(6, 1, 3, 0, '2017-11-14'),
(7, 2, 3, 0, '2017-11-14'),
(8, 3, 3, 0, '2017-11-14'),
(9, 3, 7, 1, '2017-11-14'),
(10, 4, 3, 0, '2017-11-15'),
(11, 4, 7, 1, '2017-11-15'),
(12, 5, 3, 0, '2017-11-15'),
(13, 5, 9, 1, '2017-11-15'),
(14, 6, 3, 0, '2017-11-15'),
(15, 6, 9, 0, '2017-11-15'),
(16, 6, 10, 1, '2017-11-15'),
(17, 7, 3, 0, '2017-11-15'),
(18, 7, 9, 0, '2017-11-15'),
(19, 7, 10, 0, '2017-11-15'),
(20, 7, 10, 0, '0000-00-00'),
(21, 8, 3, 0, '2017-11-15'),
(22, 8, 9, 1, '2017-11-15'),
(23, 8, 10, 0, '2017-11-15'),
(24, 9, 3, 0, '2017-11-15'),
(25, 9, 10, 0, '2017-11-15'),
(26, 9, 11, 0, '2017-11-15'),
(27, 10, 3, 0, '2017-11-15'),
(28, 10, 12, 1, '2017-11-15'),
(29, 11, 3, 0, '2017-11-15'),
(30, 11, 12, 1, '2017-11-15'),
(31, 12, 3, 0, '2017-11-15'),
(32, 12, 12, 0, '2017-11-15'),
(33, 13, 3, 0, '2017-11-15'),
(34, 13, 12, 0, '2017-11-15'),
(35, 14, 3, 0, '2017-11-15'),
(36, 14, 12, 0, '2017-11-15'),
(37, 15, 3, 0, '2017-11-15'),
(38, 15, 12, 1, '2017-11-15'),
(39, 16, 3, 0, '2017-11-15'),
(40, 16, 12, 0, '2017-11-15'),
(41, 17, 3, 0, '2017-11-15'),
(42, 17, 13, 1, '2017-11-15'),
(43, 18, 3, 0, '2017-11-15'),
(44, 18, 11, 0, '2017-11-15'),
(45, 19, 3, 0, '2017-11-15'),
(46, 19, 11, 0, '2017-11-15'),
(47, 19, 14, 1, '2017-11-15'),
(48, 20, 3, 0, '2017-11-15'),
(49, 20, 11, 0, '2017-11-15'),
(50, 21, 3, 0, '2017-11-15'),
(51, 21, 11, 0, '2017-11-15'),
(52, 22, 3, 0, '2017-11-15'),
(53, 22, 11, 1, '2017-11-15'),
(54, 22, 14, 0, '2017-11-15'),
(55, 23, 3, 0, '2017-11-15'),
(56, 23, 14, 0, '2017-11-15'),
(57, 24, 3, 0, '2017-11-15'),
(58, 24, 11, 1, '2017-11-15'),
(59, 24, 14, 0, '2017-11-15'),
(60, 25, 3, 0, '2017-11-15'),
(61, 25, 14, 0, '2017-11-15'),
(62, 26, 3, 0, '2017-11-15'),
(63, 26, 14, 1, '2017-11-15'),
(64, 27, 4, 1, '2017-11-15'),
(65, 29, 6, 1, '2017-11-15'),
(66, 0, 4, 0, '2017-11-15'),
(67, 0, 6, 0, '2017-11-15'),
(68, 30, 4, 1, '2017-11-15'),
(69, 31, 4, 1, '2017-11-15'),
(70, 32, 6, 1, '2017-11-15'),
(71, 33, 6, 1, '2017-11-15'),
(72, 0, 6, 0, '2017-11-15'),
(73, 0, 6, 0, '2017-11-15'),
(74, 0, 6, 0, '2017-11-15'),
(75, 35, 3, 0, '2017-11-15'),
(76, 35, 11, 1, '2017-11-15'),
(77, 35, 14, 0, '2017-11-15'),
(78, 36, 3, 0, '2017-11-15'),
(79, 36, 11, 1, '2017-11-15'),
(80, 36, 14, 0, '2017-11-15'),
(81, 37, 3, 0, '2017-11-15'),
(82, 37, 11, 1, '2017-11-15'),
(83, 37, 14, 0, '2017-11-15'),
(84, 38, 3, 0, '2017-11-15'),
(85, 38, 11, 1, '2017-11-15'),
(86, 38, 14, 0, '2017-11-15'),
(87, 39, 3, 0, '2017-11-15'),
(88, 40, 3, 0, '2017-11-15'),
(89, 40, 13, 0, '2017-11-15'),
(90, 40, 3, 0, '0000-00-00'),
(91, 41, 3, 0, '2017-11-15'),
(92, 41, 13, 0, '2017-11-15'),
(93, 42, 3, 0, '2017-11-15'),
(94, 42, 13, 0, '2017-11-15'),
(95, 43, 4, 1, '2017-11-15'),
(96, 45, 4, 1, '2017-11-15'),
(97, 46, 4, 1, '2017-11-15'),
(98, 47, 6, 1, '2017-11-15'),
(99, 48, 6, 1, '2017-11-15'),
(100, 0, 6, 0, '2017-11-15'),
(101, 0, 6, 0, '2017-11-15'),
(102, 0, 4, 0, '2017-11-15'),
(103, 0, 6, 0, '2017-11-15'),
(104, 0, 4, 0, '2017-11-15'),
(105, 49, 4, 1, '2017-11-15'),
(106, 50, 4, 1, '2017-11-16'),
(107, 51, 6, 1, '2017-11-16'),
(108, 52, 4, 1, '2017-11-16'),
(109, 53, 6, 1, '2017-11-16'),
(110, 54, 6, 1, '2017-11-16'),
(111, 55, 4, 1, '2017-11-16'),
(112, 0, 4, 0, '2017-11-16'),
(113, 56, 4, 1, '2017-11-16'),
(114, 57, 3, 0, '2017-11-17'),
(115, 57, 13, 0, '2017-11-17'),
(116, 57, 14, 1, '2017-11-17'),
(117, 58, 3, 0, '2017-11-17'),
(118, 58, 13, 0, '2017-11-17'),
(119, 58, 14, 0, '2017-11-17'),
(120, 59, 3, 0, '2017-11-17'),
(121, 59, 13, 0, '2017-11-17'),
(122, 59, 14, 1, '2017-11-17'),
(123, 60, 3, 0, '2017-11-17'),
(124, 60, 13, 0, '2017-11-17'),
(125, 60, 14, 0, '2017-11-17'),
(126, 60, 18, 0, '2017-11-17'),
(127, 61, 3, 0, '2017-11-17'),
(128, 61, 13, 0, '2017-11-17'),
(129, 61, 14, 0, '2017-11-17'),
(130, 61, 18, 1, '2017-11-17'),
(131, 67, 6, 1, '2017-11-17'),
(132, 68, 6, 1, '2017-11-18'),
(133, 0, 6, 0, '2017-11-18'),
(134, 69, 3, 0, '2017-11-18'),
(135, 69, 13, 0, '2017-11-18'),
(136, 69, 14, 0, '2017-11-18'),
(137, 70, 3, 0, '2017-11-18'),
(138, 70, 13, 0, '2017-11-18'),
(139, 70, 14, 0, '2017-11-18'),
(140, 70, 18, 1, '2017-11-18'),
(141, 71, 3, 0, '2017-11-18'),
(142, 71, 13, 0, '2017-11-18'),
(143, 71, 14, 0, '2017-11-18'),
(144, 71, 18, 0, '2017-11-18'),
(145, 72, 3, 0, '2017-11-18'),
(146, 72, 13, 0, '2017-11-18'),
(147, 72, 14, 0, '2017-11-18'),
(148, 72, 18, 1, '2017-11-18'),
(149, 73, 3, 0, '2017-11-18'),
(150, 73, 13, 0, '2017-11-18'),
(151, 73, 14, 0, '2017-11-18'),
(152, 73, 18, 1, '2017-11-18'),
(153, 0, 6, 0, '2017-11-18'),
(154, 74, 6, 1, '2017-11-18'),
(155, 75, 3, 0, '2017-11-18'),
(156, 75, 13, 0, '2017-11-18'),
(157, 75, 14, 0, '2017-11-18'),
(158, 75, 18, 0, '2017-11-18'),
(159, 75, 14, 0, '0000-00-00'),
(160, 76, 3, 0, '2017-11-18'),
(161, 76, 13, 0, '2017-11-18'),
(162, 76, 14, 0, '2017-11-18'),
(163, 76, 18, 0, '2017-11-18'),
(164, 76, 14, 0, '0000-00-00'),
(165, 77, 4, 1, '2017-11-20'),
(166, 78, 3, 0, '2017-11-20'),
(167, 78, 13, 0, '2017-11-20'),
(168, 78, 14, 0, '2017-11-20'),
(169, 78, 18, 1, '2017-11-20'),
(170, 79, 3, 0, '2017-11-20'),
(171, 79, 13, 0, '2017-11-20'),
(172, 79, 14, 0, '2017-11-20'),
(173, 79, 18, 0, '2017-11-20'),
(174, 80, 3, 0, '2017-11-20'),
(175, 80, 13, 0, '2017-11-20'),
(176, 80, 14, 0, '2017-11-20'),
(177, 80, 18, 0, '2017-11-20'),
(178, 81, 4, 1, '2017-11-20'),
(179, 82, 3, 0, '2017-11-20'),
(180, 82, 13, 0, '2017-11-20'),
(181, 82, 14, 0, '2017-11-20'),
(182, 82, 18, 1, '2017-11-20'),
(183, 0, 4, 0, '2017-11-20'),
(184, 0, 4, 0, '2017-11-20'),
(185, 83, 4, 1, '2017-11-20'),
(186, 84, 4, 0, '2017-11-20'),
(187, 85, 4, 0, '2017-11-20'),
(188, 86, 4, 1, '2017-11-20'),
(189, 87, 4, 1, '2017-11-20'),
(190, 88, 4, 0, '2017-11-21'),
(191, 88, 6, 1, '2017-11-21'),
(192, 89, 4, 1, '2017-11-21'),
(193, 89, 6, 0, '2017-11-21'),
(194, 90, 6, 1, '2017-11-21'),
(195, 91, 4, 1, '2017-11-22'),
(196, 92, 27, 1, '2017-11-23'),
(197, 93, 13, 0, '2017-11-25');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 7, 9),
(2, 40, 13),
(3, 75, 18),
(4, 76, 18),
(5, 84, 4),
(6, 84, 4),
(7, 85, 4),
(8, 85, 4),
(9, 85, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `luggage_instructon` text NOT NULL,
  `number_of_passanger` int(11) NOT NULL DEFAULT '0',
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `luggage_instructon`, `number_of_passanger`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 3, '', '28.4116234006984', '77.0346388220787', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', 'Jj', 0, '0.0', '0.0', 'No drop off point', 'Tuesday, Nov 14', '16:14:59', '04:14:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4116234006984,77.0346388220787&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(2, 3, '', '28.4109852507346', '77.0299198105931', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', 'Kk', 0, '0.0', '0.0', 'No drop off point', 'Tuesday, Nov 14', '16:15:24', '04:15:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4109852507346,77.0299198105931&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(3, 3, '', '28.4109770226709', '77.0299052488795', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', 'Ju', 0, '0.0', '0.0', 'No drop off point', 'Tuesday, Nov 14', '16:15:45', '04:38:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4109770226709,77.0299052488795&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-14'),
(4, 3, '', '28.409608378959', '77.0473863556981', '233, Pragati Path Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Ggg', 0, '28.406928308211', '77.0505835488439', 'D Block\nGurugram, Haryana', 'Wednesday, Nov 15', '04:43:47', '04:44:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.409608378959,77.0473863556981&markers=color:red|label:D|28.406928308211,77.0505835488439&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(5, 1, '', '28.4123479498548', '77.04345054924488', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '04:47:16', '04:47:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123479498548,77.04345054924488&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(6, 3, '', '28.412063674749', '77.0432490482926', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4113842423593', '77.050158418715', '15, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '04:47:52', '04:55:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412063674749,77.0432490482926&markers=color:red|label:D|28.4113842423593,77.050158418715&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 10, 3, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(7, 3, '', '28.4120371670743', '77.0432379408396', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4085455597201', '77.0481303334236', '288, Lokpath Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '04:55:28', '04:55:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120371670743,77.0432379408396&markers=color:red|label:D|28.4085455597201,77.0481303334236&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(8, 3, '', '28.4120371670743', '77.0432379408396', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Gggt', 0, '28.4085455597201', '77.0481303334236', '288, Lokpath Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '04:56:24', '04:56:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120371670743,77.0432379408396&markers=color:red|label:D|28.4085455597201,77.0481303334236&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 3, 1, 1, 3, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(9, 3, '', '28.4112129091111', '77.0487066730857', '5, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Ggg', 0, '28.396405543831', '77.0624362304807', 'Badshahpur, Sector 66\nGurugram, Haryana', 'Wednesday, Nov 15', '04:59:27', '04:59:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4112129091111,77.0487066730857&markers=color:red|label:D|28.396405543831,77.0624362304807&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 2, 1, 1, 3, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(10, 3, '', '28.4102394596986', '77.0457622781396', '148, Janpath Marg, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Uu', 0, '28.4085597149853', '77.0523068681359', 'B-85, Rosewood City Road, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:09:57', '05:13:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4102394596986,77.0457622781396&markers=color:red|label:D|28.4085597149853,77.0523068681359&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(11, 3, '', '28.412160100614', '77.0432013842032', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Uu', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '05:14:41', '05:16:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412160100614,77.0432013842032&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(12, 3, '', '28.4121588372498', '77.0432304220249', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', ' M', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '05:17:23', '05:17:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121588372498,77.0432304220249&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(13, 3, '', '28.4096455361534', '77.0465203374624', '241, Pragati Path Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Hy', 0, '28.4067578524743', '77.0515199750662', 'D Block\nGurugram, Haryana', 'Wednesday, Nov 15', '05:24:59', '05:24:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4096455361534,77.0465203374624&markers=color:red|label:D|28.4067578524743,77.0515199750662&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(14, 3, '', '28.4096455361534', '77.0465203374624', '241, Pragati Path Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Yay', 0, '28.4067578524743', '77.0515199750662', 'D Block\nGurugram, Haryana', 'Wednesday, Nov 15', '05:25:38', '05:25:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4096455361534,77.0465203374624&markers=color:red|label:D|28.4067578524743,77.0515199750662&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(15, 3, '', '28.4119886871009', '77.0432344946942', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Hhh', 0, '28.4097617260266', '77.0465605705976', '244, Samta Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:29:08', '05:33:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4119886871009,77.0432344946942&markers=color:red|label:D|28.4097617260266,77.0465605705976&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(16, 3, '', '28.4120549002905', '77.0432479730055', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4080536630774', '77.0480760186911', 'S11, Lokpath Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:34:26', '05:34:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120549002905,77.0432479730055&markers=color:red|label:D|28.4080536630774,77.0480760186911&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(17, 4, '', '28.41235060387191', '77.04344317317009', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'jehj', 0, '28.3585701147616', '77.09310092031956', '1879, Bhondsi, Maruti Kunj, Haryana 122102, India', 'Wednesday, Nov 15', '05:35:00', '05:35:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41235060387191,77.04344317317009&markers=color:red|label:D|28.3585701147616,77.09310092031956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(18, 4, '', '28.41235060387191', '77.04344317317009', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.3585701147616', '77.09310092031956', '1879, Bhondsi, Maruti Kunj, Haryana 122102, India', 'Wednesday, Nov 15', '05:43:34', '05:43:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41235060387191,77.04344317317009&markers=color:red|label:D|28.3585701147616,77.09310092031956&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(19, 3, '', '28.4107646693539', '77.0538856834173', '28, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '05:49:16', '05:51:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4107646693539,77.0538856834173&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(20, 4, '', '28.412349719199558', '77.04344920814037', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.37447514074657', '77.02610369771719', 'Palra Rd, Palra, Haryana 122103, India', 'Wednesday, Nov 15', '05:50:03', '05:50:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412349719199558,77.04344920814037&markers=color:red|label:D|28.37447514074657,77.02610369771719&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(21, 3, '', '28.4121000409151', '77.0433194566294', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Uuy', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '05:51:16', '05:51:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121000409151,77.0433194566294&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(22, 3, '', '28.4121391669677', '77.0432916283607', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4097045158004', '77.046939432621', '237, Pragati Path Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:53:21', '05:54:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121391669677,77.0432916283607&markers=color:red|label:D|28.4097045158004,77.046939432621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-15'),
(23, 3, '', '28.4121391669677', '77.0432916283607', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4097045158004', '77.046939432621', '237, Pragati Path Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:54:00', '05:54:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121391669677,77.0432916283607&markers=color:red|label:D|28.4097045158004,77.046939432621&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(24, 4, '', '28.412349719199558', '77.04344920814037', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.4592693', '77.0724192', 'Huda City Centre', 'Wednesday, Nov 15', '05:54:41', '09:44:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412349719199558,77.04344920814037&markers=color:red|label:D|28.359784494088842,77.025262825191&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-15'),
(35, 4, '', '28.412335564440784', '77.04344384372234', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.36823519808269', '77.02703442424536', 'Unnamed Road, Nurpur Jharsa, Haryana 122103, India', 'Wednesday, Nov 15', '09:45:19', '09:46:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335564440784,77.04344384372234&markers=color:red|label:D|28.36823519808269,77.02703442424536&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(25, 3, '', '28.4121391669677', '77.0432916283607', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Kiu', 0, '28.4112748368231', '77.0476448535919', '277, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Wednesday, Nov 15', '05:54:57', '05:54:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121391669677,77.0432916283607&markers=color:red|label:D|28.4112748368231,77.0476448535919&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(26, 3, '', '28.4120315315614', '77.0431585237384', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4072565379486', '77.0500035211444', 'D Block\nGurugram, Haryana', 'Wednesday, Nov 15', '06:04:39', '06:05:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120315315614,77.0431585237384&markers=color:red|label:D|28.4072565379486,77.0500035211444&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(27, 1, '', '41.4168464077141', '2.21524153280278', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '08:32:38', '08:33:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4168464077141,2.21524153280278&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(28, 1, '', '41.4166741748078', '2.21519719809294', 'Carrer de Trapani, 4\n08019 Barcelona', '', 5, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '08:34:44', '12:07:37 PM', '', '17/11/2017', '09:34', 6, 3, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(29, 1, '', '41.4178204331425', '2.21574906259775', 'Carrer de Cristóbal de Moura, 246\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '08:38:08', '08:40:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4178204331425,2.21574906259775&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(30, 1, '', '41.4165567767147', '2.21467872648301', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '09:31:16', '09:33:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4165567767147,2.21467872648301&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(31, 1, '', '41.4165285224074', '2.21464231298271', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '09:34:10', '09:35:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4165285224074,2.21464231298271&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(32, 1, '', '41.4165700383256', '2.21467375787554', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '09:39:40', '09:39:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4165700383256,2.21467375787554&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 2, 0, 13, 1, 0, 1, 1, '2017-11-15'),
(33, 1, '', '41.4165336092954', '2.21464944357809', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '09:40:18', '09:40:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4165336092954,2.21464944357809&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(34, 1, '', '41.4165513848026', '2.21470364484154', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 6, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '09:41:28', '12:56:13 AM', '', '17/11/2017', '11:41', 6, 2, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(36, 4, '', '28.412335564440784', '77.04344384372234', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.36823519808269', '77.02703442424536', 'Unnamed Road, Nurpur Jharsa, Haryana 122103, India', 'Wednesday, Nov 15', '09:47:07', '09:47:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335564440784,77.04344384372234&markers=color:red|label:D|28.36823519808269,77.02703442424536&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(37, 4, '', '28.412335564440784', '77.04343311488628', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '28.325957242976656', '77.07335751503706', 'New Colony Road, Mahendwara, Haryana 122103, India', 'Wednesday, Nov 15', '09:53:37', '09:55:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335564440784,77.04343311488628&markers=color:red|label:D|28.325957242976656,77.07335751503706&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(38, 4, '', '28.4123382184582', '77.04343914985657', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '10:01:11', '10:01:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123382184582,77.04343914985657&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(39, 4, '', '28.4123382184582', '77.04343914985657', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '10:04:15', '10:04:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123382184582,77.04343914985657&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(40, 4, '', '28.4123382184582', '77.04343914985657', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '10:05:21', '10:05:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4123382184582,77.04343914985657&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(41, 4, '', '28.412335859331613', '77.04343345016241', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '10:07:18', '10:07:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04343345016241&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(42, 4, '', '28.412342052038795', '77.04345423728228', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '10:07:50', '10:07:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412342052038795,77.04345423728228&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(43, 1, '', '41.42663556226428', '2.2257771715521812', 'Carrer de la Torrassa, 38, 08930 Sant Adrià de Besòs, Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Wednesday, Nov 15', '11:34:03', '11:34:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.42663556226428,2.2257771715521812&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(44, 1, '', '41.4312941774569', '2.23298527300358', 'Carrer de Francesc Teixidó, 17\n08918 Badalona, Barcelona', '', 1, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '11:38:39', '09:51:09 AM', '', '17/11/2017', '12:38', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-15'),
(45, 1, '', '41.4352954126584', '2.23726809024811', 'Carrer de la Indústria, 427\n08918 Badalona, Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '12:03:48', '12:04:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4352954126584,2.23726809024811&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-15'),
(46, 1, '', '41.4353085682947', '2.23720795075068', 'Carrer de la Indústria, 427\n08918 Badalona, Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '12:04:13', '12:04:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4353085682947,2.23720795075068&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(47, 1, '', '41.4352906275046', '2.23725875755319', 'Carrer de la Indústria, 427\n08918 Badalona, Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '12:05:33', '12:06:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4352906275046,2.23725875755319&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-11-15'),
(48, 1, '', '41.4353316135245', '2.23722871740058', 'Carrer de la Indústria, 427\n08918 Badalona, Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '12:06:31', '12:06:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4353316135245,2.23722871740058&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(49, 1, '', '41.4166083846881', '2.2147911952545', 'Carrer de Pere Moragues, 9\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Wednesday, Nov 15', '17:21:43', '05:22:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4166083846881,2.2147911952545&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-15'),
(50, 1, '', '41.4167134476857', '2.21526401645941', 'Carrer de Trapani, 2\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Thursday, Nov 16', '08:14:24', '08:15:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4167134476857,2.21526401645941&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-11-16'),
(51, 5, '', '41.41814753671323', '2.215370200574398', 'Carrer de Sàsser, 4, 08019 Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Thursday, Nov 16', '08:25:28', '08:26:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.41814753671323,2.215370200574398&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-16'),
(52, 5, '', '41.416877581195926', '2.2158466279506683', 'Carrer Galba, 26, 08019 Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Thursday, Nov 16', '08:28:19', '08:29:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.416877581195926,2.2158466279506683&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-16'),
(53, 5, '', '41.416908004099085', '2.2157416865229607', 'Carrer Galba, 2, 08019 Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Thursday, Nov 16', '08:31:28', '08:31:48 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.416908004099085,2.2157416865229607&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-16'),
(54, 1, '', '41.4166799576928', '2.21526995301247', 'Carrer de Trapani, 2\n08019 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Thursday, Nov 16', '08:57:21', '08:57:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4166799576928,2.21526995301247&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 9, 0, 0, 1, 0, 1, 1, '2017-11-16'),
(55, 1, '', '41.3015537794064', '2.07196455448866', 'C-32B, 18\n08820 El Prat de Llobregat, Barcelona', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Thursday, Nov 16', '18:50:58', '07:26:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3015537794064,2.07196455448866&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-16'),
(56, 5, '', '41.41169768561318', '2.218552641570568', 'Rambla de Prim, 19, 08019 Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Thursday, Nov 16', '19:43:00', '12:59:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.41169768561318,2.218552641570568&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 17, 0, 0, 1, 0, 1, 1, '2017-11-16'),
(57, 3, '', '28.4120686879058', '77.0432607829571', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Yyy', 0, '28.4090489551783', '77.050813883543', '21, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Friday, Nov 17', '05:09:40', '05:11:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120686879058,77.0432607829571&markers=color:red|label:D|28.4090489551783,77.050813883543&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(58, 3, '', '28.4120564751407', '77.0432035736227', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Gg', 0, '28.4128492630163', '77.0527306571603', 'C-5, Rosewood City Road, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', 'Friday, Nov 17', '09:12:55', '09:12:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120564751407,77.0432035736227&markers=color:red|label:D|28.4128492630163,77.0527306571603&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(59, 3, '', '28.4122064671731', '77.0432459369802', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Yyy', 0, '28.4124140053718', '77.0525586605072', 'B50, Rosewood City, Ghasola, Sector 49\nGurugram, Haryana 122018', 'Friday, Nov 17', '09:25:00', '09:25:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122064671731,77.0432459369802&markers=color:red|label:D|28.4124140053718,77.0525586605072&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(60, 3, '', '28.4121751437092', '77.0432463660836', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '26.4115421387624', '80.4096456617117', 'Greater Kailash Avenue, Greater Kailash, Jajmau\nKanpur, Uttar Pradesh 208008', 'Friday, Nov 17', '09:44:28', '12:59:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121751437092,77.0432463660836&markers=color:red|label:D|26.4115421387624,80.4096456617117&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 17, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(61, 3, '', '28.4121751437092', '77.0432463660836', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '26.4115421387624', '80.4096456617117', 'Greater Kailash Avenue, Greater Kailash, Jajmau\nKanpur, Uttar Pradesh 208008', 'Friday, Nov 17', '09:44:51', '09:47:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121751437092,77.0432463660836&markers=color:red|label:D|26.4115421387624,80.4096456617117&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(62, 3, '', '28.4120797607917', '77.0433212531428', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 1, '0.0', '0.0', 'No drop off point', 'Friday, Nov 17', '09:59:44', '10:01:05 AM', '', '17/11/2017', '15:29', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(63, 3, '', '28.4120797607917', '77.0433212531428', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 1, '0.0', '0.0', 'No drop off point', 'Friday, Nov 17', '09:59:47', '10:01:11 AM', '', '17/11/2017', '15:29', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(64, 3, '', '28.4120940485778', '77.0432902872562', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 1, '0.0', '0.0', 'No drop off point', 'Friday, Nov 17', '10:01:33', '10:03:07 AM', '', '17/11/2017', '15:31', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(65, 3, '', '28.412074858229', '77.0433056383144', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 1, '0.0', '0.0', 'No drop off point', 'Friday, Nov 17', '10:02:24', '10:03:02 AM', '', '17/11/2017', '15:32', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(66, 3, '', '28.412074858229', '77.0433056383144', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 1, '0.0', '0.0', 'No drop off point', 'Friday, Nov 17', '10:02:43', '10:02:55 AM', '', '17/11/2017', '15:32', 18, 2, 2, 1, 30, 0, 0, 1, 0, 1, 1, '2017-11-17'),
(67, 1, '', '41.2881637797332', '2.07279140711031', 'Avinguda les Garrigues, 1-3\n08820 El Prat de Llobregat, Barcelona', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Friday, Nov 17', '15:27:45', '03:56:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.2881637797332,2.07279140711031&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(68, 1, '', '41.4166751805269', '2.21525318920612', 'Carrer de Trapani, 2\n08019 Barcelona', '', 0, '41.3703041', '2.17141', 'Hotel Miramar', 'Saturday, Nov 18', '00:44:53', '12:47:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4166751805269,2.21525318920612&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(69, 3, '', '28.412080077973', '77.0433017214183', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4118159655284', '77.0505114644766', '28, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Saturday, Nov 18', '06:59:10', '06:59:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412080077973,77.0433017214183&markers=color:red|label:D|28.4118159655284,77.0505114644766&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(70, 3, '', '28.4120954997473', '77.043314921246', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '0.0', '0.0', 'No drop off point', 'Saturday, Nov 18', '07:02:58', '07:05:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120954997473,77.043314921246&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(71, 3, '', '28.4121312049003', '77.0432993397117', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4109610693757', '77.0480844005942', '5, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Saturday, Nov 18', '07:30:27', '07:30:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121312049003,77.0432993397117&markers=color:red|label:D|28.4109610693757,77.0480844005942&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(72, 3, '', '28.4121312049003', '77.0432993397117', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '28.4109610693757', '77.0480844005942', '5, Block C Uppal Southland Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Saturday, Nov 18', '07:30:51', '07:31:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121312049003,77.0432993397117&markers=color:red|label:D|28.4109610693757,77.0480844005942&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(73, 3, '', '28.4121704254487', '77.0433181151748', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '0.0', '0.0', 'No drop off point', 'Saturday, Nov 18', '07:36:27', '07:36:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121704254487,77.0433181151748&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(74, 5, '', '41.41566115625153', '2.2158918902277946', 'Carrer Felip de Malla, 6, 08019 Barcelona, Spain', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Saturday, Nov 18', '10:13:33', '10:35:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.41566115625153,2.2158918902277946&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(75, 3, '', '28.4121071252611', '77.043305544547', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Th?s ojjj kkk', 0, '0.0', '0.0', 'No drop off point', 'Saturday, Nov 18', '13:04:12', '01:04:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121071252611,77.043305544547&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(76, 3, '', '28.4121076135859', '77.0432996749878', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', 'Th?s ojjj kkk', 0, '0.0', '0.0', 'No drop off point', 'Saturday, Nov 18', '13:05:12', '01:05:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121076135859,77.0432996749878&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 2, 1, 0, 0, 1, 0, 1, 1, '2017-11-18'),
(77, 1, '', '41.4163045719529', '2.21725981682539', 'Carrer de Tarent, 4\n08019 Barcelona', '', 0, '41.42668', '2.2245813', 'Sant Adrià de Besòs', 'Monday, Nov 20', '11:04:45', '11:12:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4163045719529,2.21725981682539&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(78, 3, '', '28.4100457124135', '77.0408364012837', 'Dhani, Sector 72\nGurugram, Haryana', 'Bb', 0, '28.4160198663519', '77.0509188249707', '95, South City II, Sector 49\nGurugram, Haryana 122018', 'Monday, Nov 20', '12:33:38', '12:34:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4100457124135,77.0408364012837&markers=color:red|label:D|28.4160198663519,77.0509188249707&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(79, 3, '', '28.4120834020656', '77.0432832481239', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '', 0, '0.0', '0.0', 'No drop off point', 'Monday, Nov 20', '12:35:24', '12:35:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120834020656,77.0432832481239&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-20'),
(80, 3, '', '28.4162551807842', '77.0494774729013', '63, South City II, Sector 49\nGurugram, Haryana 122018', '', 0, '28.41319782171', '77.0426539331675', '1102, Sohna Road, Block S, Sispal Vihar, Sector 47\nGurugram, Haryana 122004', 'Monday, Nov 20', '12:36:21', '12:36:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4162551807842,77.0494774729013&markers=color:red|label:D|28.41319782171,77.0426539331675&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-20'),
(81, 5, '', '41.3889594437197', '2.17131190001965', 'Ronda de Sant Pere, 13\n08010 Barcelona', '', 0, '41.3853332', '2.1756182', 'Hotel Regencia Colon', 'Monday, Nov 20', '14:47:20', '02:53:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3889594437197,2.17131190001965&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(82, 3, '', '28.4109445100709', '77.0300893205256', 'E-14, Captain Chandan Lal Marg, E-Block, Tatvam Villas, Vipul World, Sector 48\nGurugram, Haryana 122018', 'Yyy', 0, '28.4124166593873', '77.0312052592635', 'Fazilpur Jharsa, Sector 72\nGurugram, Haryana', 'Monday, Nov 20', '15:29:46', '03:30:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4109445100709,77.0300893205256&markers=color:red|label:D|28.4124166593873,77.0312052592635&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 18, 2, 1, 2, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(83, 3, '', '41.3809642491702', '2.14174034585121', 'Carrer de Numància, 5\n08029 Barcelona', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Monday, Nov 20', '15:57:58', '04:06:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3809642491702,2.14174034585121&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(84, 3, '', '41.3869360481721', '2.17024739831686', 'Plaça de Catalunya, 9999\n08002 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Monday, Nov 20', '16:11:21', '04:11:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3869360481721,2.17024739831686&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-20'),
(85, 3, '', '41.3933679841857', '2.18632221221924', 'Carrer de Sardenya, 77\n08018 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Monday, Nov 20', '16:41:10', '04:41:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3933679841857,2.18632221221924&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-20'),
(86, 3, '', '41.3894432685941', '2.19405773886865', 'Carrer de la Marina, 42.B\n08005 Barcelona', '', 0, '0.0', '0.0', 'No drop off point', 'Monday, Nov 20', '16:42:34', '04:43:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3894432685941,2.19405773886865&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 2, 0, 13, 1, 0, 1, 1, '2017-11-20'),
(87, 3, '', '41.3872094768842', '2.19672616571188', 'Carrer de la Marina, 17-19\n08005 Barcelona', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Monday, Nov 20', '16:47:07', '04:53:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.3872094768842,2.19672616571188&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-20'),
(88, 5, '', '41.416578379984024', '2.2155120223760605', 'Carrer de Trapani, 4, 08019 Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Tuesday, Nov 21', '08:05:30', '08:06:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.416578379984024,2.2155120223760605&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(89, 5, '', '41.416578379984024', '2.2155120223760605', 'Carrer de Trapani, 4, 08019 Barcelona, Spain', '', 0, '41.297445', '2.0832941', 'Barcelona–El Prat Airport', 'Tuesday, Nov 21', '09:54:05', '10:02:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.416578379984024,2.2155120223760605&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(90, 3, '', '41.4166027298733', '2.21386525920534', 'Carrer de Cristóbal de Moura, 228\n08019 Barcelona', '', 0, '41.297445', '2.0832941', 'Barcelona–El Prat Airport', 'Tuesday, Nov 21', '10:07:12', '10:35:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4166027298733,2.21386525920534&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 6, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-21'),
(91, 3, '', '41.4896192105312', '2.08649173378944', 'B-30, 600\n08174 Cerdanyola del Vallès, Barcelona', '', 0, '41.3850639', '2.1734035', 'Barcelona', 'Wednesday, Nov 22', '17:18:11', '05:28:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.4896192105312,2.08649173378944&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-22'),
(92, 6, '', '41.43452876714308', '2.221533916890621', 'Carrer de Covadonga, 16, 08918 Santa Coloma de Gramenet, Barcelona, Spain', '', 0, '', '', 'Set your drop point', 'Thursday, Nov 23', '20:35:21', '08:38:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|41.43452876714308,2.221533916890621&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 27, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-23'),
(93, 6, '', '28.413500000000003', '77.041534', 'Spaze i-Tech Park', 'chug', 0, '', '', 'Set your drop point', 'Saturday, Nov 25', '10:39:14', '10:39:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.413500000000003,77.041534&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-25'),
(94, 3, '', '28.413983401564764', '77.04359505325556', '1002, Sispal Vihar Internal Rd, Sispal Vihar, Sector 49, Gurugram, Haryana 122002, India', 'ffgg', 2, '28.4592693', '77.0724192', 'Huda City Centre', 'Monday, Nov 27', '06:22:35', '06:22:35 AM', '', '27/11/2017', '11:52', 0, 2, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-27');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '112', 1),
(3, 'keselamatan', '999', 2),
(4, 'ambulance', '066', 2),
(6, 'Breakdown', '199', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Pollution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(34, 128, 1, 1),
(33, 121, 2, 1),
(32, 121, 1, 1),
(35, 128, 2, 1),
(36, 128, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 2, 'uploads/driver/1510669878document_image_12.jpg', '2018-10-24', 1),
(2, 1, 1, 'uploads/driver/1510669891document_image_11.jpg', '2018-05-31', 1),
(3, 3, 3, 'uploads/driver/1510670591document_image_33.jpg', '30-11-2017', 1),
(4, 3, 2, 'uploads/driver/1510670603document_image_32.jpg', '30-11-2017', 1),
(5, 3, 4, 'uploads/driver/1510670616document_image_34.jpg', '25-11-2017', 1),
(6, 4, 1, 'uploads/driver/1510672595document_image_41.jpg', '3-1-2020', 1),
(7, 4, 2, 'uploads/driver/1510672618document_image_42.jpg', '4-9-2020', 1),
(8, 4, 4, 'uploads/driver/1510672634document_image_44.jpg', '14-11-2017', 1),
(9, 5, 1, 'uploads/driver/1510674022document_image_51.jpg', '5-3-2026', 1),
(10, 6, 1, 'uploads/driver/1510674602document_image_61.jpg', '2017-11-14', 1),
(11, 6, 2, 'uploads/driver/1510674611document_image_62.jpg', '2017-11-14', 1),
(12, 6, 4, 'uploads/driver/1510674620document_image_64.jpg', '2017-12-28', 1),
(13, 7, 3, 'uploads/driver/1510676055document_image_73.jpg', '2017-11-29', 1),
(14, 7, 2, 'uploads/driver/1510676063document_image_72.jpg', '2017-11-30', 1),
(15, 7, 4, 'uploads/driver/1510676072document_image_74.jpg', '2017-11-30', 1),
(16, 9, 3, 'uploads/driver/1510721114document_image_93.jpg', '30-11-2017', 1),
(17, 9, 2, 'uploads/driver/1510721123document_image_92.jpg', '30-11-2017', 1),
(18, 9, 4, 'uploads/driver/1510721131document_image_94.jpg', '30-11-2017', 1),
(19, 10, 3, 'uploads/driver/1510721227document_image_103.jpg', '2017-11-28', 1),
(20, 10, 2, 'uploads/driver/1510721234document_image_102.jpg', '2017-11-29', 1),
(21, 10, 4, 'uploads/driver/1510721242document_image_104.jpg', '2017-11-28', 1),
(22, 11, 3, 'uploads/driver/1510721764document_image_113.jpg', '21-3-2019', 1),
(23, 11, 2, 'uploads/driver/1510721776document_image_112.jpg', '22-11-2018', 1),
(24, 11, 4, 'uploads/driver/1510721793document_image_114.jpg', '7-12-2018', 1),
(25, 12, 3, 'uploads/driver/1510722542document_image_123.jpg', '2017-11-29', 1),
(26, 12, 2, 'uploads/driver/1510722550document_image_122.jpg', '2017-11-29', 1),
(27, 12, 4, 'uploads/driver/1510722557document_image_124.jpg', '2017-11-29', 1),
(28, 13, 3, 'uploads/driver/1510722635document_image_133.jpg', '28-9-2018', 1),
(29, 13, 2, 'uploads/driver/1510722652document_image_132.jpg', '8-12-2018', 1),
(30, 13, 4, 'uploads/driver/1510722665document_image_134.jpg', '29-12-2018', 1),
(31, 14, 3, 'uploads/driver/1510724773document_image_143.jpg', '2017-11-29', 1),
(32, 14, 2, 'uploads/driver/1510724780document_image_142.jpg', '2017-11-28', 1),
(33, 14, 4, 'uploads/driver/1510724788document_image_144.jpg', '2017-11-21', 1),
(34, 15, 1, 'uploads/driver/1510740869document_image_151.jpg', '2024-02-25', 1),
(35, 15, 4, 'uploads/driver/1510742215document_image_154.jpg', '2017-12-31', 1),
(36, 15, 2, 'uploads/driver/1510742262document_image_152.jpg', '2018-09-30', 1),
(37, 18, 3, 'uploads/driver/1510911295document_image_183.jpg', '2017-11-17', 1),
(38, 18, 2, 'uploads/driver/1510911313document_image_182.jpg', '2017-11-17', 1),
(39, 18, 4, 'uploads/driver/1510911320document_image_184.jpg', '2017-11-17', 1),
(40, 19, 1, 'uploads/driver/1510929462document_image_191.jpg', '17-11-2017', 1),
(41, 20, 4, 'uploads/driver/1510930099document_image_204.jpg', '17-11-2017', 1),
(42, 20, 1, 'uploads/driver/1510930124document_image_201.jpg', '17-11-2017', 1),
(43, 20, 2, 'uploads/driver/1510930160document_image_202.jpg', '17-11-2017', 1),
(44, 21, 1, 'uploads/driver/1510945249document_image_211.jpg', '2018-05-31', 1),
(45, 21, 2, 'uploads/driver/1510945289document_image_212.jpg', '2018-03-22', 1),
(46, 21, 4, 'uploads/driver/1510945312document_image_214.jpg', '2017-11-17', 1),
(47, 8, 1, 'uploads/driver/1510953967document_image_81.jpg', '17-11-2025', 1),
(48, 8, 2, 'uploads/driver/1510953987document_image_82.jpg', '17-11-2025', 1),
(49, 8, 4, 'uploads/driver/1510954004document_image_84.jpg', '17-11-2025', 1),
(50, 22, 1, 'uploads/driver/1511110643document_image_221.jpg', '2023-09-26', 1),
(51, 27, 1, 'uploads/driver/1511469196document_image_271.jpg', '2-11-2018', 1),
(52, 27, 2, 'uploads/driver/1511469212document_image_272.jpg', '5-7-2019', 1),
(53, 27, 4, 'uploads/driver/1511469273document_image_274.jpg', '6-3-2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-11-14 14:31:36', '', '', 0, 0, '2017-11-14'),
(2, 3, '2017-11-14 14:43:45', '', '', 0, 0, '2017-11-14'),
(3, 4, '2017-11-14 15:17:28', '', '', 0, 0, '2017-11-14'),
(4, 6, '2017-11-14 16:09:34', '2017-11-14 16:09:08', '0 Hours 18 Minutes', 0, 18, '2017-11-14'),
(5, 7, '2017-11-15 04:43:34', '2017-11-15 04:45:19', '12 Hours 28 Minutes', 12, 28, '2017-11-14'),
(6, 9, '2017-11-15 04:46:51', '2017-11-15 04:56:37', '0 Hours 9 Minutes', 0, 9, '2017-11-14'),
(7, 10, '2017-11-15 04:55:55', '2017-11-15 04:55:52', '0 Hours 8 Minutes', 0, 8, '2017-11-14'),
(8, 11, '2017-11-15 04:57:20', '', '', 0, 0, '2017-11-14'),
(9, 12, '2017-11-15 05:34:42', '2017-11-15 05:34:57', '0 Hours 23 Minutes', 0, 23, '2017-11-14'),
(10, 13, '2017-11-15 05:34:29', '', '', 0, 0, '2017-11-14'),
(11, 14, '2017-11-15 05:59:58', '2017-11-15 06:00:05', '0 Hours 8 Minutes', 0, 8, '2017-11-14'),
(12, 14, '2017-11-15 06:04:22', '', '', 0, 0, '2017-11-15'),
(13, 4, '2017-11-15 17:20:33', '2017-11-16 08:24:05', '15 Hours 13 Minutes', 15, 13, '2017-11-15'),
(14, 6, '2017-11-15 11:38:08', '2017-11-15 17:18:55', '6 Hours 147 Minutes', 6, 147, '2017-11-15'),
(15, 6, '2017-11-16 19:37:09', '2017-11-16 19:41:52', '10 Hours 25 Minutes', 10, 25, '2017-11-16'),
(16, 4, '2017-11-16 19:42:44', '2017-11-20 16:43:39', '21 Hours 49 Minutes', 21, 49, '2017-11-16'),
(17, 18, '2017-11-17 09:35:34', '2017-11-20 12:36:47', '3 Hours 1 Minutes', 3, 1, '2017-11-17'),
(18, 20, '2017-11-17 15:05:14', '2017-11-17 15:05:07', '0 Hours 13 Minutes', 0, 13, '2017-11-17'),
(19, 6, '2017-11-18 00:43:00', '2017-11-20 11:03:46', '10 Hours 20 Minutes', 10, 20, '2017-11-17'),
(20, 21, '2017-11-17 19:04:49', '2017-11-18 03:08:45', '8 Hours 3 Minutes', 8, 3, '2017-11-17'),
(21, 8, '2017-11-17 21:27:21', '2017-11-17 21:30:40', '0 Hours 3 Minutes', 0, 3, '2017-11-17'),
(22, 21, '2017-11-19 15:39:45', '2017-11-19 15:41:13', '0 Hours 1 Minutes', 0, 1, '2017-11-19'),
(23, 21, '2017-11-20 09:38:57', '2017-11-20 09:41:24', '0 Hours 2 Minutes', 0, 2, '2017-11-20'),
(24, 18, '2017-11-20 12:36:50', '', '', 0, 0, '2017-11-20'),
(25, 6, '2017-11-20 16:10:58', '2017-11-20 16:46:12', '0 Hours 35 Minutes', 0, 35, '2017-11-20'),
(26, 4, '2017-11-20 16:46:42', '2017-11-21 10:06:40', '17 Hours 19 Minutes', 17, 19, '2017-11-20'),
(27, 6, '2017-11-21 08:04:11', '', '', 0, 0, '2017-11-21'),
(28, 4, '2017-11-22 17:17:53', '', '', 0, 0, '2017-11-22'),
(29, 27, '2017-11-23 20:34:43', '', '', 0, 0, '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(41, 'Vietnamese', 1),
(40, 'French', 1),
(39, 'Spanish', 2),
(38, 'Arabic', 1),
(37, 'Arabic', 2),
(36, 'Aymara', 2),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 1, 1, 4.5, '', 9, 0, '', '2017-11-15'),
(2, 5, 1, 0, '', 9, 5, '', '2017-11-15'),
(3, 19, 3, 1, '', 14, 0, '', '2017-11-15'),
(4, 139, 18, 3, '', 8, 0, '', '2017-11-15'),
(5, 26, 3, 3, '', 14, 0, '', '2017-11-15'),
(6, 27, 1, 2, '', 4, 1.5, '', '2017-11-15'),
(7, 29, 1, 2, '', 6, 0.5, '', '2017-11-15'),
(8, 30, 1, 0, '', 4, 2, '', '2017-11-15'),
(9, 31, 1, 1.5, '', 4, 2, '', '2017-11-15'),
(10, 33, 1, 2, '', 6, 2, '', '2017-11-15'),
(11, 10, 4, 4, '', 11, 0, '', '2017-11-15'),
(12, 11, 4, 4, '', 11, 0, '', '2017-11-15'),
(13, 36, 4, 0, '', 11, 4.5, '', '2017-11-15'),
(14, 12, 4, 4.5, '', 11, 0, '', '2017-11-15'),
(15, 37, 4, 0, '', 11, 4, '', '2017-11-15'),
(16, 13, 4, 4, '', 11, 0, '', '2017-11-15'),
(17, 38, 4, 0, '', 11, 4, '', '2017-11-15'),
(18, 14, 1, 0, '', 4, 0, '', '2017-11-15'),
(19, 43, 1, 1, '', 4, 0.5, '', '2017-11-15'),
(20, 46, 1, 0, '', 4, 1, '', '2017-11-15'),
(21, 48, 1, 0.5, '', 6, 1.5, '', '2017-11-15'),
(22, 49, 1, 1, '', 4, 0.5, '', '2017-11-15'),
(23, 18, 5, 1, '', 6, 0, '', '2017-11-16'),
(24, 51, 5, 0, '', 6, 1, '', '2017-11-16'),
(25, 52, 5, 0, '', 4, 1, '', '2017-11-16'),
(26, 55, 1, 1, '', 4, 0.5, '', '2017-11-16'),
(27, 57, 3, 3, '', 14, 0, '', '2017-11-17'),
(28, 59, 3, 3, '', 14, 0, '', '2017-11-17'),
(29, 61, 3, 3.5, '', 18, 5, '', '2017-11-17'),
(30, 67, 1, 0.5, '', 6, 1, '', '2017-11-17'),
(31, 68, 1, 0.5, '', 6, 1, '', '2017-11-18'),
(32, 34, 1, 1, '', 6, 1, '', '2017-11-18'),
(33, 70, 3, 2.5, '', 18, 0, '', '2017-11-18'),
(34, 72, 3, 3, '', 18, 2, '', '2017-11-18'),
(35, 73, 3, 3, '', 18, 2, '', '2017-11-18'),
(36, 74, 5, 0, '', 6, 1, '', '2017-11-18'),
(37, 28, 1, 0.5, '', 6, 1, '', '2017-11-19'),
(38, 77, 1, 1, '', 4, 1, '', '2017-11-20'),
(39, 81, 5, 1, '', 4, 1, '', '2017-11-20'),
(40, 82, 3, 2, '', 18, 2, '', '2017-11-20'),
(41, 83, 3, 1, '', 4, 1.5, '', '2017-11-20'),
(42, 87, 3, 1, '', 4, 1, '', '2017-11-20'),
(43, 39, 5, 1, '', 6, 0, '', '2017-11-21'),
(44, 88, 5, 0, '', 6, 1, '', '2017-11-21'),
(45, 89, 5, 0, '', 4, 1, '', '2017-11-21'),
(46, 40, 5, 1, '', 4, 0, '', '2017-11-21'),
(47, 90, 3, 1, '', 6, 1, '', '2017-11-21'),
(48, 91, 3, 0.5, '', 4, 0.5, '', '2017-11-22'),
(49, 92, 6, 0, '', 27, 1, '', '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 0, 0),
(2, 1, 1, 0, 0),
(3, 1, 1, 0, 0),
(4, 1, 1, 0, 0),
(5, 1, 1, 0, 0),
(6, 1, 1, 0, 0),
(7, 1, 1, 0, 0),
(8, 1, 1, 0, 0),
(9, 1, 3, 0, 1),
(10, 1, 3, 0, 2),
(11, 1, 3, 7, 3),
(12, 1, 3, 7, 4),
(13, 1, 1, 9, 5),
(14, 1, 3, 10, 6),
(15, 1, 3, 0, 7),
(16, 1, 3, 9, 8),
(17, 1, 3, 9, 9),
(18, 1, 3, 12, 10),
(19, 1, 3, 12, 11),
(20, 1, 3, 0, 12),
(21, 1, 3, 0, 13),
(22, 1, 3, 0, 14),
(23, 1, 3, 12, 15),
(24, 1, 3, 0, 16),
(25, 1, 4, 13, 17),
(26, 1, 4, 0, 18),
(27, 1, 3, 14, 19),
(28, 1, 4, 0, 20),
(29, 1, 3, 0, 21),
(30, 1, 3, 11, 22),
(31, 1, 3, 0, 23),
(32, 1, 4, 11, 24),
(33, 1, 3, 0, 25),
(34, 1, 3, 14, 26),
(35, 1, 1, 4, 27),
(36, 1, 1, 6, 28),
(37, 1, 1, 6, 29),
(38, 1, 1, 0, 0),
(39, 1, 1, 4, 30),
(40, 1, 1, 4, 31),
(41, 1, 1, 6, 32),
(42, 1, 1, 6, 33),
(43, 1, 1, 6, 34),
(44, 1, 1, 0, 0),
(45, 1, 1, 0, 0),
(46, 1, 1, 0, 0),
(47, 1, 4, 11, 35),
(48, 1, 4, 11, 36),
(49, 1, 4, 11, 37),
(50, 1, 4, 11, 38),
(51, 1, 4, 0, 39),
(52, 1, 4, 0, 40),
(53, 1, 4, 0, 41),
(54, 1, 4, 0, 42),
(55, 1, 1, 4, 43),
(56, 1, 1, 18, 44),
(57, 1, 1, 4, 45),
(58, 1, 1, 4, 46),
(59, 1, 1, 6, 47),
(60, 1, 1, 6, 48),
(61, 1, 1, 0, 0),
(62, 1, 1, 0, 0),
(63, 1, 1, 0, 0),
(64, 1, 1, 0, 0),
(65, 1, 1, 4, 49),
(66, 1, 1, 4, 50),
(67, 1, 5, 6, 51),
(68, 1, 5, 4, 52),
(69, 1, 5, 6, 53),
(70, 1, 1, 6, 54),
(71, 1, 1, 4, 55),
(72, 1, 5, 0, 0),
(73, 1, 5, 4, 56),
(74, 1, 3, 14, 57),
(75, 1, 3, 0, 58),
(76, 1, 3, 14, 59),
(77, 1, 3, 0, 60),
(78, 1, 3, 18, 61),
(79, 1, 3, 18, 62),
(80, 1, 3, 18, 63),
(81, 1, 3, 18, 64),
(82, 1, 3, 18, 65),
(83, 1, 3, 18, 66),
(84, 1, 1, 6, 67),
(85, 1, 1, 6, 68),
(86, 1, 1, 0, 0),
(87, 1, 3, 0, 69),
(88, 1, 3, 18, 70),
(89, 1, 3, 0, 71),
(90, 1, 3, 18, 72),
(91, 1, 3, 18, 73),
(92, 1, 5, 0, 0),
(93, 1, 5, 6, 74),
(94, 1, 3, 0, 75),
(95, 1, 3, 0, 76),
(96, 1, 1, 4, 77),
(97, 1, 3, 18, 78),
(98, 1, 3, 0, 79),
(99, 1, 3, 0, 80),
(100, 1, 5, 4, 81),
(101, 1, 3, 18, 82),
(102, 1, 3, 0, 0),
(103, 1, 3, 0, 0),
(104, 1, 3, 4, 83),
(105, 1, 3, 0, 84),
(106, 1, 3, 0, 85),
(107, 1, 3, 4, 86),
(108, 1, 3, 4, 87),
(109, 1, 5, 6, 88),
(110, 1, 5, 4, 89),
(111, 1, 3, 6, 90),
(112, 1, 3, 4, 91),
(113, 1, 6, 27, 92),
(114, 1, 6, 0, 93),
(115, 1, 3, 0, 94);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'yasir  .', 'yasir786@hotmail.es', '+34667362763', '20061983', '', '', 0, '0', 'Tuesday, Nov 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '1.22222222222', 0, '', 1, '2017-11-14', 1),
(2, 1, 'Ashish Apporio ', '', '+34667362736', '', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '104065945547688247074', 'Ashish Apporio', 'ashish@apporio.com', 'https://lh3.googleusercontent.com/-mP_gq66X3bs/AAAAAAAAAAI/AAAAAAAAAAA/ANQ0kf41-C9NDj4nR9zS0rY2mJbN4co1KA/s400/photo.jpg', '', '', 0, 0, '', 0, '', 3, '2017-11-15', 1),
(3, 1, 'try .', 'amir@apporio.com', '+34667367236', '12345678', 'http://apporio.org/YellowTaxi/yellowtaxi/uploads/swift_file74.jpeg', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '1.15384615385', 0, '', 1, '2017-11-15', 1),
(4, 1, 'Preeti Sinha', '', '+91+34697362761', '', '', '', 0, '0', 'Wednesday, Nov 15', '', 0, 0, 0, 0, 0, '177489349469206', 'upanu1012@gmail.com', 'http://graph.facebook.com/177489349469206/picture?type=large', 'Preeti', 'Sinha', '', '', '', '', '', '', 0, 0, '1.78571428571', 0, '', 2, '2017-11-15', 1),
(5, 1, 'saif .', 'yasirirfan198342@gmail.com', '+34602386391', '20061983', '', '', 0, '0', 'Thursday, Nov 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '0.75', 0, '', 1, '2017-11-16', 1),
(6, 1, 'kashif .', 'nazirmiankashif@yahoo.es', '+34670806352', '5097', '', '', 0, '0', 'Friday, Nov 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '1', 0, '', 1, '2017-11-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, '7A0EA430D62D2445B0CD52700952253FC92B39F614F84544F39FC05BFDC3834A', 1, 'BE009F0E-6A9F-4856-BCAA-1E4EE002D14E', 1),
(2, 5, 'eN9K50poDAE:APA91bEfeD7JqbJtck92-3-oPwF5LjMt3sYtIdQ8QsZc3B3h6-JJ_Pm3qKvbJw_rI40SAo44qIGFU05BA0L5q9rDya2x3AqS9LgFnEnnzhZPpHFUXJGiIVgoNcJ75O-dNXGQDmec9dEv', 0, '4266e6916bca5b6a', 1),
(3, 1, 'dmaDYPzZ5SY:APA91bGgmaI-B1nSMROemjkgm1ZWitrj2BRVbS_28aR58xgSxsCh565eAQoYsoWHB4LFf6JJfj-nl6akb0eLvnwcIxZhf6ekNAzwm_xarnMBoeRrKT5jvdZsaupW_hs8RYsSSmS2D-Ja', 0, '9f1891df4b8e4e77', 0),
(4, 3, '5C50A070E5EDE1F6A2BB264A17F7285FA25E6C7EE93998C668B6A5D876D3D6A9', 1, '4768317C-BFB0-4885-B9D4-B110B86BA7FB', 1),
(5, 4, 'emIbnkN3RLI:APA91bG7WN2G5aaCoc6AFKWI3qHmSkSs_uh-Aw0ivO8l2YvIZh6xbfZU51A8UANLXVvJZ-d7uKSpR9oflrWoNbVvv8la8TuMJcHa3AWH0aPb7YikFS5dMlc4mCbhcf89IDjE3zLuUkoO', 0, '32c87e564d926edc', 1),
(6, 3, 'EF60F9ABFC746F39C0A90069253DF865412C848A52144202BFFDF8F729F4B31A', 1, 'E128D363-73F1-4757-B199-3B19985B7E6D', 1),
(7, 3, '3B2BACB29C557A4D4ED0DDEFBF219989DE4FBEE30E633B830AFCD7E0B02C71EE', 1, '579B4D10-F30B-46EF-911B-EB5769D6244D', 1),
(8, 3, '88B70B87A7BECE2540059FAFAEEE5F29B9132CCB47B36EA8E1A047BAAF6DC3F1', 1, '66CB753C-7684-4BC2-81ED-A8D5D8D0A6F7', 1),
(9, 3, '630859C312A2F9D2CD43814A00B96C1FB56C4894F7FBBB25240C0C42F7CE206D', 1, '30BD39BC-38E7-476E-86FA-1E0C99D44594', 1),
(10, 7, 'C7E91C30C77C54E1F96B10C27AF7715857AB8977978F9E9C5A46F027D4A9BACA', 1, '6E830772-5869-4CB8-82E6-689B86672D2F', 1),
(11, 6, 'czUov0oUzA8:APA91bFYNmaTyTgNLtLkejIZNF7nkGNQgi1aEuZ68gzp5VqsgRytkB0qfGj5h86ZxN7Ee7nXThTgUzMqf2znKbsTYK7c5p0-cbMJV1tnPkVG36GOECOTxX_08q5b5WTvTukHbZMZo_Bv', 0, 'c3ac867f4da44e19', 1),
(12, 6, 'crKN5ff69fQ:APA91bFhoWRu1d5pDjNl-AADE1w4iFfP1Egy29DBcf94_sRhQCigZeBi6GR6W7jsnKlA4T7-xlCZUDLQlzTg_u3fzPikVFuH0E-8uC6vGG8Kzry4m1ZMWqoh6RrdPayOzEpM6HRl1p-8', 0, '9e7c9bbae686f364', 1),
(13, 3, 'dPsF6uASgEQ:APA91bFj4SwZROjpGpiMpbrZruDcEv56HCIm69LYm8uhGGU__EUSkO-5SivLXmevugEb69Zj1zcqiAmLKJGoPwij5VK-hIFiUiFit1x6xfgb0a_X_PFdrBJm2Rp7Yqvgf_Y1Z50y-l-8', 0, '96ac6773bae674b8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.'),
(2, 'معلومات عنا', 'أبوريو إنفولابس الجندي. المحدودة هي شهادة إسو\r\nتطبيقات الهاتف المتحرك وشبكة تطوير التطبيقات على شبكة الإنترنت في الهند. نحن نقدم نهاية إلى نهاية الحل من تصميم لتطوير البرمجيات. نحن فريق من 30+ الناس الذي يتضمن المطورين ذوي الخبرة والمصممين المبدعين.\r\nومن المعروف أبوريو إنفولابس لتقديم برامج ذات جودة ممتازة لعملائها. وتنتشر قاعدة عملائنا في أكثر من 20 دولة بما في ذلك الهند والولايات المتحدة والمملكة المتحدة وأستراليا وإسبانيا والنرويج والسويد والإمارات العربية المتحدة وقطر وسنغافورة وماليزيا ونيجيريا وجنوب أفريقيا وإيطاليا وبرمودا وهونغ كونغ.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact info', 'hello@apporio.com', '+91 8800633884', 'apporio', '467, Tower B1,, Spaze iTech Park, Sohna Road, Sector 49, Gurugram, Haryana 122018'),
(2, 'اتصل بنا', 'معلومات الاتصال', 'hello@apporio.com', '+91 8800633884', 'apporio', 'غوروغرام، هريانا، الهند أبوريو إنفولابس الجندي. Ltd.،');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_headings`
--

CREATE TABLE `web_headings` (
  `heading_id` int(11) NOT NULL,
  `heading_1` text NOT NULL,
  `heading1` text NOT NULL,
  `heading2` text NOT NULL,
  `heading3` text NOT NULL,
  `heading4` text NOT NULL,
  `heading5` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_headings`
--

INSERT INTO `web_headings` (`heading_id`, `heading_1`, `heading1`, `heading2`, `heading3`, `heading4`, `heading5`) VALUES
(1, 'Book an Yellow Taxi to your destination in town', 'A cab for every occasion and pocket ', 'Meet our Awesome Fleet', 'The widest variety of cars to choose from', 'Why Ride with Yellow Taxi?', 'A cab for every occasion and pocket'),
(2, 'حجز أبوريو تاكسي إلى وجهتك في المدينة', 'سيارة أجرة في كل مناسبة والجيب', 'تلبية أسطولنا رهيبة', 'أوسع مجموعة من السيارات للاختيار من بينها', 'لماذا ركوب مع أبوريو تاكسي؟', 'سيارة أجرة في كل مناسبة والجيب');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `app_name` varchar(255) NOT NULL DEFAULT '',
  `web_footer` varchar(765) DEFAULT '',
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `app_name`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Yellow Taxi || Website', 'Yellow Taxi', '2017 Yellow Taxi', 'uploads/website/banner_1501227855.jpg', '', '', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', '', '', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', '', '', 'uploads/website/heading1_img1501228907.png', '', 'uploads/website/heading2_img1501228907.png', '', '', '', 'uploads/website/heading3_img1501228907.png', '', '', '', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', '', '', 'uploads/website/features1_bg1501241213.png', '', '', 'uploads/website/features2_bg1501241213.png'),
(2, 'أبوريوتاكسي || موقع الكتروني', 'أبوريو تاكسي', '2017 أبوريو تاكسي', 'uploads/website/banner_1501227855.jpg', '', '', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', '', '', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', '', '', 'uploads/website/heading1_img1501228907.png', '', 'uploads/website/heading2_img1501228907.png', '', '', '', 'uploads/website/heading3_img1501228907.png', '', '', '', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', NULL, '', '', 'uploads/website/features1_bg1501241213.png', '', '', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `heading_arabic` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `content_arabic` text NOT NULL,
  `long_content` text NOT NULL,
  `long_content_arabic` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `big_image` varchar(255) NOT NULL DEFAULT '',
  `blog_date` varchar(255) NOT NULL DEFAULT '',
  `blog_time` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `heading_arabic`, `content`, `content_arabic`, `long_content`, `long_content_arabic`, `image`, `big_image`, `blog_date`, `blog_time`) VALUES
(1, 'Cabs for Every Pocket', 'سيارات الأجرة لكل جيب', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket   ', 'من سيارات السيدان وسيارات الدفع الرباعي إلى السيارات الفاخرة للمناسبات الخاصة، لدينا سيارات الأجرة لتناسب كل جيب', '<p>trthrghjjyjyrytjrgtpoogjerejggeierjg</p><p>gojeryierehe</p><p>grhpoioierejhper</p>', '', 'webstatic/img/ola-article/why-ola-2.jpg', '', 'Wednesday, Oct 25', '22:17:58'),
(2, 'Secure and Safer Rides', 'ركوب آمنة وأكثر أمنا', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'السائقين التحقق، زر تنبيه في حالات الطوارئ، وتتبع ركوب الحية هي بعض من الميزات التي لدينا في مكان لضمان تجربة السفر آمنة.', '', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', ''),
(3, 'Yellow Taxi Select', 'أبوريو حدد', 'A membership program with Yellow Taxi that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', 'برنامج العضوية مع أبوريو التي تمكنك من ركوب رئيس سيدان في فارس مصغرة، سيارات الأجرة كتاب دون التسعير الذروة، ولها وقت الانتظار صفر', '', '', 'webstatic/img/ola-article/why-ola-2.jpg', '', '', ''),
(4, 'In Cab Entertainment', 'في الكابيه الترفيه', 'Play music, watch videos and a lot more with Yellow Taxi Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', 'شغيل الموسيقى، ومشاهدة أشرطة الفيديو وأكثر من ذلك بكثير مع أبوريو اللعب! أيضا البقاء على اتصال حتى لو كنت مسافرا من خلال مناطق الشبكة الفقيرة مع شركائنا في مرفق واي فاي مجانا.', '', '', 'webstatic/img/ola-article/why-ola-9.jpg', '', '', ''),
(5, 'Cashless Rides', 'ركوب غير النقدي', 'Now go cashless and travel easy. Simply recharge your Yellow Taxi money or add your credit/debit card to enjoy hassle free payments.', 'شغيل الموسيقى، ومشاهدة أشرطة الفيديو وأكثر من ذلك بكثير مع أبوريو اللعب! من السهل جدا أن تكون قادرا على البقاء.', '', '', 'webstatic/img/ola-article/why-ola-5.jpg', '', '', ''),
(6, 'Share and Express', 'حصة والتعبير', 'To travel with the lowest fares choose Yellow Taxi Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', 'للسفر مع أدنى الأسعار اختيار أبوريو حصة. للحصول على تجربة سفر أسرع لدينا حصة اكسبرس على بعض الطرق الثابتة مع الانحرافات صفر. اختيار رحلتك والتكبير بعيدا!', '', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `favourites_driver`
--
ALTER TABLE `favourites_driver`
  ADD PRIMARY KEY (`favourite_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_headings`
--
ALTER TABLE `web_headings`
  ADD PRIMARY KEY (`heading_id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourites_driver`
--
ALTER TABLE `favourites_driver`
  MODIFY `favourite_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_headings`
--
ALTER TABLE `web_headings`
  MODIFY `heading_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
