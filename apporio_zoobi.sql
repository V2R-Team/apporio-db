-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_zoobi`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_setting`
--

CREATE TABLE `account_setting` (
  `acc_id` int(11) NOT NULL,
  `acc_site_title` varchar(255) NOT NULL,
  `acc_email` varchar(255) NOT NULL,
  `acc_url` varchar(255) NOT NULL,
  `acc_phone` varchar(255) NOT NULL,
  `acc_mobile` varchar(255) NOT NULL,
  `acc_img` varchar(255) NOT NULL,
  `acc_address` text NOT NULL,
  `acc_country` varchar(255) NOT NULL,
  `acc_state` varchar(255) NOT NULL,
  `acc_city` varchar(255) NOT NULL,
  `acc_zip` varchar(255) NOT NULL,
  `acc_map` varchar(255) NOT NULL,
  `acc_fb` varchar(255) NOT NULL,
  `acc_tw` varchar(255) NOT NULL,
  `acc_go` varchar(255) NOT NULL,
  `acc_dribble` varchar(255) NOT NULL,
  `acc_rss` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_setting`
--

INSERT INTO `account_setting` (`acc_id`, `acc_site_title`, `acc_email`, `acc_url`, `acc_phone`, `acc_mobile`, `acc_img`, `acc_address`, `acc_country`, `acc_state`, `acc_city`, `acc_zip`, `acc_map`, `acc_fb`, `acc_tw`, `acc_go`, `acc_dribble`, `acc_rss`) VALUES
(1, 'Realty Singh', 'bhagyashree@wscubetech.com', '', '1234', '12345', 'img/.png', 'aafhff', 'India', 'Rajasthan', 'Jodhpur', '342001', 'aaaffhhf', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://accounts.google.com/', 'https://dribbble.com/', 'http://www.rss.org/');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_img`, `admin_password`, `admin_phone`, `admin_email`) VALUES
(1, 'admin', 'Infolabs PVT. LTD.', 'img/1.png', 'admin', '9876543211', 'rupam.apporio@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `bill_calculate`
--

CREATE TABLE `bill_calculate` (
  `latlong_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_lat` varchar(255) NOT NULL,
  `driver_long` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_image`, `car_type_id`, `status`) VALUES
(1, 'Grand i10', '', 1, 1),
(4, 'Creta', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_image`, `status`) VALUES
(1, 'Luxury', '', 'uploads/car/car_1.png', 1),
(2, 'SUV', '', 'uploads/car/car_2.png', 1),
(3, 'Sedan', '', 'uploads/car/car_3.png', 1),
(4, 'Hatchback', '', 'uploads/car/car_4.png', 1),
(8, 'Mini', '', 'uploads/car/car_8.png', 1),
(20, 'bike', '', 'uploads/car/car_20.png', 1),
(21, 'Cab', '', 'uploads/car/car_21.png', 1),
(22, 'Laundry', '', 'uploads/car/car_22.png', 1),
(23, 'Food', '', 'uploads/car/car_23.png', 1),
(24, 'Grocery', '', 'uploads/car/car_24.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_name_arabic` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_name_arabic`, `status`) VALUES
(2, 'Gurugram', '', 1),
(3, 'Dummy City', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `expiry_date`, `status`) VALUES
(13, 'F7TG5D', '100', '2016-12-27', 1),
(14, 'GBH56D', '150', '2016-12-31', 1),
(15, 'FV453F', '50', '2017-01-31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `ride_time`, `driver_id`, `amount`, `distance`, `tot_time`, `payment_status`) VALUES
(1, 6, '28.4121299', '77.0433584', '28.4121299', '77.0433584', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '17:49:2', '17:49:22', '17:49:29', '0:0:20', '0:0:7', 3, '100', '0', '0:0:7', 0),
(2, 7, '28.4120402', '77.0433584', '28.4120402', '77.0433584', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '17:50:36', '17:50:41', '17:50:46', '0:0:5', '0:0:5', 3, '100', '0', '0:0:5', 0),
(3, 8, '28.4120402', '77.0433584', '28.4120402', '77.0433584', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '17:52:5', '17:52:8', '17:52:10', '0:0:3', '0:0:2', 3, '100', '0', '0:0:2', 0),
(4, 10, '28.4121704', '77.0433135', '28.4121704', '77.0433135', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '18:2:51', '18:3:0', '18:3:5', '0:0:9', '0:0:5', 3, '100', '0', '0:0:5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `rc` text NOT NULL,
  `insurance` text NOT NULL,
  `other_docs` text NOT NULL,
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `completed_rides` varchar(255) NOT NULL,
  `reject_rides` varchar(255) NOT NULL,
  `cancelled_rides` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `rc`, `insurance`, `other_docs`, `current_lat`, `current_long`, `current_location`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `status`) VALUES
(1, 'ankit', 'ankit@gmail.com', '8989898989', '', '123456', '', 0, '', 1, 1, '5656', 2, 'Thursday, Jan 5', 'uploads/driver/1483619015license_1.png', 'uploads/driver/1483619015rc_1.png', 'uploads/driver/1483619015insurance_1.jpg', '', '28.4120422', '77.0433564', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '', '', '', 1, 0, 2, 1),
(2, 'chunnu', 'chunnu@gmail.com', '9785363191', '', '123456', 'dyzYBfQvgEc:APA91bGp4vuPkPfVRtiDzynVUqO3BqHJJgjQGiDLgAHtWKA4EPi2_bii5yzhivfMjaonSXXiKV30U4ySP7UvfbOh83UZCVf-n9qCrhRyAft4VlOPpbGc0z-sUltk_hVmKaWMyRrJItr-', 2, '', 1, 1, 'gdgdgd', 2, 'Thursday, Jan 5', 'uploads/driver/1483626596license_2.jpg', 'uploads/driver/1483626596rc_2.jpg', 'uploads/driver/1483626596insurance_2.jpg', '', '28.4120454', '77.0433566', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '', '', '', 1, 0, 2, 1),
(3, 'ankit', 'ankit1@gmail.com', '8989898986', '', '123456', 'duoCStzpo5k:APA91bEmbjaEFgAgGJxoHOOp2-C0v2eGz-n8PgB7fwLOcmr4NZFelT5RWNCNsY4D9O0C5HEmj69dsVnfcaf4GN3mw7mNv07y-ASavxaxvuDYCKQ5jx_kNpyvDbYOhuYwaHPW9M9GoRfe', 2, '', 1, 1, '3456', 2, 'Saturday, Jan 7', 'uploads/driver/1483791340license_3.png', 'uploads/driver/1483791340rc_3.png', 'uploads/driver/1483791340insurance_3.png', '', '28.4120402', '77.0433584', 'Unnamed Road, Tikli, Haryana 122103, India', '4', '', '', 1, 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `arabic_title` varchar(255) NOT NULL,
  `arabic_description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `title`, `description`, `arabic_title`, `arabic_description`) VALUES
(1, 'Language', 'For English, you are kindly requested to change your phone\'s language to English via your phone\'s settings', 'Ù„Ù„ØªØ­ÙˆÙŠÙ„', 'Ù„Ù„ØªØ­ÙˆÙŠÙ„ Ø§Ù„Ù‰ Ø§Ù„Ù„ØºØ© Ø§Ù„Ø¹Ø±Ø¨ÙŠØ© Ø§Ù„Ø±Ø¬Ø§Ø¡ ØªØºÙŠÙŠØ± Ù„ØºØ© Ø§Ù„Ø¬ÙˆÙ‘Ø§Ù„ Ø¹Ù† Ø·Ø±ÙŠÙ‚ Ø§Ù„Ø§Ø¹Ø¯Ø§Ø¯Ø§Øª Ø§Ù„Ø®Ø§ØµÙ‘Ø© Ø¨Ø¬ÙˆØ§Ù„Ùƒ\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `title_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `desc`, `title_arabic`, `title_description`) VALUES
(1, 'About Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '', ''),
(2, 'Keshav Goyal', '9560506619', '', ''),
(3, 'Terms and Conditions', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 3, 6, '1', 'Cash', 'Android', '100', '07 Jan, 2017', 'Done'),
(2, 4, 6, '1', 'Cash', 'Android', '100', '07 Jan, 2017', 'Done'),
(3, 4, 6, '1', 'Cash', 'Android', '100', '07 Jan, 2017', 'Done'),
(4, 4, 6, '1', 'Cash', 'Android', '100', '07 Jan, 2017', 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_key` varchar(255) NOT NULL,
  `push_message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_key`, `push_message`) VALUES
(1, 'PN1', 'New Ride Allocated'),
(2, 'PN2', 'New Ride Allocated'),
(3, 'PN3', 'Ride cancelled'),
(4, 'PN4', 'Ride rejected by driver'),
(5, 'PN5', 'Ride accepted by driver'),
(6, 'PN6', 'Driver reach at your doorstep'),
(7, 'PN7', 'Ride Completed');

-- --------------------------------------------------------

--
-- Table structure for table `rate_card`
--

CREATE TABLE `rate_card` (
  `rate_card_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `first_2km` varchar(255) NOT NULL,
  `after_2km` varchar(255) NOT NULL,
  `time_charges` varchar(255) NOT NULL,
  `waiting_charges` varchar(255) NOT NULL,
  `extra_charges` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate_card`
--

INSERT INTO `rate_card` (`rate_card_id`, `city_id`, `car_type_id`, `first_2km`, `after_2km`, `time_charges`, `waiting_charges`, `extra_charges`) VALUES
(1, 2, 1, '100', '10', '1', '1', '5'),
(2, 3, 2, '10', '1', '23', '4', '2'),
(9, 2, 2, '50', '5', '2', '1', '10'),
(10, 2, 3, '60', '3', '1', '2', '20'),
(11, 2, 4, '30', '4', '1', '1', '30'),
(12, 2, 8, '30', '3', '1', '1', '10'),
(13, 3, 1, '100', '10', '4', '4', '50'),
(14, 3, 3, '80', '10', '7', '6', '58'),
(15, 3, 4, '50', '8', '2', '4', '30'),
(16, 3, 8, '150', '20', '4', '2', '4');

-- --------------------------------------------------------

--
-- Table structure for table `rating_customer`
--

CREATE TABLE `rating_customer` (
  `rating_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating_driver`
--

CREATE TABLE `rating_driver` (
  `rating_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_driver`
--

INSERT INTO `rating_driver` (`rating_id`, `driver_id`, `user_id`, `rating_star`, `comment`) VALUES
(1, 3, 6, '5.0', '1'),
(2, 3, 6, '5.0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `status`) VALUES
(1, 2, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Thursday, Jan 5', '06:03 PM', '', '', 0, 1, 1, 1, 1),
(2, 1, '', '28.43818460283074', '77.03572444617748', '1199G, Guru Haridas Road, Islampur Village, Sector 38, Gurugram, Haryana 122005', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Thursday, Jan 5', '08:01 PM', '', '', 0, 1, 1, 1, 1),
(3, 1, '', '28.43818460283074', '77.03572444617748', '1199G, Guru Haridas Road, Islampur Village, Sector 38, Gurugram, Haryana 122005', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Thursday, Jan 5', '08:03 PM', '', '', 0, 1, 1, 1, 1),
(4, 1, '', '28.43817929609174', '77.035839445889', '1199G, Guru Haridas Road, Islampur Village, Sector 38, Gurugram, Haryana 122005', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Thursday, Jan 5', '08:05 PM', '', '', 0, 1, 1, 1, 1),
(5, 1, '', '28.43817929609174', '77.035839445889', '1199G, Guru Haridas Road, Islampur Village, Sector 38, Gurugram, Haryana 122005', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Thursday, Jan 5', '08:09 PM', '', '', 0, 1, 1, 1, 1),
(6, 6, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Saturday, Jan 7', '05:46 PM', '', '', 3, 1, 1, 7, 1),
(7, 6, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Saturday, Jan 7', '05:50 PM', '', '', 3, 1, 1, 7, 1),
(8, 6, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.4591471', '77.0725327', 'HUDA City Centre, Sector 29, Gurugram, Haryana, India', 'Saturday, Jan 7', '05:51 PM', '', '', 3, 1, 1, 7, 1),
(9, 6, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.458856', '77.07224099999999', 'Decathlon Huda City Center, Gurugram, Haryana, India', 'Saturday, Jan 7', '05:58 PM', '', '', 0, 1, 1, 1, 1),
(10, 6, '', '28.412049519951964', '77.04335834831', '68, Plaza Street, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station, Delhi, India', 'Saturday, Jan 7', '06:02 PM', '', '', 3, 1, 1, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_track`
--

CREATE TABLE `ride_track` (
  `ride_track_id` int(11) NOT NULL,
  `ride_id` bigint(11) NOT NULL,
  `driver_id` bigint(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `driver_lat` text NOT NULL,
  `driver_long` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_track`
--

INSERT INTO `ride_track` (`ride_track_id`, `ride_id`, `driver_id`, `user_id`, `driver_lat`, `driver_long`) VALUES
(1, 6, 3, 6, '28.4120402', '77.0433584'),
(2, 6, 3, 6, '28.4120402', '77.0433584'),
(3, 7, 3, 6, '28.4120402', '77.0433584'),
(4, 8, 3, 6, '28.4120402', '77.0433584'),
(5, 10, 3, 6, '28.4120402', '77.0433584');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `social_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `register_date`, `device_id`, `flag`, `referral_code`, `coupon_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `social_token`, `token_created`, `login_logout`, `rating`, `status`) VALUES
(1, 'hxhz', 'gzgs@hx.com', '2356256566', 'fstssgsgsy', '', 'Thursday, Jan 5', 'dEnt5M9wQTg:APA91bHs8uGn4QqVzP02-OyZFppvLZa8G3N0gZ2987UhxonI3YtnHbSd-y1G5D8LupOWa6p0jHnMRhsxgp1pDBr8WUIup_ED5w45z4TWjpa77xJnLDRv7tzLQTK3edsMrndUxPAJUVyc', 2, '7cja41', '', 0, 0, 0, 0, 0, '', 0, 1, '', 1),
(2, 'ankittest', 'ankittest@gmail.com', '9999999999', '123456', '', 'Thursday, Jan 5', 'f-NWrLEx2eI:APA91bF23TB_PiuFOIOXNwTGywAPSn2kztt3W9xeoT0Ph_b6qz962Wekqpq4HOkuj3g7E-QKgh-gn5lYyHcvEu11xKrJ1xqOrjiBxp2j3gLOCDLXSDMCUAclavLqkPcOtrfDDfi2VD7B', 2, 'BWOMii', '', 0, 0, 0, 0, 0, '', 0, 1, '', 1),
(3, 'testing', '1274398417@gmail.com', '1274398417', '1274398417', '', 'Friday, Jan 6', 'ad95c59bb00adecce2698da4352f041c9ae0801246c12a31ab04d4809cd071c5', 1, 'M07B7U', '', 0, 0, 0, 0, 0, '', 0, 2, '', 1),
(4, 'Tahir', 'ertahir99@gmail.com', '9997278179', '123456', '', 'Friday, Jan 6', 'cfuZKqLKjgs:APA91bEYrTc48nHFjCpr5pg13sE75W9Lrd0AbxBWADxyxvcK3xv-Hdvw2-jki5PZCqv-fDAdxbwMLY5M3oo0Ipua4qIUz6POy3EM3YlA3D4wuOP2JmCvtPOHn6ljHbQOloh6eYDK3LlS', 2, 'PkBbN5', '', 0, 0, 0, 0, 0, '', 0, 1, '', 1),
(5, 'testing', '1290078401@gmail.com', '1290078401', '1290078401', '', 'Saturday, Jan 7', 'ad95c59bb00adecce2698da4352f041c9ae0801246c12a31ab04d4809cd071c5', 1, 'qxPdbo', '', 0, 0, 0, 0, 0, '', 0, 1, '', 1),
(6, 'ghhhgb', 'ggh@gmail.com', '5632124532', '124456', '', 'Saturday, Jan 7', 'ePRc17I1M-c:APA91bEuwuUyc5Jmw6RetcuL_TFYPQJMGc5JRymavkIKNKlJzgMOgVhZon8YYxm2jsZVwQGJK2a1c5He03izdwfTVgqD6kLoaDqLyc7NDkYRH3C4g3WqfXtZ891dpU8LiOmCcENO8LHR', 2, 'OpwiRL', '', 0, 0, 0, 0, 0, '', 0, 1, '5', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_setting`
--
ALTER TABLE `account_setting`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bill_calculate`
--
ALTER TABLE `bill_calculate`
  ADD PRIMARY KEY (`latlong_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rate_card`
--
ALTER TABLE `rate_card`
  ADD PRIMARY KEY (`rate_card_id`);

--
-- Indexes for table `rating_customer`
--
ALTER TABLE `rating_customer`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `rating_driver`
--
ALTER TABLE `rating_driver`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `ride_track`
--
ALTER TABLE `ride_track`
  ADD PRIMARY KEY (`ride_track_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_setting`
--
ALTER TABLE `account_setting`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bill_calculate`
--
ALTER TABLE `bill_calculate`
  MODIFY `latlong_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rate_card`
--
ALTER TABLE `rate_card`
  MODIFY `rate_card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `rating_customer`
--
ALTER TABLE `rating_customer`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rating_driver`
--
ALTER TABLE `rating_driver`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ride_track`
--
ALTER TABLE `ride_track`
  MODIFY `ride_track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
